<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCombinationWarehouseStoreOutChallanProduct extends Model
{
    use SoftDeletes;
    protected $table = 'product_combination_warehouse_store_out_challan_product';
    protected $fillable = ['id','product_combination_warehouse_store_in_challan_product_id','warehouse_store_out_challan_product_id', 'product_combination_id','requested_quantity','received_quantity','stock_quantity'];
    protected $dates = ['deleted_at'];
}
