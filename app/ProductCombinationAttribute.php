<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCombinationAttribute extends Model
{
    use SoftDeletes;
    protected $table = 'product_combination_attributes';
    protected $fillable = ['attribute_value_id','product_combination_id'];
    protected $dates = ['deleted_at'];

    public function combination(){
        return $this->belongsTo('App\ProductCombination');
    }

    public function attributeValue(){
        return $this->belongsTo('App\AttributeValue');
    }
}
