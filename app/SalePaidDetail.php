<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalePaidDetail extends Model
{
    use SoftDeletes;
    protected $table = 'sale_paid_details';
    protected $fillable = ['sale_paid_id','meta_data', 'value'];
    protected $dates = ['deleted_at'];
}
