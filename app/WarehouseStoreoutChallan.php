<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseStoreoutChallan extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse_storeout_challans';
    protected $fillable = ['warehouse_id','user_id','outlet_id','warehouse_store_out_type_id','challan_no','request_date','status'];
    protected $dates = ['deleted_at'];
}
