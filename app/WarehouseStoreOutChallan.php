<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseStoreOutChallan extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse_store_out_challans';
    protected $fillable = ['company_id','warehouse_id','out_let_id','warehouse_store_out_type_id','challan_no','request_date','status','created_by'];
    protected $dates = ['deleted_at'];

    public function challanProducts(){
        return $this->hasMany('App\WarehouseStoreOutChallanProduct');
    }
}
