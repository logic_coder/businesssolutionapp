<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponRestrictionProduct extends Model
{
    use SoftDeletes;
    protected $table = 'coupon_restriction_product';
    protected $fillable = ['coupon_restriction_id', 'product_id'];

    protected $dates = ['deleted_at'];

}
