<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;
    protected $table = "roles";
    protected $fillable = ['compnay_id','name', 'slug'];
    protected $dates = ['deleted_at'];

    public function permissions(){
        return $this->belongsToMany('App\Permission');
    }
    public function users(){
        return $this->belongsToMany('App\User');
    }
}
