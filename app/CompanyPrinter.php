<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyPrinter extends Model
{
    use SoftDeletes;
    protected $table = "company_printers";
    protected $fillable = ['company_id','name','model','type','status'];
    protected $dates = ['deleted_at'];
    
    public function company(){
        return $this->belongsTo('App\Company');
    }
}
