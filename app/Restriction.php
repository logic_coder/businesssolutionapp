<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Restriction extends Model
{
    use SoftDeletes;
    protected $table = 'restrictions';
    protected $fillable = ['coupon_id', 'number_of_products_required_in_carts'];

    protected $dates = ['deleted_at'];
    public function coupon(){
        return $this->belongsTo('App\Coupon');
    }
    public function categories()
    {
        return $this->hasMany('App\CouponRestrictionCategories');
    }
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
