<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;
    protected $table = 'coupons';
    protected $fillable = [ 'company_id', 'name','company_currency_id','description','code','high_light','status','discount_percent','discount_amount',
                            'vat_include_on_discount_amount','discount_amount_currency_id','no_discount','customer_id','valid_from','valid_to',
                            'minimum_purchase_amount','minimum_purchase_amount_currency_id','vat_include_on_minimum_purchase_amount'
                            ,'total_coupons','total_coupons_for_each_user','number_of_products_required_in_carts','exclude_discounted_products',
                        'apply_discount_option','product_selection_button','product_selection_checked'];

    protected $dates = ['deleted_at'];

    public function companyCurrency()
    {
        return $this->hasOne('App\CompanyCurrency');
    }
    public function company()
    {
        return $this->belongsTo('App\Company');
    }
    public function couponRestrictions()
    {
        return $this->hasMany('App\CouponRestriction');
    }
}