<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerOrderDetailPaid extends Model
{
    use SoftDeletes;
    protected $table = 'customer_order_detail_paids';
    protected $fillable = ['customer_order_detail_id','payment_method_id','paid_amount', 'paid_date'];
    protected $dates = ['deleted_at'];

    public function customerOrderDetailPaidDetails()
    {
        return $this->hasMany('App\CustomerOrderDetailPaidDetail');
    }
    public function paymentMethod()
    {
        return $this->belongsTo('App\PaymentMethod');
    }
}
