<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyCurrency extends Model
{
    use SoftDeletes;
    protected $table = "company_currencies";
    protected $fillable = ['company_id','currency_id','default'];
    protected $dates = ['deleted_at'];

    public function coupons(){
        return $this->belongsToMany('App\Coupon');
    }
    public function currency(){
        return $this->belongsTo('App\Currency');
    }
}
