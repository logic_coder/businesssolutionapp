<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use SoftDeletes;
    protected $table = "currencies";
    protected $fillable = ['name','code'];
    protected $dates = ['deleted_at'];

    public function companyCurrencies(){
        return $this->hasMany('App\CompanyCurrency');
    }
}
