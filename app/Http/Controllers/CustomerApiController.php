<?php

namespace App\Http\Controllers;
use App\Company;
use App\Customer;
use App\Gender;
use App\OutLet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use App\Http\Requests\CustomerRequest;

class CustomerApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       dd('Perfect Body...');
    }
    /**
     * Get All customer Info
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCustomerLists()
    {
        return Customer::where('company_id', Auth::user()->company_id)->orderBy('id', 'DESC')->get();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveCustomer(CustomerRequest $request )
    {
        $message = "";

        if( $_COOKIE['out_let_id'] ) {
            $customer_data['out_let_id'] = $_COOKIE['out_let_id'];
            $customer_data['customer_no'] = $request->customer_no;
            $customer_data['gender_id'] = $request->gender_id;
            $customer_data['country_id'] = $request->country_id;
            $customer_data['first_name'] = $request->first_name;
            $customer_data['last_name'] = $request->last_name;
            $customer_data['cell_phone'] = isset($request->cell_phone) ? $request->cell_phone:'';
            $customer_data['work_phone'] = isset($request->work_phone) ? $request->work_phone:'';
            $customer_data['email'] = isset($request->email) ? $request->email:'';
            $customer_data['date_of_birth'] = isset($request->date_of_birth) ? $request->date_of_birth:'1971-01-01';
            $customer_data['address'] = isset($request->address) ? $request->address:'';
            $customer_data['address2'] = isset($request->address2) ? $request->address2:'';
            $customer_data['city'] = $request->city;
            $customer_data['zip'] = isset($request->zip) ? $request->zip:'';
            $customer_created = Company::find( Auth::user()->company_id )->customers()->create($customer_data);
            if( $customer_created ){
                return response([
                    'message' =>"Customer Created successfully"
                ],201);
            }else{
                return response([
                    'message' => "Error: Customer Not Created, Please Try again"
                ],500);
            }            
        }
    }
    /**
     * Update a existing resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */  
    public function update_customer_details(CustomerRequest $request){
        $message = "";
        if( $_COOKIE['out_let_id']  ) {
            $customer_data['customer_no'] = $request->customer_no;
            $customer_data['gender_id'] = $request->gender_id;
            $customer_data['country_id'] = $request->country_id;
            $customer_data['first_name'] = $request->first_name;
            $customer_data['last_name'] = $request->last_name;
            $customer_data['cell_phone'] = $request->cell_phone;
            $customer_data['work_phone'] = $request->work_phone;
            $customer_data['email'] = $request->email;
            $customer_data['date_of_birth'] = $request->date_of_birth;
            $customer_data['address'] = $request->address;
            $customer_data['address2'] = $request->address2;
            $customer_data['city'] = $request->city;
            $customer_data['zip'] = $request->zip;

            $customer_updated = Customer::find( $request->id )->update( $customer_data );

            if( $customer_updated ){
                return response([
                    'message' => "Customer updated successfully"
                ],200);
            }else{
                return response([
                    'message' => "Error: Customer Not Updated, Please Try again"
                ],500);
            } 
        }
    }
    /**
     * Get customer Info by customer_id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function getCustomerData( Request $request )
    {
        $customer_data = Customer::find( $request->customer_id);
        return $customer_data;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd('I am in Create...');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeCustomer( Request $request )
    {
        $return_array['success'] = '';
                 
        if(!Auth::user()->can('delete-customer')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }        
        elseif(!empty( $request->item_ids)){
            Customer::destroy( $request->item_ids );
            return response([
                'message' => 'Customer deleted successfully!'
            ],200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
