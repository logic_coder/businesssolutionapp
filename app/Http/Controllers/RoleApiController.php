<?php

namespace App\Http\Controllers;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleApiController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeRole( Request $request )
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('add-edit-access-permission')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids)){
            Role::destroy( $request->item_ids );
            return response([
                'message' => 'Role deleted successfully!!!'
            ],200);
        }
    }
}
