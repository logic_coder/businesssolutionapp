<?php
namespace App\Http\Controllers;
use App\Company;
use App\Product;
use App\CategoryProduct;
use App\OutLet;
use App\Sale;
use App\SalePaid;
use App\UserDetail;
use App\SalePaidDetail;
use App\CustomerOrderDetail; 
use App\OutLetProduct;
use App\OutLetProductProductCombinationSale;
use App\Http\Requests\SaleRequest;
use App\Http\Requests\OutLetProductSaleRequest;
use App\Http\Requests\SalePaidRequest;
use App\Http\Requests\SalePaidDetailRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psy\Util\Json;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use Response;
use Illuminate\Routing\Route;
use DB;


class SaleApiController extends SalePaidApiController
{
    
    protected function saveSaleDetail(OutLetProductSaleRequest $outLetProductSale, SaleRequest $request, SalePaidRequest $salePaidRequest, SalePaidDetailRequest $salePaidDetailRequest)
    {
        $return_array['success'] = '';        
        $message = "";
        
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-sales-page')){
                return response([
                    'message' => 'Error: Operation not permitted!'
                ],401);
            }
        }
        if( $request ){
            if( $request->invoice_prefix_id != null || $request->invoice_prefix_id != '' ){
                $customer_order_details_data['invoice_prefix_id'] = $request->invoice_prefix_id;
            }else{
                $customer_order_details_data['invoice_prefix_id'] = NULL;
            }           
            $customer_order_details_data['company_id'] = Auth::user()->company_id;
            $customer_order_details_data['out_let_id'] = $_COOKIE['out_let_id'];
            $customer_order_details_data['sales_by'] = Auth::user()->id;
            $customer_order_details_data['customer_id'] = isset($request->customer_id) ? $request->customer_id : null;
            $customer_order_details_data['sale_date'] = date("Y-m-d");
            $customer_order_details_data['company_currency_id'] = $request->company_currency_id;
            $customer_order_details_data['sub_total'] = $request->sub_total;
            $customer_order_details_data['discount_amount'] = $request->discount_amount;
            $customer_order_details_data['coupon_id'] = $request->coupon_id;
            $customer_order_details_data['coupon_discount_amount'] = $request->coupon_discount_amount;
            $customer_order_details_data['total_amount_exclusive_vat'] = $request->total_amount_exclusive_vat;
            $customer_order_details_data['vat_amount'] = $request->vat_amount;            
            $customer_order_details_data['net_amount'] = $request->net_amount;
            $customer_order_details_data['sales_note'] = isset($request->sales_note) ? $request->sales_note : null;
            $customer_order_details_data['status'] = $request->status ? $request->status : "sold";            
            $sale_created = OutLet::find( $customer_order_details_data['out_let_id'] )->sales()->create($customer_order_details_data);
            $customer_order_details_update_data['order_no'] = Auth::user()->company_id."-".$customer_order_details_data['out_let_id']."-".Auth::user()->id."-".date("Y").date("m").date("d").$sale_created->id;
            $customer_order_details_update_data['invoice_no'] = Auth::user()->company_id.$customer_order_details_data['out_let_id'].Auth::user()->id.date("Y").date("m").date("d").$sale_created->id;
            $sale_created->update($customer_order_details_update_data);

            if( $sale_created ){ 
                if( $request->customer_order_detail_items ){
                    foreach ( $request->customer_order_detail_items as $customer_order_detail_item ){
                        $customer_order_detail_item_data['out_let_product_product_combination_id'] = $customer_order_detail_item['out_let_product_product_combination_id'];
                        $customer_order_detail_item_data['product_id'] = $customer_order_detail_item['product']['id'];
                        $customer_order_detail_item_data['name'] = $customer_order_detail_item['product']['name'];
                        $customer_order_detail_item_data['cost_price'] = $customer_order_detail_item['cost_price'];
                        $customer_order_detail_item_data['unit_price'] = $customer_order_detail_item['unit_price'];
                        $customer_order_detail_item_data['quantity'] = $customer_order_detail_item['quantity'];
                        $out_let_product_sale_created = $sale_created->outLetProductProductCombinationSales()->create($customer_order_detail_item_data);
                    }
                }
                if($request->sale_paids){
                    foreach ($request->sale_paids as $sale_paid) { 
                        $customer_order_detail_paid_data['paid_amount'] = 0;
                        /* Insert Sale Paid and Sale Paid Detail */
                        if($request->cash_payment_amount || $request->digital_payment_amount){
                            if($sale_paid['payment_method']['name']=="Cash"){
                                $customer_order_detail_paid_data['paid_amount'] = $request->cash_payment_amount ? $request->cash_payment_amount : 0;

                            }else if($sale_paid['payment_method']['name'] =="Master Card" || $sale_paid['payment_method']['name'] =="Visa Card" || $sale_paid['payment_method']['name'] =="Bkash"){
                                $customer_order_detail_paid_data['paid_amount'] = $request->digital_payment_amount ? $request->digital_payment_amount : 0;     
                            }
                            if( $customer_order_detail_paid_data['paid_amount'] > 0 ){
                                $customer_order_detail_paid_data['payment_method_id'] = $sale_paid['payment_method']['id'];                
                                $customer_order_detail_paid_data['paid_date'] = date('Y-m-d');
                                $sale_paid_created = $sale_created->salePaids()->create($customer_order_detail_paid_data);     
                                if($sale_paid['sale_paid_details']){    
                                    foreach($sale_paid['sale_paid_details'] as $payment_detail){
                                        $payment_detail_data['meta_data'] = $payment_detail['meta_data'];
                                        $payment_detail_data['value'] = $payment_detail['value'];
                                        $sale_paid_created->salePaidDetails()->create($payment_detail_data);
                                    }
                                } 
                            }                           
                        }
                    }                
                }                 
                $return_array['sale_id'] = $sale_created->id;
                $return_array['order_no'] = $customer_order_details_update_data['order_no'];
                $return_array['invoice_no'] = $customer_order_details_update_data['invoice_no'];

                if($request->status == "hold-on"){
                    $message = "Order Hold Successfully";
                }else{
                    $message = "Sale Created Successfully";
                }
                return response([
                    'sale_id' => $sale_created->id,
                    'message' => $message
                ],201);

            }else{
                return response([
                    'sale_id' => "",
                    'message' => "Error: Sale Not Created, Please Try again"
                ],500);
            }            
        }
    }
    protected function updateSaleDetail(OutLetProductSaleRequest $outLetProductSale, SaleRequest $request, SalePaidRequest $salePaidRequest, SalePaidDetailRequest $salePaidDetailRequest)
    {
        $return_array['success'] = '';
        $message = "";
        
        if(!Auth::user()->can('edit-sales')){            
            return response([
                    'sale_id' => $request->id,
                    'message' => 'Error: Operation not permitted!'
            ],401);
        }
        if( $request ){
            if( $request->invoice_prefix_id != null || $request->invoice_prefix_id != '' ){
                $customer_order_details_data['invoice_prefix_id'] = $request->invoice_prefix_id;
            }else{
                $customer_order_details_data['invoice_prefix_id'] = NULL;
            }           
            $customer_order_details_data['out_let_id'] = $_COOKIE['out_let_id'];
            $customer_order_details_data['sales_by'] = Auth::user()->id;
            $customer_order_details_data['customer_id'] = isset($request->customer_id) ? $request->customer_id : null;
            $customer_order_details_data['sale_date'] = Carbon::now();            
            $customer_order_details_data['company_currency_id'] = $request->company_currency_id;
            $customer_order_details_data['sub_total'] = $request->sub_total;
            $customer_order_details_data['discount_amount'] = $request->discount_amount;
            $customer_order_details_data['coupon_id'] = $request->coupon_id;
            $customer_order_details_data['coupon_discount_amount'] = $request->coupon_discount_amount;
            $customer_order_details_data['total_amount_exclusive_vat'] = $request->total_amount_exclusive_vat;
            $customer_order_details_data['vat_amount'] = $request->vat_amount;
            $customer_order_details_data['net_amount'] = $request->net_amount;
            $customer_order_details_data['sales_note'] = isset($request->sales_note) ? $request->sales_note : null;
            $customer_order_details_data['status'] = $request->status;
            $saleObj = Sale::find($request->id);
            $sale_updated = $saleObj->update( $customer_order_details_data );           
            if($request->remove_customer_order_detail_items){
                foreach( $request->remove_customer_order_detail_items as $remove_customer_order_detail_item ){
                    $customerOrderDetailItem = OutLetProductProductCombinationSale::find($remove_customer_order_detail_item['id']);
                    $customerOrderDetailItem->forceDelete();
                    //$customer_order_detail_item = OutLetProductSale::where('id',$remove_customer_order_detail_item['id'])->get();
                    //$this->removeCustomerOrderDetailItems( $customer_order_detail_item );
                }
            }            
            if( $sale_updated ){ 
                if( $request->customer_order_detail_items ){
                    foreach ( $request->customer_order_detail_items as $customer_order_detail_item ){
                        $customer_order_detail_item_data['sale_id'] = $request->id;
                        $customer_order_detail_item_data['out_let_product_product_combination_id'] = $customer_order_detail_item['out_let_product_product_combination_id'];
                        $customer_order_detail_item_data['product_id'] = $customer_order_detail_item['product']['id'];
                        $customer_order_detail_item_data['name'] = $customer_order_detail_item['product']['name'];                        
                        $customer_order_detail_item_data['cost_price'] = $customer_order_detail_item['cost_price'];
                        $customer_order_detail_item_data['unit_price'] = $customer_order_detail_item['unit_price'];
                        $customer_order_detail_item_data['quantity'] = $customer_order_detail_item['quantity'];
                        $out_let_product_sale_updated = OutLetProductProductCombinationSale::updateOrCreate(['id' => $customer_order_detail_item['id'] ], $customer_order_detail_item_data);
                    }
                }
                /* Remove Digital Payment entry when no payment method is get selected */
                if($request->remove_sale_paid_history){ 
                    foreach( $request->remove_sale_paid_history as $remove_sale_paid ){
                        if($remove_sale_paid['payment_method']['name'] == "Master Card" || $remove_sale_paid['payment_method']['name'] == "Visa Card" || $remove_sale_paid['payment_method']['name'] == "Bkash"){
                            $salePaid = SalePaid::find($remove_sale_paid['id']);
                            $salePaid->forceDelete();
                            break;
                        }
                    }          
                }
                /* Insert/Update/Delete Sale Paid and Sale Paid Detail Based On Condition*/
                if($request->sale_paids){
                    foreach ($request->sale_paids as $sale_paid) { 
                        $customer_order_detail_paid_data['paid_amount'] = 0;
                        /* Remove Cash Payment entry when paid_amount <= 0 */
                        if($request->cash_payment_amount <= 0 && $sale_paid['id']&& $sale_paid['payment_method']['name']=="Cash") {
                            $salePaid = SalePaid::find($sale_paid['id']);
                            $salePaid->forceDelete();                            
                        }   
                        /* Insert or Update Sale Paid and Sale Paid Detail */
                        if($request->cash_payment_amount || $request->digital_payment_amount){
                            if($sale_paid['payment_method']['name']=="Cash"){
                                $customer_order_detail_paid_data['paid_amount'] = $request->cash_payment_amount ? $request->cash_payment_amount : 0;

                            }else if($sale_paid['payment_method']['name'] =="Master Card" || $sale_paid['payment_method']['name'] =="Visa Card" || $sale_paid['payment_method']['name'] =="Bkash"){
                                $customer_order_detail_paid_data['paid_amount'] = $request->digital_payment_amount ? $request->digital_payment_amount : 0;     
                            }
                            if( $customer_order_detail_paid_data['paid_amount'] > 0 ){
                                $customer_order_detail_paid_data['sale_id'] = $request->id;            
                                $customer_order_detail_paid_data['payment_method_id'] = $sale_paid['payment_method']['id'];                
                                $customer_order_detail_paid_data['paid_date'] = date('Y-m-d');
                                $sale_paid_updated = SalePaid::updateOrCreate(['id' => $sale_paid['id'] ], $customer_order_detail_paid_data);
                                if($sale_paid['sale_paid_details']){
                                    $payment_detail_data['sale_paid_id'] = $sale_paid_updated->id;
                                    foreach($sale_paid['sale_paid_details'] as $payment_detail){
                                        $payment_detail_data['id'] = $payment_detail['id'];
                                        $payment_detail_data['meta_data'] = $payment_detail['meta_data'];
                                        $payment_detail_data['value'] = $payment_detail['value'];
                                        $sale_paid_detail_updated = SalePaidDetail::updateOrCreate(['id' => $payment_detail_data['id'] ], $payment_detail_data);
                                    }
                                } 
                            }                           
                        }
                    }                
                } 
                $return_array['sale_id'] = $request->id;
                if($request->status == 'cancel'){
                    $message = "Order Cancelled Successfully"; 
                }else{
                    $message = "Sale Updated Successfully";
                }
                return response([
                    'sale_id' => $request->id,
                    'message' => $message
                ],200);
            }else{
                return response([
                    'sale_id' => $request->id,
                    'message' => "Error: Sale Not Updated, Please Try again"
                ],500);
            }
        }
    }
    /**
     * Remove customer order detail item based on item id
     *
     * @param  \Illuminate\Http\Request  $request which contains customer_order_detail_items
     * @return Success/Failure Message
     */
    protected function removeCustomerOrderDetailItems( $customer_order_detail_items )
    {
        if( $customer_order_detail_items ){
            $customer_order_detail_item_ids = [];
            foreach( $customer_order_detail_items as $customer_order_detail_item ){
                $customerOrderDetailItem = OutLetProductSale::find($customer_order_detail_item->id);
                $customerOrderDetailItem->forceDelete();
            }
        }
    }
    /**
     * Remove customer order detail digital payment data based on digital payment content
     *
     * @param  \Illuminate\Http\Request  $request which contains remove_sale_paid_history
     * @return Success/Failure Message
     */
    protected function removeSalePaidHistory($remove_sale_paid_history, $cash_payment_method_id){
        $return_array['success'] = $remove_sale_paid_history;
        return $return_array;
        if( $remove_sale_paid_history ){
            foreach( $remove_sale_paid_history as $remove_sale_paid ){
                if($remove_sale_paid->payment_method['name']=="Cash" && is_null($cash_payment_method_id) ){
                    $salePaid = SalePaid::find($remove_sale_paid->id);
                    $salePaid->forceDelete();
                }
                if($remove_sale_paid->payment_method['name'] == "Master Card" || $remove_sale_paid->payment_method['name'] == "Visa Card" || $remove_sale_paid->payment_method['name'] == "Bkash"){
                    //foreach ($remove_sale_paid->sale_paid_details) {
                    //}
                    $salePaid = SalePaid::find($remove_sale_paid->id);
                    $salePaid->forceDelete();
                }
            }
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeSale( Request $request )
    {        
        if(!isset($_COOKIE['user_id'])){  
            if(!Auth::user()->can('delete-sales')){
                return response([
                    'message' => 'Error: Operation not permitted!'
                ],401);
            }
        }
        elseif(!empty( $request->item_ids )){
            $query_result = DB::table('sale_paid_details')
                ->join('sale_paids','sale_paid_details.sale_paid_id' ,'=' ,'sale_paids.id')
                ->whereIn('sale_paids.sale_id', $request->item_ids)
                ->update(['sale_paid_details.deleted_at' => DB::raw('NOW()')]);
            DB::table('sale_paids')->whereIn('sale_id', $request->item_ids )->update(['deleted_at' => DB::raw('NOW()')]);
            DB::table('out_let_product_product_combination_sales')->whereIn('sale_id', $request->item_ids )->update(['deleted_at' => DB::raw('NOW()')]);
            $sale_deleted = DB::table('sales')->whereIn('id', $request->item_ids )->update(['deleted_at' => DB::raw('NOW()')]);
            if( $sale_deleted ){
                return response([
                    'message' => 'Sale Deleted Successfully'
                ],200);
            }else{                
                return response([
                    'message' => 'Error: Sale Not Deleted, Please Try again'
                ],500);
            }
        }
    } 
    /**
    * Get customer sale detail data based on customer sale id
    *
    * @param  \Illuminate\Http\Request  $request which contains sale_id
    * @return sale data
    */
    protected function getSaleDetail(Request $request)
    {        
        $product_sales_data =  Sale::where('id', $request->sale_id)->with(
        ['outLetProductProductCombinationSales' => function ($query) { 
            $query->with(['outLetProductProductCombination' => function($query){
                
            }]);
        }])->first();

        if($product_sales_data){
            $salePaidHistory = SalePaid::where('sale_id', $request->sale_id)->with('SalePaidDetails','paymentMethod')->get();
            if(sizeof($salePaidHistory) <= 0){
                /* For Cash Payment */
                $sale_paid_history_obj = (object) array("id" => null);
                $payment_method_obj = (object) array("id" => $request->cash_payment_method_id, "name" => 'Cash');
                $sale_paid_details_obj = (object) array("id" => null, "meta_data" => 'Payment By',"value" => 'Self');
                $sale_paid_history_obj->payment_method = $payment_method_obj; 
                $sale_paid_history_obj->sale_paid_details = array($sale_paid_details_obj); 
                $salePaidHistory[0] = $sale_paid_history_obj;

                /* For Digital Payment */
                $sale_paid_history_obj = (object) array("id" => null,
                );
                $payment_method_obj = (object) array("name" => 'Select Payment Method'
                );
                $sale_paid_details_obj = (object) array("id" => null, "meta_data" => '',"value" => ''
                );
                $sale_paid_history_obj->payment_method = $payment_method_obj; 
                $sale_paid_history_obj->sale_paid_details[0] = $sale_paid_details_obj; 
                $sale_paid_history_obj->sale_paid_details[1] = $sale_paid_details_obj;
                $salePaidHistory[1] = $sale_paid_history_obj; 

            }else if(sizeof($salePaidHistory) == 1 ){         
                foreach($salePaidHistory as $key => $sale_paid) {            
                    if($sale_paid->paymentMethod->name != "Cash"){
                        /* For Cash Payment */
                        $sale_paid_history_obj = (object) array("id" => null,
                        );
                        $payment_method_obj = (object) array("id" => $request->cash_payment_method_id, "name" => 'Cash'
                        );
                        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => 'Payment By',"value" => 'Self'
                        );
                        $sale_paid_history_obj->payment_method = $payment_method_obj; 
                        $sale_paid_history_obj->sale_paid_details = array($sale_paid_details_obj); 
                        $salePaidHistory[$key+1] = $sale_paid_history_obj;
                        /* For Digital Payment*/
                        $product_sales_data->digital_payment_amount = $sale_paid->paid_amount;
                        $product_sales_data->cash_payment_amount = 0;
                        $product_sales_data->paymentMethod = $sale_paid->paymentMethod->name;
                    }else{
                        /* For Digital Payment */
                        $sale_paid_history_obj = (object) array("id" => null, 
                        );
                        $payment_method_obj = (object) array("name" => 'Select Payment Method'
                        );
                        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => '',"value" => ''
                        );
                        $sale_paid_history_obj->payment_method = $payment_method_obj; 
                        $sale_paid_history_obj->sale_paid_details[0] = $sale_paid_details_obj; 
                        $sale_paid_history_obj->sale_paid_details[1] = $sale_paid_details_obj;
                        $salePaidHistory[$key+1] = $sale_paid_history_obj; 
                        /* For Cash Payment*/
                        $product_sales_data->cash_payment_amount = $sale_paid->paid_amount;
                        $product_sales_data->digital_payment_amount = 0;
                    }
                }            
            }
            else if(sizeof($salePaidHistory) == 2 ){         
                foreach($salePaidHistory as $key => $sale_paid) {  
                    if($sale_paid->paymentMethod->name == "Cash"){
                        $product_sales_data->cash_payment_amount = $sale_paid->paid_amount;
                    }else if($sale_paid->paymentMethod->name == "Master Card" || $sale_paid->paymentMethod->name == "Visa Card" || $sale_paid->paymentMethod->name == "Bkash"){ 
                        $product_sales_data->digital_payment_amount = $sale_paid->paid_amount;
                        $product_sales_data->paymentMethod = $sale_paid->paymentMethod->name;
                    }
                }
            }
            $product_sales_data->sale_paids = $salePaidHistory;
            foreach ($product_sales_data->outLetProductProductCombinationSales as $key => $out_let_product_product_combination_sales){
                $out_let_product_id = $out_let_product_product_combination_sales->outLetProductProductCombination->out_let_product_id;
                ////////////////////
                $sales_data = OutLet::where('id', $_COOKIE['out_let_id'])->with(
                ['outLetProducts' => function($query) use($out_let_product_id){
                    $query->where('out_let_products.id','=', $out_let_product_id);
                    $query->with(['product'=>function($query){
                        $query->with(['images' => function($query){
                            $query->where('product_images.is_cover_image', '=', '1');
                        }]);
                        $query->with(['prices' => function($query){
                            $query->where('product_prices.active', '=', '1');
                        }]);
                        $query->with(['categories' => function($query){
                        }]);
                    }]);
                    $query->with(['outLetProductCombinations'=>function($query){                
                        $query->with(['combinationAttributeValues' => function($query){
                            $query->with([ 'attributeValue' => function($query){
                                $query->with('attribute');
                            }]);
                        }]);
                    }]);
                }])->first();
                $product_sales_data->outLetProductProductCombinationSales[$key]->categories = $sales_data->outLetProducts[0]->product->categories;
                $product_sales_data->outLetProductProductCombinationSales[$key]->product = $sales_data->outLetProducts[0]->product;
                $product_sales_data->outLetProductProductCombinationSales[$key]->product_id =$sales_data->outLetProducts[0]->product->id;                  
                $product_sales_data->outLetProductProductCombinationSales[$key]->vat_percent = $sales_data->outLetProducts[0]->product->prices[0]->vat->percent;
                if( sizeof( $sales_data->outLetProducts[0]->outLetProductCombinations ) <= 0 ){
                    $combinations_obj = array(); 
                    $product_combination = OutLetProduct::with('out_let_product_combination')->where('id', '=', $out_let_product_id )->orderBy('id', 'ASC')->first();
 
                    $sold_quantity = DB::table('out_let_product_product_combination_sales')
                    ->join('sales', 'sales.id', '=', 'out_let_product_product_combination_sales.sale_id')
                    ->whereIn('sales.status', ['sold','hold-on'])
                    ->where('out_let_product_product_combination_id', '=', $product_combination->out_let_product_combination[0]->id)
                    ->whereNull('out_let_product_product_combination_sales.deleted_at')
                    ->sum('out_let_product_product_combination_sales.quantity');
                    $combinations_obj[] = (object) array("id" => $product_combination->out_let_product_combination[0]->id,
                        "out_let_id" => $product_combination->out_let_id,
                        "out_let_product_id" => $product_combination->out_let_product_combination[0]->out_let_product_id,                    
                        "product_combination_id" => null,
                        "product_id" => $product_combination->product_id,
                        "stock_quantity" => ($product_combination->out_let_product_combination[0]->stock_quantity-$sold_quantity),
                    );
                    $product_sales_data->outLetProductProductCombinationSales[$key]->combination = "No";
                    $product_sales_data->outLetProductProductCombinationSales[$key]->out_let_product_product_combination_id = $product_combination->out_let_product_combination[0]->id;
                    $product_sales_data->outLetProductProductCombinationSales[$key]->out_let_product_combinations = $combinations_obj;
                }else{  
                    $combinations_obj = array();              
                    $product_sales_data->outLetProductProductCombinationSales[$key]->combination = "Yes";
                    $product_sales_data->outLetProductProductCombinationSales[$key]->out_let_product_id = null;
                    foreach ($sales_data->outLetProducts[0]->outLetProductCombinations as $index_product_combination => $out_let_product_combination) {
                        $sold_quantity = DB::table('out_let_product_product_combination_sales')
                        ->join('sales', 'sales.id', '=', 'out_let_product_product_combination_sales.sale_id')
                        ->whereIn('sales.status', ['sold','hold-on'])
                        ->where('out_let_product_product_combination_id', '=', $out_let_product_combination->pivot->id)
                        ->whereNull('out_let_product_product_combination_sales.deleted_at')
                        ->sum('out_let_product_product_combination_sales.quantity'); 
                        $sales_data->outLetProducts[0]->outLetProductCombinations[$index_product_combination]->stock_quantity = $out_let_product_combination->pivot->stock_quantity - $sold_quantity;
                        $combinations_obj[$index_product_combination] = $out_let_product_combination;
                    }
                    $product_sales_data->outLetProductProductCombinationSales[$key]->out_let_product_combinations = $combinations_obj;
                } 
            }
            $product_sales_data->customer_order_detail_items = $product_sales_data->outLetProductProductCombinationSales;
        }
        return Response::json($product_sales_data);
        //dd($product_sales_data); 
        
        /*$product_sales_data =  Sale::where('id', $request->sale_id)->with(
            ['outLetProductSales' => function ($query) { 
                $query->with(['outLetProduct'=>function($query){
                    $query->with(['product'=>function($query){
                        $query->with(['images' => function($query){
                            $query->where('product_images.is_cover_image', '=', '1');
                        }]);
                        $query->with(['categories'=>function($query){
                        
                        }]); 
                        $query->with(['prices'=>function($query){
                            $query->where('product_prices.active', '=', '1');
                            $query->with(['vat'=>function($query){
                            
                            }]); 
                        }]);                       
                        $query->with(['combinations' => function($query){
                            $query->with(['combinationAttributeValues' => function($query){
                                $query->with([ 'attributeValue' => function($query){
                                    $query->with('attribute');
                                }]);
                            }]);
                            $query->select('product_combinations.*', 'out_let_product.*')
                            ->join('out_let_product', function($join) {
                            $join->on('out_let_product.product_combination_id', '=', 'product_combinations.id');                   
                            });
                        }]);                         
                    }]);                    
                }]);                
            }])->first(); */    
               
        if( $product_sales_data ){            
            $salePaidHistory = SalePaid::where('sale_id', $request->sale_id)->with('SalePaidDetails','paymentMethod')->get();
            if(sizeof($salePaidHistory) <= 0){
                /* For Cash Payment */
                $sale_paid_history_obj = (object) array("id" => null, 
                );
                $payment_method_obj = (object) array("id" => $request->cash_payment_method_id, "name" => 'Cash'
                );
                $sale_paid_details_obj = (object) array("id" => null, "meta_data" => 'Payment By',"value" => 'Self'
                );
                $sale_paid_history_obj->payment_method = $payment_method_obj; 
                $sale_paid_history_obj->sale_paid_details = array($sale_paid_details_obj); 
                $salePaidHistory[0] = $sale_paid_history_obj;

                /* For Digital Payment */
                $sale_paid_history_obj = (object) array("id" => null,
                );
                $payment_method_obj = (object) array("name" => 'Select Payment Method'
                );
                $sale_paid_details_obj = (object) array("id" => null, "meta_data" => '',"value" => ''
                );
                $sale_paid_history_obj->payment_method = $payment_method_obj; 
                $sale_paid_history_obj->sale_paid_details[0] = $sale_paid_details_obj; 
                $sale_paid_history_obj->sale_paid_details[1] = $sale_paid_details_obj;
                $salePaidHistory[1] = $sale_paid_history_obj; 

            }else if(sizeof($salePaidHistory) == 1 ){         
                foreach($salePaidHistory as $key => $sale_paid) {            
                    if($sale_paid->paymentMethod->name != "Cash"){
                        /* For Cash Payment */
                        $sale_paid_history_obj = (object) array("id" => null,
                        );
                        $payment_method_obj = (object) array("id" => $request->cash_payment_method_id, "name" => 'Cash'
                        );
                        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => 'Payment By',"value" => 'Self'
                        );
                        $sale_paid_history_obj->payment_method = $payment_method_obj; 
                        $sale_paid_history_obj->sale_paid_details = array($sale_paid_details_obj); 
                        $salePaidHistory[$key+1] = $sale_paid_history_obj;
                        /* For Digital Payment*/
                        $product_sales_data->digital_payment_amount = $sale_paid->paid_amount;
                        $product_sales_data->cash_payment_amount = 0;
                        $product_sales_data->paymentMethod = $sale_paid->paymentMethod->name;
                    }else{
                        /* For Digital Payment */
                        $sale_paid_history_obj = (object) array("id" => null, 
                        );
                        $payment_method_obj = (object) array("name" => 'Select Payment Method'
                        );
                        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => '',"value" => ''
                        );
                        $sale_paid_history_obj->payment_method = $payment_method_obj; 
                        $sale_paid_history_obj->sale_paid_details[0] = $sale_paid_details_obj; 
                        $sale_paid_history_obj->sale_paid_details[1] = $sale_paid_details_obj;
                        $salePaidHistory[$key+1] = $sale_paid_history_obj; 
                        /* For Cash Payment*/
                        $product_sales_data->cash_payment_amount = $sale_paid->paid_amount;
                        $product_sales_data->digital_payment_amount = 0;
                    }
                }            
            }
            else if(sizeof($salePaidHistory) == 2 ){         
                foreach($salePaidHistory as $key => $sale_paid) {  
                    if($sale_paid->paymentMethod->name == "Cash"){
                        $product_sales_data->cash_payment_amount = $sale_paid->paid_amount;
                    }else if($sale_paid->paymentMethod->name == "Master Card" || $sale_paid->paymentMethod->name == "Visa Card" || $sale_paid->paymentMethod->name == "Bkash"){ 
                        $product_sales_data->digital_payment_amount = $sale_paid->paid_amount;
                        $product_sales_data->paymentMethod = $sale_paid->paymentMethod->name;
                    }
                }
            }
            $product_sales_data->sale_paids = $salePaidHistory; 
            foreach ($product_sales_data->outLetProductSales as $key => $out_let_product_sale) {
                $product_sales_data->outLetProductSales[$key]->product_id = $out_let_product_sale->id; 
                $product_sales_data->outLetProductSales[$key]->categories = $out_let_product_sale->outLetProduct->product->categories;  
                $product_sales_data->outLetProductSales[$key]->vat_percent = $out_let_product_sale->outLetProduct->product->prices[0]->vat->percent;                    
                if( sizeof( $out_let_product_sale->outLetProduct->product->combinations ) <= 0 ){
                    $combinations_obj = (object) array("id" => $out_let_product_sale->outLetProduct->id,
                        "out_let_id" => $out_let_product_sale->outLetProduct->out_let_id,
                        "product_combination_id" => null,
                        "product_id" => $out_let_product_sale->outLetProduct->product_id,
                        "stock_quantity" => $out_let_product_sale->outLetProduct->stock_quantity,
                    );
                    $product_sales_data->outLetProductSales[$key]->combination = "No";
                    $product_sales_data->outLetProductSales[$key]->out_let_product_id = $out_let_product_sale->outLetProduct->id;
                    $product_sales_data->outLetProductSales[$key]->combinations = array($combinations_obj);
                }else{
                    $product_sales_data->outLetProductSales[$key]->combination = "Yes";
                    $product_sales_data->outLetProductSales[$key]->combinations = $out_let_product_sale->outLetProduct->product->combinations; 
                }         
            }
        }        
        $product_sales_data->customer_order_detail_items = $product_sales_data->outLetProductSales;
        return Response::json($product_sales_data);
    }
    protected function generateSalePaidDetails(){
        /* For Cash Payment */
        $salePaidHistory = [];
        $sale_paid_history_obj = (object) array("id" => null, 
        );
        $payment_method_obj = (object) array("name" => 'Cash'
        );
        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => 'Payment By',"value" => 'Self'
        );
        $sale_paid_history_obj->payment_method = $payment_method_obj; 
        $sale_paid_history_obj->sale_paid_details = array($sale_paid_details_obj); 
        $salePaidHistory[0] = $sale_paid_history_obj;

        /* For Digital Payment */
        $sale_paid_history_obj = (object) array("id" => null,
        );
        $payment_method_obj = (object) array("name" => 'Select Payment Method'
        );
        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => '',"value" => ''
        );
        $sale_paid_history_obj->payment_method = $payment_method_obj; 
        $sale_paid_history_obj->sale_paid_details[0] = $sale_paid_details_obj; 
        $sale_paid_history_obj->sale_paid_details[1] = $sale_paid_details_obj;
        $salePaidHistory[1] = $sale_paid_history_obj; 
        return Response::json($salePaidHistory);
    }
    /**
     * Get Total Sales Amount daily/weekly/monthly
     *
     * @return \Illuminate\Http\Response
     */
    protected function getSalesReport( Request $request ){ 
        $yesterday = Carbon::yesterday()->format('Y-m-d'); 
        $yesterday_sales = Sale::whereDate('sale_date', $yesterday)->where('status', 'sold')          
            ->where(function ($query) use($request){
            if($request->out_let_id){
                $query->where('out_let_id',$request->out_let_id);
            }
            if($request->company_id){
                $query->where('company_id',$request->company_id);
            }
        })->sum('net_amount');

        $daily_sales = Sale::whereDate('sale_date',date('Y-m-d'))->where('status', 'sold')          
            ->where(function ($query) use($request){
            if($request->out_let_id){
                $query->where('out_let_id',$request->out_let_id);
            }
            if($request->company_id){
                $query->where('company_id',$request->company_id);
            }
        })->sum('net_amount');

        $startOfCurrentWeek = Carbon::parse('last saturday')->startOfDay();
        $startOfLastWeek  = $startOfCurrentWeek->copy()->subDays(7);
        $endOfCurrentWeek = Carbon::parse('next friday')->endOfDay();
        $endOfLastWeek  = $endOfCurrentWeek->copy()->subDays(7);

        $last_week_sales = Sale::whereBetween('sale_date', [
            $startOfLastWeek,$endOfLastWeek])->where('status', 'sold')
            ->where(function ($query) use($request){
            if($request->out_let_id){
                $query->where('out_let_id',$request->out_let_id);
            }
            if($request->company_id){
                $query->where('company_id',$request->company_id);
            }
        })->sum('net_amount');

        $weekly_sales = Sale::whereBetween('sale_date', [
            $startOfCurrentWeek,$endOfCurrentWeek])->where('status', 'sold')
            ->where(function ($query) use($request){
            if($request->out_let_id){
                $query->where('out_let_id',$request->out_let_id);
            }
            if($request->company_id){
                $query->where('company_id',$request->company_id);
            }
        })->sum('net_amount');

        $startOfCurrentMonth = Carbon::today()->startOfMonth();
        $endOfCurrentMonth = $startOfCurrentMonth->copy()->endOfMonth();
        $startOfLastMonth = Carbon::today()->startOfMonth()->subMonth();
        $endOfLastMonth = $startOfLastMonth->copy()->endOfMonth();

        $last_month_sales = Sale::whereBetween('sale_date', [$startOfLastMonth, $endOfLastMonth])->where('status', 'sold')
            ->where(function ($query) use($request){
            if($request->out_let_id){
                $query->where('out_let_id',$request->out_let_id);
            }
            if($request->company_id){
                $query->where('company_id',$request->company_id);
            }
        })->sum('net_amount');

        $monthly_sales = Sale::whereBetween('sale_date', [$startOfCurrentMonth, $endOfCurrentMonth])->where('status', 'sold')
            ->where(function ($query) use($request){
            if($request->out_let_id){
                $query->where('out_let_id',$request->out_let_id);
            }
            if($request->company_id){
                $query->where('company_id',$request->company_id);
            }
        })->sum('net_amount');
         
        $daily_sales_incread_decreased_percentage = $yesterday_sales ? ( ($daily_sales - $yesterday_sales)/$yesterday_sales ) * 100 : $daily_sales * 100;

        $weekly_sales_incread_decreased_percentage = $last_week_sales ? ( ($weekly_sales - $last_week_sales)/$last_week_sales ) * 100 : $weekly_sales * 100;

        $monthly_sales_incread_decreased_percentage = $last_month_sales ? ( ($monthly_sales - $last_month_sales)/$last_month_sales ) * 100 : $monthly_sales * 100;

        $most_recent_three_sales = Sale::select('sales.id', 'sales.created_at', 'sales.net_amount','sales_by')->where('status', 'sold')->where(function ($query) use($request){
            if($request->out_let_id){
                $query->where('out_let_id', $request->out_let_id);
            }
            if($request->company_id){
                $query->where('company_id', $request->company_id);
            }
        })->with(
        ['salePaids' => function ($query){ 
            $query->with(['paymentMethod' => function($query){
                 
            }]);
        }])->orderBy('sales.id', 'DESC')->limit(3)->get();   

        if($most_recent_three_sales){
            // Get the current date and time
            $currentDateTime = Carbon::now();
            foreach($most_recent_three_sales as $key => $value){
                $sales_executive = UserDetail::select('first_name','last_name')->where('user_id', $value->sales_by)->first();
                // Calculate the difference
                $interval = $currentDateTime->diff($value->created_at);
                // Extract days, hours, minutes, and seconds
                $days = $interval->days;
                $hours = $interval->h;
                $minutes = $interval->i;
                $seconds = $interval->s;
                $most_recent_three_sales[$key]->converted_duration = $hours." hour(s) ".$minutes." minutes ".$seconds." seconds";
                $most_recent_three_sales[$key]->converted_duration_in_seconds = ($hours*3600)+($minutes*60)+$seconds;
                if($sales_executive){
                    $most_recent_three_sales[$key]->sales_executive = $sales_executive->first_name. " ".$sales_executive->last_name;
                }
                $most_recent_three_sales[$key]->sales_executive = "N/A";
            }
        }     
        // $most_recent_three_sales = $most_recent_three_sales->sortByDesc(function ($sale) {
        //     return $sale->converted_duration_in_seconds;
        // });

        //////////////////////////////
        $total_sales_amount_obj = array("daily" => $daily_sales, "daily_sales_incread_decreased_percentage" => $daily_sales_incread_decreased_percentage, "weekly_sales_incread_decreased_percentage" => $weekly_sales_incread_decreased_percentage , "monthly_sales_incread_decreased_percentage" => $monthly_sales_incread_decreased_percentage,"weekly" => $weekly_sales,"monthly" => $monthly_sales,"most_recent_three_sales" => $most_recent_three_sales
        );
        return $total_sales_amount_obj;
    }
    protected function getYearlySalesOverview( Request $request ){
        $out_let_id = $request->out_let_id ? $request->out_let_id : null;
        $company_id = $request->company_id ? $request->company_id : null;
        $year = $request->year ? $request->year : date('Y');  
        $result = DB::select("SELECT 
                SUM(CASE WHEN month = 'Jan' THEN total ELSE 0 END) AS 'Jan',
                SUM(CASE WHEN month = 'Feb' THEN total ELSE 0 END) AS 'Feb',
                SUM(CASE WHEN month = 'Mar' THEN total ELSE 0 END) AS 'Mar',
                SUM(CASE WHEN month = 'Apr' THEN total ELSE 0 END) AS 'Apr',
                SUM(CASE WHEN month = 'May' THEN total ELSE 0 END) AS 'May',
                SUM(CASE WHEN month = 'Jun' THEN total ELSE 0 END) AS 'Jun',
                SUM(CASE WHEN month = 'Jul' THEN total ELSE 0 END) AS 'Jul',
                SUM(CASE WHEN month = 'Aug' THEN total ELSE 0 END) AS 'Aug',
                SUM(CASE WHEN month = 'Sep' THEN total ELSE 0 END) AS 'Sep',
                SUM(CASE WHEN month = 'Oct' THEN total ELSE 0 END) AS 'Oct',
                SUM(CASE WHEN month = 'Nov' THEN total ELSE 0 END) AS 'Nov',
                SUM(CASE WHEN month = 'Dec' THEN total ELSE 0 END) AS 'Dec'
            FROM (
                SELECT 
                    DATE_FORMAT(sale_date, '%b') AS month, 
                    SUM(net_amount) AS total
                FROM sales
                WHERE sale_date LIKE '$year%' 
                  AND (out_let_id = COALESCE(NULLIF('$out_let_id', ''), out_let_id))
                  AND (company_id = COALESCE(NULLIF('$company_id', ''), company_id))
                  AND deleted_at IS NULL
                  AND status = 'sold'
                GROUP BY DATE_FORMAT(sale_date, '%Y-%m')
            ) AS sale_year_month"
        ); 
        return $result;        
    } 
    protected function getCategoryWiseSalesOverview( Request $request ){
        $out_let_id = $request->out_let_id ? $request->out_let_id : null;
        $company_id = $request->company_id ? $request->company_id : null;
        $year = $request->year ? $request->year : date('Y'); 
        // Category Max and Min Sales // 
        $category_wise_sales = array();
        $data_series = array();
        $data_labels = array();
        $top_sold_products = DB::select("SELECT TEMP.id, TEMP.product_id, TEMP.unit_price , TEMP.quantity, SUM(TEMP.Sale_price) AS total_sale_product_wise
            FROM (
                SELECT sales.id, combination_sales.product_id, combination_sales.unit_price , combination_sales.quantity, 
                (combination_sales.unit_price * combination_sales.quantity) AS Sale_price FROM sales
                JOIN out_let_product_product_combination_sales AS combination_sales ON sales.id = combination_sales.sale_id 
                WHERE sales.sale_date LIKE '$year%' AND sales.deleted_at IS NULL AND combination_sales.deleted_at IS NULL
                AND (out_let_id = COALESCE(NULLIF('$out_let_id', ''), out_let_id))
                  AND (company_id = COALESCE(NULLIF('$company_id', ''), company_id)) AND sales.status = 'sold'
            ) TEMP GROUP BY product_id ORDER BY total_sale_product_wise DESC LIMIT 6");
        if( $top_sold_products ){
            foreach($top_sold_products as $key => $value){
                //echo $value->total_sale_product_wise;
                if ($value->product_id) {                    
                    $product_category = DB::select("SELECT categories.id, categories.name FROM categories JOIN category_product ON categories.id = category_product.category_id WHERE category_product.product_id = $value->product_id");                    
                    if($product_category){
                        foreach($product_category as $category_key => $category_value){
                            if(array_key_exists($category_value->name, $category_wise_sales)){
                                $category_wise_sales[$category_value->name] += $value->total_sale_product_wise;
                            }else{
                                $category_wise_sales[$category_value->name] = $value->total_sale_product_wise;
                                $data_series[] = $value->total_sale_product_wise;
                                $data_labels[] = $category_value->name;
                            }
                        }
                    }
                }       
            }
        }
        return response()->json(['data_series' => $data_series, 'data_labels' => $data_labels]); 
    }
    protected function getRevenueStatsData( Request $request ){
        $out_let_id = $request->out_let_id ? $request->out_let_id : null;
        $company_id = $request->company_id ? $request->company_id : null;
        $year = $request->year ? $request->year : date('Y'); 

        $sales_result = DB::select("SELECT 
            SUM(IF(month = 'Jan', total, 0)) AS 'Jan',
            SUM(IF(month = 'Feb', total, 0)) AS 'Feb',
            SUM(IF(month = 'Mar', total, 0)) AS 'Mar',
            SUM(IF(month = 'Apr', total, 0)) AS 'Apr',
            SUM(IF(month = 'May', total, 0)) AS 'May',
            SUM(IF(month = 'Jun', total, 0)) AS 'Jun',
            SUM(IF(month = 'Jul', total, 0)) AS 'Jul',
            SUM(IF(month = 'Aug', total, 0)) AS 'Aug',
            SUM(IF(month = 'Sep', total, 0)) AS 'Sep',
            SUM(IF(month = 'Oct', total, 0)) AS 'Oct',
            SUM(IF(month = 'Nov', total, 0)) AS 'Nov',
            SUM(IF(month = 'Dec', total, 0)) AS 'Dec'
            FROM ( SELECT DATE_FORMAT(sale_date, '%b') AS month, SUM(net_amount) as total FROM sales 
                WHERE sale_date like '$year%' AND (out_let_id = COALESCE(NULLIF('$out_let_id', ''), out_let_id))
                  AND (company_id = COALESCE(NULLIF('$company_id', ''), company_id)) AND status = 'sold' AND deleted_at IS NULL GROUP BY DATE_FORMAT(sale_date, '%Y-%m')) as sale_year_month"
        ); 
        $costs_result = DB::select("SELECT 
            SUM(IF(month = 'Jan', total, 0)) AS 'Jan',
            SUM(IF(month = 'Feb', total, 0)) AS 'Feb',
            SUM(IF(month = 'Mar', total, 0)) AS 'Mar',
            SUM(IF(month = 'Apr', total, 0)) AS 'Apr',
            SUM(IF(month = 'May', total, 0)) AS 'May',
            SUM(IF(month = 'Jun', total, 0)) AS 'Jun',
            SUM(IF(month = 'Jul', total, 0)) AS 'Jul',
            SUM(IF(month = 'Aug', total, 0)) AS 'Aug',
            SUM(IF(month = 'Sep', total, 0)) AS 'Sep',
            SUM(IF(month = 'Oct', total, 0)) AS 'Oct',
            SUM(IF(month = 'Nov', total, 0)) AS 'Nov',
            SUM(IF(month = 'Dec', total, 0)) AS 'Dec'
            FROM (
                SELECT DATE_FORMAT(sales.sale_date, '%b') AS month, out_let_product_product_combination_sales.cost_price, 
                out_let_product_product_combination_sales.quantity, SUM(out_let_product_product_combination_sales.cost_price * 
                out_let_product_product_combination_sales.quantity) AS total
                FROM sales 
                JOIN out_let_product_product_combination_sales ON sales.id = out_let_product_product_combination_sales.sale_id
                WHERE sales.sale_date like '$year%' AND (out_let_id = COALESCE(NULLIF('$out_let_id', ''), out_let_id))
                  AND (company_id = COALESCE(NULLIF('$company_id', ''), company_id)) AND sales.status = 'sold' AND sales.deleted_at IS NULL GROUP BY DATE_FORMAT(sale_date, '%Y-%m')) as sale_year_month"
        );  
        
        if($sales_result && $costs_result){
            foreach($sales_result[0] as $key => $value ){
                $sales_data_series[] = $value;
                $net_profit_data_series[] = $value - $costs_result[0]->$key;
            }
        }
        return response()->json(['net_profit_result' => $net_profit_data_series, 'sales_result' => $sales_data_series]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
