<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;
use App\User; 
// use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Response;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use DB;
use Cookie;
use Artisan;
 
class FrontendController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth:api', ['except' => ['login']]);
    } 
    public function loginMe(Request $request)
    {  
        $credentials = $request->only('email', 'password'); 
        //Crean token
        try {
            if(!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                 'success' => false,
                 'message' => 'Login credentials are invalid, Please try again!',
                ], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                 'success' => false,
                 'message' => $e->getMessage(),
                ], 401);
        }
  
        //Token created, return with success response and jwt token
        return response()->json([
            'success' => true,
            'token' => $token,
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::check()){
//            $request->session()->put('out_let_id', '34500');
            return redirect('frontend/login');
        }else{
            return redirect('frontend/dashboard');
        }
    }

    public function login()
    {
        $out_let_id = session()->get('out_let_id');
        return view('frontend.login.index');
    }
    public function doLogin(Request $request)
    {
        $return_array['success'] = "Not LogedIn";
        if (Auth::attempt(['email' => $request->login_email_or_mobile, 'password' => $request->password, 'active' => 1])) {
            Redis::set('success', 'You are successfully LogedIn');
            $return_array['success'] = "success"; 
        }
        return $return_array;
    }
    public function logout( Request $request ){       

        // $token = $request->bearerToken();
        // \Log::info('Request headers:', $request->headers->all());
        // \Log::info('Request body:', $request->all());
        //  // Check if the token is present
        // if (!$token) {
        //     return response()->json(['error' => 'Token is required'], 401);
        // }else{
        //     JWTAuth::invalidate($token);
        //     return response()->json([
        //         'success' => true,
        //         'message' => 'User logged out successfully'
        //     ]);
        // } 
        $user = Auth::user();
        $userid = $user->id;
        DB::table('oauth_access_tokens')->where('user_id', $userid)->update(['revoked' => true]);        
        $response = new Response([
            'status' => 'success',
            'message' => 'Logged out successfully.',
        ], 200);
        $response->withCookie(cookie('user_id', null, time() - 3600));
        $response->withCookie(cookie('out_let_id', null, time() - 3600));
        return $response;        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
