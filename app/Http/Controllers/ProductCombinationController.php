<?php

namespace App\Http\Controllers;
use App\Product;
use App\Attribute;
use App\ProductCombination;
use Illuminate\Http\Request;
use App\Http\Requests\ProductCombinationRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use DB;

class ProductCombinationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('access-product-combination')) {
            abort(401);
        }
        $combinations = DB::table('product_combinations')
            ->join('products', 'products.id', '=', 'product_combinations.product_id')
            ->select('product_id', DB::raw('count(*) as total'))
            ->whereNull('products.deleted_at')
            ->whereNull('product_combinations.deleted_at')
            ->groupBy('product_id')
            ->get();
        return view('product_combination.index', compact('combinations'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('access-product-combination')) {
            abort(401);
        }
        $product_lists = Product::orderBy('name', 'ASC')->pluck('name','id');
        $attributes = Attribute::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('product_combination.create',compact('product_lists','attributes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->can('access-product-combination')){
            abort(401);
        }
        $product = Product::findorfail( $request->product_id );
        $combination = $product->combinations()->create(['product_id' => $request->product_id, 'name' => $request->name]);
        $combination_id = $combination->id;
        $combination_attributes = explode(',', $request->product_combination);

        foreach ($combination_attributes as $attribute){
            $combination = ProductCombination::findorfail( $combination_id );
            $combination->combinationAttributeValues()->create(['attribute_value_id' => $attribute, 'product_combination_id' => $combination_id]);

        }
        return redirect('product_combination')->with('success','Product Combination added successfully!');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('access-product-combination')) {
            abort(401);
        }
        $product_id = $id;
        return view('product_combination.edit', compact('product_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->can('access-product-combination')){
            abort(401);
        }
        $product = Product::findorfail( $request->product_id );
        $combination = $product->combinations()->create(['product_id' => $request->product_id, 'name' => $request->name]);
        $combination_id = $combination->id;

        $combination_attributes = explode(',', $request->product_combination);

        foreach ($combination_attributes as $attribute){
            $combination = ProductCombination::findorfail( $combination_id );
            $combination->attributeValues()->create(['attribute_value_id' => $attribute, 'product_combination_id' => $combination_id]);

        }
        return redirect('product_combination')->with('success','Product Combination updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( ProductCombination $productCombination )
    {

    }

}
