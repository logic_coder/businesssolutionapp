<?php

namespace App\Http\Controllers;
use App\CompanyCurrency;
use App\Company;
use App\Currency;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyCurrencyRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CompanyCurrencyController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('access-company-currency-settings')) {
            abort(401);
        }
        $company_currencies = CompanyCurrency::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('company_currency.index', compact('company_currencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('access-company-currency-settings')) {
            abort(401);
        }
        $company_currencies = '';
        $currency_lists = Currency::orderBy('name', 'ASC')->get(); //->pluck('name','code','id');
//        foreach ($currency_lists as $currency){
//            dd($currency->name);
//        }
        return view('company_currency.create', compact('company_currencies','currency_lists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyCurrencyRequest $request)
    {
        if(!Auth::user()->can('access-company-currency-settings')) {
            abort(401);
        }
        $companies = Company::find(Auth::user()->company_id);
        $currency_data['currency_id'] = $request->currency_id;
        $currency_data['default'] = $request->default ? "Yes": "No";
        $company_currency = $companies->currencies()->create( $currency_data );
        if( $company_currency && $request->default){
            $this->changeDefaultCurrency($company_currency);
        }
        return back()->with('success','Company currency added successfully!');
    }
    public function changeDefaultCurrency($company_currency){
        DB::table('company_currencies')->where('id', '!=', $company_currency->id)->update(['default' => 'No']);
        return true;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('access-company-currency-settings')) {
            abort(401);
        }
        $company_currencies = CompanyCurrency::find( $id );
        $currency_lists = Currency::orderBy('name', 'ASC')->get();
        return view('company_currency.edit', compact('company_currencies', 'currency_lists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyCurrencyRequest $request, $id)
    {
        if(!Auth::user()->can('access-company-currency-settings')) {
            abort(401);
        }
        $company_currency = CompanyCurrency::find($id);
        $currency_data['currency_id'] = $request->currency_id;
        $currency_data['default'] = $request->default ? "Yes": "No";
        $company_currency->update( $currency_data );
        if( $company_currency && $request->default){
            $this->changeDefaultCurrency($company_currency);
        }
        return redirect('company_currency')->with('success','Company currency updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
       
    }

}
