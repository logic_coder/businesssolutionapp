<?php

namespace App\Http\Controllers;
use App\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentMethodApiController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removePaymentMethod( Request $request )
    {
        $return_array['message'] = '';
        if(!Auth::user()->can('access-global-payment-method-settings')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids )){
            PaymentMethod::destroy( $request->item_ids );
            return response([
                'message' => 'Payment method deleted successfully!!!'
            ],200);
        }
        return $return_array;
    }
    protected function getCompanyPaymentMethod()
    {
        return PaymentMethod::where('company_id', Auth::user()->company_id)->orderby('id','DESC')->get();
    }
}
