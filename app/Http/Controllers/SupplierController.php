<?php
namespace App\Http\Controllers;

use App\Company;
use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Requests\SupplierRequest;
use Illuminate\Support\Facades\Auth;


class SupplierController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('access-vendor-supplier')) {
            abort(401);
        }
        $suppliers = Supplier::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('supplier.index', compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('access-vendor-supplier')) {
            abort(401);
        }
        return view('supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SupplierRequest $request)
    {
        if(!Auth::user()->can('access-vendor-supplier')) {
            abort(401);
        }
        Company::find(Auth::user()->company_id)->suppliers()->create($request->all());
        return redirect('supplier')->with('success','Supplier added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('access-vendor-supplier')){
            abort(401);
        }
        $suppliers = Supplier::find($id);
        return view('supplier.edit', compact('suppliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SupplierRequest $request, $id)
    {
        if(!Auth::user()->can('access-vendor-supplier')){
            abort(401);
        }
        $supplier = Supplier::find($id);
        $supplier->update( $request->all() );
        return redirect('supplier')->with('success','Supplier updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
       
    }
}
