<?php

namespace App\Http\Controllers;
use App\Company;
use App\OutLet;
use App\Product;
use App\ProductCombination;
use App\WarehouseStoreOutChallan;
use App\WarehuseStoreOutChallanRequest;
use App\Http\Requests\WarehouseStoreOutChallanRequest;
use App\WarehouseStoreOutChallanProduct;
use App\WarehouseStoreOutChallanProductCombination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Psy\Util\Json;
use Carbon\Carbon;
use Response;
use DB;

class WarehouseStoreOutChallanApiController extends Controller
{
    protected function saveWarehouseStoreOutChallan(WarehouseStoreOutChallanRequest $request)
    {
        $return_array['success'] = '';
        $message = "";
        $warehouse_store_out_challan_product = '';
        if( $request ){
            $challan_data['company_id'] = Auth::user()->company_id;
            $challan_data['out_let_id'] = $request->out_let_id;
            $challan_data['warehouse_id'] = $request->warehouse_id;
            $challan_data['request_date'] = $request->request_date;
            $challan_data['created_by'] = Auth::user()->id;
            $challan_data['status'] = $request->status;
            $warehouse_store_out_challan = Company::find($challan_data['company_id'])->warehouseStoreOutChallans()->create($challan_data);

            $now = Carbon::now();
            $challan_no_data['challan_no'] = $now->year.$warehouse_store_out_challan->id;
            $warehouse_store_out_challan->update($challan_no_data);

            foreach($request->product_combination_list as $product_combination) {
                $challan_product_data['product_id'] = $product_combination['id'];
                $warehouse_store_out_challan_product_inserted = false;
                $product_combination_data = [];
                foreach($product_combination['combinations'] as $combination) {
                    if(isset($combination['requested_quantity'])){
                        if($combination['requested_quantity']){
                            if(!$warehouse_store_out_challan_product_inserted){
                                $warehouse_store_out_challan_product = $warehouse_store_out_challan->challanProducts()->create($challan_product_data);
                                $warehouse_store_out_challan_product_inserted = true;
                            }
                            if(isset($combination['id'])) {
                                if($challan_data['status'] == "Auto Received"){
                                    $product_combination_data[$combination['id']] = ['product_combination_warehouse_store_in_challan_product_id' => $combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity'], 'received_quantity' => $combination['requested_quantity'], 'stock_quantity' => $combination['requested_quantity']];
                                }else{
                                    $product_combination_data[$combination['id']] = ['product_combination_warehouse_store_in_challan_product_id' => $combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity']];
                                }
                                /* Update stock_quantity in Table: product_combination_warehouse_store_in_challan_product */
                                DB::table('product_combination_warehouse_store_in_challan_product')->where('id', $combination['product_combination_warehouse_store_in_challan_product_id'])->update(array('stock_quantity' => $combination['stock_remain']));
                            }
                        }
                    }
                }
                if(!isset($combination['id']) && $warehouse_store_out_challan_product_inserted) {
                    if($challan_data['status'] == "Auto Received") {
                        $warehouse_store_out_challan_product->storeOutProductCombinations()->attach([null], array('product_combination_warehouse_store_in_challan_product_id' => $combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity'], 'received_quantity' => $combination['requested_quantity'], 'stock_quantity' => $combination['requested_quantity']));
                    }else{
                        $warehouse_store_out_challan_product->storeOutProductCombinations()->attach([null], array('product_combination_warehouse_store_in_challan_product_id' => $combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity']));
                    }
                    /* Update stock_quantity in Table: product_combination_warehouse_store_in_challan_product */
                    DB::table('product_combination_warehouse_store_in_challan_product')->where('id', $combination['product_combination_warehouse_store_in_challan_product_id'])->update(array('stock_quantity' => $combination['stock_remain']));
                }
                if(isset($combination['id']) && $warehouse_store_out_challan_product_inserted) {
                    $warehouse_store_out_challan_product->storeOutProductCombinations()->sync($product_combination_data);
                }
            }
            $return_array['warehouse_store_out_challan_id'] = $warehouse_store_out_challan->id;
            if( $warehouse_store_out_challan ){
                $message = 'Warehouse StoreOut Challan Created Successfully';
            }else{
                $message = 'Error: Warehouse StoreOut Challan Not Created, Please Try again';
            }
        }
        return response([
            'warehouse_store_out_challan_id' => $warehouse_store_out_challan->id,
            'message'=> $message
        ]);
    }
    public function updateWarehouseStoreOutChallan(WarehouseStoreOutChallanRequest $request)
    {
        $return_array['success'] = '';
        $message = "";
        $warehouse_store_out_challan_product = '';
        if( $request ) {
            $challan_data['company_id'] = Auth::user()->company_id;
            $challan_data['out_let_id'] = $request->out_let_id;
            $challan_data['warehouse_id'] = $request->warehouse_id;
            $challan_data['request_date'] = $request->request_date;
            $challan_data['status'] = $request->status;
            $warehouse_store_out_challan = WarehouseStoreOutChallan::find( $request->challan_id );
            $warehouse_store_out_challan->update( $challan_data );
            $product_combination_data = [];
            
            if($request->remove_old_product_combinations){
                foreach( $request->remove_old_product_combinations as $old_product_combination ){
                    $challan_products = $warehouse_store_out_challan->challanProducts()->where('product_id',$old_product_combination['product_id'])->get();                    
                    $return_array['success'] = $this->remove_old_product_combinations( $challan_products, $old_product_combination['combination'] );
                    //return $return_array;
                }
            }
            foreach($request->product_combination_list as $product_combination){
                $warehouse_store_out_challan_product_inserted = false;
                $challan_product_data['warehouse_store_out_challan_id'] = $request->challan_id;
                $challan_product_data['product_id'] = $product_combination['product_id'];
                $product_combination_data = [];
                foreach ($product_combination['combinations'] as $combination) {                    
                    if (!isset($combination['received_quantity'])){
                        $combination['received_quantity'] = null;
                    }
                    $combination['lost_quantity'] = null;
                    $combination['return_quantity'] = null;
                    $combination['stock_quantity'] = null;
                    if(isset($combination['missing_status'])){
                        if($combination['missing_status'] == "Lost"){
                            $combination['lost_quantity'] = $combination['missing_quantity'];
                        }
                        elseif($combination['missing_status'] == "Return Warehouse"){
                            $combination['return_quantity'] = $combination['missing_quantity'];
                        }
                    }
                    if (isset($combination['requested_quantity'])){
                        if ($combination['requested_quantity']) {
                            if(!$warehouse_store_out_challan_product_inserted){
                                $warehouse_store_out_challan_product_updated = WarehouseStoreOutChallanProduct::updateOrCreate(['id' => $product_combination['warehouse_store_out_challan_product_id']], $challan_product_data);
                                $warehouse_store_out_challan_product_inserted = true;
                            }
                            if(isset($combination['id'])) {                                                           
                                if($challan_data['status'] == "Auto Received") {
                                    $product_combination_data[$combination['id']] = ['product_combination_warehouse_store_in_challan_product_id' => $combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['requested_quantity'], 'stock_quantity' => $combination['requested_quantity'], 'missing_status' => $combination['missing_status']];
                                }
                                elseif($challan_data['status'] == "Received") {
                                    $product_combination_data[$combination['id']] = ['product_combination_warehouse_store_in_challan_product_id' => $combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['received_quantity'], 'stock_quantity' => $combination['received_quantity'],'lost_quantity' => $combination['lost_quantity'], 'return_quantity' => $combination['return_quantity'], 'missing_status' => $combination['missing_status']];
                                }
                                else{
                                    $product_combination_data[$combination['id']] = ['product_combination_warehouse_store_in_challan_product_id' => $combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity'],'received_quantity' => null,'stock_quantity' => null,'lost_quantity' => null,'return_quantity' => null];
                                }
                                /* Update stock_quantity in Table: product_combination_warehouse_store_in_challan_product */
                                DB::table('product_combination_warehouse_store_in_challan_product')->where('id', $combination['product_combination_warehouse_store_in_challan_product_id'])->update(array('stock_quantity' => $combination['stock_remain']+$combination['return_quantity']));
                            }
                        }
                    }
                }

                if(!isset($combination['id']) && $warehouse_store_out_challan_product_inserted) {
                    $product_combination_value = DB::table('product_combination_warehouse_store_out_challan_product')->where('warehouse_store_out_challan_product_id', $product_combination['warehouse_store_out_challan_product_id'])->first();
                    if($product_combination_value){
                        if($challan_data['status'] == "Auto Received") {
                            DB::table('product_combination_warehouse_store_out_challan_product')->where('warehouse_store_out_challan_product_id', $product_combination['warehouse_store_out_challan_product_id'])->update(array('requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['requested_quantity'],'stock_quantity' => $combination['requested_quantity'], 'missing_status' => $combination['missing_status']));
                        }
                        elseif($challan_data['status'] == "Received") {
                            DB::table('product_combination_warehouse_store_out_challan_product')->where('warehouse_store_out_challan_product_id', $product_combination['warehouse_store_out_challan_product_id'])->update(array('requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['received_quantity'], 'stock_quantity' => $combination['received_quantity'], 'lost_quantity' => $combination['lost_quantity'], 'return_quantity' => $combination['return_quantity'], 'missing_status' => $combination['missing_status']));
                        }
                        else{
                            DB::table('product_combination_warehouse_store_out_challan_product')->where('warehouse_store_out_challan_product_id', $product_combination['warehouse_store_out_challan_product_id'])->update(array('requested_quantity' => $combination['requested_quantity'],'received_quantity' => null,'stock_quantity' => null,'lost_quantity' => null,'return_quantity' => null));
                        }
                    }else{
                        if($challan_data['status'] == "Auto Received") { 
                            $warehouse_store_out_challan_product_updated->product_combination()->create(['product_combination_warehouse_store_in_challan_product_id' =>$combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity'], 'received_quantity' => $combination['requested_quantity'], 'stock_quantity' => $combination['requested_quantity'], 'missing_status' => $combination['missing_status']]);
                        }
                        elseif($challan_data['status'] == "Received") {
                            $warehouse_store_out_challan_product_updated->product_combination()->create(['product_combination_warehouse_store_in_challan_product_id' =>$combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['received_quantity'],'stock_quantity' => $combination['received_quantity'], 'lost_quantity' => $combination['lost_quantity'], 'return_quantity' => $combination['return_quantity'], 'missing_status' => $combination['missing_status']]);
                        }
                        else{
                            $warehouse_store_out_challan_product_updated->product_combination()->create(['product_combination_warehouse_store_in_challan_product_id' =>$combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity'], 'received_quantity' => null, 'stock_quantity' => null , 'lost_quantity' => null , 'return_quantity' => null ]);
                        }
                        /* Update stock_quantity in Table: product_combination_warehouse_store_in_challan_product */
                    DB::table('product_combination_warehouse_store_in_challan_product')->where('id', $combination['product_combination_warehouse_store_in_challan_product_id'])->update(array('stock_quantity' => $combination['stock_remain']+$combination['return_quantity']));
                    }
                }
                elseif(isset($combination['id']) && $warehouse_store_out_challan_product_inserted){
                    $warehouse_store_out_challan_product_updated->storeOutProductCombinations()->sync($product_combination_data);
                }
            }
            if( $warehouse_store_out_challan ){
                $message = "Warehouse StoreOut Challan Updated Successfully";
            }else{
                $message = "Error: Warehouse StoreOut Challan Not Updated, Please Try again";
            }
        }
        return response([
            'message'=> $message
        ]);
    }
    protected function remove_old_product_combinations( $challan_products, $combination )
    {
        if( $challan_products ){
            foreach( $challan_products as $challan_product ){
                if( $combination == "Yes"){
                    $product_combination_data = [];
                    $store_out_product_combinations = $challan_product->storeOutProductCombinations()->get(); 
                    foreach ($store_out_product_combinations as $store_out_product_combination) {
                        $product_combination_warehouse_store_in_challan_product_id = $store_out_product_combination->pivot->product_combination_warehouse_store_in_challan_product_id;
                        $requested_quantity = $store_out_product_combination->pivot->requested_quantity;
                        DB::table('product_combination_warehouse_store_in_challan_product')->where('id', $product_combination_warehouse_store_in_challan_product_id)->update(array('stock_quantity' => DB::raw('stock_quantity + ' .$requested_quantity)));
                    }
                    //
                    $challan_product->storeOutProductCombinations()->sync($product_combination_data);
                }else{
                    $store_out_product_combinations = $challan_product->product_combination()->get();                     
                    foreach ($store_out_product_combinations as $store_out_product_combination) {
                        $product_combination_warehouse_store_in_challan_product_id = $store_out_product_combination->product_combination_warehouse_store_in_challan_product_id;
                        $requested_quantity = $store_out_product_combination->requested_quantity;
                        DB::table('product_combination_warehouse_store_in_challan_product')->where('id', $product_combination_warehouse_store_in_challan_product_id)->update(array('stock_quantity' => DB::raw('stock_quantity + ' .$requested_quantity)));
                        
                    }                    
                    $challan_product->product_combination()->delete();
                }
                $challan_product->forceDelete();
            }
        }
        return true;
    }
    /**
     * Get WarehouseStoreOutChallan Data
     *
     * @param  int  $challan_id
     * @return \Illuminate\Http\Response
     */
    public function getChallanData(Request $request)
    {
        $challan_data = null; 
        $product_challan_infos = null; 
        if($request->challan_id){
            $challan_data =  WarehouseStoreOutChallan::where('id', $request->challan_id )->with(
            ['challanProducts' => function($query){
                $query->with(['product','storeOutProductCombinations'=>function($query){
                    $query->with(['combinationAttributeValues' => function($query){
                        $query->with([ 'attributeValue' => function($query){
                            $query->with('attribute');
                        }]);
                    }]);
                }]);
            }])->first();
        }elseif($request->challan_no && $request->request_date ){
            $challan_data =  WarehouseStoreOutChallan::where('challan_no', $request->challan_no )
            ->where('request_date', '=', $request->request_date )
            ->where('out_let_id', $_COOKIE['out_let_id'] )->with(
            ['challanProducts' => function($query){
                $query->with(['product','storeOutProductCombinations'=>function($query){
                    $query->with(['combinationAttributeValues' => function($query){
                        $query->with([ 'attributeValue' => function($query){
                            $query->with('attribute');
                        }]);
                    }]);
                }]);
            }])->first();            
        }
        if($challan_data){
            $product_challan_infos = (object) array("challan_id" => $challan_data->id, "challan_no" => $challan_data->challan_no, "request_date" => $challan_data->request_date, "out_let_id" => $challan_data->out_let_id, "warehouse_id" => $challan_data->warehouse_id, "status" => $challan_data->status );
            foreach ( $challan_data->challanProducts as $challan_product ){
                $combination_data = Product::where('id', $challan_product->product_id )->with(
                    ['combinations' => function($query){
                        $query->with([
                            'combinationAttributeValues' => function($query){
                                $query->with(['attributeValue' => function($query){
                                    $query->with('attribute');
                                }]);
                            }
                        ]);
                    }
                    ]
                )->orderBy('id', 'ASC')->first();
                if( $challan_product->id && sizeof( $combination_data->combinations ) <= 0 ) {
                    $product_combination = WarehouseStoreOutChallanProduct::with('product_combination')->where('id', '=', $challan_product->id )->orderBy('id', 'ASC')->first();
                    foreach ( $product_combination->product_combination as $combination ){
                        if( $combination->product_combination_id == null ){
                            $product_combination_warehouse_store_in_challan_product_obj = DB::table('product_combination_warehouse_store_in_challan_product')->where('id', $combination->product_combination_warehouse_store_in_challan_product_id)->first();
                            $stock_remain = 0;
                            if($product_combination_warehouse_store_in_challan_product_obj){
                                $stock_remain = $product_combination_warehouse_store_in_challan_product_obj->stock_quantity;
                            }
                            $product_combination_warehouse_store_out_challan_product_obj = DB::table('product_combination_warehouse_store_out_challan_product')->where('product_combination_warehouse_store_in_challan_product_id', $combination->product_combination_warehouse_store_in_challan_product_id)->first();

                            $stock_quantity = "";                        
                            if( $product_combination_warehouse_store_out_challan_product_obj->received_quantity ){
                                $stock_quantity = $stock_remain + $product_combination_warehouse_store_out_challan_product_obj->received_quantity;   
                            }elseif( $product_combination_warehouse_store_out_challan_product_obj->requested_quantity ){
                                $stock_quantity = $stock_remain + $product_combination_warehouse_store_out_challan_product_obj->requested_quantity; 
                            }
                            $combination_data->combination = "No";
                            $combination_data->product_id = $combination_data->id;
                            $combination_data->name = $combination_data->name;
                            $combination_data->challan_no = $challan_data->challan_no;
                            $combination_data->warehouse_store_in_challan_product_id = null;
                            if($product_combination_warehouse_store_in_challan_product_obj){
                                $combination_data->warehouse_store_in_challan_product_id = $product_combination_warehouse_store_in_challan_product_obj->warehouse_store_in_challan_product_id;
                            }
                            $combination_data->warehouse_store_out_challan_product_id = $product_combination->product_combination[0]->warehouse_store_out_challan_product_id;
                            $combinations_obj = (object) array("id" => null,"stock_remain" => $stock_remain, "requested_quantity" => $product_combination->product_combination[0]->requested_quantity,"received_quantity" => $product_combination->product_combination[0]->received_quantity,"stock_quantity" => $stock_quantity,"missing_quantity" => null,"missing_status" => $product_combination->product_combination[0]->missing_status, "product_combination_warehouse_store_in_challan_product_id" => $combination->product_combination_warehouse_store_in_challan_product_id,"product_id" => $product_combination->product_id);
                            $combination_data->combinations[] = $combinations_obj;
                        }
                    }
                }else{
                    $combination_data->product_id = $challan_product->product_id;
                    $combination_data->challan_no = $challan_data->challan_no;
                    $combination_data->combination = "Yes";
                }
                $product_challan_infos->product_combination_list[] = $combination_data;
            }
            $index_product_combination_list = 0;            
            if( isset($product_challan_infos->product_combination_list) ){
                foreach( $product_challan_infos->product_combination_list as $combination ){
                    $index_combinations = 0;
                    if(sizeof($combination->combinations) > 0 ){
                        $index_combination_obj = 0;
                        foreach($combination->combinations as $combination_obj){       
                            $index_combination_obj = $index_combination_obj + 1;
                            $index_challan_product = 0;
                            foreach( $challan_data->challanProducts as $challan_product ) {
                                $index_challan_product = $index_challan_product + 1;
                                if ( $challan_product->storeOutProductCombinations ) {
                                    $index_store_in_product_combination = 0;
                                    foreach($challan_product->storeOutProductCombinations as $store_out_product_combination ){        
                                        if( $store_out_product_combination->pivot->product_combination_id == $combination_obj->id ){
                                            $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->product_combination_warehouse_store_in_challan_product_id = $store_out_product_combination->pivot->product_combination_warehouse_store_in_challan_product_id;
                                            
                                            $product_combination_warehouse_store_in_challan_product_obj = DB::table('product_combination_warehouse_store_in_challan_product')->where('id', $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->product_combination_warehouse_store_in_challan_product_id)->first();
                                            $product_challan_infos->product_combination_list[$index_product_combination_list]->warehouse_store_in_challan_product_id = null; 
                                            if($product_combination_warehouse_store_in_challan_product_obj){
                                                $product_challan_infos->product_combination_list[$index_product_combination_list]->warehouse_store_in_challan_product_id = $product_combination_warehouse_store_in_challan_product_obj->warehouse_store_in_challan_product_id;
                                                $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->stock_remain = $product_combination_warehouse_store_in_challan_product_obj->stock_quantity;
                                             }
                                            $product_challan_infos->product_combination_list[$index_product_combination_list]->warehouse_store_out_challan_product_id = $challan_product->id;
                                            $product_challan_infos->product_combination_list[$index_product_combination_list]->warehouse_store_out_challan_product_id = $challan_product->id;
                                            $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->received_quantity = $store_out_product_combination->pivot->received_quantity;
                                            $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->requested_quantity = $store_out_product_combination->pivot->requested_quantity;                                                                  
                                            $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->missing_status = $store_out_product_combination->pivot->missing_status;

                                            if( $store_out_product_combination->pivot->received_quantity ){
                                                $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->stock_quantity = $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->stock_remain + $store_out_product_combination->pivot->received_quantity;   
                                            }elseif( $store_out_product_combination->pivot->requested_quantity ){
                                                $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->stock_quantity = $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->stock_remain + $store_out_product_combination->pivot->requested_quantity;   
                                            }                                    
                                        }
                                    }
                                }else{

                                }
                                $index_challan_product = $index_challan_product + 1;
                            }
                            $index_combinations = $index_combinations + 1;
                        }
                    }
                    $index_product_combination_list = $index_product_combination_list + 1;
                }
            }
                
        }
        return Response::json($product_challan_infos);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeWarehouseStoreOutChallan( Request $request )
    {
        $return_array['success'] = "";
        $message = "";

        if(!Auth::user()->can('access-warehouse-store-out-challan')){
            Redis::set('error', 'Error: Operation not permitted!!!');
            return $return_array;
        }
        $store_out_challan_product_quantity = DB::table('product_combination_warehouse_store_out_challan_product')
            ->join('warehouse_store_out_challan_products','product_combination_warehouse_store_out_challan_product.warehouse_store_out_challan_product_id' ,'=' ,'warehouse_store_out_challan_products.id')
            ->whereIn('warehouse_store_out_challan_products.warehouse_store_out_challan_id', $request->item_ids)
            ->get();

        $query_result = DB::table('product_combination_warehouse_store_out_challan_product')
            ->join('warehouse_store_out_challan_products','product_combination_warehouse_store_out_challan_product.warehouse_store_out_challan_product_id' ,'=' ,'warehouse_store_out_challan_products.id')
            ->whereIn('warehouse_store_out_challan_products.warehouse_store_out_challan_id', $request->item_ids)
            ->update(['product_combination_warehouse_store_out_challan_product.deleted_at' => DB::raw('NOW()')]);
        DB::table('warehouse_store_out_challan_products')->whereIn('warehouse_store_out_challan_id', $request->item_ids )->update(['deleted_at' => DB::raw('NOW()')]);
        $warehouse_store_in_challan_deleted = DB::table('warehouse_store_out_challans')->whereIn('id', $request->item_ids )->update(['deleted_at' => DB::raw('NOW()')]);

        if( $warehouse_store_in_challan_deleted ){
            if($store_out_challan_product_quantity){
                foreach($store_out_challan_product_quantity as $each_item){
                    DB::table('product_combination_warehouse_store_in_challan_product')->where('id', $each_item->product_combination_warehouse_store_in_challan_product_id)->update(array('stock_quantity' => DB::raw('stock_quantity + ' .$each_item->requested_quantity)));
                }
            }
            $message = "Warehouse StoreOut Challan Deleted Successfully";
        }else{
            $message = "Error: Warehouse StoreOut Challan Not Deleted, Please Try again";
        }
        return response([
            'message'=> $message
        ]);
    }
}
