<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Validation\Rule;
use App\Product;
use App\ProductImage;
use Psy\Util\Json;
use Validator;
use Image;
use File;

class ProductImageApiController extends Controller
{    
	protected function saveProductCoverImage(  Request $request )    
    {
    	$return_array['success'] = "";  
    	$message = "";  	
		if( $request ){
	        /*
	         * Member data to insert
	         */	                  
			$dir_path = 'uploads/company/'.Auth::user()->company_id.'/product_images/'.$request->product_id;
			$dir_path_resize = $dir_path.'/150x150';

			if (!File::exists($dir_path))
			{
				File::makeDirectory($dir_path, 0777, true);
			}
			if (!File::exists($dir_path_resize))
			{
				File::makeDirectory($dir_path_resize, 0777, true);
			}

			$product_cover_image = ProductImage::where('product_id', $request->product_id)->where('is_cover_image','=', '1')->get();

			if(sizeof($product_cover_image) && ( $request->remove_cover_photo !=null )){
				if( unlink(public_path($dir_path.'/'.$product_cover_image[0]->name)) ){
					unlink(public_path($dir_path_resize.'/'.$product_cover_image[0]->name));
					$product_cover_image_deleted = ProductImage::find($product_cover_image[0]->id)->forceDelete();
				}                
			}
			
	        if ( !is_string($request->cover_file) ) {	
	        	$file = $request->cover_file;	
	        	$extension = $file->getClientOriginalExtension(); // getting image 	        	              
                $product_photo['name'] = 'file_'.\Carbon\Carbon::now().'.'.$extension; // renaming image
                $file->move($dir_path, $product_photo['name']); // uploading file to given path

                // Start : Image Resize
                $image = Image::make(public_path($dir_path.'/'.$product_photo['name']));
                // resize the image to a height of 45 and constrain aspect ratio (auto width)
                $image->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image->save(public_path($dir_path_resize.'/'.$product_photo['name']));
                // End : Image Resize
                $product = Product::find($request->product_id);                
                $product_photo['is_cover_image'] = '1';
                $product->images()->create($product_photo);	
                $return_array['success'] = "Success...";	        
	        }
	        $message = 'Cover Photo Updated Successfully';	          	        
	    	//Redis::set('success', 'Cover Photo Updated Successfully');
	    }else{
          $message = 'Cover Photo Updated Successfully';
        } 
        return response([
	            'message'=> $message
        ]); 
	}
	protected function getProductCoverImage(Request $request){
		$product_cover_object = [];
		$product_cover_image = ProductImage::where('product_id', $request->product_id)->where('is_cover_image','=', '1')->orderby('id','DESC')->get();		
		return $product_cover_image; 
	}

	protected function saveProductImageGallery(  Request $request )    
    {
		$return_array['success'] = "";
		$val = "";	 
		$message = "";
		if( $request ){
	        /*
	         * Member data to insert
	         */
	        $return_array['success'] = '';
	        $product_gallery = json_decode($request->get('product_gallery'), True);	 
	        $remove_gallery_photos = json_decode($request->get('remove_gallery_photos'), True);      
	        $product_id = $product_gallery['product_id']; 	        
			$dir_path = 'uploads/company/'.Auth::user()->company_id.'/product_images/'.$product_id;
			$dir_path_resize = $dir_path.'/150x150';

			$return_array['success'] = $remove_gallery_photos;
			//return $return_array; 
			if (!File::exists($dir_path))
			{
				File::makeDirectory($dir_path, 0777, true);
			}
			if (!File::exists($dir_path_resize))
			{
				File::makeDirectory($dir_path_resize, 0777, true);
			}
			if(sizeof($remove_gallery_photos)){				
				foreach ($remove_gallery_photos as $key => $photo_id) {
					$val = $val."__".$photo_id;
					$product_image = ProductImage::where('id', '=', $photo_id)->get();
					if( unlink(public_path($dir_path.'/'.$product_image[0]->name)) ){
						unlink(public_path($dir_path_resize.'/'.$product_image[0]->name));
						$product_image_deleted = ProductImage::where('id', '=', $product_image[0]->id)->forceDelete();
					}
				}
			}
	        if ( $request->pics ) {
	        	$index = 1;
	        	foreach ( $request->pics as $file )
            	{	        	
		        	$extension = $file->getClientOriginalExtension(); // getting image extension                
	                $product_photo['name'] = 'file_'.$index.'_'.\Carbon\Carbon::now().'.'.$extension; // renaming image
	                $file->move($dir_path, $product_photo['name']); // uploading file to given path

	                // Start : Image Resize
	                $image = Image::make(public_path($dir_path.'/'.$product_photo['name']));
	                // resize the image to a height of 45 and constrain aspect ratio (auto width)
	                $image->resize(null, 150, function ($constraint) {
	                    $constraint->aspectRatio();
	                });
	                $image->save(public_path($dir_path_resize.'/'.$product_photo['name']));
	                // End : Image Resize
	                $product = Product::find($product_id);
	                $product->images()->create($product_photo);
	                $index = $index + 1;
		        }
            }
            $message = 'Gallery Updated Successfully';
    		//Redis::set('success', 'Gallery Updated Successfully');
	    }else{
	    	$message = 'Error: Gallery Not Updated, Please Try again';
          //Redis::set('error', 'Error: Gallery Not Updated, Please Try again');
        } 
        //$return_array['success'] = $val; 
	    return response([
	            'message'=> $message
        ]);
	}
	protected function getProductImageGallery(Request $request){
		$product_gallery_object = [];
		$product_gallery = ProductImage::where('product_id', $request->product_id)->where('is_cover_image','=', '0')->orderby('id','DESC')->get();
		/*$product_name = Product::find($request->product_id)->name;
		if($product_gallery){
			$dir_path = '/uploads/company/'.Auth::user()->company_id.'/out_lets/'.Redis::get('out_let_id').'/product_images/'.$request->product_id;
			foreach($product_gallery as $key => $product_gallery_photo) {    
				$photo_obj = (object) array("src" => $dir_path."/".$product_gallery_photo->name, "caption" => $product_name
                        );
				$product_gallery_object[$key] = $photo_obj; 
			}  
		}*/
		return $product_gallery; 
	}
	
}
