<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Laravel\Passport\Client;
//use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Controller;

class LoginApiController extends Controller
{
    private $client;

    public function __construct()
    {
//        $this->client = Client::find(1);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function checkLoginAuthentication( Request $request )
    {

        return response()->json([
          'success' => true,
          'token' => 'Thisistoken',
          'user' => 'users'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('appToken')->accessToken;
           //After successfull authentication, notice how I return json parameters
            return response()->json([
              'success' => true,
              'token' => $success,
              'user' => $user
            ]);

            /*$return_array['success'] = 'success';
            $return_array['data'] = $this->userOutlets();
            return $return_array;*/
        }else{
            //$return_array['error'] = 'failed';
            //return $return_array;
        }

//        user : logiccoder for grant type password
//        $params = [
//            'grant_type' => 'password',
//            'client_id' => 3,
//            'client_secret' => 'ZwhJLk8zxgdaQVjriOig16nEgcjsOnh5juaHjans',
//            'username' => $request->email,
//            'passward' => $request->password,
//            'scope' => '*',
//        ];
//        $request->request->add($params);
//        $proxy = Request::create('oauth/token', 'post');
//        return Route::dispatch($proxy);
    }

    /**
     * Show the form for creating a new resource.s
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
