<?php

namespace App\Http\Controllers;

use App\UserTitle;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Requests\UserTitleRequest;
use Illuminate\Support\Facades\Auth;

class UserTitleController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('access-position-title')){
            abort(401);
        }
        $user_titles = UserTitle::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('user_title.index', compact('user_titles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('access-position-title')){
            abort(401);
        }
        return view('user_title.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserTitleRequest $request)
    {
        if(!Auth::user()->can('access-position-title')){
            abort(401);
        }
        $companies = Company::find(Auth::user()->company_id);
        $companies->userTitles()->create($request->all());
        return redirect('user_title')->with('success','Title / Possition added successfully!');    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('access-position-title')){
            abort(401);
        }
        $user_titles = UserTitle::find($id);
        return view('user_title.edit', compact('user_titles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserTitleRequest $request, $id)
    {
        if(!Auth::user()->can('access-position-title')){
            abort(401);
        }
        $user_titles = UserTitle::find($id);
        $user_titles->update($request->all());
        return redirect('user_title')->with('success','Title / Possition updated successfully!');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      
    }
}
