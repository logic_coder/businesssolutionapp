<?php
namespace App\Http\Controllers;
use App\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WarehouseApiController extends Controller
{
    protected function getWarehouses()
    {
        return Warehouse::all();
    }
    protected function getWarehouseData( Request $request )
    {
       return Warehouse::where('id', $request->warehouse_id)->first();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeWarehouse( Request $request )
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('access-warehouse')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids)){
            Warehouse::destroy( $request->item_ids );
            return response([
                'message' => 'Warehouse deleted successfully!'
            ],200);
        }
    }
}
