<?php
namespace App\Http\Controllers;
use App\BusinessCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class BusinessCategoryApiController extends Controller
{
    /**
     * Display a listing of the business category.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategories()
    {
        $categories = BusinessCategory::get();
        return $categories;
    }
}
