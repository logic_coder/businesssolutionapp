<?php

namespace App\Http\Controllers;
use App\Company;
use App\CustomerOrderDetail;
use App\CustomerOrderDetailItem;
use App\CustomerOrderDetailPaid;
use App\CustomerOrderDetailPaidDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psy\Util\Json;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Response;

class CustomerOrderDetailPaidApiController extends Controller
{
    /**
     * Get total paid amount for a specific order based on customer order detail id
     *
     * @param  \Illuminate\Http\Request  $request which contains customer_order_detail_id
     * @return total paid amount
     */
    public function getCustomerOrderDetailPaidsData(Request $request)
    {
        $paid_amount = CustomerOrderDetailPaid::where('customer_order_detail_id', $request->customer_order_detail_id)->sum('paid_amount');
        return number_format($paid_amount, 2, '.','');
    }
    /**
     * Get paid history for a specific order based on customer order detail id
     *
     * @param  \Illuminate\Http\Request  $request which contains customer_order_detail_id
     * @return paid history
     */
    public function getCustomerOrderDetailPaidHistory(Request $request)
    {
        //return CustomerOrderDetailPaid::where('customer_order_detail_id', $request->customer_order_detail_id)->with('CustomerOrderDetailPaidDetails','paymentMethod')->get();
        $salePaidHistory = [];
        if ($request->customer_order_detail_id) {
            $salePaidHistory = CustomerOrderDetailPaid::where('customer_order_detail_id', $request->customer_order_detail_id)->with('CustomerOrderDetailPaidDetails','paymentMethod')->orderBy('id', 'DESC')->get();
        }        
        if(sizeof($salePaidHistory) <= 0){
            /* For Cash Payment */
            $sale_paid_history_obj = (object) array("id" => null, 
            );
            $payment_method_obj = (object) array("id" => $request->cash_payment_method_id, "name" => 'Cash'
            );
            $sale_paid_details_obj = (object) array("id" => null, "meta_data" => 'Payment By',"value" => 'Self'
            );
            $sale_paid_history_obj->payment_method = $payment_method_obj; 
            $sale_paid_history_obj->sale_paid_details = array($sale_paid_details_obj); 
            $salePaidHistory[0] = $sale_paid_history_obj;

            /* For Digital Payment */
            $sale_paid_history_obj = (object) array("id" => null,
            );
            $payment_method_obj = (object) array("name" => 'Select Payment Method'
            );
            $sale_paid_details_obj = (object) array("id" => null, "meta_data" => '',"value" => ''
            );
            $sale_paid_history_obj->payment_method = $payment_method_obj; 
            $sale_paid_history_obj->sale_paid_details[0] = $sale_paid_details_obj; 
            $sale_paid_history_obj->sale_paid_details[1] = $sale_paid_details_obj;
            $salePaidHistory[1] = $sale_paid_history_obj; 

        }else if(sizeof($salePaidHistory) == 1 ){         
                foreach($salePaidHistory as $key => $sale_paid) {            
                    if($sale_paid->paymentMethod->name != "Cash"){
                        /* For Cash Payment */
                        $sale_paid_history_obj = (object) array("id" => null,
                        );
                        $payment_method_obj = (object) array("id" => $request->cash_payment_method_id, "name" => 'Cash'
                        );
                        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => 'Payment By',"value" => 'Self'
                        );
                        $sale_paid_history_obj->payment_method = $payment_method_obj; 
                        $sale_paid_history_obj->sale_paid_details = array($sale_paid_details_obj); 
                        $salePaidHistory[$key+1] = $sale_paid_history_obj;
                        
                    }else{
                        /* For Digital Payment */
                        $sale_paid_history_obj = (object) array("id" => null, 
                        );
                        $payment_method_obj = (object) array("name" => 'Select Payment Method'
                        );
                        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => '',"value" => ''
                        );
                        $sale_paid_history_obj->payment_method = $payment_method_obj; 
                        $sale_paid_history_obj->sale_paid_details[0] = $sale_paid_details_obj; 
                        $sale_paid_history_obj->sale_paid_details[1] = $sale_paid_details_obj;
                        $salePaidHistory[$key+1] = $sale_paid_history_obj;
                    }
                }            
            }
            return Response::json($salePaidHistory);
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeCustomerOrderDetailPaid( Request $request )
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('delete-sales')){
                return response([
                    'message' => 'Error: Operation not permitted!'
                ],401);
            }
        }
        elseif(!empty( $request->paid_id)){
            $customer_order_detail_paid_deleted = CustomerOrderDetailPaid::destroy( $request->paid_id );            
            if( $customer_order_detail_paid_deleted ){                
                return response([
                    'message' => 'Customer Order Paid Deleted Successfully'
                ],200);
            }else{
                return response([
                    'message' => 'Error: Customer Order Paid Not Deleted, Please Try again'
                ],500);                
            }
        }
    }
}
