<?php
namespace App\Http\Controllers;
use App\Vat;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Requests\VatRequest;
use Illuminate\Support\Facades\Auth;

class VatController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('access-global-vat-settings')){
            abort(401);
        }
        $vats = Vat::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('vat.index', compact('vats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('access-global-vat-settings')){
            abort(401);
        }
        return view('vat.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VatRequest $request)
    {
        if(!Auth::user()->can('access-global-vat-settings')){
            abort(401);
        }       
        $companies = Company::find(Auth::user()->company_id);
        $companies->vats()->create($request->all());        
        return back()->with('success','VAT added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('access-global-vat-settings')){
            abort(401);
        }
        $vats = Vat::find($id);
        return view('vat.edit', compact('vats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VatRequest $request, $id)
    {
        if(!Auth::user()->can('access-global-vat-settings')){
            abort(401);
        }
        $vats = Vat::find($id);
        $vats->update($request->all());
        return redirect('vat')->with('success','VAT updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        
    }
}
