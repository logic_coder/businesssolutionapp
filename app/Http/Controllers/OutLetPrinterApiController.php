<?php
namespace App\Http\Controllers;
use App\Company; 
use App\OutLet;
use App\Printer;
use App\OutLetPrinter;  
use App\CompanyPrinter;
use Illuminate\Http\Request;
use App\Http\Requests\OutLetPrinterRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class OutLetPrinterApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $outlet_printers = OutLet::where('id',$request->out_let_id)->with('printers')->first();
        if($outlet_printers){
            if(sizeof($outlet_printers->printers) <= 0 ){
                $outlet_printers = OutLet::where('id',$request->out_let_id)->first();
                $printers = CompanyPrinter::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
                $outlet_printers->{'printers'} = $printers;      
            }
        }       
        return $outlet_printers; 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
