<?php

namespace App\Http\Controllers;

use App\Company;
use App\Attribute;
use App\AttributeValue;
use App\Http\Requests\AttributeValueRequest;
use Illuminate\Http\Request;
use App\Http\Requests\AttributeValueRequestRequest;
use Illuminate\Support\Facades\Auth;

class AttributeValueController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     */
    public function index(Attribute $attribute)
    {
        if(!Auth::user()->can('access-product-attributes')) {
            abort(401);
        }
        return view('attribute_value.index', compact('attribute'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Attribute $attribute)
    {
        if(!Auth::user()->can('access-product-attributes')) {
            abort(401);
        }
        $attribute_lists = Attribute::where('company_id', Auth::user()->company_id)->orderBy('name', 'ASC')->pluck('name','id');
        $selected_color = '#000000';
        $attribute_value = '';
        return view('attribute_value.create', compact('attribute_lists','attribute','attribute_value','selected_color'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AttributeValueRequest|Request $request
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeValueRequest $request)
    {
        if(!Auth::user()->can('access-product-attributes')) {
            abort(401);
        }
        $attribute = Attribute::findorfail( $request->attribute_id );
        $attribute->attributeValues()->create($request->all());
        return redirect()->back()->with('success','Attribute value added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Attribute $attribute, AttributeValue $attribute_value)
    {
        if(!Auth::user()->can('access-product-attributes')){
            abort(401);
        }
        $attribute_lists = Attribute::where('company_id', Auth::user()->company_id)->orderBy('name', 'ASC')->pluck('name','id');
        $selected_color = $attribute_value->color ? $attribute_value->color : '#000000';
        return view('attribute_value.edit', compact('attribute_lists','attribute','attribute_value','selected_color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AttributeValueRequest $request, Attribute $attribute, AttributeValue $attribute_value)
    {
        if(!Auth::user()->can('access-product-attributes')){
            abort(401);
        }
        $attribute_value->update( $request->all() );
        return redirect('attribute/'.$request->attribute_id.'/attribute_value')->with('success','Attribute value updated successfully!');
    }
    public function getAttributeDetails( Attribute $attribute )
    {
        return array( $attribute, '');
    }
    public function getAttributeValueDetails( Attribute $attribute, AttributeValue $attribute_value )
    {
        return array( $attribute, $attribute_value);
    }

}
