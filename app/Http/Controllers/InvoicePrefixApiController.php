<?php

namespace App\Http\Controllers;
use App\InvoicePrefix;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoicePrefixApiController extends Controller
{
    public function getInvoicePrefixLists()
    {
        return InvoicePrefix::where('company_id', Auth::user()->company_id)->orderBy('id', 'DESC')->get();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeInvoicePrefix( Request $request )
    {
        $return_array['message'] = '';

        if(!Auth::user()->can('access-invoice-prefix-settings')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids )){
            InvoicePrefix::destroy( $request->item_ids );
            return response([
                'message' => 'Invoice prefix deleted successfully!!!'
            ],200);
        }
        return $return_array;
    }
    /**
     * Get Invoice Prefix Info by invoice_prefix_id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function getInvoicePrefixData( Request $request )
    {
        $invoice_prefix_data = InvoicePrefix::find( $request->invoice_prefix_id);
        return $invoice_prefix_data;
    }
}
