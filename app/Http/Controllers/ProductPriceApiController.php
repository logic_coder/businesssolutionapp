<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;
use App\ProductPrice;
use Illuminate\Support\Facades\Auth;

class ProductPriceApiController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeProductPrice( Request $request )
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('access-product-prices')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids)){
            ProductPrice::destroy( $request->item_ids );
            return response([
                'message' => 'Product Price deleted successfully!'
            ],200);
        }
        return $return_array;
    }
}
