<?php
namespace App\Http\Controllers;
use App\Company;
use App\Customer;
use App\InvoicePrefix;
use App\Vat;
use App\Gender;
use App\OutLet;
use App\Category;
use App\CustomerOrderStatus;
use App\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class BasicResourcesApiController extends Controller
{
    
    public function Test(Request $request){
         
        $credentials = $request->only('email', 'password');

        //valid credential
        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        // Request is validated
        // Creat token
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                    'success' => false,
                    'message' => 'Could not create token.',
                ], 500);
        }
    
        //Token created, return with success response and jwt token
        return response()->json([
            'success' => true,
            'access_token' => $token,
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::where('company_id', Auth::user()->company_id)->orderBy('id', 'DESC')->get();
        $invoice_prefixes = InvoicePrefix::where('company_id', Auth::user()->company_id)->orderBy('id', 'DESC')->get();
        $vats = Vat::where('company_id', Auth::user()->company_id)->orderby('id','DESC')->get();
        $categories = Category::where('company_id', Auth::user()->company_id)
            ->where('active', 1)->orderby('id','DESC')->get();
        $customer_order_status = CustomerOrderStatus::where('company_id', Auth::user()->company_id)->orderby('id','DESC')->get();
        $currencies = Currency::where('company_id', Auth::user()->company_id)
            ->Join('company_currencies', 'company_currencies.currency_id', '=', 'currencies.id')
            ->where('company_currencies.deleted_at', "=", null)->orderby('company_currencies.id','DESC')->get();

        return response()->json([
            'status' => 'SUCCESS',
            'customers' => $customers,
            'invoice_prefixes' => $invoice_prefixes,
            'vats' => $vats,
            'categories' => $categories,
            'customer_order_status' => $customer_order_status,
            'currencies' => $currencies,
        ]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
