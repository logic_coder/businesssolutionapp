<?php
namespace App\Http\Controllers;
use App\Company;
use App\OutLet;
use App\OutLetType;
use Illuminate\Http\Request;
use App\Http\Requests\OutLetRequest;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
//use Input;
use Image;
use File;


class OutLetController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('create-view-outlets')) {
            abort(401);
        }
        $outlets = OutLet::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('out_let.index', compact('outlets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('create-view-outlets')) {
            abort(401);
        }
        $outlet_types = OutLetType::orderBy('name', 'ASC')->pluck('name','id');
        return view('out_let.create',compact('outlet_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OutLetRequest $request)
    {
        if(!Auth::user()->can('create-view-outlets')) {
            abort(401);
        }
        $outlet_data = $request->all();        
        $start_date = Carbon::parse($request->start_date);
        $outlet_data['end_date'] = $start_date->addMonths($request->duration)->toDateString();
        $outlet_data['trade_license_photo'] = '';
        $dir_path = 'uploads/out_lets/trade_license';

        if (!File::exists($dir_path))
        {
            File::makeDirectory($dir_path, 0775, true);
        }
        if ($request->trade_license_photo) {
            $file = $request->trade_license_photo;
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $outlet_data['trade_license_photo'] = 'license_'.\Carbon\Carbon::now().'.'.$extension; // renaming image
            $file->move($dir_path, $outlet_data['trade_license_photo']); // uploading file to given path
        }
        Company::find(Auth::user()->company_id)->outlets()->create($outlet_data);
        return redirect('out_let')->with('success','Outlet added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('create-view-outlets')){
            abort(401);
        }
        $outlets = OutLet::find($id);
        $outlet_types = OutLetType::orderBy('name', 'ASC')->pluck('name','id');
        return view('out_let.edit', compact('outlets','outlet_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OutLetRequest $request, $id)
    {
        if(!Auth::user()->can('create-view-outlets')){
            abort(401);
        }
        $outlet = OutLet::find($id);
        $outlet_data = $request->all();
        $start_date = Carbon::parse($request->start_date);
        $outlet_data['end_date'] = $start_date->addMonths($request->duration)->toDateString();
        $outlet_data['trade_license_photo'] = $outlet->trade_license_photo;

        if ($request->trade_license_photo) {
            $file = $request->trade_license_photo;
            $dir_path = 'uploads/out_lets/trade_license';
            if($outlet_data['trade_license_photo']){
                unlink(public_path($dir_path.'/'.$outlet_data['trade_license_photo']));
            }            
            $extension =  $file->getClientOriginalExtension(); // getting image extension
            $outlet_data['trade_license_photo'] = 'license_'.\Carbon\Carbon::now().'.'.$extension; // renaming image
            $file->move($dir_path, $outlet_data['trade_license_photo']); // uploading file to given path
        }
        $outlet->update( $outlet_data );
        return redirect('out_let')->with('success','Outlet updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        
    }
}
