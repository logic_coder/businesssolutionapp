<?php
namespace App\Http\Controllers;
use App\Company;
use App\Http\Requests\WarehouseStoreInChallanRequest;
use App\Warehouse;
use App\Supplier;
use App\Category;
use App\Product;
use App\WarehouseStoreInChallan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WarehouseStoreInChallanController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('access-warehouse-store-in-challan')){
            abort(401);
        }
        $store_in_challans = WarehouseStoreInChallan::with('challanProducts')->where('company_id', Auth::user()->company_id)->orderBy('id', 'DESC')->get();
//        dd( $store_in_challans );
        return view('warehouse_store_in_challan.index', compact('store_in_challans'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('access-warehouse-store-in-challan')) {
            abort(401);
        }
        return view('warehouse_store_in_challan.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WarehouseStoreInChallanRequest $request)
    {
        if(!Auth::user()->can('access-warehouse-store-in-challan')) {
            abort(401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit( WarehouseStoreInChallan $challan )
    {
        if(!Auth::user()->can('access-warehouse-store-in-challan')){
            abort(401);
        }
        return view('warehouse_store_in_challan.edit', compact('challan'));
    }
    /**
     * Show the challan form for the specified resource.
     *
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function receiveChallan( WarehouseStoreInChallan $challan )
    {
        if(!Auth::user()->can('access-warehouse-store-in-challan')){
            abort(401);
        }
        return view('warehouse_store_in_challan.receive', compact('challan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AttributeRequest|Request $request
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update()
    {
        if(!Auth::user()->can('access-warehouse-store-in-challan')){
            abort(401);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( )
    {
        if(!Auth::user()->can('access-warehouse-store-in-challan')){
            abort(401);
        }
    }
}
