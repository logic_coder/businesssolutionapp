<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OutLetStoreInChallanController extends Controller
{
   public function __construct(){
        //$this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {            
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-outlet-store-in-challan')){
                abort(401);
            }            
        } 
        $challan_id = '';
        return view('frontend/out_let_store_in_challan.receive', compact('challan_id'));
    }   

    protected function receiveChallan($challan_id){
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-outlet-store-in-challan')){
                abort(401);
            }  
        }    
        return view('frontend/out_let_store_in_challan.receive', compact('challan_id'));
    } 
}
