<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Company;
use App\Vat;
use App\PaymentMethod;
use App\InvoicePrefix;
use App\User;
use App\UserTitle;
use App\UserDetail;
use App\Module;
use App\Permission;
use App\Role;
use App\UserRole;
use App\Category;
use App\Gender;
use App\CustomerOrderStatus;
use Illuminate\Support\Facades\Auth;

class SignupController extends Controller
{
    /**
     * Signup a Company to start their business, it's starting point of a Business
     *
     * @return \Illuminate\Http\Response
     */
    protected function doCompanySignup(UserRequest $request){  
        /**
         * Company Info
         */
        $company_data['country_id'] = $request->country_id;
        $company_data['business_category_id'] = $request->category_id;
        $company_data['name'] = $request->business_name; 
        $company_data['active'] = "1";        
        $company = Company::create($company_data);

        /**
         * Vat
         */
        $vatObj = new Vat();
        $vatObj->company_id = $company->id;
        $vatObj->name = 'General';
        $vatObj->percent = '12';
        $vatObj->save();

        /**
         * Payment Method
         */
        $paymentMethodObj = new PaymentMethod();
        $paymentMethodObj->company_id = $company->id;
        $paymentMethodObj->name = 'Cash';
        $paymentMethodObj->save();

        /**
         * Invoice Prefix
         */
        $invoicePrefixObj = new InvoicePrefix();
        $invoicePrefixObj->company_id = $company->id;
        $invoicePrefixObj->name = 'Gen';
        $invoicePrefixObj->save();

        /**
         * User
         */
        $user_data['email'] = $request->email;
        $user_data['is_pos_admin'] = "1";
        $user_data['password'] = bcrypt( $request->password );
        $user_data['active'] = "1"; 
        $companies = Company::find($company->id);
        $user = $companies->users()->create( $user_data );

        /**
         * User Title
         */
        $userTitleObj = new UserTitle();
        $userTitleObj->company_id = $company->id;
        $userTitleObj->title = 'Administrator';
        $userTitleObj->save();

        /**
         * Role
         */
        $role = new Role();
        $role->company_id = $company->id;
        $role->name = 'Company Administrator( Read Only )';
        $role->slug = 'administrator';
        $role->save();
        $role->users()->attach($user->id);

        $permisions = Permission::all();
        foreach ($permisions as $permision) {
            $role->permissions()->attach($permision);
        }

        /**
         * Category
         */
        $categoryObj = new Category();
        $categoryObj->company_id = $company->id;
        $categoryObj->parent_id = '0';
        $categoryObj->name = 'Home';
        $categoryObj->is_shop_default = '1';
        $categoryObj->level_depth = '0';
        $categoryObj->nleft = '0';
        $categoryObj->nright = '0';
        $categoryObj->active = '1';
        $categoryObj->position = '1';
        $categoryObj->is_root_category = '1';
        $categoryObj->save();

        /**
         * CustomerOrderStatus
         */
        if($request->category_name=="Jewellers"){
            $customer_order_status_list = ['Not Assigned' => 'not-assigned','Karigor' => 'karigor', 'Chila Polish' => 'chila-polish', 'Show Room' => 'show-room','Halmark' => 'halmark', 'Delivered' => 'delivered' ];
            foreach( $customer_order_status_list as $name => $slug ){
                $customerOrderStatusObj = new CustomerOrderStatus();
                $customerOrderStatusObj->company_id = $company->id;
                $customerOrderStatusObj->name = $name;
                $customerOrderStatusObj->slug = $slug;
                $customerOrderStatusObj->save();
            }
        } 
        $return_array['company_id'] = "Not LogedIn";
        if (Auth::attempt(['email' => $user_data['email'], 'password' => $request->password, 'active' => 1])) {
            return response([
                'message' => 'Company profile created successfully!'
            ],200);
            //$return_array['company_id'] = $company->id; 
        }
        //return $return_array;
    }
    public function doCompanySignIn(Request $request){
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'active' => 1])) {
            return response([
                'message' => 'LoggedIn successfully!'
            ],200);
        }else{
            return response([
                'message' => 'Login credentials are invalid, Please try again!'
            ],401);
        }
    }
    /**
     * Signin to System
     *
     * @return \Illuminate\Http\Response
     */
    protected function doCompanySignin__(Request $request){  
        /*$return_array['company_id'] = "Not LogedIn";
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'active' => 1])) {
            Redis::set('success', 'LogedIn successfully');
            $return_array['company_id'] = 'LogedIn successfully'; 
        }
        return $return_array;*/
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
