<?php

namespace App\Http\Controllers;
use App\Category;
use App\Product;
use App\OutLet;
use App\OutLetProduct;
use App\CategoryProduct;
use App\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class ProductApiController extends Controller
{
    /**
     * Get Products For Specific Company
     *
     * @return Products Object
     */
    public function getCompanyProduct()
    {       
        return Product::where('company_id', auth::user()->company_id)->get();
    }
    /**
     * Get Products For Specific Out Let
     *
     * @return Products Object
     */
    public function outLetProducts()
    {        
        /*$out_let_product = OutLet::where('id', Redis::get('out_let_id') )->with(
        ['products' => function($query){
            $query->groupBy('out_let_product.product_id');
            $query->with(['images' => function($query){
                $query->where('product_images.is_cover_image', '=', '1');
            }]);
            $query->with(['prices' => function($query){
                $query->where('product_prices.active', '=', '1');
            }]);
            $query->with(['combinations' => function($query){
                $query->with(['combinationAttributeValues' => function($query){
                    $query->with([ 'attributeValue' => function($query){
                        $query->with('attribute');
                    }]);
                }]);
                $query->select('product_combinations.*', 'out_let_product.*')
                    ->join('out_let_product', function($join) {
                    $join->on('out_let_product.product_combination_id', '=', 'product_combinations.id');                   
                });
            }]);            
        }])->get();*/        

        $out_let_product = OutLet::where('id', $_COOKIE['out_let_id'] )->with(
        ['outLetProducts' => function($query){
            $query->groupBy('out_let_products.product_id');
            $query->with(['product'=>function($query){
                $query->with(['images' => function($query){
                    $query->where('product_images.is_cover_image', '=', '1');
                }]);
                $query->with(['prices' => function($query){
                    $query->where('product_prices.active', '=', '1');
                }]);
            }]);
        }])->get();
        return $out_let_product;        
    }
    /**
     * Get Products For Specific Out Let by name and/or category
     *
     * @param int $category_id
     * @param string $name
     * @return Products Object
     */
    public function searchOutLetProducts(Request $request)
    {        
        /*$out_let_product = OutLet::where('id', Redis::get('out_let_id') )->with(
        ['products' => function($query) use($request){
            $query->groupBy('out_let_product.product_id');
            if($request->name){
                $query->where('name', 'like',  '%' . $request->name . '%')->with('categories')->whereHas('categories', function($query) use($request) {
                    if($request->category_id){
                        $query->where('categories.id', $request->category_id);
                    }
                });
            }elseif($request->name == "" && $request->category_id ){
                $query->with('categories')->whereHas('categories', function($query) use($request) {
                    $query->where('categories.id', $request->category_id);
                });
            }
            $query->with(['images' => function($query){
                $query->where('product_images.is_cover_image', '=', '1');
            }]);
            $query->with(['prices' => function($query){
                $query->where('product_prices.active', '=', '1');
            }]);
            $query->with(['combinations' => function($query){
                $query->with(['combinationAttributeValues' => function($query){
                    $query->with([ 'attributeValue' => function($query){
                        $query->with('attribute');
                    }]);
                }]);
                $query->select('product_combinations.*', 'out_let_product.*')
                    ->join('out_let_product', function($join) {
                    $join->on('out_let_product.product_combination_id', '=', 'product_combinations.id');                   
                });
            }]);            
        }])->get();*/
        //$out_let_product = OutLet::where('id', Redis::get('out_let_id') )->with(
        $out_let_product = OutLet::where('id', $_COOKIE['out_let_id'] )->with(
        ['outLetProducts' => function($query) use($request){
            $query->groupBy('out_let_products.product_id');            
            $query->with(['product'=>function($query) use($request){
                if($request->name){
                    $query->where('products.name', 'like',  '%' . $request->name . '%')->with('categories')->whereHas('categories', function($query) use($request) {
                    if($request->category_id){
                        $query->where('categories.id', $request->category_id);
                    }
                });
                }elseif($request->name == "" && $request->category_id ){
                    $query->with('categories')->whereHas('categories', function($query) use($request) {
                        $query->where('categories.id', $request->category_id);
                    });
                }
                $query->with(['images' => function($query){
                    $query->where('product_images.is_cover_image', '=', '1');
                }]);
                $query->with(['prices' => function($query){
                    $query->where('product_prices.active', '=', '1');
                }]);            
            }]);
        }])->get();
        return $out_let_product;        
    }
    /**
     * Get Specific Product For Specific Out Let
     * @param int $product_id
     * @return Product Object
     */
    public function getOutLetProductSalesData(Request $request)
    {        
        $product_data = "";        
        /*$product_sales_data = OutLet::where('id', Redis::get('out_let_id') )->with(
        ['products' => function($query) use($request){
            $query->where('out_let_product.product_id','=', $request->product_id);
            $query->with(['images' => function($query){
                $query->where('product_images.is_cover_image', '=', '1');
            }]);
            $query->with(['prices' => function($query){
                $query->where('product_prices.active', '=', '1');
                $query->with(['vat' => function($query){                    
                }]);
            }]);
            $query->with(['categories' => function($query){
            }]);
            $query->with(['combinations' => function($query) use($request){                
                $query->with(['combinationAttributeValues' => function($query){
                    $query->with([ 'attributeValue' => function($query){
                        $query->with('attribute');
                    }]);
                }]);
                $query->select('product_combinations.*', 'out_let_product.*')
                    ->join('out_let_product', function($join) use($request){
                    $join->on('out_let_product.product_combination_id', '=', 'product_combinations.id')->where('out_let_product.out_let_id','=', Redis::get('out_let_id'));                   
                });
            }]);            
        }])->first(); */
        $product_sales_data = OutLet::where('id', $_COOKIE['out_let_id'] )->with(
        ['outLetProducts' => function($query) use($request){
            $query->where('out_let_products.product_id','=', $request->product_id);
            /*$query->with(['product'=>function($query) use($request){
                $query->with(['prices' => function($query){
                    $query->where('product_prices.active', '=', '1');
                }]);
            }]);*/
            $query->with(['product'=>function($query){
                $query->with(['prices' => function($query){
                    $query->where('product_prices.active', '=', '1');
                }]);
                $query->with(['categories' => function($query){
                }]);
                $query->with(['images' => function($query){
                    $query->where('product_images.is_cover_image', '=', '1');
                }]);
            }]);
            $query->with(['outLetProductCombinations'=>function($query){                
                $query->with(['combinationAttributeValues' => function($query){
                    $query->with([ 'attributeValue' => function($query){
                        $query->with('attribute');
                    }]);
                }]);
            }]);
        }])->first();        

        if( $product_sales_data && sizeof($product_sales_data->outLetProducts) ){
            $product_sales_data->outLetProducts[0]->product_id = $product_sales_data->outLetProducts[0]->product->id;
            $product_sales_data->outLetProducts[0]->categories = $product_sales_data->outLetProducts[0]->product->categories;
            $product_sales_data->outLetProducts[0]->unit_price = $product_sales_data->outLetProducts[0]->product->prices[0]->price;
                $product_sales_data->outLetProducts[0]->quantity = 1;              
            $product_sales_data->outLetProducts[0]->cost_price = $product_sales_data->outLetProducts[0]->product->prices[0]->cost;
            $product_sales_data->outLetProducts[0]->vat_percent = $product_sales_data->outLetProducts[0]->product->prices[0]->vat->percent;
            if( sizeof($product_sales_data->outLetProducts[0]->outLetProductCombinations) <= 0 ){
                $product_combination = OutLetProduct::with('out_let_product_combination')->where('id', '=', $product_sales_data->outLetProducts[0]->id )->orderBy('id', 'ASC')->first();                               
                $sold_quantity = DB::table('out_let_product_product_combination_sales')
                ->join('sales', 'sales.id', '=', 'out_let_product_product_combination_sales.sale_id')
                ->whereIn('sales.status', ['sold','hold-on'])
                ->where('out_let_product_product_combination_id', '=', $product_combination->out_let_product_combination[0]->id)->whereNull('out_let_product_product_combination_sales.deleted_at')
                ->sum('out_let_product_product_combination_sales.quantity');
                $combinations_obj = (object) array("id" => $product_combination->out_let_product_combination[0]->id,
                    "out_let_id" => $product_combination->out_let_id,
                    "out_let_product_id" => $product_combination->out_let_product_combination[0]->out_let_product_id,                    
                    "product_combination_id" => null,
                    "product_id" => $product_combination->product_id,
                    "stock_quantity" => ($product_combination->out_let_product_combination[0]->stock_quantity-$sold_quantity),
                );
                $product_sales_data->outLetProducts[0]->combination = "No";
                $product_sales_data->outLetProducts[0]->out_let_product_product_combination_id = $product_combination->out_let_product_combination[0]->id;
                $product_sales_data->outLetProducts[0]->outLetProductCombinations[] = $combinations_obj;
            }else{                
                $product_sales_data->outLetProducts[0]->combination = "Yes";
                $product_sales_data->outLetProducts[0]->out_let_product_id = null;
                foreach ($product_sales_data->outLetProducts[0]->outLetProductCombinations as $index => $out_let_product_combination) {
                    $sold_quantity = DB::table('out_let_product_product_combination_sales')
                    ->join('sales', 'sales.id', '=', 'out_let_product_product_combination_sales.sale_id')
                    ->whereIn('sales.status', ['sold','hold-on'])
                    ->where('out_let_product_product_combination_id', '=', 
                    $out_let_product_combination->pivot->id)->whereNull('out_let_product_product_combination_sales.deleted_at')
                    ->sum('out_let_product_product_combination_sales.quantity');                    
                    $product_sales_data->outLetProducts[0]->outLetProductCombinations[$index]->stock_quantity = $out_let_product_combination->pivot->stock_quantity - $sold_quantity; 
                }                
            }
            $product_data = $product_sales_data->outLetProducts[0];
        }

        /*if( $product_sales_data && sizeof( $product_sales_data->products ) > 0 ){
            $product_sales_data->products[0]->product_id = $product_sales_data->products[0]->id;
            $product_sales_data->products[0]->unit_price = $product_sales_data->products[0]->prices[0]->price;
                $product_sales_data->products[0]->quantity = 1;              
            $product_sales_data->products[0]->cost_price = $product_sales_data->products[0]->prices[0]->cost;
            $product_sales_data->products[0]->vat_percent = $product_sales_data->products[0]->prices[0]->vat->percent;

            if( sizeof( $product_sales_data->products[0]->combinations ) <= 0 ){
                $combinations_obj = (object) array("id" => $product_sales_data->products[0]->pivot->id,
                    "out_let_id" => $product_sales_data->products[0]->pivot->out_let_id,
                    "product_combination_id" => null,
                    "product_id" => $product_sales_data->products[0]->pivot->product_id,
                    "stock_quantity" => $product_sales_data->products[0]->pivot->stock_quantity,
                );
                $product_sales_data->products[0]->combination = "No";
                $product_sales_data->products[0]->out_let_product_id = $product_sales_data->products[0]->pivot->id;
                $product_sales_data->products[0]->combinations[] = $combinations_obj;
            }else{
                $product_sales_data->products[0]->combination = "Yes";
                $product_sales_data->products[0]->out_let_product_id = null;
            }
            $product_data = $product_sales_data->products[0];
        }*/
        //return $product_data;    
        return $product_data;    
    }
    /**
     * Get Product Name For Specific Product
     *
     * @return Product Name
     */
    protected function getProductName(Request $request)
    {
        $productBasic = Product::where('id', $request->product_id)->get();
        if($productBasic){
            $return_array['success'] =  true;
            foreach ($productBasic as $product) {
                $return_array['data'] =  $product->name;
            }
        }else{
            $return_array['error_message'] = 'Product not available...';
        }
        return $return_array;
    }
    protected function removeImage( Request $request )
    {
        $return_array['success'] = '';
        $return_array['error'] = '';
        $dir_path = 'uploads/out_lets/product_images';
        $dir_path_resize = 'uploads/out_lets/product_images/45x45';

        if( unlink(public_path($dir_path.'/'.$request->image_name)) )
        {
            unlink(public_path($dir_path_resize.'/'.$request->image_name));
            $return_array['success'] = 'success';
        }else{
            $return_array['error'] = 'failed';
        }
        return $return_array;
    }
    public function searchProducts( Request $request ){
        if( $request->category_id ){
            $query_string =  Product::with('categories')->whereHas('categories', function($query) use($request) {
                $query->where('categories.id', $request->category_id);
            });
        }else{
            $query_string =  Product::with('categories')->whereHas('categories', function($query) use($request) {
                $query->where('categories.id', '!=', $request->category_id);
            });
        }
        $query_string = $query_string->Where('company_id', '=', Auth::user()->company_id);

        if( $request->name ){
            $query_string = $query_string->where('name', 'like',  '%' . $request->name . '%');
        }
        if( $request->item_code ){
            $query_string = $query_string->where('item_code', 'like', '%' . $request->item_code . '%');
        }
        if( $request->bar_code ){
            $query_string = $query_string->where('bar_code', 'like', '%' . $request->bar_code . '%');
        }
        return $query_string->get();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeProduct( Request $request )
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('delete-products')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids)){
            Product::destroy( $request->item_ids );
            return response([
                'message' => 'Product deleted successfully!'
            ],200);
        }
    }
}
