<?php

namespace App\Http\Controllers;
use App\Company;
use App\Role;
use App\Permission;
use App\Module;
use App\Http\Requests\RoleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class RoleController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('add-edit-access-permission')) {
            abort(401);
        }
        $roles = Role::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->get();
        return view('role.index', compact('roles'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('add-edit-access-permission')) {
            abort(401);
        }
//        $role_lists = Role::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->pluck('name','id');
        return view('role.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        if(!Auth::user()->can('add-edit-access-permission')) {
            abort(401);
        }
        $role_data['name'] = $request->name;
        $role_data['slug'] = Str::slug($request->name, '-');
        $role = Company::find(Auth::user()->company_id)->roles()->create( $role_data );
        $permission = Permission::get();
        $role->permissions()->sync( $permission );
        return redirect('role/'.$role->id.'/permission_role')->with('success','Role added successfully!');    
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Role $role)
    {
        if(!Auth::user()->can('add-edit-access-permission')){
            abort(401);
        }
        return view('role.edit', compact('role'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param AttributeRequest|Request $request
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(RoleRequest $request, Role $role)
    {
        if(!Auth::user()->can('add-edit-access-permission')){
            abort(401);
        }
        $role_data['name'] = $request->name;
        $role_data['slug'] = Str::slug($request->name, '-');
        $role->update( $role_data );
        return redirect('role')->with('success','Role updated successfully!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Role $role )
    {
        
    }     
}
