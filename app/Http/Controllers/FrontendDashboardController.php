<?php
namespace App\Http\Controllers;
use App\User;
use App\Sale;
use App\OutLet;
use App\Company;
use Carbon\Carbon;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Crypt;


class FrontendDashboardController extends Controller
{
    public function __construct(){
//        $this->middleware('auth');
        //Company::where('id', $request->company_id)->first();

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {       
        $out_let = "";
        $userDetail = ""; 
        
        if(!isset($_COOKIE['user_id'])){
            return redirect('frontend/login');
        }else{   
            $company_details = $this->getCompanyDetails($request);
            $one = Route::current();
            $two = Route::currentRouteName();
            $three = Route::currentRouteAction();
            $out_let_id = $_COOKIE['out_let_id'];    
            return view('frontend.dashboard.index', compact('userDetail','out_let','company_details','out_let_id'));
        }     
        
        /* if(!Auth::check()){
            return redirect('frontend/login');
        }else{   
            $company_details = $this->getCompanyDetails();      
            return view('frontend.dashboard.index', compact('userDetail','out_let','company_details'));
        } */
    }
    public function getCompanyDetails(Request $request){  
        $user_id = ""; 
        if(isset($_COOKIE['company_user_id'])){
            $user_id = $request->cookie('company_user_id');
        }
        if(isset($_COOKIE['user_id'])){
            $user_id = $_COOKIE['user_id'];
        }     
        $user_id = $user_id ? $user_id : "";  
        $user = User::find($user_id);
        if($user_id){
            return ([
                'user_id' => $user_id,
                'company_details' => Company::where('id',  $user->company_id)->first()
            ]);   
        }
        return ([
            'user_id' => "",
            'company_details' => ""
        ]);
    }
    /**
     * Generate daily sales report for logged user's Outlet
     *
     * @return Pdf generated report
     */
    protected function dailyReport(Request $request){   
        $out_let_id = "";           
        if(isset($_COOKIE['company_out_let_id'])){
            $out_let_id = $request->cookie('company_out_let_id');
        }
        if(isset($_COOKIE['out_let_id'])){
            $out_let_id = $_COOKIE['out_let_id'];
        }
        $company_details = $this->getCompanyDetails($request);  
        
        if( sizeof($company_details) > 0 ) {
            $user_id = $company_details['user_id'];
            $company_details = $company_details['company_details'];
            $company_id = $company_details->id;
        }     
        $report_for = "Daily Sales Report";
        $report_for_date = "Date: ".date('Y-m-d H:i:s');
        $report_for_month = "";    
        $from_date = $today_date = date('Y-m-d');
        $report_generated_on = date('Y-m-d H:i:s');
        $out_let = OutLet::where('id',$out_let_id)->get();

         
        // $sales = Sale::whereDate('sale_date',date('Y-m-d'))
        // ->where('out_let_id',$out_let_id)
        // ->orWhere('company_id',$company_id)->get();    

        $sales = Sale::whereDate('sale_date',date('Y-m-d'))->where('status', 'sold')          
            ->where(function ($query) use($out_let_id, $company_id){
            if($out_let_id){
                $query->where('out_let_id',$out_let_id);
            }
            if($company_id){
                $query->where('company_id',$company_id);
            }
        })->get();
 
        $pdf = PDF::loadView('dashboard.sales-report', compact('out_let','sales', 'report_for','report_for_date','from_date','today_date','report_for_month','report_generated_on','company_details','user_id'));

        //return view('dashboard.sales-report', compact('out_let','sales', 'report_for','report_for_date','from_date','today_date','report_for_month','report_generated_on','company_details'));

        // (Optional) Setup the paper size and orientation
        $pdf->setPaper('A4', 'portrait');
        // download PDF file with download method
        $sales_report_file_name = "daily_sales_".date('Y-m-d').".pdf";

        //$output = $pdf->output();
        /*return new Response($output, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' =>  "inline; filename='invoice.pdf'",
        ]);*/
        //return view('dashboard.sales-report', compact('out_let','sales', 'report_for','report_for_date','from_date','today_date','report_for_month','report_generated_on','company_details'));
        
        return $pdf->download($sales_report_file_name);
    }
    /**
     * Generate weekly sales report for logged user's Outlet
     *
     * @return Pdf generated report
     */
    protected function weeklyReport(Request $request){
        $out_let_id = "";
        if(isset($_COOKIE['company_out_let_id'])){
            $out_let_id = $request->cookie('company_out_let_id');
        }
        if(isset($_COOKIE['out_let_id'])){
            $out_let_id = $_COOKIE['out_let_id'];
        }
        $company_details = $this->getCompanyDetails($request);  
        if( sizeof($company_details) > 0 ) {
            $user_id = $company_details['user_id'];
            $company_details = $company_details['company_details'];
            $company_id = $company_details->id;
        }         
        $report_for = "Weekly Sales Report";
        $today_date = date('Y-m-d');
        $from_date = Carbon::parse('last saturday')->startOfDay();
        $to_date = Carbon::parse('next friday')->endOfDay();
        $report_for_date = "Date: ".$from_date." To ".$to_date;    
        $report_for_month = "";    
        $report_generated_on = date('Y-m-d H:i:s');
        $out_let = OutLet::where('id',$out_let_id)->get();
        $sales = Sale::whereBetween('sale_date', [
            $from_date,$to_date])->where('status', 'sold')
            ->where(function ($query) use($out_let_id, $company_id){
            if($out_let_id){
                $query->where('out_let_id',$out_let_id);
            }
            if($company_id){
                $query->where('company_id',$company_id);
            }
        })->get();

        // return view('dashboard.sales-report', compact('out_let','sales', 'report_for','report_for_date','from_date','to_date','today_date','report_for_month','report_generated_on','company_details','user_id'));
        $pdf = PDF::loadView('dashboard.sales-report', compact('out_let','sales', 'report_for','report_for_date','from_date','to_date','today_date','report_for_month','report_generated_on','company_details','user_id'));
        // (Optional) Setup the paper size and orientation
        $pdf->setPaper('A4', 'portrait');
        // download PDF file with download method
        $sales_report_file_name = "weekly_sales_".$from_date.".pdf";
        return $pdf->download($sales_report_file_name);
    }
    /**
     * Generate monthly sales report for logged user's Outlet
     *
     * @return Pdf generated report
     */
    protected function monthlyReport(Request $request){
        $out_let_id = "";
        if(isset($_COOKIE['company_out_let_id'])){
            $out_let_id = $request->cookie('company_out_let_id');
        }
        if(isset($_COOKIE['out_let_id'])){
            $out_let_id = $_COOKIE['out_let_id'];
        }
        $company_details = $this->getCompanyDetails($request);  
        if( sizeof($company_details) > 0 ) {
            $user_id = $company_details['user_id'];
            $company_details = $company_details['company_details'];
            $company_id = $company_details->id;
        }  
        $report_for = "Monthly Sales Report";
        $today_date = date('Y-m-d');
        $from_date = Carbon::now()->startOfMonth();
        $to_date = Carbon::now()->endOfMonth();
        $month = date('m');
        $report_for_date = "Month: ".date('F, Y');
        $report_for_month = "Date: ".$from_date." To ".$to_date;   
        $report_generated_on = date('Y-m-d H:i:s');
        $startOfCurrentMonth = Carbon::today()->startOfMonth();
        $endOfCurrentMonth = $startOfCurrentMonth->copy()->endOfMonth();
        $out_let = OutLet::where('id',$out_let_id)->get();

        $sales = Sale::whereBetween('sale_date', [$startOfCurrentMonth, $endOfCurrentMonth])->where('status', 'sold')
            ->where(function ($query) use($out_let_id, $company_id){
            if($out_let_id){
                $query->where('out_let_id',$out_let_id);
            }
            if($company_id){
                $query->where('company_id',$company_id);
            }
        })->get();
        // return view('dashboard.sales-report', compact('out_let','sales', 'report_for','report_for_date','from_date','to_date','month','today_date','report_for_month','report_generated_on','company_details'));
       
        $pdf = PDF::loadView('dashboard.sales-report', compact('out_let','sales', 'report_for','report_for_date','from_date','to_date','month','today_date','report_for_month','report_generated_on','company_details','user_id'));
        // (Optional) Setup the paper size and orientation
        $pdf->setPaper('A4', 'portrait');
        // download PDF file with download method
        $sales_report_file_name = "monthly_sales_".date('F_Y').".pdf";
        return $pdf->download($sales_report_file_name);
    }
    /**
     * Generate sales report for custom dates for logged user's Outlet
     * 
     * @return Pdf generated report
     */
    protected function reportBetween(Request $request){        
        $out_let_id = "";
        if(isset($_COOKIE['company_out_let_id'])){
            $out_let_id = $request->cookie('company_out_let_id');
        }
        if(isset($_COOKIE['out_let_id'])){
            $out_let_id = $_COOKIE['out_let_id'];
        }
        $company_details = $this->getCompanyDetails($request);  
        if( sizeof($company_details) > 0 ) {
            $user_id = $company_details['user_id'];
            $company_details = $company_details['company_details'];
            $company_id = $company_details->id;
        }  
        $report_for = "Custom Sales Report";
        $today_date = date('Y-m-d');
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $report_for_date = "Date: ".$from_date." To ".$to_date;
        $report_for_month = "";     
        $report_generated_on = date('Y-m-d H:i:s');    
        $out_let = OutLet::where('id',$out_let_id)->get();
        
        $sales = Sale::whereBetween('sale_date', [$from_date, $to_date])->where('status', 'sold')
            ->where(function ($query) use($out_let_id, $company_id){
            if($out_let_id){
                $query->where('out_let_id',$out_let_id);
            }
            if($company_id){
                $query->where('company_id',$company_id);
            }
        })->get();
        //return view('dashboard.sales-report', compact('out_let','sales', 'report_for','report_for_date','from_date','to_date','today_date','report_for_month','report_generated_on','company_details'));
        $pdf = PDF::loadView('dashboard.sales-report', compact('out_let','sales', 'report_for','report_for_date','from_date','to_date','today_date','report_for_month','report_generated_on','company_details','user_id'));
        // (Optional) Setup the paper size and orientation
        $pdf->setPaper('A4', 'portrait');
        // download PDF file with download method
        $sales_report_file_name = "sales_between_".$from_date."_and_".$to_date.".pdf";
        return $pdf->download($sales_report_file_name);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
