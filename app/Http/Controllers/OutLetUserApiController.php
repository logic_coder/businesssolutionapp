<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Company;
use Cookie;
use Illuminate\Http\Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Validator;

class OutLetUserApiController extends Controller
{
    protected $user;
 
    public function __construct()
    {
        // $this->user = JWTAuth::parseToken()->authenticate();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Return outlets assigned to Logged user
     *
     * @return \Illuminate\Http\Response
     */
    protected function userOutlets(){
        $outlets = Auth::user()->out_lets()->get();
        if(sizeof($outlets)){
            return response()->json([
                'success' => true,
                'message' => 'OutLet found',
                'data' => $outlets,
            ], 200); 
        }
        return response()->json([
            'success' => false,
            'message' => 'OutLet not found for this user',
            'data' => $outlets,
        ], 404); 
    }

    protected function getCompanyOutlets( Request $request ){
        $company_id = $request->company_id;
        $outlets = Company::find($company_id)->outlets()->select('out_lets.id','out_lets.name','out_lets.location')->get();
        if(sizeof($outlets)){
            return response()->json([
                'success' => true,
                'message' => 'OutLet found',
                'data' => $outlets,
            ], 200); 
        }
        return response()->json([
            'success' => false,
            'message' => 'OutLet not found for this company',
            'data' => $outlets,
        ], 404); 
    }
    /**
     * Enter into outlet assigned to Logged user and by selecting his choice
     *
     * @return \Illuminate\Http\Response
     */
    protected function enterInOutlet( Request $request ){
        $user = Auth::guard('api')->user()->id;        
        $out_let_id =  $request->out_let_id;        
        $response = new Response('Cookie set Successfully.');
        $response->withCookie(cookie('user_id', $user, time() + (600)));
        $response->withCookie(cookie('out_let_id', $out_let_id, time() + (600)));
        return $response;


    }    
    protected function companyEnterInOutlet( Request $request ){
        $out_let_id =  $request->out_let_id;   
        $response = new Response('Cookie set Successfully.');
        $response->withCookie(cookie('company_out_let_id', $out_let_id, time() + (600)));
        $response->withCookie(cookie('company_user_id', Auth::user()->id, time() + (600)));
        return $response;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
