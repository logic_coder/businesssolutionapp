<?php
namespace App\Http\Controllers;
use App\CompanyCurrency;
use App\Currency;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CurrencyApiController extends Controller
{
    /**
     * Get All currency Info
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCurrencyLists()
    {
        return Currency::all();
    }
    /**
     * Get Company currency Info
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCompanyCurrencyLists()
    {
        return Currency::where('company_id', Auth::user()->company_id)
            ->Join('company_currencies', 'company_currencies.currency_id', '=', 'currencies.id')
            ->where('company_currencies.deleted_at', "=", null)->orderby('company_currencies.id','DESC')->get();
    }
    protected function getCompanyCurrencyDetails(Request $request)
    {
//        $companyCurrency = CompanyCurrency::find( $request->coupon_currency_id);
        $companyCurrency = CompanyCurrency::find( $request->company_currency_id );
        if($companyCurrency){
            $return_array['success'] =  true;
            $return_array['data'] =  $companyCurrency->currency;
        }else{
            $return_array['error_message'] = 'Company currency not available...';
        }
        return $return_array;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeCompanyCurrency( Request $request )
    {
        $return_array['message'] = '';
        if(!Auth::user()->can('access-company-currency-settings')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids )){
            CompanyCurrency::destroy( $request->item_ids );
            return response([
                'message' => 'Company currency deleted successfully!!!'
            ],200);
        }
        return $return_array;
    }
}
