<?php
namespace App\Http\Controllers;
use App\Category;
use App\Company;
use App\Coupon;
use App\CouponRestriction;
use App\CustomerOrderDetail;
use App\Http\Requests\CouponRequest;
use App\Http\Requests\CouponRestrictionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psy\Util\Json;
use Carbon\Carbon;

class CouponApiController extends CurrencyApiController
{
    protected function generateCouponCode(){
        $data['couponCode'] =  uniqid('Saj', FALSE);
        return $data;
    }
    protected function saveCouponSettings(CouponRequest $request, CouponRestrictionRequest $couponRestrictionRequest)
    {
        $message = "";
        if( $request ){
            if( $request->customer_id != null || $request->customer_id != ''){
                $coupon_data['customer_id'] = $request->customer_id;
            }else{
                $coupon_data['customer_id'] = NULL;
            }
            $coupon_data['name'] = $request->name;
            $coupon_data['company_currency_id'] = $request->company_currency_id;
            $coupon_data['description'] = $request->description;
            $coupon_data['code'] = $request->code;
            $coupon_data['high_light'] = $request->high_light;
            $coupon_data['status'] = $request->status;
            $coupon_data['valid_from'] = $request->valid_from;
            $coupon_data['valid_to'] = $request->valid_to;
            $coupon_data['minimum_purchase_amount'] = $request->minimum_purchase_amount;
            $coupon_data['vat_include_on_minimum_purchase_amount'] = $request->vat_include_on_minimum_purchase_amount;
            $coupon_data['total_coupons'] = $request->total_coupons;
            $coupon_data['total_coupons_for_each_user'] = $request->total_coupons_for_each_user;
            $coupon_data['discount_percent'] = $request->discount_percent;
            $coupon_data['exclude_discounted_products'] = isset($request->exclude_discounted_products) ? $request->exclude_discounted_products : "No";
            $coupon_data['discount_amount'] = $request->discount_amount;
            $coupon_data['no_discount'] = $request->no_discount;
            $coupon_data['apply_discount_option'] = $request->apply_discount_option;
            $coupon_data['product_selection_button'] = $request->product_selection_button;
            $coupon_data['product_selection_checked'] = $request->product_selection_checked;
            $coupon_created = Company::find( Auth::user()->company_id )->coupons()->create($coupon_data);

            if( $coupon_created ){
                if( $request->coupon_restrictions ){
                    foreach ( $request->coupon_restrictions as $coupon_restriction ){
                        $coupon_restriction_data['number_of_products_required_in_carts'] = $coupon_restriction['number_of_products_required_in_carts'];
                        $coupon_restriction_data['rule_concerning_products'] = isset($coupon_restriction['rule_concerning_products']) ? $coupon_restriction['rule_concerning_products']: false;
                        $coupon_restriction_data['rule_concerning_categories'] = isset($coupon_restriction['rule_concerning_categories']) ? $coupon_restriction['rule_concerning_categories']: false;
                        $coupon_restriction_created = $coupon_created->couponRestrictions()->create($coupon_restriction_data);
                        if( $coupon_restriction_created ){
                            $category_ids = [];
                            $product_ids = [];
                            if( $coupon_restriction['categories'] ){
                                foreach($coupon_restriction['categories'] as $categories){
                                    array_push( $category_ids, $categories['id'] );
                                }
                                $coupon_restriction_category_created = $coupon_restriction_created->categories()->sync( $category_ids );
                            }
                            if( $coupon_restriction['products'] ){
                                foreach($coupon_restriction['products'] as $products){
                                    array_push( $product_ids, $products['id'] );
                                }
                                $coupon_restriction_product_created = $coupon_restriction_created->products()->sync( $product_ids );
                            }
                        }
                    }
                }
                $message = "Coupon Created successfully";               
            }else{
                $message = "Error: Coupon Creation Error";
            }
            return response([
                'coupon_id' => $coupon_created->id, 
                'message'=> $message
            ]);
        }
    }
    protected function updateCouponSettings(CouponRequest $request, CouponRestrictionRequest $couponRestrictionRequest )
    {
        if( $request ) {
            if( $request->customer_id != null || $request->customer_id != ''){
                $coupon_data['customer_id'] = $request->customer_id;
            }else{
                $coupon_data['customer_id'] = NULL;
            }
            $coupon_data['name'] = $request->name;
            $coupon_data['company_currency_id'] = $request->company_currency_id;
            $coupon_data['description'] = $request->description;
            $coupon_data['code'] = $request->code;
            $coupon_data['high_light'] = $request->high_light;
            $coupon_data['status'] = $request->status;
            $coupon_data['valid_from'] = $request->valid_from;
            $coupon_data['valid_to'] = $request->valid_to;
            $coupon_data['minimum_purchase_amount'] = $request->minimum_purchase_amount;
            $coupon_data['vat_include_on_minimum_purchase_amount'] = $request->vat_include_on_minimum_purchase_amount;
            $coupon_data['total_coupons'] = $request->total_coupons;
            $coupon_data['total_coupons_for_each_user'] = $request->total_coupons_for_each_user;
            $coupon_data['discount_percent'] = $request->discount_percent;
            $coupon_data['exclude_discounted_products'] = isset($request->exclude_discounted_products) ? $request->exclude_discounted_products : "No";
            $coupon_data['discount_amount'] = $request->discount_amount;
            $coupon_data['no_discount'] = $request->no_discount;
            $coupon_data['apply_discount_option'] = $request->apply_discount_option;
            $coupon_data['product_selection_button'] = $request->product_selection_button;
            $coupon_data['product_selection_checked'] = $request->product_selection_checked;

            $coupon_updated = Coupon::find($request->id)->update( $coupon_data );

            if( $request->remove_all_old_coupon_restrictions == "Yes"){
                $coupon_restrictions = Coupon::find($request->id)->couponRestrictions()->get();
                $this->remove_old_coupon_restrictions( $coupon_restrictions );
            }
            if($request->remove_old_coupon_restrictions){
                foreach( $request->remove_old_coupon_restrictions as $old_coupon_restriction ){
                    $coupon_restriction = CouponRestriction::where('id',$old_coupon_restriction['id'])->get();
                    $this->remove_old_coupon_restrictions( $coupon_restriction );
                }
            }
            if( $coupon_updated ){
                if( $request->coupon_restrictions ){
                    foreach ( $request->coupon_restrictions as $coupon_restriction ){
                        $coupon_restriction_data['coupon_id'] = $request->id;
                        $coupon_restriction_data['number_of_products_required_in_carts'] = $coupon_restriction['number_of_products_required_in_carts'];
                        $coupon_restriction_data['rule_concerning_products'] = isset($coupon_restriction['rule_concerning_products']) ? $coupon_restriction['rule_concerning_products']: false;
                        $coupon_restriction_data['rule_concerning_categories'] = isset($coupon_restriction['rule_concerning_categories']) ? $coupon_restriction['rule_concerning_categories']: false;
                        $coupon_restriction_updated = CouponRestriction::updateOrCreate(['id' => $coupon_restriction['id'] ], $coupon_restriction_data);

                        $category_ids = [];
                        $product_ids = [];

                        if( $coupon_restriction['categories'] ){
                            foreach($coupon_restriction['categories'] as $categories){
                                array_push($category_ids, $categories['id'] );
                            }
                            $coupon_restriction_category_create_or_update = $coupon_restriction_updated->categories()->sync( $category_ids );
                        }else{
                            $coupon_restriction_category_create_or_update = $coupon_restriction_updated->categories()->sync( $category_ids );
                        }
                        if( $coupon_restriction['products'] ){
                            foreach($coupon_restriction['products'] as $products){
                                array_push($product_ids, $products['id'] );
                            }
                            $coupon_restriction_product_create_or_update = $coupon_restriction_updated->products()->sync( $product_ids );
                        }else{
                            $coupon_restriction_product_create_or_update = $coupon_restriction_updated->products()->sync( $product_ids );
                        }
                    }
                }
                $message = "Coupon Updated successfully";                
            }else{
                $message = "Error: Coupon Updated Error";      
            }
            return response([
                'message'=> $message
            ]);
        }
    }
    protected function remove_old_coupon_restrictions( $coupon_restrictions )
    {
        if( $coupon_restrictions ){
            $coupon_restriction_ids = [];
            foreach( $coupon_restrictions as $coupon_restriction ){
                $category_ids = [];
                $product_ids = [];
                $coupon_restriction_category_updated = $coupon_restriction->categories()->sync( $category_ids );
                $coupon_restriction_product_updated = $coupon_restriction->products()->sync( $product_ids );
                $couponRestriction = CouponRestriction::find($coupon_restriction->id);
                $couponRestriction->forceDelete();
            }
        }
    }
    /**
     * Get coupon data based on coupon id
     *
     * @param  \Illuminate\Http\Request  $request which contains coupon_id
     * @return coupon data
     */
    public function getCouponData(Request $request)
    {
        return Coupon::where('id', $request->coupon_id)->with(
            ['couponRestrictions' => function ($query) {
                $query->with(['products', 'categories' => function ($query) {
                }]);
            }])->first();
    }
    /**
     * Get coupon discount amount based on coupon code applied
     *
     * @param  \Illuminate\Http\Request  $request which contains coupon_code
     * @return coupon_discount_amount
     */
    public function applyCouponCode(Request $request){
        //Auth::user()->company_id
        $return_array['discount_amount'] = 0.00;
        $return_array['success'] = true;
        $return_array['error_message'] = "";
        $coupon_code = $request->coupon_code;
        $coupons = Coupon::where('status', 1)
                ->where('code', $coupon_code)
                ->where('company_id', Auth::user()->company_id)
                ->Where(function ($query){
                    $query->whereDate('valid_from', '<=', date("Y-m-d"))
                        ->whereDate('valid_to', '>=', date("Y-m-d"));
            })->first();
        if($coupons){
            $numberOfCouponUsed = $this->numberOfCouponUsed($coupons->id);
            if( $coupons->total_coupons > $numberOfCouponUsed ){
                $thisCustomerCouponUsed = 0;
                if( $request->customer_id ){
                    $thisCustomerCouponUsed = CustomerOrderDetail::where(
                        ['coupon_id' =>  $coupons->id],
                        ['customer_id' =>  $request->customer_id]
                    )->count();
                }
                if( $coupons->total_coupons_for_each_user > $thisCustomerCouponUsed ){
                    $couponMatched = false;
                    if( $coupons->vat_include_on_minimum_purchase_amount == "Yes"){
                        if( $request->total_amount_inclusive_vat >= $coupons->minimum_purchase_amount && $request->company_currency_id==$coupons->company_currency_id ){
                            $couponMatched = true;
                        }else{
                            $couponMatched = false;
                        }
                    }else{
                        if( $request->total_amount_exclusive_vat >= $coupons->minimum_purchase_amount && $request->company_currency_id==$coupons->company_currency_id ){
                            $couponMatched = true;
                        }else{
                            $couponMatched = false;
                        }
                    }
                    if( $couponMatched ){
                        if( ( $coupons->customer_id && ( $coupons->customer_id == $request->customer_id )) || !$coupons->customer_id ){
                            $couponRestrictions = CouponRestriction::where('coupon_id', $coupons->id )->with(['categories','products'])->get();
                            if($couponRestrictions){
                                foreach( $couponRestrictions as $couponRestriction ){
                                    $couponMatched = false;
                                    foreach( $couponRestriction->products as $restrictionProduct ){
                                        $restrictionFailed = false;
                                        if( $request->customer_order_detail_items ){
                                            foreach ( $request->customer_order_detail_items as $customer_order_detail_item ) {
                                                if (($customer_order_detail_item['product_id'] == $restrictionProduct->id) && $customer_order_detail_item['quantity'] >= $couponRestriction->number_of_products_required_in_carts && $customer_order_detail_item['price_exclusive_vat'] > 0) {
                                                    $couponMatched = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if( $couponMatched ){
                                            break;
                                        }else{
                                            $restrictionFailed = true;
                                        }
                                    }
                                    if( !$couponMatched ){                      
                                        foreach( $couponRestriction->categories as $restrictionCategory ){
                                            $restrictionFailed = false;
                                            if( $request->customer_order_detail_items ){
                                                $restriction_category_id = $restrictionCategory->id; 
                                                foreach ( $request->customer_order_detail_items as $customer_order_detail_item ){
                                                    /*if(($customer_order_detail_item['category_id'] == $restrictionCategory->id) && $customer_order_detail_item['quantity'] >= $couponRestriction->number_of_products_required_in_carts && $customer_order_detail_item['price_exclusive_vat'] > 0 ) {
                                                        $couponMatched = true;
                                                        break;
                                                    }*/                                               
                                                    $category_filtered = array_filter($customer_order_detail_item['categories'], function($category) use ($restriction_category_id){
                                                        return $category['id'] == $restriction_category_id;
                                                    });
                                                    
                                                    if(!empty($category_filtered) && $customer_order_detail_item['quantity'] >= $couponRestriction->number_of_products_required_in_carts && $customer_order_detail_item['price_exclusive_vat'] > 0){
                                                        $return_array['success'] =  $category_filtered;
                                                        $couponMatched = true;
                                                        break;
                                                    } 
                                                }                    
                                            }
                                            if( $couponMatched ){
                                                break;
                                            }else{
                                                $restrictionFailed = true;
                                            }
                                        }
                                    }
                                }
                                if( !$restrictionFailed ){
                                    $return_array['coupon_id'] = $coupons->id;
                                    if( $coupons->no_discount == "No" ){
                                        if($coupons->apply_discount_option=="Amount"){
                                            $return_array['discount_amount'] = $coupons->discount_amount;
                                        }elseif($coupons->apply_discount_option=="Percent"){
                                            if($coupons->exclude_discounted_products == "Yes"){
                                                $return_array['discount_amount'] =  ( $request->total_amount_exclusive_vat * $coupons->discount_percent ) / 100;
                                            }else{
                                                $return_array['discount_amount'] =  ( $request->total_amount_exclusive_vat * $coupons->discount_percent ) / 100;
                                            }
                                        }
                                    }
                                }else{
                                    $return_array['error_message'] = "You can not use this coupon, coupon restriction doesn’t matched";
                                }
                            }
                        }elseif( $coupons->customer_id && ( $coupons->customer_id != $request->customer_id )){
                            $return_array['error_message'] = "You can not use this coupon, coupon code doesn’t matched with your one";
                        }
                    }else{
                        $currencyApiClass = new CurrencyApiController();
                        $couponRequest = new Request();
                        $request->request->add(['company_currency_id' => $coupons->company_currency_id]);
                        $currencyDetails = $currencyApiClass :: getCompanyCurrencyDetails($request);
                        $currencyName = $currencyDetails['data']->name."(".$currencyDetails['data']->code.")";
                        $return_array['discount_amount'] = 0.00;
                        $return_array['error_message'] = "You can not use this coupon, minimum purchase amount to use this coupon is:". $coupons->minimum_purchase_amount.$currencyName;
                    }
                }else{
                    $return_array['error_message'] = "You can not use this coupon, your limit reached";
                }
            }else{
                $return_array['error_message'] = "You can not use this coupon, total limit reached";
            }
        }else{
            $return_array['success'] = false;
            $return_array['error_message'] = "Coupon code doesn’t matched or validity expired";
        }
//        $return_array['coupon_discount_amount'] = $request->coupon_code;
        return $return_array;
    }
    public function numberOfCouponUsed( $coupon_id ){
        return CustomerOrderDetail::where(['coupon_id' =>  $coupon_id])->count();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeCoupon( Request $request )
    {
        $return_array['message'] = '';
        if(!Auth::user()->can('access-coupon-settings')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids )){
            Coupon::destroy( $request->item_ids );
            return response([
                'message' => 'Coupon deleted successfully!!!'
            ],200);
        }
        return $return_array;
    }
}
