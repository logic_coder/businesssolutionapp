<?php
namespace App\Http\Controllers;
use App\OutLet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OutLetApiController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeOutLet( Request $request )
    {
        $return_array['message'] = '';
        if(!Auth::user()->can('delete-outlets')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids )){
            OutLet::destroy( $request->item_ids );
            return response([
                'message' => 'OutLet deleted successfully!'
            ],200);
        }
        return $return_array;
    }
    protected function getCompanyOutLet(){
        return OutLet::where('company_id', Auth::user()->company_id)->orderby('id','ASC')->get();
    }
    protected function getOutLetData( Request $request )
    {
       return OutLet::where('id', $request->out_let_id)->first();
    }
}
