<?php
namespace App\Http\Controllers;
use App\Company;
use App\Sale;
use App\SalePaid;
use App\SalePaidDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psy\Util\Json;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Response;

class SalePaidApiController extends Controller
{
    /**
     * Get paid history for a specific order based on sale id
     *
     * @param  \Illuminate\Http\Request  $request which contains sale_id
     * @return  paid details
     */
    protected function getSalePaidHistory(Request $request)
    {
        $salePaidHistory = [];
        if ($request->sale_id) {
            $salePaidHistory = SalePaid::where('sale_id', $request->sale_id)->with('SalePaidDetails','paymentMethod')->get();
        }        
        if(sizeof($salePaidHistory) <= 0){
            /* For Cash Payment */
            $sale_paid_history_obj = (object) array("id" => null, 
            );
            $payment_method_obj = (object) array("id" => $request->cash_payment_method_id, "name" => 'Cash'
            );
            $sale_paid_details_obj = (object) array("id" => null, "meta_data" => 'Payment By',"value" => 'Self'
            );
            $sale_paid_history_obj->payment_method = $payment_method_obj; 
            $sale_paid_history_obj->sale_paid_details = array($sale_paid_details_obj); 
            $salePaidHistory[0] = $sale_paid_history_obj;

            /* For Digital Payment */
            $sale_paid_history_obj = (object) array("id" => null,
            );
            $payment_method_obj = (object) array("name" => 'Select Payment Method'
            );
            $sale_paid_details_obj = (object) array("id" => null, "meta_data" => '',"value" => ''
            );
            $sale_paid_history_obj->payment_method = $payment_method_obj; 
            $sale_paid_history_obj->sale_paid_details[0] = $sale_paid_details_obj; 
            $sale_paid_history_obj->sale_paid_details[1] = $sale_paid_details_obj;
            $salePaidHistory[1] = $sale_paid_history_obj; 

        }else if( sizeof($salePaidHistory) == 1 ){         
                foreach($salePaidHistory as $key => $sale_paid) {            
                    if($sale_paid->paymentMethod->name != "Cash"){
                        /* For Cash Payment */
                        $sale_paid_history_obj = (object) array("id" => null,
                        );
                        $payment_method_obj = (object) array("id" => $request->cash_payment_method_id,"name" => 'Cash'
                        );
                        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => 'Payment By',"value" => 'Self'
                        );
                        $sale_paid_history_obj->payment_method = $payment_method_obj; 
                        $sale_paid_history_obj->sale_paid_details = array($sale_paid_details_obj); 
                        $salePaidHistory[$key+1] = $sale_paid_history_obj;
                        
                    }else{
                        /* For Digital Payment */
                        $sale_paid_history_obj = (object) array("id" => null, 
                        );
                        $payment_method_obj = (object) array("name" => 'Select Payment Method'
                        );
                        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => '',"value" => ''
                        );
                        $sale_paid_history_obj->payment_method = $payment_method_obj; 
                        $sale_paid_history_obj->sale_paid_details[0] = $sale_paid_details_obj; 
                        $sale_paid_history_obj->sale_paid_details[1] = $sale_paid_details_obj;
                        $salePaidHistory[$key+1] = $sale_paid_history_obj;
                    }
                }            
            }
            return Response::json($salePaidHistory);
    }
    /**
     * Get total paid amount for a specific sales based on sales id
     *
     * @param  \Illuminate\Http\Request  $request which contains sale_id
     * @return total paid amount
     */
    public function getSalePaidsData(Request $request)
    {
        $paid_amount = SalePaid::where('sale_id', $request->sale_id)->sum('paid_amount');
        return number_format($paid_amount, 2, '.','');
    }
}
