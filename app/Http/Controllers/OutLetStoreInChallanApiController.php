<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\WarehuseStoreOutChallanRequest;
use App\Role;
use App\OutLet;
use App\Product;
use App\WarehouseStoreOutChallan;
use App\WarehouseStoreOutChallanProduct;
use App\Http\Requests\WarehouseStoreOutChallanRequest;
use App\WarehouseStoreOutChallanProductCombination;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Psy\Util\Json;
use Carbon\Carbon;
use Response;
use DB;

class OutLetStoreInChallanApiController extends Controller
{
    /**
     * Display a listing of the resource 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Receive WarehouseStoreOutChallan Data to specified OutLet
     *
     * @param  Object $request which contains Challan info
     * @return \Illuminate\Http\Response
     */
    public function receiveOutLetStoreInChallan(WarehouseStoreOutChallanRequest $request)
    {        
        $return_array['success'] = ''; 
        $message = "";

        $user_roles = Auth::user()->roles;  
                  
        if (!Auth::user()->can('access-outlet-store-in-challan') && $user_roles[0]->slug != 'administrator') {
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        $warehouse_store_out_challan_product = '';
        if( $request ) {           
            $challan_data['status'] = $request->status;            
            $product_combination_data = [];
            $warehouse_store_out_challan = WarehouseStoreOutChallan::find( $request->challan_id );
            $warehouse_store_out_challan->update( $challan_data );
            $out_let = OutLet::find($warehouse_store_out_challan->out_let_id);
           
            foreach($request->product_combination_list as $product_combination){
                $out_let_store_in_challan_received = false;
                $product_combination_data = [];
                $out_let_data = [];
                $out_let_product_combination_data = [];
                $out_let_data['product_id'] = $product_combination['product_id'];
                foreach($product_combination['combinations'] as $combination) {                    
                    if(!isset($combination['received_quantity'])){
                        $combination['received_quantity'] = null;
                    }
                    $combination['lost_quantity'] = null;
                    $combination['return_quantity'] = null;
                    $combination['stock_quantity'] = null;
                    $out_let_product_combination_data['stock_quantity'] = null;
                    if(isset($combination['missing_status'])){
                        if($combination['missing_status'] == "Lost"){
                            $combination['lost_quantity'] = $combination['missing_quantity'];
                        }
                        elseif($combination['missing_status'] == "Return Warehouse"){
                            $combination['return_quantity'] = $combination['missing_quantity'];
                        }
                    }
                    if(isset($combination['requested_quantity'])){
                        if($combination['requested_quantity']) {                              
                            if(isset($combination['id'])) {                               
                                if($challan_data['status'] == "Auto Received") {
                                    $product_combination_data[$combination['id']] = ['product_combination_warehouse_store_in_challan_product_id' => $combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['requested_quantity'], 'stock_quantity' => $combination['requested_quantity'], 'missing_status' => $combination['missing_status']];
                                    $out_let_product_combination_data['stock_quantity'] = $combination['requested_quantity'];
                                }
                                elseif($challan_data['status'] == "Received") {
                                    $product_combination_data[$combination['id']] = ['product_combination_warehouse_store_in_challan_product_id' => $combination['product_combination_warehouse_store_in_challan_product_id'],'requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['received_quantity'], 'stock_quantity' => $combination['received_quantity'],'lost_quantity' => $combination['lost_quantity'], 'return_quantity' => $combination['return_quantity'], 'missing_status' => $combination['missing_status']];
                                    $out_let_product_combination_data['stock_quantity'] = $combination['received_quantity'];
                                }                                
                                /* Update stock_quantity in Table: product_combination_warehouse_store_in_challan_product */
                                DB::table('product_combination_warehouse_store_in_challan_product')->where('id', $combination['product_combination_warehouse_store_in_challan_product_id'])->update(array('stock_quantity' => $combination['stock_remain']+$combination['return_quantity']));
                                /* Sync Data in Table: product_combination_warehouse_store_out_challan_product */
                                $warehouse_store_out_challan_product = WarehouseStoreOutChallanProduct::find( $product_combination['warehouse_store_out_challan_product_id']);
                                $store_out_product_combinations_updated = $warehouse_store_out_challan_product->storeOutProductCombinations()->sync($product_combination_data);
                                if($store_out_product_combinations_updated){
                                    $out_let_store_in_challan_received = true;                                   
                                    $out_let_product_combination_data['product_combination_id'] = $combination['id'];
                                    //////////////////////////////////////////////////
                                    $out_let_products = $out_let->outLetProducts()
                                    ->where('product_id', $product_combination['product_id'])
                                    ->first();
                                    if( $out_let_products ){
                                       $out_let_products_combination = $out_let_products->outLetProductCombinations()->where('product_combination_id',$combination['id'])->first();
                                        if( $out_let_products_combination ){            
                                           DB::table('out_let_product_product_combination')->where('id', $out_let_products_combination->pivot->id)->update(array('stock_quantity' => DB::raw('stock_quantity + ' .$out_let_product_combination_data['stock_quantity'])));
                                        }else{
                                            $out_let_products->outLetProductCombinations()
                                            ->attach([null],$out_let_product_combination_data);
                                        }
                                    }else{  
                                        $return_array['success'] = "Inside Ouh 2j6ktLet Product 1";
                                        $out_let_products_created = $out_let->outLetProducts()->create($out_let_data);
                                        if(isset($combination['id']) && $out_let_products_created) {
                                            $return_array['success'] = "Inside OutLet Product combination 1";
                                           $out_let_products_created->outLetProductCombinations()->attach([null],$out_let_product_combination_data);

                                        }                                       
                                    }
                                }
                            }
                        }
                    }
                }
                if(!isset($combination['id'])) {
                    $product_combination_value = DB::table('product_combination_warehouse_store_out_challan_product')->where('warehouse_store_out_challan_product_id', $product_combination['warehouse_store_out_challan_product_id'])->first();
                    if($product_combination_value){
                        if($challan_data['status'] == "Auto Received") {
                            DB::table('product_combination_warehouse_store_out_challan_product')->where('warehouse_store_out_challan_product_id', $product_combination['warehouse_store_out_challan_product_id'])->update(array('requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['requested_quantity'],'stock_quantity' => $combination['requested_quantity'], 'missing_status' => $combination['missing_status']));
                            $out_let_product_combination_data['stock_quantity'] = $combination['requested_quantity'];
                            ///////////////////////////////////////////////////
                            //$out_let->products()->attach([null],$out_let_data);
                            $out_let_store_in_challan_received = true;
                        }
                        elseif($challan_data['status'] == "Received") {
                            DB::table('product_combination_warehouse_store_out_challan_product')->where('warehouse_store_out_challan_product_id', $product_combination['warehouse_store_out_challan_product_id'])->update(array('requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['received_quantity'], 'stock_quantity' => $combination['received_quantity'], 'lost_quantity' => $combination['lost_quantity'], 'return_quantity' => $combination['return_quantity'], 'missing_status' => $combination['missing_status']));
                            $out_let_product_combination_data['stock_quantity'] = $combination['received_quantity'];
                            //////////////////////////////////////////////////
                            //$out_let->products()->attach([null],$out_let_data);
                            $out_let_store_in_challan_received = true;
                        }
                        /*else{
                            DB::table('product_combination_warehouse_store_out_challan_product')->where('warehouse_store_out_challan_product_id', $product_combination['warehouse_store_out_challan_product_id'])->update(array('requested_quantity' => $combination['requested_quantity'],'received_quantity' => null,'stock_quantity' => null,'lost_quantity' => null,'return_quantity' => null));
                            $out_let_store_in_challan_received = true;
                        }*/
                        /* Update stock_quantity in Table: product_combination_warehouse_store_in_challan_product */
                        DB::table('product_combination_warehouse_store_in_challan_product')->where('id', $combination['product_combination_warehouse_store_in_challan_product_id'])->update(array('stock_quantity' => $combination['stock_remain']+$combination['return_quantity']));
                            
                            $out_let_products = $out_let->outLetProducts()
                            ->where('product_id', $product_combination['product_id'])
                            ->first();
                            if( $out_let_products ){
                               $out_let_products_combination = $out_let_products->out_let_product_combination()->first();
                                if( $out_let_products_combination ){
                                   DB::table('out_let_product_product_combination')->where('id', $out_let_products_combination->id)->update(array('stock_quantity' => DB::raw('stock_quantity + ' .$out_let_product_combination_data['stock_quantity'])));
                                }else{
                                    $out_let_products->outLetProductCombinations()
                                    ->attach([null],$out_let_product_combination_data);
                                }
                            }else{  
                                $return_array['success'] = "Inside OutLet Product 1";
                                $out_let_products_created = $out_let->outLetProducts()->create($out_let_data);
                                if($out_let_products_created) {
                                    $return_array['success'] = "Inside OutLet Product combination 1";
                                   $out_let_products_created->outLetProductCombinations()->attach([null],$out_let_product_combination_data);

                                }                                       
                            }
                        /*$out_let_products = $out_let->products()->where('product_id', $product_combination['product_id'])->get();

                        if( !$out_let_products->isEmpty() ){
                           DB::table('out_let_product')->where('id', $out_let_products[0]->pivot->id)->update(array('stock_quantity' => DB::raw('stock_quantity + ' .$out_let_data['stock_quantity'])));
                        }else{
                           $out_let->products()->attach([null],$out_let_data);
                        }*/                        
                    }
                }                
            }
            if( $out_let_store_in_challan_received ){  
                return response([
                    'message'=> "OutLet StoreIn Challan Received Successfully"
                ], 200);
            }else{
                return response([
                    'message'=> "Error: OutLet StoreIn Challan Not Received, Please Try again"
                ], 500);
            }
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
