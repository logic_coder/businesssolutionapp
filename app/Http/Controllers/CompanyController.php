<?php

namespace App\Http\Controllers;
use App\User;
use App\Company;
use App\Country;
use App\BusinessCategory;
use App\Currency;
use App\TimeZone;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Facades\Auth;
use Image;
use File;
class CompanyController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('access-company-info')){
            abort(401);
        }
        $company = Company::findorfail($id);
        $countries = Country::orderBy('name', 'ASC')->pluck('name','id');
        $currencies = Currency::orderBy('id', 'ASC')->pluck('name','id');
        $time_zones = TimeZone::orderBy('id', 'ASC')->pluck('location','id');
        $business_categories = BusinessCategory::orderBy('id', 'Desc')->pluck('name','id');
        return view('company.edit', compact('company','countries','business_categories','currencies','time_zones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, $id)
    {
        if(!Auth::user()->can('access-company-info')){
            abort(401);
        }      
        $companies = Company::find($id);
        $company_data = $request->all();        
        $company_data['receipt_logo'] = $companies->receipt_logo;
        $dir_path = 'uploads/company/'.$id.'/receipt_logo';
        $dir_path_resize = $dir_path.'/50x50';

        if (!File::exists($dir_path))
        {
            File::makeDirectory($dir_path, 0777, true);

        }
        if (!File::exists($dir_path_resize))
        {
            File::makeDirectory($dir_path_resize, 0777, true);
        }

        if ($request->receipt_logo) {
            $file = $request->receipt_logo;
            if($company_data['receipt_logo']){
                unlink(public_path($dir_path.'/'.$company_data['receipt_logo']));
                unlink(public_path($dir_path_resize.'/'.$company_data['receipt_logo']));
            }            
            $extension =  $file->getClientOriginalExtension(); // getting image extension
            $company_data['receipt_logo'] = 'receipt_logo_'.\Carbon\Carbon::now().'.'.$extension; // renaming image
            $file->move($dir_path, $company_data['receipt_logo']); // uploading file to given path

            // Start : Image Resize
            $image = Image::make(public_path($dir_path.'/'.$company_data['receipt_logo']));
            // resize the image to a height of 45 and constrain aspect ratio (auto width)
            $image->resize(null, 45, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->save(public_path($dir_path_resize.'/'.$company_data['receipt_logo']));
            // End : Image Resize
        }

        $companies->update($company_data);
        return back()->with('success','Company updated successfully!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
