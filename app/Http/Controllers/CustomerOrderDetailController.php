<?php

namespace App\Http\Controllers;
use Company;
use App\Customer;
use App\CustomerOrderDetail;
use App\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class CustomerOrderDetailController extends Controller
{
    public function __construct(){
        //$this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-sales-page')){
                abort(401);
            }
        }
        $customerOrderDetails = CustomerOrderDetail::where('out_let_id', $_COOKIE['out_let_id'])->orderBy('id', 'DESC')->get();
        return view('frontend.customer_order_detail.index', compact('customerOrderDetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-sales-page')){
                abort(401);
            }
        }
        return view('frontend.customer_order_detail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerOrderDetail $customer_order_detail)
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('edit-sales')){
                abort(401);
            }
        }
        return view('frontend.customer_order_detail.edit', compact('customer_order_detail'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Show the paid details for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPaidDetails(CustomerOrderDetail $customer_order_detail)
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-sales-page')){
                abort(401);
            }
        }
        return view('frontend.customer_order_detail.paid_detail', compact('customer_order_detail'));
    }
}
