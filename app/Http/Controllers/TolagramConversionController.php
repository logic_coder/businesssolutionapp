<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Company;

class TolagramConversionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "Hello Dello...";
    }
    /**
     * Tola---Gram Conversion
     *
     * @return \Illuminate\Http\Response
     */
    public function tolaGram()
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::check()){
                abort(401);
            }            
        } 
        // $company_id = Auth::user()->company_id; 
        // dd($company_id );
        // $company_details = Company::where('id',  $company_id)->first();  
        return view('tolagram.convert');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Tola To Gram Conversion:
    //1. 16 Ana = 11.664 gram
    //2. 96 rotti = 11.664 gram
    //3. 96 rotti = 11.664 gram
    //1 rotti = 11.664/96 gram = 0.1215 gram
    //15 rotti = (11.664/96) * 15 = 1.8225 gram
    //
    //
    //4. 1 rotti = 11.664/96 gram = 0.1215 gram
    //1 rotti = 10 milli = 0.1215 gram
    //10 milli = 0.1215 gram
    //1 milli = 0.1215/10 = 0.01215 gram
    //7 milli = 0.01215 * 7 = 0.08505 gram
    protected function tolaToGram(Request $request){
        $rotti_from_tola = $request->weight_tola * 96;
        $rotti_from_ana = $request->weight_ana * 6;
        $weight_rotti = $rotti_from_tola + $rotti_from_ana + $request->weight_rotti;
        $gram_from_rotti = 0.1215 * $weight_rotti;
        $gram_from_milli = 0.01215 * $request->weight_milli;
        $gram = round( $gram_from_rotti + $gram_from_milli, 4);
        $price_per_gram = $request->unit_price_tola/11.664;
        $price = $price_per_gram * $gram;
        $return_array['gram'] = $gram;
        $return_array['material_price'] = round($price);
        $return_array['total_price'] = round($price + $request->making_charge);
        $return_array['material_price'] = number_format($return_array['material_price']);
        $return_array['total_price'] = number_format($return_array['total_price']);
        return $return_array;
    }
    //1. 0.01215 gram = 1 milli
    //1 gram = ( 1 / 0.01215) milli =  82.3045 milli
    //11.664 gram =  ( 82.3045 * 11.664 ) = 959.9997 milli
    //
    //2. 10 milli = 1 rotti
    //1 milli = ( 1/10 ) rotti = 0.1 rotti
    //959.9997 =  959.9997 * 0.1 = 96 rotti
    //
    //3. 6 rotti = 1 Ana
    //1 rotti = ( 1/6 ) Ana = 0.16667 Ana
    //96 = 96 *  0.16667 = 16 Ana
    //
    //4. 16 Ana = 1 Tola
    //1 Ana = ( 1/16 ) Tola = 0.0625 Tola
    //24 Ana =
    protected function gramToTola(Request $request){
        $milli_from_gram = round($request->gram * 82.3045, 3);
        $milli = $milli_from_gram % 10;
        $rotti_from_milli = (int)($milli_from_gram / 10);
        if( $milli == 9 ){
            $milli = 0;
            $rotti_from_milli = $rotti_from_milli + 1;
        }
        $rotti = $rotti_from_milli % 6;
        $ana_from_rotti = (int)($rotti_from_milli / 6);
        $ana = $ana_from_rotti % 16;
        $tola = (int)($ana_from_rotti / 16);
        $return_array['weight_tola'] = $tola;
        $return_array['weight_ana'] = $ana;
        $return_array['weight_rotti'] = $rotti;
        $return_array['weight_milli'] = $milli;
        $return_array['material_price'] = round( $request->gram * $request->unit_price_gram );
        $return_array['total_price'] = round($return_array['material_price'] + $request->making_charge);
        $return_array['material_price'] = number_format($return_array['material_price']);
        $return_array['total_price'] = number_format($return_array['total_price']);
        return $return_array;
    }
}
