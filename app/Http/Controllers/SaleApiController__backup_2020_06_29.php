<?php
namespace App\Http\Controllers;
use App\Company;
use App\OutLet;
use App\Sale;
use App\SalePaid;
use App\SalePaidDetail;
use App\CustomerOrderDetail; 
use App\OutLetProductSale;
use App\Http\Requests\SaleRequest;
use App\Http\Requests\OutLetProductSaleRequest;
use App\Http\Requests\SalePaidRequest;
use App\Http\Requests\SalePaidDetailRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psy\Util\Json;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use Response;

class SaleApiController extends Controller
{
    
     protected function saveSaleDetail(OutLetProductSaleRequest $outLetProductSale, SaleRequest $request, SalePaidRequest $salePaidRequest, SalePaidDetailRequest $salePaidDetailRequest)
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('access-sales-page')){
            Redis::set('error', 'Error: Operation not permitted!!!');
            return $return_array;
        }
        if( $request ){
            if( $request->invoice_prefix_id != null || $request->invoice_prefix_id != '' ){
                $customer_order_details_data['invoice_prefix_id'] = $request->invoice_prefix_id;
            }else{
                $customer_order_details_data['invoice_prefix_id'] = NULL;
            }           
            $customer_order_details_data['out_let_id'] = Redis::get('out_let_id');
            $customer_order_details_data['sales_by'] = Auth::user()->id;
            $customer_order_details_data['customer_id'] = isset($request->customer_id) ? $request->customer_id : null;
            $customer_order_details_data['sale_date'] = Carbon::now();            
            $customer_order_details_data['company_currency_id'] = $request->company_currency_id;
            $customer_order_details_data['sub_total'] = $request->sub_total;
            $customer_order_details_data['discount_amount'] = $request->discount_amount;
            $customer_order_details_data['coupon_id'] = $request->coupon_id;
            $customer_order_details_data['coupon_discount_amount'] = $request->coupon_discount_amount;
            $customer_order_details_data['total_amount_exclusive_vat'] = $request->total_amount_exclusive_vat;
            $customer_order_details_data['vat_amount'] = $request->vat_amount;            
            $customer_order_details_data['net_amount'] = $request->net_amount;
            $customer_order_details_data['sales_note'] = isset($request->sales_note) ? $request->sales_note : null;
            $sale_created = OutLet::find( $customer_order_details_data['out_let_id'] )->sales()->create($customer_order_details_data);
            $customer_order_details_update_data['order_no'] = Auth::user()->company_id."-".$customer_order_details_data['out_let_id']."-".Auth::user()->id."-".date("Y").date("m").date("d").$sale_created->id;
            $customer_order_details_update_data['invoice_no'] = Auth::user()->company_id.$customer_order_details_data['out_let_id'].Auth::user()->id.date("Y").date("m").date("d").$sale_created->id;
            $sale_created->update($customer_order_details_update_data);

            if( $sale_created ){ 
                if( $request->customer_order_detail_items ){
                    foreach ( $request->customer_order_detail_items as $customer_order_detail_item ){
                        $customer_order_detail_item_data['out_let_product_id'] = $customer_order_detail_item['out_let_product_id'];
                        $customer_order_detail_item_data['name'] = $customer_order_detail_item['name'];
                        $customer_order_detail_item_data['cost_price'] = $customer_order_detail_item['cost_price'];
                        $customer_order_detail_item_data['unit_price'] = $customer_order_detail_item['unit_price'];
                        $customer_order_detail_item_data['quantity'] = $customer_order_detail_item['quantity'];
                        $out_let_product_sale_created = $sale_created->outLetProductSales()->create($customer_order_detail_item_data);
                    }
                }
                if( $request->cash_payment_method_id ){
                    $customer_order_detail_paid_data['payment_method_id'] = $request->cash_payment_method_id;
                    $customer_order_detail_paid_data['paid_amount'] = $request->cash_payment_amount;
                    $customer_order_detail_paid_data['paid_date'] = date('Y-m-d');//Carbon::now();
                    $sale_paid_created = $sale_created->salePaids()->create($customer_order_detail_paid_data);     

                    $payment_detail_data['meta_data'] = "Payment By";
                    $payment_detail_data['value'] = "Self";
                    $sale_paid_created->salePaidDetails()->create($payment_detail_data);
                }
                if( $request->digital_payment_amount!=0 && !is_nan($request->digital_payment_amount )){
                    $customer_order_detail_paid_data['payment_method_id'] = $request->payment_method_id;
                    $customer_order_detail_paid_data['paid_amount'] = $request->digital_payment_amount;
                    $customer_order_detail_paid_data['paid_date'] = date('Y-m-d');
                    //Carbon::now();
                    $sale_paid_created = $sale_created->salePaids()->create($customer_order_detail_paid_data);
                    if($request->payment_details){
                        foreach($request->payment_details as $payment_detail){
                            $payment_detail_data['meta_data'] = $payment_detail['meta_data'];
                            $payment_detail_data['value'] = $payment_detail['value'];
                            $sale_paid_created->salePaidDetails()->create($payment_detail_data);
                        }
                    }
                }
                $return_array['sale_id'] = $sale_created->id;
                Redis::set('success', 'Sale Created Successfully');
            }else{
                  Redis::set('error', 'Error: Sale Not Created, Please Try again');
              }
            return $return_array;
        }
    }
     protected function updateSaleDetail(OutLetProductSaleRequest $outLetProductSale, SaleRequest $request, SalePaidRequest $salePaidRequest, SalePaidDetailRequest $salePaidDetailRequest)
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('edit-sales')){
            Redis::set('error', 'Error: Operation not permitted!!!');
            return $return_array;
        }
        if( $request ){
            if( $request->invoice_prefix_id != null || $request->invoice_prefix_id != '' ){
                $customer_order_details_data['invoice_prefix_id'] = $request->invoice_prefix_id;
            }else{
                $customer_order_details_data['invoice_prefix_id'] = NULL;
            }           
            $customer_order_details_data['out_let_id'] = Redis::get('out_let_id');
            $customer_order_details_data['sales_by'] = Auth::user()->id;
            $customer_order_details_data['customer_id'] = isset($request->customer_id) ? $request->customer_id : null;
            $customer_order_details_data['sale_date'] = Carbon::now();            
            $customer_order_details_data['company_currency_id'] = $request->company_currency_id;
            $customer_order_details_data['sub_total'] = $request->sub_total;
            $customer_order_details_data['discount_amount'] = $request->discount_amount;
            $customer_order_details_data['coupon_id'] = $request->coupon_id;
            $customer_order_details_data['coupon_discount_amount'] = $request->coupon_discount_amount;
            $customer_order_details_data['total_amount_exclusive_vat'] = $request->total_amount_exclusive_vat;
            $customer_order_details_data['vat_amount'] = $request->vat_amount;
            $customer_order_details_data['net_amount'] = $request->net_amount;
            $customer_order_details_data['sales_note'] = isset($request->sales_note) ? $request->sales_note : null;
            $saleObj = Sale::find($request->id);
            $sale_updated = $saleObj->update( $customer_order_details_data );           
            if($request->remove_customer_order_detail_items){
                foreach( $request->remove_customer_order_detail_items as $remove_customer_order_detail_item ){
                    $customer_order_detail_item = OutLetProductSale::where('id',$remove_customer_order_detail_item['id'])->get();
                    $this->removeCustomerOrderDetailItems( $customer_order_detail_item );
                }
            }
            if($request->sale_paid_cash_payment_id && is_null($request->cash_payment_method_id) ){
                $salePaid = SalePaid::find($request->sale_paid_cash_payment_id);
                $salePaid->forceDelete();
            }
            if($request->remove_sale_paid_history){ 
                foreach( $request->remove_sale_paid_history as $remove_sale_paid ){
                    if($remove_sale_paid['payment_method']['name'] == "Master Card" || $remove_sale_paid['payment_method']['name'] == "Visa Card" || $remove_sale_paid['payment_method']['name'] == "Bkash"){
                        $salePaid = SalePaid::find($remove_sale_paid['id']);
                        $salePaid->forceDelete();
                    }
                }          
            }
            if( $sale_updated ){ 
                if( $request->customer_order_detail_items ){
                    foreach ( $request->customer_order_detail_items as $customer_order_detail_item ){
                        $customer_order_detail_item_data['sale_id'] = $request->id;
                        $customer_order_detail_item_data['out_let_product_id'] = $customer_order_detail_item['out_let_product_id'];
                        $customer_order_detail_item_data['name'] = $customer_order_detail_item['name'];
                        $customer_order_detail_item_data['cost_price'] = $customer_order_detail_item['cost_price'];
                        $customer_order_detail_item_data['unit_price'] = $customer_order_detail_item['unit_price'];
                        $customer_order_detail_item_data['quantity'] = $customer_order_detail_item['quantity'];
                        //$out_let_product_sale_created = $sale_created->outLetProductSales()->create($customer_order_detail_item_data);

                        $out_let_product_sale_updated = OutLetProductSale::updateOrCreate(['id' => $customer_order_detail_item['id'] ], $customer_order_detail_item_data);
                    }
                }
                if( $request->cash_payment_method_id ){
                    $customer_order_detail_paid_data['sale_id'] = $request->id;
                    $customer_order_detail_paid_data['payment_method_id'] = $request->cash_payment_method_id;
                    $customer_order_detail_paid_data['paid_amount'] = $request->cash_payment_amount;
                    $customer_order_detail_paid_data['paid_date'] = date('Y-m-d');//Carbon::now();
                    //$sale_paid_created = $sale_created->salePaids()->create($customer_order_detail_paid_data);
                    $sale_paid_updated = SalePaid::updateOrCreate(['id' => $request->sale_paid_cash_payment_id ], $customer_order_detail_paid_data);
                    
                    $payment_detail_data['sale_paid_id'] = $sale_paid_updated->id;
                    $payment_detail_data['meta_data'] = "Payment By";
                    $payment_detail_data['value'] = "Self";
                    $sale_paid_detail_updated = SalePaidDetail::updateOrCreate(['id' => $request->sale_paid_detail_id_for_cash ], $payment_detail_data);
                }
                if( $request->digital_payment_amount!=0 && !is_nan($request->digital_payment_amount )){
                    $customer_order_detail_paid_data['payment_method_id'] = $request->payment_method_id;
                    $customer_order_detail_paid_data['paid_amount'] = $request->digital_payment_amount;
                    $customer_order_detail_paid_data['paid_date'] = date('Y-m-d');
                    //Carbon::now();
                    //$sale_paid_created = $sale_created->salePaids()->create($customer_order_detail_paid_data);
                    $sale_paid_updated = SalePaid::updateOrCreate(['id' => $request->sale_paid_digital_payment_id ], $customer_order_detail_paid_data);
                    $payment_detail_data['sale_paid_id'] = $sale_paid_updated->id;
                    if($request->payment_details){
                        foreach($request->payment_details as $payment_detail){
                            $payment_detail_data['meta_data'] = $payment_detail['meta_data'];
                            $payment_detail_data['value'] = $payment_detail['value'];
                            $sale_paid_detail_updated = SalePaidDetail::updateOrCreate(['id' => $payment_detail['sale_paid_detail_id'] ], $payment_detail_data);
                        }
                    }
                }
                $return_array['sale_id'] = $request->id;
                Redis::set('success', 'Sale Created Successfully');
            }else{
                  Redis::set('error', 'Error: Sale Not Created, Please Try again');
              }
            return $return_array;
        }
    }
    /**
     * Remove customer order detail item based on item id
     *
     * @param  \Illuminate\Http\Request  $request which contains customer_order_detail_items
     * @return Success/Failure Message
     */
    protected function removeCustomerOrderDetailItems( $customer_order_detail_items )
    {
        if( $customer_order_detail_items ){
            $customer_order_detail_item_ids = [];
            foreach( $customer_order_detail_items as $customer_order_detail_item ){
                $customerOrderDetailItem = OutLetProductSale::find($customer_order_detail_item->id);
                $customerOrderDetailItem->forceDelete();
            }
        }
    }
    /**
     * Remove customer order detail digital payment data based on digital payment content
     *
     * @param  \Illuminate\Http\Request  $request which contains remove_sale_paid_history
     * @return Success/Failure Message
     */
    protected function removeSalePaidHistory($remove_sale_paid_history, $cash_payment_method_id){
        $return_array['success'] = $remove_sale_paid_history;
        return $return_array;
        if( $remove_sale_paid_history ){
            foreach( $remove_sale_paid_history as $remove_sale_paid ){
                if($remove_sale_paid->payment_method['name']=="Cash" && is_null($cash_payment_method_id) ){
                    $salePaid = SalePaid::find($remove_sale_paid->id);
                    $salePaid->forceDelete();
                }
                if($remove_sale_paid->payment_method['name'] == "Master Card" || $remove_sale_paid->payment_method['name'] == "Visa Card" || $remove_sale_paid->payment_method['name'] == "Bkash"){
                    //foreach ($remove_sale_paid->sale_paid_details) {
                    //}
                    $salePaid = SalePaid::find($remove_sale_paid->id);
                    $salePaid->forceDelete();
                }
            }
        }
    }
    /**
     * Get customer sale detail data based on customer sale id
     *
     * @param  \Illuminate\Http\Request  $request which contains sale_id
     * @return sale data
     */
    protected function getSaleDetail(Request $request)
    {
        $product_sales_data =  Sale::where('id', $request->sale_id)->with(
            ['salePaids', 'outLetProductSales' => function ($query) { 
                $query->with(['outLetProduct'=>function($query){
                    $query->with(['product'=>function($query){
                        $query->with(['images' => function($query){
                            $query->where('product_images.is_cover_image', '=', '1');
                        }]);
                        $query->with(['categories'=>function($query){
                        
                        }]); 
                        $query->with(['prices'=>function($query){
                            $query->where('product_prices.active', '=', '1');
                            $query->with(['vat'=>function($query){
                            
                            }]); 
                        }]);                       
                        $query->with(['combinations' => function($query){
                            $query->with(['combinationAttributeValues' => function($query){
                                $query->with([ 'attributeValue' => function($query){
                                    $query->with('attribute');
                                }]);
                            }]);
                            $query->select('product_combinations.*', 'out_let_product.*')
                            ->join('out_let_product', function($join) {
                            $join->on('out_let_product.product_combination_id', '=', 'product_combinations.id');                   
                            });
                        }]);                         
                    }]);                    
                }]);                
            }])->first();
            
        if( $product_sales_data ){
            foreach ($product_sales_data->outLetProductSales as $key => $out_let_product_sale) {
                $product_sales_data->outLetProductSales[$key]->categories = $out_let_product_sale->outLetProduct->product->categories;  
                $product_sales_data->outLetProductSales[$key]->vat_percent = $out_let_product_sale->outLetProduct->product->prices[0]->vat->percent;                    
                if( sizeof( $out_let_product_sale->outLetProduct->product->combinations ) <= 0 ){
                    $combinations_obj = (object) array("id" => $out_let_product_sale->outLetProduct->id,
                        "out_let_id" => $out_let_product_sale->outLetProduct->out_let_id,
                        "product_combination_id" => null,
                        "product_id" => $out_let_product_sale->outLetProduct->product_id,
                        "stock_quantity" => $out_let_product_sale->outLetProduct->stock_quantity,
                    );
                    $product_sales_data->outLetProductSales[$key]->combination = "No";
                    $product_sales_data->outLetProductSales[$key]->out_let_product_id = $out_let_product_sale->outLetProduct->id;
                    $product_sales_data->outLetProductSales[$key]->combinations = array($combinations_obj);
                }else{
                    $product_sales_data->outLetProductSales[$key]->combination = "Yes";
                    $product_sales_data->outLetProductSales[$key]->combinations = $out_let_product_sale->outLetProduct->product->combinations; 
                }         
            }
        }
        $product_sales_data->customer_order_detail_items = $product_sales_data->outLetProductSales;
        return Response::json($product_sales_data);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
