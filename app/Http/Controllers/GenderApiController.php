<?php

namespace App\Http\Controllers;

use App\Gender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GenderApiController extends Controller
{
    public function getGenderLists()
    {
        return Gender::all();
    }
}
