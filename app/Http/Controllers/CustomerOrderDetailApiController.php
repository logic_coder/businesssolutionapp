<?php

namespace App\Http\Controllers;
use App\Category;
use App\Company;
use App\Coupon;
use App\OutLet;
use App\CustomerOrderDetail;
use App\CustomerOrderDetailItem;
use App\CustomerOrderDetailPaid;
use App\CustomerOrderDetailPaidDetail;
use App\Http\Requests\CustomerOrderDetailRequest;
use App\Http\Requests\CustomerOrderDetailItemRequest;
use App\Http\Requests\CustomerOrderDetailPaidRequest;
use App\Http\Requests\CustomerOrderDetailPaidDetailRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psy\Util\Json;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use DB;
use Response;

class CustomerOrderDetailApiController extends Controller
{
    protected function saveCustomerOrderDetail(CustomerOrderDetailRequest $request, CustomerOrderDetailItemRequest $customerOrderDetailItemRequest, CustomerOrderDetailPaidRequest $customerOrderDetailPaidRequest, CustomerOrderDetailPaidDetailRequest $customerOrderDetailPaidDetailRequest)
    {        
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-sales-page')){
                return response([
                    'message' => 'Error: Operation not permitted!'
                ],401);
            }
        }
        if( $request ){
            if( $request->invoice_prefix_id != null || $request->invoice_prefix_id != '' ){
                $customer_order_details_data['invoice_prefix_id'] = $request->invoice_prefix_id;
            }else{
                $customer_order_details_data['invoice_prefix_id'] = NULL;
            }
            $customer_order_details_data['company_id'] = Auth::user()->company_id;
            $customer_order_details_data['out_let_id'] = $_COOKIE['out_let_id'];
            $customer_order_details_data['sales_by'] = Auth::user()->id;
            $customer_order_details_data['customer_id'] = $request->customer_id;
            $customer_order_details_data['order_date'] = $request->order_date;
            $customer_order_details_data['expected_delivery_date'] = $request->expected_delivery_date;
            $customer_order_details_data['company_currency_id'] = $request->company_currency_id;
            $customer_order_details_data['total_amount_exclusive_vat'] = $request->total_amount_exclusive_vat;
            $customer_order_details_data['total_vat'] = $request->total_vat;
            $customer_order_details_data['total_amount_inclusive_vat'] = $request->total_amount_inclusive_vat;
            $customer_order_details_data['total_item_discount'] = $request->total_item_discount;
            $customer_order_details_data['coupon_id'] = $request->coupon_id;
            $customer_order_details_data['coupon_discount_amount'] = $request->coupon_discount_amount;
            $customer_order_details_data['net_amount'] = $request->net_amount;

            $customer_order_created = OutLet::find( $customer_order_details_data['out_let_id'] )->customerOrderDetails()->create($customer_order_details_data);

            //$customer_order_created = Company::find( Auth::user()->company_id )->customerOrderDetails()->create($customer_order_details_data);
            $customer_order_details_update_data['order_no'] = Auth::user()->company_id."-".$_COOKIE['out_let_id']."-".Auth::user()->id."-".date("Y").date("m").date("d").$customer_order_created->id;
            $customer_order_details_update_data['invoice_no'] = Auth::user()->company_id.$_COOKIE['out_let_id'].Auth::user()->id.date("Y").date("m").date("d").$customer_order_created->id;
            $customer_order_created->update($customer_order_details_update_data);

            if( $customer_order_created ){
                if( $request->customer_order_detail_items ){
                    foreach ( $request->customer_order_detail_items as $customer_order_detail_item ){
                        $customer_order_detail_item_data['description'] = $customer_order_detail_item['description'];
                        $customer_order_detail_item_data['category_id'] = $customer_order_detail_item['categories'][0]['id'];
                        $customer_order_detail_item_data['quantity'] = $customer_order_detail_item['quantity'];
                        $customer_order_detail_item_data['unit_price'] = $customer_order_detail_item['unit_price'];
                        $customer_order_detail_item_data['weight_tola'] = isset($customer_order_detail_item['weight_tola']) ? $customer_order_detail_item['weight_tola']: null;
                        $customer_order_detail_item_data['weight_ana'] = isset($customer_order_detail_item['weight_ana']) ? $customer_order_detail_item['weight_ana']: null;
                        $customer_order_detail_item_data['weight_rotti'] = isset($customer_order_detail_item['weight_rotti']) ? $customer_order_detail_item['weight_rotti']: null;
                        $customer_order_detail_item_data['weight_milli'] = isset($customer_order_detail_item['weight_milli']) ? $customer_order_detail_item['weight_milli']: null;
                        $customer_order_detail_item_data['material_price'] = $customer_order_detail_item['material_price'];
                        $customer_order_detail_item_data['making_charge'] = $customer_order_detail_item['making_charge'];
                        $customer_order_detail_item_data['price_exclusive_vat'] = $customer_order_detail_item['price_exclusive_vat'];
                        $customer_order_detail_item_data['vat_id'] = isset($customer_order_detail_item['vat_id']) ? $customer_order_detail_item['vat_id']: null;
                        $customer_order_detail_item_data['vat_amount'] = $customer_order_detail_item['vat_amount'];
                        $customer_order_detail_item_data['price_inclusive_vat'] = $customer_order_detail_item['price_inclusive_vat'];
                        $customer_order_detail_item_data['discount_amount'] = $customer_order_detail_item['discount_amount'];
                        $customer_order_detail_item_data['status'] = $customer_order_detail_item['status'];
                        $customer_order_detail_item_created = $customer_order_created->customerOrderDetailItems()->create($customer_order_detail_item_data);
                    }
                }
                if($request->sale_paids){
                    foreach ($request->sale_paids as $sale_paid) { 
                        $customer_order_detail_paid_data['paid_amount'] = 0;
                        /* Insert Sale Paid and Sale Paid Detail */
                        if($request->cash_payment_amount || $request->digital_payment_amount){
                            if($sale_paid['payment_method']['name']=="Cash"){
                                $customer_order_detail_paid_data['paid_amount'] = $request->cash_payment_amount ? $request->cash_payment_amount : 0;

                            }else if($sale_paid['payment_method']['name'] =="Master Card" || $sale_paid['payment_method']['name'] =="Visa Card" || $sale_paid['payment_method']['name'] =="Bkash"){
                                $customer_order_detail_paid_data['paid_amount'] = $request->digital_payment_amount ? $request->digital_payment_amount : 0;     
                            }
                            if( $customer_order_detail_paid_data['paid_amount'] > 0 ){
                                $customer_order_detail_paid_data['payment_method_id'] = $sale_paid['payment_method']['id'];                
                                $customer_order_detail_paid_data['paid_date'] = date('Y-m-d');
                                $customer_order_detail_paid_created = $customer_order_created->customerOrderDetailPaids()->create($customer_order_detail_paid_data);     
                                if($sale_paid['sale_paid_details']){    
                                    foreach($sale_paid['sale_paid_details'] as $payment_detail){                        
                                        $payment_detail_data['meta_data'] = $payment_detail['meta_data'];
                                        $payment_detail_data['value'] = $payment_detail['value'];
                                        $customer_order_detail_paid_created->customerOrderDetailPaidDetails()->create($payment_detail_data);
                                    }
                                } 
                            }                           
                        }
                    }                
                }
                /*
                if($request->paid_amount){
                    $customer_order_detail_paid_data['payment_method_id'] = $request->payment_method_id;
                    $customer_order_detail_paid_data['paid_amount'] = $request->paid_amount;
                    $customer_order_detail_paid_data['paid_date'] = date('Y-m-d');
                    $customer_order_detail_paid_created = $customer_order_created->customerOrderDetailPaids()->create($customer_order_detail_paid_data);
                    if($request->payment_details){
                        foreach($request->payment_details as $payment_detail){
                            $payment_detail_data['meta_data'] = $payment_detail['meta_data'];
                            $payment_detail_data['value'] = $payment_detail['value'];
                            $customer_order_detail_paid_created->customerOrderDetailPaidDetails()->create($payment_detail_data);
                        }
                    }
                }*/
                return response([
                    'customer_order_detail_id' => $customer_order_created->id,
                    'message' =>'Customer Order Created Successfully'
                ],201);
            }else{
                return response([
                    'customer_order_detail_id' => "",
                    'message' => "Error: Customer Order Not Created, Please Try again"
                ],500);
            }
        }
    }
    protected function updateCustomerOrderDetail(CustomerOrderDetailRequest $request, CustomerOrderDetailItemRequest $customerOrderDetailItemRequest, CustomerOrderDetailPaidRequest $customerOrderDetailPaidRequest, CustomerOrderDetailPaidDetailRequest $customerOrderDetailPaidDetailRequest)
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('edit-sales')){
                return response([
                    'message' => 'Error: Operation not permitted!'
                ],401);
            }
        }
        if( $request ) {
            if( $request->invoice_prefix_id != null || $request->invoice_prefix_id != '' ){
                $customer_order_details_data['invoice_prefix_id'] = $request->invoice_prefix_id;
            }else{
                $customer_order_details_data['invoice_prefix_id'] = NULL;
            }
            $customer_order_details_data['out_let_id'] = $_COOKIE['out_let_id'];
            $customer_order_details_data['sales_by'] = Auth::user()->id;
            $customer_order_details_data['customer_id'] = $request->customer_id;
            $customer_order_details_data['order_date'] = $request->order_date;
            $customer_order_details_data['expected_delivery_date'] = $request->expected_delivery_date;
            $customer_order_details_data['company_currency_id'] = $request->company_currency_id;
            $customer_order_details_data['total_amount_exclusive_vat'] = $request->total_amount_exclusive_vat;
            $customer_order_details_data['total_vat'] = $request->total_vat;
            $customer_order_details_data['total_amount_inclusive_vat'] = $request->total_amount_inclusive_vat;
            $customer_order_details_data['total_item_discount'] = $request->total_item_discount;
            $customer_order_details_data['coupon_id'] = $request->coupon_id;
            $customer_order_details_data['coupon_discount_amount'] = $request->coupon_discount_amount;
            $customer_order_details_data['net_amount'] = $request->net_amount;
            $customerOrderDetailObj = CustomerOrderDetail::find($request->id);
            $customer_order_detail_updated = $customerOrderDetailObj->update( $customer_order_details_data );

            if($request->remove_customer_order_detail_items){
                foreach( $request->remove_customer_order_detail_items as $remove_customer_order_detail_item ){
                    $customer_order_detail_item = CustomerOrderDetailItem::where('id',$remove_customer_order_detail_item['id'])->get();
                    $this->remove_customer_order_detail_items( $customer_order_detail_item );
                }
            }
            if( $customer_order_detail_updated ){
                if( $request->customer_order_detail_items ){
                    foreach ( $request->customer_order_detail_items as $customer_order_detail_item ){
                        $customer_order_detail_item_data['customer_order_detail_id'] = $request->id;
                        $customer_order_detail_item_data['description'] = $customer_order_detail_item['description'];
                        $customer_order_detail_item_data['category_id'] = $customer_order_detail_item['categories'][0]['id'];
                        $customer_order_detail_item_data['quantity'] = $customer_order_detail_item['quantity'];
                        $customer_order_detail_item_data['unit_price'] = $customer_order_detail_item['unit_price'];
                        $customer_order_detail_item_data['weight_tola'] = isset($customer_order_detail_item['weight_tola']) ? $customer_order_detail_item['weight_tola']: null;
                        $customer_order_detail_item_data['weight_ana'] = isset($customer_order_detail_item['weight_ana']) ? $customer_order_detail_item['weight_ana']: null;
                        $customer_order_detail_item_data['weight_rotti'] = isset($customer_order_detail_item['weight_rotti']) ? $customer_order_detail_item['weight_rotti']: null;
                        $customer_order_detail_item_data['weight_milli'] = isset($customer_order_detail_item['weight_milli']) ? $customer_order_detail_item['weight_milli']: null;
                        $customer_order_detail_item_data['material_price'] = $customer_order_detail_item['material_price'];
                        $customer_order_detail_item_data['making_charge'] = $customer_order_detail_item['making_charge'];
                        $customer_order_detail_item_data['price_exclusive_vat'] = $customer_order_detail_item['price_exclusive_vat'];
                        $customer_order_detail_item_data['vat_id'] = isset($customer_order_detail_item['vat_id']) ? $customer_order_detail_item['vat_id']: null;
                        $customer_order_detail_item_data['vat_amount'] = $customer_order_detail_item['vat_amount'];
                        $customer_order_detail_item_data['price_inclusive_vat'] = $customer_order_detail_item['price_inclusive_vat'];
                        $customer_order_detail_item_data['discount_amount'] = $customer_order_detail_item['discount_amount'];                        $customer_order_detail_item_data['status'] = $customer_order_detail_item['status'];
                        $customer_order_detail_item_data['status'] = $customer_order_detail_item['status'];
                        $customer_order_detail_item_updated = CustomerOrderDetailItem::updateOrCreate(['id' => $customer_order_detail_item['id'] ], $customer_order_detail_item_data);
                    }
                }                
                /* Remove Digital Payment entry when no payment method is get selected */
                if($request->remove_sale_paid_history){ 
                    foreach( $request->remove_sale_paid_history as $remove_sale_paid ){
                        if($remove_sale_paid['payment_method']['name'] == "Master Card" || $remove_sale_paid['payment_method']['name'] == "Visa Card" || $remove_sale_paid['payment_method']['name'] == "Bkash"){
                            $orderPaid = CustomerOrderDetailPaid::find($remove_sale_paid['id']);
                            $orderPaid->forceDelete();
                            break;
                        }
                    }          
                }
                /* Insert/Update/Delete Sale Paid and Sale Paid Detail Based On Condition*/
                if($request->sale_paids){
                    foreach ($request->sale_paids as $sale_paid) { 
                        $customer_order_detail_paid_data['paid_amount'] = 0;
                        /* Remove Cash Payment entry when paid_amount <= 0 */
                        if($request->cash_payment_amount <= 0 && $sale_paid['id'] && $sale_paid['payment_method']['name']=="Cash") {
                            $orderPaid = CustomerOrderDetailPaid::find($sale_paid['id']);
                            $orderPaid->forceDelete();                            
                        }   
                        /* Insert or Update Sale Paid and Sale Paid Detail */
                        if($request->cash_payment_amount || $request->digital_payment_amount){
                            if($sale_paid['payment_method']['name']=="Cash"){
                                $customer_order_detail_paid_data['paid_amount'] = $request->cash_payment_amount ? $request->cash_payment_amount : 0;

                            }else if($sale_paid['payment_method']['name'] =="Master Card" || $sale_paid['payment_method']['name'] =="Visa Card" || $sale_paid['payment_method']['name'] =="Bkash"){
                                $customer_order_detail_paid_data['paid_amount'] = $request->digital_payment_amount ? $request->digital_payment_amount : 0;     
                            }
                            if( $customer_order_detail_paid_data['paid_amount'] > 0 ){
                                $customer_order_detail_paid_data['customer_order_detail_id'] = $request->id;            
                                $customer_order_detail_paid_data['payment_method_id'] = $sale_paid['payment_method']['id'];                
                                $customer_order_detail_paid_data['paid_date'] = date('Y-m-d');                                
                                $order_paid_updated = CustomerOrderDetailPaid::updateOrCreate(['id' => $sale_paid['id'] ], $customer_order_detail_paid_data);
                                if($sale_paid['sale_paid_details']){
                                    $payment_detail_data['customer_order_detail_paid_id'] = $order_paid_updated->id;
                                    foreach($sale_paid['sale_paid_details'] as $payment_detail){
                                        $payment_detail_data['id'] = $payment_detail['id'];
                                        $payment_detail_data['meta_data'] = $payment_detail['meta_data'];
                                        $payment_detail_data['value'] = $payment_detail['value'];
                                        $order_paid_detail_updated = CustomerOrderDetailPaidDetail::updateOrCreate(['id' => $payment_detail_data['id'] ], $payment_detail_data);
                                    }
                                } 
                            }                           
                        }
                    }                
                }
                if(isset($request->new_payment)){
                    $message = "Customer Order Paid Successfully";
                }else{
                    $message = "Customer Order Updated Successfully";
                }
                return response([
                    'customer_order_detail_id' =>  $request->id,
                    'message' =>$message
                ],200);
            }else{
                if(isset($request->new_payment)){
                    $message = "Error: Customer Order Paid Not Deleted, Please Try again";
                }else{
                    $message = "Error: Customer Order Not Updated, Please Try again";
                }
                
                return response([
                    'customer_order_detail_id' => "",
                    'message' => $message
                ],500);
            }           
            return $return_array;
        }
    }
    protected function remove_customer_order_detail_items( $customer_order_detail_items )
    {
        if( $customer_order_detail_items ){
            $customer_order_detail_item_ids = [];
            foreach( $customer_order_detail_items as $customer_order_detail_item ){
                $customerOrderDetailItem = CustomerOrderDetailItem::find($customer_order_detail_item->id);
                $customerOrderDetailItem->forceDelete();
            }
        }
    }
    /**
     * Get customer order detail data based on customer order id
     *
     * @param  \Illuminate\Http\Request  $request which contains customer_order_detail_id
     * @return sale data
     */
    protected function getCustomerOrderDetail(Request $request)
    {
        $customer_order_detail_data = CustomerOrderDetail::where('id', $request->customer_order_detail_id)->with(
          ['customerOrderDetailItems' => function ($query) { }])->first();
               
        if( $customer_order_detail_data ){ 
            $salePaidHistory = []; 
            if($request->sale_paid_history == "Yes"){          
                $salePaidHistory = CustomerOrderDetailPaid::where('customer_order_detail_id', $request->customer_order_detail_id)->with('customerOrderDetailPaidDetails','paymentMethod')->get();
            }
            if(sizeof($salePaidHistory) <= 0){
                /* For Cash Payment */
                $sale_paid_history_obj = (object) array("id" => null, 
                );
                $payment_method_obj = (object) array("id" => $request->cash_payment_method_id, "name" => 'Cash'
                );
                $sale_paid_details_obj = (object) array("id" => null, "meta_data" => 'Payment By',"value" => 'Self'
                );
                $sale_paid_history_obj->payment_method = $payment_method_obj; 
                $sale_paid_history_obj->sale_paid_details = array($sale_paid_details_obj); 
                $salePaidHistory[0] = $sale_paid_history_obj;

                /* For Digital Payment */
                $sale_paid_history_obj = (object) array("id" => null,
                );
                $payment_method_obj = (object) array("name" => 'Select Payment Method'
                );
                $sale_paid_details_obj = (object) array("id" => null, "meta_data" => '',"value" => ''
                );
                $sale_paid_history_obj->payment_method = $payment_method_obj; 
                $sale_paid_history_obj->sale_paid_details[0] = $sale_paid_details_obj; 
                $sale_paid_history_obj->sale_paid_details[1] = $sale_paid_details_obj;
                $salePaidHistory[1] = $sale_paid_history_obj; 

            }else if(sizeof($salePaidHistory) == 1 ){         
                foreach($salePaidHistory as $key => $sale_paid) {            
                    if($sale_paid->paymentMethod->name != "Cash"){
                        /* For Cash Payment */
                        $sale_paid_history_obj = (object) array("id" => null,
                        );
                        $payment_method_obj = (object) array("id" => $request->cash_payment_method_id, "name" => 'Cash'
                        );
                        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => 'Payment By',"value" => 'Self'
                        );
                        $sale_paid_history_obj->payment_method = $payment_method_obj; 
                        $sale_paid_history_obj->sale_paid_details = array($sale_paid_details_obj); 
                        $salePaidHistory[$key+1] = $sale_paid_history_obj;
                        /* For Digital Payment*/
                        $customer_order_detail_data->digital_payment_amount = $sale_paid->paid_amount;
                        $customer_order_detail_data->cash_payment_amount = 0;
                        $customer_order_detail_data->paymentMethod = $sale_paid->paymentMethod->name;
                    }else{
                        /* For Digital Payment */
                        $sale_paid_history_obj = (object) array("id" => null, 
                        );
                        $payment_method_obj = (object) array("name" => 'Select Payment Method'
                        );
                        $sale_paid_details_obj = (object) array("id" => null, "meta_data" => '',"value" => ''
                        );
                        $sale_paid_history_obj->payment_method = $payment_method_obj; 
                        $sale_paid_history_obj->sale_paid_details[0] = $sale_paid_details_obj; 
                        $sale_paid_history_obj->sale_paid_details[1] = $sale_paid_details_obj;
                        $salePaidHistory[$key+1] = $sale_paid_history_obj; 
                        /* For Cash Payment*/
                        $customer_order_detail_data->cash_payment_amount = $sale_paid->paid_amount;
                        $customer_order_detail_data->digital_payment_amount = 0;
                    }
                }            
            }
            else if(sizeof($salePaidHistory) == 2 ){         
                foreach($salePaidHistory as $key => $sale_paid) {  
                    if($sale_paid->paymentMethod->name == "Cash"){
                        $customer_order_detail_data->cash_payment_amount = $sale_paid->paid_amount;
                    }else if($sale_paid->paymentMethod->name == "Master Card" || $sale_paid->paymentMethod->name == "Visa Card" || $sale_paid->paymentMethod->name == "Bkash"){ 
                        $customer_order_detail_data->digital_payment_amount = $sale_paid->paid_amount;
                        $customer_order_detail_data->paymentMethod = $sale_paid->paymentMethod->name;
                    }
                }
            }
            $customer_order_detail_data->sale_paids = $salePaidHistory;
             foreach ($customer_order_detail_data->customerOrderDetailItems as $key => $order_items) {                                   
                if( $order_items->category_id ){
                    $category_obj = (object) array("id" => $order_items->category_id                        
                    );                    
                    $customer_order_detail_data->customerOrderDetailItems[$key]->categories = array($category_obj);
                }        
            }
        }                
        return Response::json($customer_order_detail_data);        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeCustomerOrderDetail( Request $request )
    {
        $return_array['success'] = "";
        if(!Auth::user()->can('delete-sales')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids)){
            $query_result = DB::table('customer_order_detail_paid_details')
                ->join('customer_order_detail_paids','customer_order_detail_paid_details.customer_order_detail_paid_id' ,'=' ,'customer_order_detail_paids.id')
                ->whereIn('customer_order_detail_paids.customer_order_detail_id', $request->item_ids)
                ->update(['customer_order_detail_paid_details.deleted_at' => DB::raw('NOW()')]);
            DB::table('customer_order_detail_paids')->whereIn('customer_order_detail_id', $request->item_ids )->update(['deleted_at' => DB::raw('NOW()')]);
            DB::table('customer_order_detail_items')->whereIn('customer_order_detail_id', $request->item_ids )->update(['deleted_at' => DB::raw('NOW()')]);
            $customer_order_detail_deleted = DB::table('customer_order_details')->whereIn('id', $request->item_ids )->update(['deleted_at' => DB::raw('NOW()')]);
            if( $customer_order_detail_deleted ){
                return response([
                    'message' => 'Customer Order Deleted Successfully'
                ],200);
            }else{
                return response([
                    'message' => 'Error: Customer Order Not Deleted, Please Try again'
                ],500);                
            }
        }
    }    
}
