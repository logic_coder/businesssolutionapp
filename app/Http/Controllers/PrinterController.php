<?php

namespace App\Http\Controllers;
use App\Company; 
use App\Printer; 
use Illuminate\Http\Request;
use App\Http\Requests\PrinterRequest;
use Illuminate\Support\Facades\Auth;
 
 
class PrinterController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('access-outlet-printer-settings')) {
            abort(401);
        }
        $printers = Printer::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('printer.index', compact('printers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('access-outlet-printer-settings')) {
            abort(401);
        }
        $status_lists = ['active' => 'Active','in-active' => 'Inactive'];
        $type_lists = ['pos' => 'Pos','regular' => 'Regular'];
        return view('printer.create',compact('status_lists','type_lists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrinterRequest $request)
    {
        if(!Auth::user()->can('access-outlet-printer-settings')) {
            abort(401);
        } 
        $printer = Company::find(Auth::user()->company_id)->printers()->create($request->all());
        if( $request->status && $request->status=='active' ){
            $data['status'] = 'in-active';
            //$this->toggleStatus( $data , $printer->id );
        }
        return redirect('printer')->with('success','Printer added successfully!');           
    }

    /**
     * Toggle Status for the specified resource.
     *
     * @param  $data ,int  $price_id
     * @return \Illuminate\Http\Response
     */
    protected function toggleStatus( $data , $printer_id )
    {
        Printer::where('id', '!=', $printer_id )
            ->where('company_id', '=', Auth::user()->company_id)->update( $data );
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Printer $printer)
    {
        if(!Auth::user()->can('access-outlet-printer-settings')) {
            abort(401);
        }
        //$company_printer =  CompanyPrinter::find($id);
        $status_lists = ['active' => 'Active','in-active' => 'Inactive'];
        $type_lists = ['pos' => 'Pos','regular' => 'Regular'];
        return view('printer.edit',compact('printer','status_lists','type_lists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrinterRequest $request, Printer $printer)
    {
        if(!Auth::user()->can('access-outlet-printer-settings')) {
            abort(401);
        }
        $printer->update( $request->all() );

        if( $request->status && $request->status=='active' ){
            $data['status'] = 'in-active';
            //$this->toggleStatus( $data , $printer->id );
        }
        return redirect('printer')->with('success','Printer updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
