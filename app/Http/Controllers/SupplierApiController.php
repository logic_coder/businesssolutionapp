<?php

namespace App\Http\Controllers;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupplierApiController extends Controller
{

    protected function getSuppliers()
    {
        return Supplier::all();
    }
    protected function getSupplierData( Request $request )
    {
        return Supplier::where('id', $request->supplier_id)->first();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeSupplier( Request $request )
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('access-vendor-supplier')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids)){
            Supplier::destroy( $request->item_ids );
            return response([
                'message' => 'Supplier deleted successfully!'
            ],200);
        }
    }
}
