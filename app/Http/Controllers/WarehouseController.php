<?php

namespace App\Http\Controllers;

use App\Company;
use App\Warehouse;
use Illuminate\Http\Request;
use App\Http\Requests\WarehouseRequest;
use Illuminate\Support\Facades\Auth;
class WarehouseController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('access-warehouse')) {
            abort(401);
        }
        $warehouses = Warehouse::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('warehouse.index', compact('warehouses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('access-warehouse')) {
            abort(401);
        }
        return view('warehouse.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WarehouseRequest $request)
    {
        if(!Auth::user()->can('access-warehouse')) {
            abort(401);
        }
        Company::find(Auth::user()->company_id)->warehouses()->create($request->all());
        return redirect()->back()->with('success','Warehouse added successfully!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Warehouse $warehouse
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Warehouse $warehouse)
    {
        if(!Auth::user()->can('access-warehouse')){
            abort(401);
        }
        return view('warehouse.edit', compact('warehouse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param WarehouseRequest|Request $request
     * @param Warehouse $warehouse
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(WarehouseRequest $request, Warehouse $warehouse)
    {
        if(!Auth::user()->can('access-warehouse')){
            abort(401);
        }
        $warehouse->update( $request->all() );
        return redirect()->back()->with('success','Warehouse updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Warehouse $warehouse )
    {
        if(!Auth::user()->can('access-warehouse')){
            abort(401);
        }
        if(!empty( $warehouse )){
            Warehouse::destroy( $warehouse->id );
            flash('Warehouse deleted successfully', 'success');
        }
        return redirect()->back();
    }

    /**
     * Remove specified resources from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeWarehouse( Request $request )
    {
        if(!Auth::user()->can('access-warehouse')){
            abort(401);
        }
        if(!empty( $request->warehouse_ids )){
            Warehouse::destroy( $request->warehouse_ids );
            flash('Warehouse deleted successfully', 'success');
        }
        return redirect()->back();
    }
}
