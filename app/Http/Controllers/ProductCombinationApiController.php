<?php

namespace App\Http\Controllers;
use App\Company;
use App\Attribute;
use App\AttributeValue;
use App\WarehouseStoreInChallanProduct;
use Illuminate\Http\Request;
use App\Product;
use App\ProductCombination;
use App\ProductCombinationAttribute;
use App\ProductCombinationWarehouseStoreInChallanProduct;
use App\Http\Requests\ProductCombinationRequest;
use App\Http\Requests\ProductCombinationAttributeRequest;
use Illuminate\Support\Facades\Auth;
use Psy\Util\Json;
use DB;

class ProductCombinationApiController extends Controller
{
    protected function generateCombinations($attribute_value_arrays, $index = 0) {
        if (!isset($attribute_value_arrays[$index])) {
            return array();
        }
        if ($index == count($attribute_value_arrays) - 1) {
            return $attribute_value_arrays[$index];
        }
        // generate combinations
        $temp = $this->generateCombinations($attribute_value_arrays, $index + 1);
        $results = array();
        // concat each array from temp with each element from $arrays[$index]
        foreach ($attribute_value_arrays[$index] as $value) {
            foreach ($temp as $temp_value) {
                $results[] = is_array($temp_value) ?
                    array_merge(array($value), $temp_value) :
                    array($value, $temp_value);
            }
        }
        return $results;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function saveProductCombination(ProductCombinationRequest $request)
    {
        $return_array['success'] = '';
        $message = "";
        $product_combination_object = '';
        $product_combination_attribute = '';
        if( $request ){
            if( $request->combination_values ){
                $combinations = $this->generateCombinations($request->combination_values);
                foreach( $combinations as $combination ){
                    $product_combination['product_id'] = $request->product_id;
                    $product_combination_created = ProductCombination::create($product_combination);
                    if( is_array($combination) ){
                        foreach( $combination as $attribute_value ){
                            $combination_data['attribute_value_id'] = $attribute_value;
                            $product_combination_attribute = $product_combination_created->combinationAttributeValues()->create($combination_data);
                        }
                    }else{
                        $combination_data['attribute_value_id'] = $combination;
                        $product_combination_attribute = $product_combination_created->combinationAttributeValues()->create($combination_data);
                    }
                }
            }
        }
        if($product_combination_created){
            $message = 'Product Combination Created Successfully';
        }else{
            $message = 'Error: Product Combination Not Created, Please Try again';
        }
        return response([
            'product_id' => $request->product_id,
            'message'=> $message
        ]); 
    }
    /**
     * Update resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function updateProductCombination(ProductCombinationRequest $request)
    {
        $return_array['success'] = '';
        $product_combination_object = '';
        $product_combination_attribute = '';
        if( $request ){
            if( $request->combination_values ){
                $combinations = $this->generateCombinations($request->combination_values);
                foreach( $combinations as $combination ){
                    $product_combination = new ProductCombination();
                    $product_combination->product_id = $request->product_id;
                    $product_combination_object = $product_combination->save();
                    if( is_array($combination) ){
                        foreach( $combination as $attribute_value ){
                            $combination_data['attribute_value_id'] = $attribute_value;
                            $product_combination_attribute = $product_combination->combinationAttributeValues()->create($combination_data);
                        }
                    }else{
                        $combination_data['attribute_value_id'] = $combination;
                        $product_combination_attribute = $product_combination->combinationAttributeValues()->create($combination_data);
                    }
                }
            }
        }
        if($product_combination_object){
            $message = 'Product Combination Updated Successfully';
        }else{
            $message = 'Error: Product Combination Not Updated, Please Try again';
        }
        return response([
            'product_id' => $request->product_id,
            'message'=> $message
        ]); 
    }
    protected function getProductCombinationData( Request $request )
    {
        return Product::where('id', $request->product_id )->with(
            ['combinations' => function($query){
                    $query->with([
                        'combinationAttributeValues' => function($query){
                            $query->with(['attributeValue' => function($query){
                                $query->with('attribute');
                            }]);
                        }
                    ]);
                }
            ]
        )->orderBy('id', 'ASC')->first();
//        if (isset($request->challan_product_id) && sizeof($combination_data->combinations) <= 0 ) {
//            $product_combination = WarehouseStoreInChallanProduct::with('product_combination')->where('id', '=', $request->challan_product_id)->orderBy('id', 'ASC')->first();
//            foreach ( $product_combination->product_combination as $combination){
//                if($combination->product_combination_id == null){
//                    $product_combination->combination = "No";
//                    $product_combination->warehouse_store_in_challan_product_id = $product_combination->product_combination[0]->warehouse_store_in_challan_product_id;
//                    $combinations_obj = (object) array("requested_quantity" => $product_combination->product_combination[0]->requested_quantity,"received_quantity" => $product_combination->product_combination[0]->received_quantity,"missing_quantity" => null,"product_id" =>$product_combination->product_id);
//                    $product_combination->combinations = array($combinations_obj);
//                    $product_combination->name = $combination_data->name;
//                }
//            }
//            return $product_combination;
//        }else{
//            $combination_data->product_id = $request->product_id;
//            $combination_data->combination = "Yes";
//            return $combination_data;
//        }
    }
    /**
     * Remove multiple resources from storage.
     *
     * @param  $product_combination_attributes object
     * @return \Illuminate\Http\Response
     */
    public function removeProductCombinationAttributes( $product_combination_attributes )
    {
        foreach( $product_combination_attributes as $product_combination_attribute )
        {
            ProductCombinationAttribute::destroy( $product_combination_attribute->id );
        }
        return true;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteProductCombinationData( Request $request )
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('access-product-combination')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->combination_ids )){
            foreach( $request->combination_ids as $combination_id ){
                $product_combination_attributes = ProductCombinationAttribute::where('product_combination_id', '=', $combination_id)->get();
                $this->removeProductCombinationAttributes( $product_combination_attributes );
            }
            ProductCombination::destroy( $request->combination_ids );
            return response([
                'message' => 'Product combination deleted successfully!'
            ],200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeProductCombinationsByProduct( Request $request )
    {                
        $return_array['success'] = "";
        if(!Auth::user()->can('access-product-combination')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids )){
            $query_result = DB::table('product_combination_attributes')
                ->join('product_combinations','product_combination_attributes.product_combination_id' ,'=' ,'product_combinations.id')
                ->whereIn('product_combinations.product_id', $request->item_ids)
                ->update(['product_combination_attributes.deleted_at' => DB::raw('NOW()')]);
            DB::table('product_combinations')->whereIn('product_id', $request->item_ids )->update(['deleted_at' => DB::raw('NOW()')]);
            return response([
                'message' => 'Product Combination Deleted Successfully!'
            ],200);
        }
    }
}
