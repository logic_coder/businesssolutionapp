<?php
namespace App\Http\Controllers;
use App\Company;
use App\Vat;
use App\Product;
use App\ProductPrice;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductPriceRequest;
use Illuminate\Support\Facades\Auth;

class ProductPriceController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @param Attribute $attribute
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {        
        if(!Auth::user()->can('access-product-prices')) {
            abort(401);
        }
        return view('product_price.index', compact('product'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        if(!Auth::user()->can('access-product-prices')) {
            abort(401);
        }
        $vat_lists = Vat::orderBy('percent', 'ASC')->pluck('percent','id');
        $status_lists = ['1' => 'Active','0' => 'Inactive'];
        $tab_active = 'price';
        return view('product_price.create', compact('vat_lists','product','status_lists','tab_active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductPriceRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductPriceRequest $request, Product $product)
    {
        if(!Auth::user()->can('access-product-prices')) {
            abort(401);
        } 
        $price = $product->prices()->create($request->all());
        if( $request->active ){
            $data['active'] = '0';
            $this->toggleStatus( $data , $price->id, $product->id );
        }
        return redirect()->back()->with('success','Product Price added successfully!');    
    }

    /**
     * Toggle Status for the specified resource.
     *
     * @param  $data ,int  $price_id
     * @return \Illuminate\Http\Response
     */
    protected function toggleStatus( $data , $price_id , $product_id )
    {
        ProductPrice::where('id', '!=', $price_id )->where('product_id', '=', $product_id )->update( $data );
        return true;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, ProductPrice $product_price)
    {
        if(!Auth::user()->can('access-product-prices')){
            abort(401);
        }
        $vat_lists = Vat::orderBy('percent', 'ASC')->pluck('percent','id');
        $status_lists = ['1' => 'Active','0' => 'Inactive'];
        $tab_active = 'price';
        return view('product_price.edit', compact('vat_lists','product','product_price','status_lists','tab_active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductPriceRequest $request, Product $product, ProductPrice $product_price)
    {
        
        if(!Auth::user()->can('access-product-prices')){
            abort(401);
        }
        $product_price->update( $request->all() );
        if( $request->active ){
            $data['active'] = '0';
            $this->toggleStatus( $data , $product_price->id , $product->id );
        }        
        //return redirect()->back()->with('success','Product Price updated successfully');
        return redirect('product/'.$product->id.'/product_price')->with('success','Product Price updated successfully!');     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product, ProductPrice $product_price)
    {
        
    }

}
