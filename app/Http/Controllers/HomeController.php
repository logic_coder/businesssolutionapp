<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        echo bcrypt('123456');exit;
        if(!Auth::check()){
            return view('landing_page.index');
        }else{
            return redirect('dashboard');
        }
    }
    public function logout(Request $request)
    {
         // Log out the user
        Auth::logout();

        // Optionally, invalidate the session
        $request->session()->invalidate();

        // Regenerate the session token for security
        $request->session()->regenerateToken();

        // Redirect to home or login page with a success message
        
        return response()->json(['status' => 'success', 'message' => 'Logged out successfully.', 200]);

    }
}
