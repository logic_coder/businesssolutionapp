<?php
namespace App\Http\Controllers;
use App\Company; 
use App\OutLet;
use App\Printer;
use App\OutLetPrinter;  
use Illuminate\Http\Request;
use App\Http\Requests\OutLetPrinterRequest;
use Illuminate\Support\Facades\Auth;

class OutLetPrinterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Printer $printer)
    {
        if(!Auth::user()->can('access-outlet-printer-settings')) {
            abort(401);
        }
        $printer_out_lets = $printer->outlets;
        $out_let_lists = OutLet::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->pluck('name','id');
        return view('outlet_printer.create', compact('printer','out_let_lists','printer_out_lets'));
    }

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request, Printer $printer)
    {               
        $printer->outlets()->sync( $request->out_let_ids );
        return redirect('printer/'.$printer->id.'/outlet_printer')->with('success','Printer Assigned successfully!');            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
