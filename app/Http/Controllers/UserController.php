<?php

namespace App\Http\Controllers;
use App\Company;
use App\Http\Requests\UserDetailRequest;
use App\Http\Requests\UserRequest;
use App\OutLet;
use App\User;
use App\UserDetail;
use App\UserTitle;
use App\RoleUser;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use  Validator,  Redirect, Input;

class UserController extends Controller
{
    public function __construct(){
        if(!isset($_COOKIE['user_id'])){
            $this->middleware('auth');
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('create-view-users')){
            abort(401);
        }
        $users = User::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->get();
        // dd($users);
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('create-view-users')){
            abort(401);
        }
        $role_lists = Role::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->pluck('name','id');
        $out_let_lists = OutLet::where('company_id', Auth::user()->company_id)->orderBy('id', 'ASC')->pluck('name','id');
        return view('user.create', compact('role_lists','out_let_lists'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if(!Auth::user()->can('create-view-users')){
            abort(401);
        }
        //$user_data = $request->all();        
        $user_data['email'] = $request->email;
        $user_data['password'] = bcrypt( $request->password );
        $user_data['active'] = "1";
        //dd($user_data);
        $companies = Company::find(Auth::user()->company_id);
        $user = $companies->users()->create( $user_data );
        $role = Role::find( $request->role_id );
        $role->users()->attach( $user->id );
        $user->out_lets()->sync( $request->out_let_ids );
        return redirect('user')->with('success','User added successfully!');           
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if(!Auth::user()->can('edit-update-user')){
            abort(401);
        }  
        $user_detail = $user->userDetail;
        $user_out_lets = $user->out_lets;
        $user_id = $user->id;
        $company_id = Auth::user()->company_id;
        $role_lists = Role::where('company_id', $company_id)->orderBy('id', 'ASC')->pluck('name','id');
        $title_lists = UserTitle::where('company_id', $company_id)->orderBy('id', 'ASC')->pluck('title','id');
        $out_let_lists = OutLet::where('company_id', $company_id)->orderBy('id', 'ASC')->pluck('name','id');
        return view('user.edit', compact('user','user_detail','role_lists','title_lists','out_let_lists','user_out_lets', 'company_id','user_id'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, UserDetailRequest $detail_request, User $user)
    {
        
        if(!Auth::user()->can('edit-update-user')){
            abort(401);
        }  
        $user->update($request->all());
        if( $user->userDetail ){
            $user_detail = $user->userDetail;
            $user_detail->update($request->all());
        }else{
            $user->userDetail()->create($request->all());
        }
        if( $user->roles ){
            foreach( $user->roles as $user_role ){
                $user_role = Role::find( $user_role->id );
                $user->roles()->detach( $user_role->id );
            }
        }
        $user->out_lets()->sync( $request->out_let_ids );
        $user->roles()->attach($request->role_id );
        return redirect('user')->with('success','User updated successfully!');     
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function frontendEdit(User $user)
    {
        if(!isset($_COOKIE['user_id'])){
            abort(401);
        }  
        $user_id = $_COOKIE['user_id'];
        $user = User::find( $user_id );
        $company_id = $user->company_id;
        $user_detail = $user->userDetail;
        $user_out_lets = $user->out_lets;
        $role_lists = Role::where('company_id', $company_id)->orderBy('id', 'ASC')->pluck('name','id');
        $title_lists = UserTitle::where('company_id', $company_id)->orderBy('id', 'ASC')->pluck('title','id');
        $out_let_lists = OutLet::where('company_id', $company_id)->orderBy('id', 'ASC')->pluck('name','id');
        return view('user.frontend-edit', compact('user','user_detail','role_lists','title_lists','out_let_lists','user_out_lets','company_id','user_id'));
    }

    public function frontendUserUpdate(Request $request, $id ){
        if(!isset($_COOKIE['user_id'])){
            abort(401);
        }    
        /////////////////
            $inputs = $request->all();
                     
            $rules = [];
            if( is_numeric( $inputs['email'] ) ){
                $rules = array_merge($rules,['email' => 'required|regex:/(01)[0-9]{9}$/|unique:users,email,'.$id]);
            }else if( !is_numeric( $inputs['email'] ) ){
                $rules = array_merge($rules,['email' => 'required|email|unique:users,email,'.$id]);
            }


            if( !empty( $inputs['contact_email'] ) ){
                $rules = array_merge($rules,['contact_email' => 'email|unique:users,contact_email,'.$id]);
            }
            if( !empty( $inputs['work_phone'] ) ){
                $rules = array_merge($rules,['work_phone' => 'regex:/(01)[0-9]{9}$/']);
            }
            $rules = array_merge($rules,['first_name' => 'required|min:3|max:20']);
            $rules = array_merge($rules,['last_name' => 'required|min:3|max:20']);
            $rules = array_merge($rules,['date_of_birth' => 'required']);
            $rules = array_merge($rules,['cell_phone' => 'required|regex:/(01)[0-9]{9}$/']);
            $rules = array_merge($rules,['address' => 'required|min:15|max:200']);
            $rules = array_merge($rules,['city' => 'required|min:3|max:50']);
            $rules = array_merge($rules,['zip' => 'required|regex:/^[0-9]{4}$/']);

            $messages = array(
                'email.required' => 'Login Id is required',   
                'email.unique' => 'Login Id is already exist', 
                'email.regex' => 'Login Id format is invalid',               
            );

            $validation = Validator::make($inputs, $rules, $messages);

            if ($validation->fails()) {
                session()->flash('alert-class', 'alert-danger');
                session()->flash('status', ["<strong>Error!<strong>", "Validation failed. Please try again later!"]);
                return redirect()->back()->withErrors($validation->messages())->withInput();
            }

        /////////////////
        $user = User::find($id);
        $user->update($request->all());
        if( $user->userDetail ){
            $user_detail = $user->userDetail;
            $user_detail->update($request->all());
        }else{
            $user->userDetail()->create($request->all());
        }       
        return redirect()->back()->with('success','User updated successfully!');   
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( User $user )
    {
        
    }
    /**
     * Remove specified resources from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeAttributes( Request $request )
    {
        if(!Auth::user()->can('access-product-attributes')){
            abort(401);
        }
        if(!empty( $request->attribute_ids )){
            Attribute::destroy( $request->attribute_ids );
            flash('Attribute deleted successfully', 'success');
        }
        return redirect('attribute');
    }
}
