<?php

namespace App\Http\Controllers;
use App\Company; 
use App\CompanyPrinter; 
use Illuminate\Http\Request;
use App\Http\Requests\CompanyPrinterRequest;
use Illuminate\Support\Facades\Auth;
 
 
class CompanyPrinterController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('access-company-printer-settings')) {
            abort(401);
        }
        $company_printers = CompanyPrinter::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('company_printer.index', compact('company_printers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('access-company-printer-settings')) {
            abort(401);
        }
        $status_lists = ['active' => 'Active','in-active' => 'Inactive'];
        $type_lists = ['pos' => 'Pos','regular' => 'Regular'];
        return view('company_printer.create',compact('status_lists','type_lists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyPrinterRequest $request)
    {
        if(!Auth::user()->can('access-company-printer-settings')) {
            abort(401);
        } 
        $company_printer = Company::find(Auth::user()->company_id)->companyPrinter()->create($request->all());
        if( $request->status && $request->status=='active' ){
            $data['status'] = 'in-active';
            $this->toggleStatus( $data , $company_printer->id );
        }
        return redirect('company_printer')->with('success','Company Printer added successfully!');    
    }

    /**
     * Toggle Status for the specified resource.
     *
     * @param  $data ,int  $price_id
     * @return \Illuminate\Http\Response
     */
    protected function toggleStatus( $data , $company_printer_id )
    {
        CompanyPrinter::where('id', '!=', $company_printer_id )
                    ->where('company_id', '=', Auth::user()->company_id)->update( $data );
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyPrinter $company_printer)
    {
        if(!Auth::user()->can('access-company-printer-settings')) {
            abort(401);
        }
        //$company_printer =  CompanyPrinter::find($id);
        $status_lists = ['active' => 'Active','in-active' => 'Inactive'];
        $type_lists = ['pos' => 'Pos','regular' => 'Regular'];
        return view('company_printer.edit',compact('company_printer','status_lists','type_lists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyPrinterRequest $request, CompanyPrinter $company_printer)
    {
        if(!Auth::user()->can('access-company-printer-settings')) {
            abort(401);
        }
        $company_printer->update( $request->all() );

        if( $request->status && $request->status=='active' ){
            $data['status'] = 'in-active';
            $this->toggleStatus( $data , $company_printer->id );
        }
        return redirect('company_printer')->with('success','Company Printer updated successfully!');           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
