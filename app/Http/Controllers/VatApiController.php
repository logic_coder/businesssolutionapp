<?php

namespace App\Http\Controllers;
use App\Vat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VatApiController extends Controller
{
    /**
     * Get VatLists For Specific Company
     *
     * @return VatLists Object
     */
    public function getVatLists()
    {
        return Vat::where('company_id', Auth::user()->company_id)->orderby('id','DESC')->get();
    }
    public function getVatPercent( Request $request )
    {
        return Vat::find( $request->vat_id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeVat( Request $request )
    {
        $return_array['message'] = '';
        if(!Auth::user()->can('access-global-vat-settings')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids )){
            Vat::destroy( $request->item_ids );
            return response([
                'message' => 'Vat deleted successfully!!!'
            ],200);
        }
        return $return_array;
    }
}
