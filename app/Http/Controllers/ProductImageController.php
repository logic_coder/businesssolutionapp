<?php
namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductImageRequest;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Input;
use Image;
use File;

class ProductImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        if(!Auth::user()->can('create-view-products')){
            abort(401);
        }
        $tab_active = 'media';
        return view('product_image.index', compact('product','tab_active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( ProductImageRequest $request, Product $product )
    {
        $dir_path = 'uploads/out_lets/product_images/';
        $dir_path_resize = 'uploads/out_lets/product_images/45x45/';
        
        if (!File::exists($dir_path))
        {
            File::makeDirectory($dir_path, 0777, true);

        }
        if (!File::exists($dir_path_resize))
        {
            File::makeDirectory($dir_path_resize, 0777, true);
        }
        if (Input::file('file')) {

            foreach ( Input::file('file') as $file )
            {
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $product_photo['name'] = 'product_'.\Carbon\Carbon::now().'.'.$extension; // renaming image
                $file->move($dir_path, $product_photo['name']); // uploading file to given path

                // Start : Image Resize
                $image = Image::make(public_path($dir_path.'/'.$product_photo['name']));
                // resize the image to a height of 45 and constrain aspect ratio (auto width)
                $image->resize(null, 45, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image->save(public_path($dir_path_resize.'/'.$product_photo['name']));
                // End : Image Resize
                $product->images()->create($product_photo);
                sleep(1);
            }

        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
