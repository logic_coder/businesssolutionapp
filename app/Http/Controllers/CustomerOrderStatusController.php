<?php
namespace App\Http\Controllers;
use App\CustomerOrderStatus;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Requests\CustomerOrderStatusRequest;
use Illuminate\Support\Facades\Auth;
use \Cviebrock\EloquentSluggable\Services\SlugService;


class CustomerOrderStatusController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('access-customer-order-statuses-settings')){
            abort(401);
        }
        $customerOrderStatuses = CustomerOrderStatus::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('customer_order_status.index', compact('customerOrderStatuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('access-customer-order-statuses-settings')){
            abort(401);
        }
        return view('customer_order_status.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerOrderStatusRequest $request)
    {
        if(!Auth::user()->can('access-customer-order-statuses-settings')){
            abort(401);
        }
        $companies = Company::find(Auth::user()->company_id);
        $data['name'] =  $request->name;
        $data['slug'] = str_replace(" ", "-", strtolower($data['name']));
        $companies->customerOrderstatuses()->create($data);
        return redirect('customer_order_status')->with('success','Customer order statuses added successfully!');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('access-customer-order-statuses-settings')){
            abort(401);
        }
        $customerOrderStatuses = CustomerOrderStatus::find($id);
        return view('customer_order_status.edit', compact('customerOrderStatuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerOrderStatusRequest $request, $id)
    {
        if(!Auth::user()->can('access-customer-order-statuses-settings')){
            abort(401);
        }
        $customerOrderStatuses = CustomerOrderStatus::find($id);
        $data['name'] =  $request->name;
        $data['slug'] = str_replace(" ", "-", strtolower($data['name']));
        $customerOrderStatuses->update($data);
        return redirect('customer_order_status')->with('success','Customer order statuses updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

    }
}
