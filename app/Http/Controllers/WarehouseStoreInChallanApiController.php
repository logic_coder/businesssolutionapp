<?php

namespace App\Http\Controllers;
use App\Company;
use App\Product;
use App\ProductCombination;
use App\WarehouseStoreInChallan;
use App\WarehuseStoreInChallanRequest;
use App\Http\Requests\WarehouseStoreInChallanRequest;
use App\WarehouseStoreInChallanProduct;
use App\WarehouseStoreInChallanProductCombination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Psy\Util\Json;
use Carbon\Carbon;
use Response;
use DB;

class WarehouseStoreInChallanApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function saveWarehouseStoreInChallan(WarehouseStoreInChallanRequest $request)
    {
        $return_array['success'] = '';
        $message = "";
        $warehouse_store_in_challan_product = '';
        if( $request ) {
            $challan_data['company_id'] = Auth::user()->company_id;
            $challan_data['supplier_id'] = $request->supplier_id;
            $challan_data['warehouse_id'] = $request->warehouse_id;
            $challan_data['request_date'] = $request->request_date;
            $challan_data['created_by'] = Auth::user()->id;
            $challan_data['status'] = $request->status;
            $warehouse_store_in_challan = Company::find($challan_data['company_id'])->warehouseStoreInChallans()->create($challan_data);

            $now = Carbon::now();
            $challan_no_data['challan_no'] = $now->year.$warehouse_store_in_challan->id;
            $warehouse_store_in_challan->update($challan_no_data);

            foreach($request->product_combination_list as $product_combination) {
                $challan_product_data['product_id'] = $product_combination['id'];
                $warehouse_store_in_challan_product_inserted = false;
                $product_combination_data = [];

                foreach($product_combination['combinations'] as $combination) {
                    if (isset($combination['requested_quantity'])){
                        if ($combination['requested_quantity']) {
                            if(!$warehouse_store_in_challan_product_inserted){
                                $warehouse_store_in_challan_product = $warehouse_store_in_challan->challanProducts()->create($challan_product_data);
                                $warehouse_store_in_challan_product_inserted = true;
                            }
                            if(isset($combination['id'])) {
                                if($challan_data['status'] == "Auto Received"){
                                    $product_combination_data[$combination['id']] = ['requested_quantity' => $combination['requested_quantity'], 'received_quantity' => $combination['requested_quantity'],'stock_quantity' => $combination['requested_quantity']];
                                }else{
                                    $product_combination_data[$combination['id']] = ['requested_quantity' => $combination['requested_quantity']];
                                }
                            }
                        }
                    }
                }
                if(!isset($combination['id']) && $warehouse_store_in_challan_product_inserted) {
                    if($challan_data['status'] == "Auto Received") {
                        $warehouse_store_in_challan_product->storeInProductCombinations()->attach([null], array('requested_quantity' => $combination['requested_quantity'], 'received_quantity' => $combination['requested_quantity'], 'stock_quantity' => $combination['requested_quantity']));
                    }else{
                        $warehouse_store_in_challan_product->storeInProductCombinations()->attach([null], array('requested_quantity' => $combination['requested_quantity']));
                    }
                }
                if(isset($combination['id']) && $warehouse_store_in_challan_product_inserted) {
                    $warehouse_store_in_challan_product->storeInProductCombinations()->sync($product_combination_data);
                }
            }
            $return_array['warehouse_store_in_challan_id'] = $warehouse_store_in_challan->id;
            if( $warehouse_store_in_challan ){
                $message = 'Warehouse StoreIn Challan Created Successfully';
            }else{
                $message = 'Error: Warehouse StoreIn Challan Not Created, Please Try again';
            }
        }
        return response([
            'warehouse_store_in_challan_id' => $warehouse_store_in_challan->id,
            'message'=> $message
        ]);
    }
    public function updateWarehouseStoreInChallan(WarehouseStoreInChallanRequest $request)
    {
        $return_array['success'] = '';
        $message = "";
        $warehouse_store_in_challan_product = '';
        if( $request ) {
            $challan_data['warehouse_id'] = $request->warehouse_id;
            $challan_data['supplier_id'] = $request->supplier_id;
            $challan_data['request_date'] = $request->request_date;
            $challan_data['status'] = $request->status;
            $warehouse_store_in_challan = WarehouseStoreInChallan::find( $request->challan_id );
            $warehouse_store_in_challan->update( $challan_data );
            $product_combination_data = [];
            
            if($request->remove_old_product_combinations){
                foreach( $request->remove_old_product_combinations as $old_product_combination ){
                    $challan_products = $warehouse_store_in_challan->challanProducts()->where('product_id',$old_product_combination['product_id'])->get();                    
                    $this->remove_old_product_combinations( $challan_products, $old_product_combination['combination'] );
                }
            }
            foreach($request->product_combination_list as $product_combination){
                $warehouse_store_in_challan_product_inserted = false;
                $challan_product_data['warehouse_store_in_challan_id'] = $request->challan_id;
                $challan_product_data['product_id'] = $product_combination['product_id'];
                $product_combination_data = [];
                foreach ($product_combination['combinations'] as $combination) {
                    if (!isset($combination['received_quantity'])){
                        $combination['received_quantity'] = null; 
                    }
                    $combination['lost_quantity'] = null;
                    $combination['return_quantity'] = null;
                    $combination['stock_quantity'] = null;
                    if(isset($combination['missing_status'])){
                        if($combination['missing_status'] == "Lost"){
                            $combination['lost_quantity'] = $combination['missing_quantity'];
                        }
                        elseif($combination['missing_status'] == "Return Supplier"){
                            $combination['return_quantity'] = $combination['missing_quantity'];
                        }
                    }
                    if(isset($combination['requested_quantity'])){
                        if($combination['requested_quantity']) {
                            if(!$warehouse_store_in_challan_product_inserted){
                                $warehouse_store_in_challan_product_updated = WarehouseStoreInChallanProduct::updateOrCreate(['id' => $product_combination['warehouse_store_in_challan_product_id'] ], $challan_product_data);
                                $warehouse_store_in_challan_product_inserted = true;
                            }
                            if(isset($combination['id'])) {
                                if($challan_data['status'] == "Auto Received") {
                                    $product_combination_data[$combination['id']] = ['requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['requested_quantity'],'stock_quantity' => $combination['requested_quantity'], 'missing_status' => $combination['missing_status']];
                                }
                                elseif($challan_data['status'] == "Received") {
                                    $product_combination_data[$combination['id']] = ['requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['received_quantity'],'stock_quantity' => $combination['received_quantity'],'lost_quantity' => $combination['lost_quantity'], 'return_quantity' => $combination['return_quantity'], 'missing_status' => $combination['missing_status']];
                                }
                                else{
                                    $product_combination_data[$combination['id']] = ['requested_quantity' => $combination['requested_quantity'],'received_quantity' => null,'stock_quantity' => null,'lost_quantity' => null,'return_quantity' => null];
                                }
                            }
                        }
                    }
                }
                if(!isset($combination['id']) && $warehouse_store_in_challan_product_inserted) {
                    $product_combination_value = DB::table('product_combination_warehouse_store_in_challan_product')->where('warehouse_store_in_challan_product_id', $product_combination['warehouse_store_in_challan_product_id'])->first();
                    if($product_combination_value){
                        if($challan_data['status'] == "Auto Received") {
                            DB::table('product_combination_warehouse_store_in_challan_product')->where('warehouse_store_in_challan_product_id', $product_combination['warehouse_store_in_challan_product_id'])->update(array('requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['requested_quantity'],'stock_quantity' => $combination['requested_quantity'], 'missing_status' => $combination['missing_status']));
                        }
                        elseif($challan_data['status'] == "Received") {
                            DB::table('product_combination_warehouse_store_in_challan_product')->where('warehouse_store_in_challan_product_id', $product_combination['warehouse_store_in_challan_product_id'])->update(array('requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['received_quantity'], 'stock_quantity' => $combination['received_quantity'], 'lost_quantity' => $combination['lost_quantity'], 'return_quantity' => $combination['return_quantity'], 'missing_status' => $combination['missing_status']));
                        }
                        else{
                            DB::table('product_combination_warehouse_store_in_challan_product')->where('warehouse_store_in_challan_product_id', $product_combination['warehouse_store_in_challan_product_id'])->update(array('requested_quantity' => $combination['requested_quantity'],'received_quantity' => null,'stock_quantity' => null,'lost_quantity' => null,'return_quantity' => null));
                        }
                    }else{
                        if($challan_data['status'] == "Auto Received") {
                            $warehouse_store_in_challan_product_updated->product_combination()->create(['requested_quantity' => $combination['requested_quantity'], 'received_quantity' => $combination['requested_quantity'], 'stock_quantity' => $combination['requested_quantity'], 'missing_status' => $combination['missing_status']]);
                        }
                        elseif($challan_data['status'] == "Received") {
                            $warehouse_store_in_challan_product_updated->product_combination()->create(['requested_quantity' => $combination['requested_quantity'],'received_quantity' => $combination['received_quantity'],'stock_quantity' => $combination['received_quantity'], 'lost_quantity' => $combination['lost_quantity'], 'return_quantity' => $combination['return_quantity'], 'missing_status' => $combination['missing_status']]);
                        }
                        else{
                            $warehouse_store_in_challan_product_updated->product_combination()->create(['requested_quantity' => $combination['requested_quantity'], 'received_quantity' => null, 'stock_quantity' => null , 'lost_quantity' => null , 'return_quantity' => null ]);
                        }
                    }
                }
                else if(isset($combination['id']) && $warehouse_store_in_challan_product_inserted){
                    $warehouse_store_in_challan_product_updated->storeInProductCombinations()->sync($product_combination_data);
                }
            }
            if( $warehouse_store_in_challan ){
                $message = "Warehouse StoreIn Challan Updated Successfully";
            }else{
                $message = "Error: Warehouse StoreIn Challan Not Updated, Please Try again";
            }
        }
        return response([
            'message'=> $message
        ]);
    }
    protected function remove_old_product_combinations( $challan_products, $combination )
    {
        if( $challan_products ){
            foreach( $challan_products as $challan_product ){
                if( $combination == "Yes"){
                    $product_combination_data = [];
                    $challan_product->storeInProductCombinations()->sync($product_combination_data);
                }else{
                    $challan_product->product_combination()->delete();
                }
                $challan_product->forceDelete();
            }
        }
        return true;
    }
    /**
     * Get WarehouseStoreInChallan Data
     *
     * @param  int  $challan_id
     * @return \Illuminate\Http\Response
     */
    public function getChallanData(Request $request)
    {
        $challan_data =  WarehouseStoreInChallan::where('id', $request->challan_id )->with(
            ['challanProducts' => function($query){
                $query->with(['product','storeInProductCombinations'=>function($query){
                    $query->with(['combinationAttributeValues' => function($query){
                        $query->with([ 'attributeValue' => function($query){
                            $query->with('attribute');
                        }]);
                    }]);
                }]);
            }])->first();
        $product_challan_infos = (object) array("challan_id" => $challan_data->id, "request_date" => $challan_data->request_date, "supplier_id" => $challan_data->supplier_id, "warehouse_id" => $challan_data->warehouse_id, "status" => $challan_data->status );
        foreach ( $challan_data->challanProducts as $challan_product ){
            $combination_data = Product::where('id', $challan_product->product_id )->with(
                ['combinations' => function($query){
                        $query->with([
                            'combinationAttributeValues' => function($query){
                                $query->with(['attributeValue' => function($query){
                                    $query->with('attribute');
                                }]);
                            }
                        ]);
                    }
                ]
            )->orderBy('id', 'ASC')->first();
            if( $challan_product->id && sizeof( $combination_data->combinations ) <= 0 ){
                $product_combination = WarehouseStoreInChallanProduct::with('product_combination')->where('id', '=', $challan_product->id )->orderBy('id', 'ASC')->first();
                foreach ( $product_combination->product_combination as $combination ){
                    if( $combination->product_combination_id == null ){
                        $combination_data->combination = "No";
                        $combination_data->product_id = $combination_data->id;
                        $combination_data->name = $combination_data->name;
                        $combination_data->warehouse_store_in_challan_product_id = $product_combination->product_combination[0]->warehouse_store_in_challan_product_id;
                        $combinations_obj = (object) array("id" => null, "requested_quantity" => $product_combination->product_combination[0]->requested_quantity,"received_quantity" => $product_combination->product_combination[0]->received_quantity,"stock_quantity" => $product_combination->product_combination[0]->stock_quantity,"missing_quantity" => null,"missing_status" => $product_combination->product_combination[0]->missing_status,"product_id" => $product_combination->product_id);
                        $combination_data->combinations[] = $combinations_obj;
                    }
                }
            }else{
                $combination_data->product_id = $challan_product->product_id;
                $combination_data->combination = "Yes";
            }
            $product_challan_infos->product_combination_list[] = $combination_data;
        }
        $index_product_combination_list = 0;
        foreach( $product_challan_infos->product_combination_list as $combination ){
            $index_combinations = 0;
            if(sizeof($combination->combinations) > 0 ){
                foreach($combination->combinations as $combination_obj){
                    $index_challan_product = 0;
                    foreach( $challan_data->challanProducts as $challan_product ) {
                        $index_challan_product = $index_challan_product + 1; 
                        if ( $challan_product->storeInProductCombinations ) {  
                            $index_store_in_product_combination = 0;
                            foreach($challan_product->storeInProductCombinations as $store_in_product_combination ){
                                if( $store_in_product_combination->pivot->product_combination_id == $combination_obj->id ){
                                    $product_challan_infos->product_combination_list[$index_product_combination_list]->warehouse_store_in_challan_product_id = $challan_product->id;
                                    $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->received_quantity = $store_in_product_combination->pivot->received_quantity;
                                    $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->requested_quantity = $store_in_product_combination->pivot->requested_quantity;
                                    $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->stock_quantity = $store_in_product_combination->pivot->stock_quantity;
                                    $product_challan_infos->product_combination_list[$index_product_combination_list]->combinations[$index_combinations]->missing_status = $store_in_product_combination->pivot->missing_status;
                                }
                            }
                        }else{

                        }
                        $index_challan_product = $index_challan_product + 1;
                    }
                    $index_combinations = $index_combinations + 1;
                }
            }
            $index_product_combination_list = $index_product_combination_list + 1;
        }
        return Response::json($product_challan_infos);
    }
    /**
     * Get WarehouseStoreInChallan Product Combinations
     *
     * @param  int  $challan_product_id
     * @return \Illuminate\Http\Response
     */
    public function getWarehouseStoreinChallanProductCombination(Request $request)
    {
        $storein_challan_product_data =  WarehouseStoreInChallanProduct::where('id', $request->warehouse_store_in_challan_product_id )->with(
            ['product','storeInProductCombinations'=>function($query){
                $query->with(['combinationAttributeValues' => function($query){
                    $query->with([ 'attributeValue' => function($query){
                        $query->with('attribute');
                    }]);
                }]);
            }])->first();
//        return $storein_challan_product_data;

        $combination_data = Product::where('id', $request->product_id )->with(
            ['combinations' => function($query){
                    $query->with([
                        'combinationAttributeValues' => function($query){
                            $query->with(['attributeValue' => function($query){
                                $query->with('attribute');
                            }]);
                        }
                    ]);
                }
            ]
        )->orderBy('id', 'ASC')->first();

        if( $request->warehouse_store_in_challan_product_id && sizeof( $combination_data->combinations ) <= 0 ) {
            $product_combination = WarehouseStoreInChallanProduct::with('product_combination')->where('id', '=', $request->warehouse_store_in_challan_product_id )->orderBy('id', 'ASC')->first();
            foreach ( $product_combination->product_combination as $combination ){
                if( $combination->product_combination_id == null ){
                    $stock_quantity = $product_combination->product_combination[0]->stock_quantity;
                    if($stock_quantity==null){
                        $stock_quantity = 0;
                    }
                    $combination_data->combination = "No"; 
                    $combination_data->warehouse_store_out_challan_product_id = null;                     
                    $combination_data->product_id = $combination_data->id;
                    $combination_data->name = $combination_data->name;
                    $combinations_obj = (object) array("id" => null, "product_combination_warehouse_store_in_challan_product_id" => $product_combination->product_combination[0]->id,"received_quantity" => $product_combination->product_combination[0]->received_quantity,"stock_quantity" => $stock_quantity,"missing_status" => $product_combination->product_combination[0]->missing_status,"missing_quantity" => null,"product_id" => $product_combination->product_id);
                    $combination_data->combinations[] = $combinations_obj;
                }
            }
        }else{
            $combination_data->product_id = $request->product_id;
            $combination_data->combination = "Yes";
            $combination_data->warehouse_store_out_challan_product_id = null;             
        }
        $product_combination_list = $combination_data;
        $index_product_combination_list = 0;
        $index_combinations = 0;
        if(sizeof($product_combination_list->combinations) > 0 ){
            foreach($product_combination_list->combinations as $combination_obj){
                $index_challan_product = 0;
                $index_challan_product = $index_challan_product + 1;
                if ( $storein_challan_product_data->storeInProductCombinations ) {
                    $index_store_in_product_combination = 0;
                    foreach($storein_challan_product_data->storeInProductCombinations as $store_in_product_combination ){
                        if( $store_in_product_combination->pivot->product_combination_id == $combination_obj->id ){
                            $stock_quantity = $store_in_product_combination->pivot->stock_quantity;
                            if($stock_quantity==null){
                                $stock_quantity = 0;
                            }
                            $product_combination_list->combinations[$index_combinations]->received_quantity = $store_in_product_combination->pivot->received_quantity;
                            $product_combination_list->combinations[$index_combinations]->stock_quantity = $stock_quantity;
                            $product_combination_list->combinations[$index_combinations]->product_combination_warehouse_store_in_challan_product_id = $store_in_product_combination->pivot->id;
                        }
                    }
                }else{

                }
                $index_challan_product = $index_challan_product + 1;
                $index_combinations = $index_combinations + 1;
            }
        }
        $index_product_combination_list = $index_product_combination_list + 1;
        return Response::json($product_combination_list);
    }
    /**
     * Get product_combination_warehouse_store_in_challan_product data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function getProductCombinationChallanProduct( Request $request )
    {
        return WarehouseStoreInChallanProduct::with('product_combination')->where('id', '=', $request->challan_product_id)->orderBy('id', 'ASC')->first();
    }
    /**
     * Search WarehouseStoreInChallan Products where status=received
     *
     * @param int $warehouse_id
     * @param int $category_id
     * @param string $name
     * @return \Illuminate\Http\Response
     */
    public function searchWarehouseProducts(Request $request)
    {
        return WarehouseStoreInChallan::where('warehouse_id', $request->warehouse_id)
            ->where(function ($query){
                $query->where('status','=', 'Received')
                    ->orWhere('status','=', 'Auto Received');
            })->with(
                ['challanProducts' => function($query) use($request){
                    $query->with(['product'=>function($query) use($request){
                        if($request->name){
                            $query->where('name', 'like',  '%' . $request->name . '%')->with('categories')->whereHas('categories', function($query) use($request) {
                                if($request->category_id){
                                    $query->where('categories.id', $request->category_id);
                                }
                            });
                        }
                        if($request->item_code){
                            $query->where('item_code', 'like',  '%' . $request->item_code . '%')->with('categories')->whereHas('categories', function($query) use($request) {
                                if($request->category_id){
                                    $query->where('categories.id', $request->category_id);
                                }
                            });
                        }
                        if($request->bar_code){
                            $query->where('bar_code', 'like',  '%' . $request->bar_code . '%')->with('categories')->whereHas('categories', function($query) use($request) {
                                if($request->category_id){
                                    $query->where('categories.id', $request->category_id);
                                }
                            });
                        }
                        if($request->name=="" AND $request->item_code=="" AND $request->bar_code==""){
                            if($request->category_id){
                                $query->with('categories')->whereHas('categories', function($query) use($request) {
                                    if($request->category_id){
                                        $query->where('categories.id', $request->category_id);
                                    }
                                });
                            }else{
                                $query->with('categories');
                            }
                        }
                    }]);
                }])->orderBy('id', 'DESC')->get();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeWarehouseStoreInChallan( Request $request )
    {
        $return_array['success'] = "";
        $message = "";
        if(!Auth::user()->can('access-warehouse-store-in-challan')){
            Redis::set('error', 'Error: Operation not permitted!!!');
            return $return_array;
        }
        $query_result = DB::table('product_combination_warehouse_store_in_challan_product')
            ->join('warehouse_store_in_challan_products','product_combination_warehouse_store_in_challan_product.warehouse_store_in_challan_product_id' ,'=' ,'warehouse_store_in_challan_products.id')
            ->whereIn('warehouse_store_in_challan_products.warehouse_store_in_challan_id', $request->item_ids)
            ->update(['product_combination_warehouse_store_in_challan_product.deleted_at' => DB::raw('NOW()')]);
        DB::table('warehouse_store_in_challan_products')->whereIn('warehouse_store_in_challan_id', $request->item_ids )->update(['deleted_at' => DB::raw('NOW()')]);
        $warehouse_store_in_challan_deleted = DB::table('warehouse_store_in_challans')->whereIn('id', $request->item_ids )->update(['deleted_at' => DB::raw('NOW()')]);

        if( $warehouse_store_in_challan_deleted ){
            $message = 'Warehouse StoreIn Challan Deleted Successfully';
        }else{
            $message = 'Error: Warehouse StoreIn Challan Not Deleted, Please Try again';
        }
        return response([
            'message'=> $message
        ]);
    }
}
