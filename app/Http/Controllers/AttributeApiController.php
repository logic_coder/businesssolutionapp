<?php
namespace App\Http\Controllers;
use App\Attribute;
use App\AttributeValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AttributeApiController extends Controller
{
    /**
     * Get attribute and corresponding attribute values
     *
     * @return \Illuminate\Http\Response
     */
    public function getAttributes()
    {
//        return Attribute::find('company_id', Auth::user()->company_id)->attributeValues()->get();

        return Attribute::where('company_id', Auth::user()->company_id)->with(
            ['attributeValues' => function ($query) { }])->get();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeAttribute( Request $request )
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('access-product-attributes')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids)){
            Attribute::destroy( $request->item_ids );
            return response([
                'message' => 'Attribute deleted successfully!'
            ],200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeAttributeValue( Request $request )
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('access-product-attributes')){
            Redis::set('error', 'Error: Operation not permitted!!!');
        }
        elseif(!empty( $request->item_ids)){
            AttributeValue::destroy( $request->item_ids );
            return response([
                'message' => 'Attribute value deleted successfully!'
            ],200);
        }
    }
}
