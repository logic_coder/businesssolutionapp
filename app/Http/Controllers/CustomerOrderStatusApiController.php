<?php

namespace App\Http\Controllers;
use App\CustomerOrderStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class CustomerOrderStatusApiController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeCustomerOrderStatus( Request $request )
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('access-customer-order-statuses-settings')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids )){
            CustomerOrderStatus::destroy( $request->item_ids );
            return response([
                'message' => 'Customer order statuses deleted successfully!!!'
            ],200);
        }
        return $return_array;
    }
    protected function getCompanyCustomerOrderStatus()
    {
        return CustomerOrderStatus::where('company_id', Auth::user()->company_id)->orderby('id','DESC')->get();
    }
}
