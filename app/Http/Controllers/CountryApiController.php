<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CountryApiController extends Controller
{
    public function getCountryLists()
    {
        return Country::all();
    }
}
