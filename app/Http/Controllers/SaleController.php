<?php
namespace App\Http\Controllers;
use Company;
use App\Customer;
use App\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class SaleController extends Controller
{
    public function __construct(){
        //$this->middleware('auth');
    }
    /**
     * Display list of order which are not in hold-on
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-sales-page')){
                abort(401);
            }
        }
        $sale_status_tab = "All";
        $redirect_url = "/frontend/sale";
        $saleDetails = Sale::where('out_let_id', $_COOKIE['out_let_id'])
        ->where('status', '=', 'sold')
        ->orderBy('id', 'DESC')->get();
        return view('frontend.sale.index', compact('saleDetails','sale_status_tab','redirect_url'));
    }
    /**
     * Display list of Hold Order
     *
     * @return \Illuminate\Http\Response
     */
    public function getHoldOrder()
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-sales-page')){
                abort(401);
            }
        }
        $sale_status_tab = "On Hold";
        $redirect_url = "/frontend/hold_order";
        $saleDetails = Sale::where('out_let_id', $_COOKIE['out_let_id'])
        ->where('status', '=', 'hold-on')
        ->orderBy('id', 'DESC')->get();
        return view('frontend.sale.index', compact('saleDetails','sale_status_tab','redirect_url'));
    }    
    /**
     * Display list of Canceled Order
     *
     * @return \Illuminate\Http\Response
     */
    public function getCanceledOrder()
    {
        
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-sales-page')){
                abort(401);
            }
        }
        $sale_status_tab = "Canceled";
        $redirect_url = "/frontend/canceled_order";
        $saleDetails = Sale::where('out_let_id', $_COOKIE['out_let_id'])
        ->where('status', '=', 'cancel')
        ->orderBy('id', 'DESC')->get();
        return view('frontend.sale.index', compact('saleDetails','sale_status_tab','redirect_url'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-sales-page')){
                abort(401);
            }
        }
        return view('frontend.sale.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('edit-sales')){
                abort(401);
            }
        }
        return view('frontend.sale.edit', compact('sale'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Show the paid details for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPaidDetails(CustomerOrderDetail $customer_order_detail)
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('access-sales-page')){
                abort(401);
            }
        }
        return view('frontend.customer_order_detail.paid_detail', compact('customer_order_detail'));
    }
}
