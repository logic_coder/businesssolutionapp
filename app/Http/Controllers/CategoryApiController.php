<?php

namespace App\Http\Controllers;
use App\Category;
use App\CategoryProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class CategoryApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::get();
        return $categories;
    }
    protected function getCategories()
    {
        return Category::where('company_id', Auth::user()->company_id)
            ->where('active', 1)->orderby('id','DESC')->get();
    }
    public function getTreeData( Request $request ){
        $categories = Category::where('parent_id', '=', 0)->where('company_id', Auth::user()->company_id)->get();
        $dynaConfig = $this->treeCategories( $categories, $index = 0, $parent_node = $request->parent, $this_category = $request->this_category, $product_id = $request->product_id);
        $return_array['data'] = $dynaConfig;
        return $return_array;
    }

    /**
     * List sub-categories during Add new category and Edit existing Category
     * @param Object $categories, int $index, int $parent_node, int $this_node
     * @return \Illuminate\Http\Response
     */
    public function treeCategories($categories, $index, $parent_node = 0, $this_category = 0, $product_id = 0 )
    {
        $tree_categories = [];
        foreach( $categories as $category) {
            $index++;
            $tree_category = [];
            $tree_category['title'] = $category->name;
            $tree_category['key'] = $category->id;
            if( $product_id ){
                $product_category = CategoryProduct::where('product_id', $product_id )->where('category_id', $category->id )->first();
                if( $product_category ){
                    $tree_category['selected'] = true;
                }
            }
            if( $parent_node == $category->id ) {
                $tree_category['selected'] = true;
            }
            // Need this during category edit, The category you are editing is not selectable
            if( $this_category == $category->id || ( $this_category !=0 && $this_category == $category->parent_id )) {
                $tree_category['unselectable'] = true;
            }
            if(count($category->childs)){
                $tree_category['folder'] = true;
                if( $this_category != $category->id )
                {
                    $tree_category['children'] = $this->treeCategories( $category->childs, $index, $parent_node, $this_category, $product_id );
                }
            }else{
                if( $index == 1 ){
                    $tree_category['folder'] = true; // Make default category as 'Folder'
                }
                else{
                    $tree_category['folder'] = false;
                }
            }
            $tree_categories[] = $tree_category;
        }
        return $tree_categories;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeCategory( Request $request )
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('access-category')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids )){
            foreach( $request->item_ids as $item_id ){
                $categories = Category::where('id', '=', $item_id)->get();
                $this->removeChildCategories( $categories );
            }
            return response([
                'message' => 'Category deleted successfully!!!'
            ],200);
        }
    }
    /**
     * Remove multiple resources from storage.
     *
     * @param  $category object
     * @return \Illuminate\Http\Response
     */
    public function removeChildCategories( $categories )
    {
        foreach( $categories as $category )
        {
            Category::destroy( $category->id );
            if(count($category->childs)){
                $this->removeChildCategories( $category->childs );
            }
        }
        return true;
    }
}
