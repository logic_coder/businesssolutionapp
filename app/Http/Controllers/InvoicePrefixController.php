<?php

namespace App\Http\Controllers;
use App\InvoicePrefix;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Requests\InvoicePrefixRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InvoicePrefixController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('access-invoice-prefix-settings')){
            abort(401);
        }
        $invoice_prefixes = InvoicePrefix::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get();
        return view('invoice_prefix.index', compact('invoice_prefixes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('access-invoice-prefix-settings')){
            abort(401);
        }
        $invoice_prefixes = "";
        return view('invoice_prefix.create', compact('invoice_prefixes'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InvoicePrefixRequest $request)
    {
        if(!Auth::user()->can('access-invoice-prefix-settings')){
            abort(401);
        }
        $companies = Company::find(Auth::user()->company_id);
        $invoice_prefix_data['name'] = $request->name;
        $invoice_prefix_data['default'] = $request->default ? "Yes": "No";
        $invoice_prefix_created = $companies->invoicePrefixes()->create($invoice_prefix_data);
        if( $invoice_prefix_created && $request->default ){
            $this->changeDefaultInvoicePrefix($invoice_prefix_created);
        }
        return redirect('invoice_prefix')->with('success','Invoice prefix added successfully!');        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('access-invoice-prefix-settings')){
            abort(401);
        }
        $invoice_prefixes = InvoicePrefix::find($id);
        return view('invoice_prefix.edit', compact('invoice_prefixes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InvoicePrefixRequest $request, $id)
    {
        if(!Auth::user()->can('access-invoice-prefix-settings')){
            abort(401);
        }
        $invoice_prefixes = InvoicePrefix::find($id);        
        $invoice_prefix_data['name'] = $request->name;
        $nivoice_prefix_data['default'] = $request->default ? "Yes": "No";
        $invoice_prefixes->update($nivoice_prefix_data);
        if( $invoice_prefixes && $request->default){
            $this->changeDefaultInvoicePrefix($invoice_prefixes);
        }
        return redirect('invoice_prefix')->with('success','Invoice prefix updated successfully!'); 
    }
    public function changeDefaultInvoicePrefix($invoice_prefixes){
        DB::table('invoice_prefixes')->where('id', '!=', $invoice_prefixes->id)->update(['default' => 'No']);
        return true;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        
    }
}
