<?php

namespace App\Http\Controllers;

use App\Company;
use App\Customer;
use App\User;
use App\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class CustomerController extends Controller
{
    public function __construct(){
        //$this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('create-view-customer')){
                abort(401);
            }
            $customers = Customer::where('company_id', Auth::user()->company_id)->orderBy('id', 'DESC')->get();
            return view('frontend.customer.index', compact('customers'));
        }        
        $user = User::find($_COOKIE['user_id']);
        $customers = Customer::where('company_id', $user->company_id)->orderBy('id', 'DESC')->get();
        return view('frontend.customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('create-view-customer')){
                abort(401);
            }
        }
        return view('frontend.customer.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($customer)
    {        
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('edit-update-customer')){
                abort(401);
            }
        }
        return view('frontend.customer.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, UserDetailRequest $detail_request, User $user)
    {
        if(!isset($_COOKIE['user_id'])){
            if(!Auth::user()->can('edit-update-customer')){
                abort(401);
            }
        }
        $user->update($request->all());
        if( $user->userDetail ){
            $user_detail = $user->userDetail;
            $user_detail->update($request->all());
        }else{
            $user->userDetail()->create($request->all());
        }
        if( $user->roles ){
            foreach( $user->roles as $user_role ){
                $user_role = Role::find( $user_role->id );
                $user->roles()->detach( $user_role->id );
            }
        }
        $user->roles()->attach($request->role_id );
        flash('User updated successfully', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Customer $customer )
    {

    }
}
