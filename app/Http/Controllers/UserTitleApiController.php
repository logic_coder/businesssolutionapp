<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\UserTitle;
use Illuminate\Support\Facades\Auth;

class UserTitleApiController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeUserTitle( Request $request )
    {
        $return_array['success'] = '';
        if(!Auth::user()->can('access-position-title')){
            return response([
                'message' => 'Error: Operation not permitted!'
            ],401);
        }
        elseif(!empty( $request->item_ids)){
            UserTitle::destroy( $request->item_ids );
            return response([
                'message' => 'Title / Possition deleted successfully!'
            ],200);
        }
    }
}
