<?php
namespace App\Http\Controllers;
use App\Company;
use App\Http\Requests\UserDetailRequest;
use App\Http\Requests\UserRequest;
use App\OutLet;
use App\User;
use App\UserDetail;
use App\UserTitle;
use App\RoleUser;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;
use File;

class UserApiController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeUser( Request $request )
    {
        $return_array['success'] = '';
        $return_array['data'] = $request->item_ids;

        // Need to check this portion of code very carefully //
        foreach( $request->item_ids as $item_id ) {
            $user = User::find($item_id);
            if (!empty($user)) {
                if ($user->roles) {
                    foreach ($user->roles as $user_role) {
                        $user_role = Role::find($user_role->id);
                    }
                }
            }
            if (!Auth::user()->can('delete-user') || $user_role->slug == 'administrator') {
                return response([
                    'message' => 'Error: Operation not permitted!'
                ],401);
            }
            if (!empty($user_role)) {
                $user->roles()->detach($user_role->id);
            }
            if ($user->userDetail) {
                $user->userDetail()->delete();
            }
            User::destroy($user->id);            
        }
        return response([
            'message' => 'User deleted successfully!!!'
        ],200);
    }
    /**
     * Update user pfofile photo
     *
     * @param  profile photo
     * @return \Illuminate\Http\Response
     */
    protected function saveProfilePhoto(  Request $request )    
    {
        $return_array['success'] = "Image uploaded successfully";  
        $company_id = $request->company_id ? $request->company_id : Auth::user()->company_id;
        if( $request ){                             
            $dir_path = 'uploads/company/'.$company_id.'/user_profile_photo/'.$request->user_id;
            $dir_path_resize = $dir_path.'/32x32';

            if (!File::exists($dir_path))
            {
                File::makeDirectory($dir_path, 0777, true);
            }
            if (!File::exists($dir_path_resize))
            {
                File::makeDirectory($dir_path_resize, 0777, true);
            }

            $user_profile_photo = UserDetail::where('user_id', $request->user_id)->get();
            $user_detail = "";
            $user_detail_id = null;

            if(sizeof($user_profile_photo) && ( $request->remove_profile_photo !=null )){
                $user_detail = UserDetail::find( $user_profile_photo[0]->id );
                $user_detail_id = $user_detail->id;  
                if( $user_profile_photo[0]->photo ){
                    if( unlink(public_path($dir_path.'/'.$user_profile_photo[0]->photo)) ){
                        unlink(public_path($dir_path_resize.'/'.$user_profile_photo[0]->photo)); 
                        $profile_photo['photo'] = null;
                        $user_detail_id = $user_detail->id;                 
                        $user_detail->update( $profile_photo );
                        $message = 'Profile Photo Deleted Successfully';          
                        return response()->json([
                             'message' => $message,
                        ], 200); 
                    }
                }                
            }            
            if ( !is_string($request->profile_file) ) {   
                if(sizeof($user_profile_photo)){
                    $user_detail = UserDetail::find( $user_profile_photo[0]->id );
                    $user_detail_id = $user_detail->id;  
                    if( $user_profile_photo[0]->photo ){                        
                        if( unlink(public_path($dir_path.'/'.$user_profile_photo[0]->photo)) ){
                            unlink(public_path($dir_path_resize.'/'.$user_profile_photo[0]->photo)); 
                            $profile_photo['photo'] = null;                            
                            $user_detail->update( $profile_photo );
                        }  
                    }             
                }  
                $file = $request->profile_file;   
                $extension = $file->getClientOriginalExtension(); // getting image                            
                $profile_photo['photo'] = 'file_'.\Carbon\Carbon::now().'.'.$extension; // renaming image
                $file->move($dir_path, $profile_photo['photo']); // uploading file to given path

                // Start : Image Resize
                $image = Image::make(public_path($dir_path.'/'.$profile_photo['photo']));
                // resize the image to a height of 45 and constrain aspect ratio (auto width)
                $image->resize(null, 32, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image->save(public_path($dir_path_resize.'/'.$profile_photo['photo']));
                // End : Image Resize
                //Insert or Update user details table with profile photo name
                $profile_photo['user_id'] = $request->user_id;
                UserDetail::updateOrCreate(['id' => $user_detail_id ], $profile_photo);
                $return_array['success'] = "Success...";            
            }     
            $message = 'Profile Photo Updated Successfully';          
            return response()->json([
                 'message' => $message,
            ], 200); 
        }else{
            $message = 'Error: Profile Photo Not Updated, Please Try again';
            return response()->json([
                 'message' => $message,
            ], 500); 
        } 
        
        
    }
    /**
     * Get user pfofile photo
     *
     * @param  user id
     * @return \Illuminate\Http\Response
     */
    protected function getProfilePhoto(Request $request){
        $user_profile_photo = UserDetail::where('user_id', $request->user_id)->get();      
        return $user_profile_photo; 
    }
}
