<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiscountCouponOutletRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'discount_coupon_id' => 'required',
                    'outlet_id' => 'required',
                ];
                break;

            case 'PUT':
                return [
                    'discount_coupon_id' => 'required',
                    'outlet_id' => 'required',
                ];
                break;
        }
    }
}
