<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OutletChallaninRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'warehouse_id' => 'required',
                    'outlet_id' => 'required',
                    'challan_no' => 'required',
                    'status' => 'required',
                ];
                break;

            case 'PUT':
                return [
                    'warehouse_id' => 'required',
                    'outlet_id' => 'required',
                    'challan_no' => 'required',
                    'status' => 'required',
                ];
                break;
        }
    }
}

