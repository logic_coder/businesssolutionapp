<?php
namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => ['required','min:3', Rule::unique('suppliers')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                        })
                    ],
                    'code' => ['required','min:3', Rule::unique('suppliers')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                        })
                    ],
                    'phone' => 'required|regex:/(01)[0-9]{9}$/|unique:suppliers,phone',
                    'address' => 'required|min:15|max:200',
                    'city' => 'required|min:3|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',                    
                    'email' => 'required|email|unique:suppliers,email',
                ];
                break;

            case 'PUT':
                return [
                    'name' => ['required','min:3', Rule::unique('suppliers')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                            $query->where('id', '!=', $this->route('supplier'));
                        })
                    ],
                    'code' => ['required','min:3', Rule::unique('suppliers')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                            $query->where('id', '!=', $this->route('supplier'));
                        })
                    ],
                    'address' => 'required|min:15|max:200',
                    'city' => 'required|min:3|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',
                    'phone' => 'required|regex:/(01)[0-9]{9}$/|unique:suppliers,phone,'.$this->route('supplier'),
                    'email' => 'required|email|unique:suppliers,email,'.$this->route('supplier'),
                ];
                break;
        }
    }
}
