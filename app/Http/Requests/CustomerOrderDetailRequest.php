<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Factory;

class CustomerOrderDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                $rules = [
                    'customer_id' => 'required',
                    'order_date' => 'required|date',
                    'expected_delivery_date' => 'required|date|after_or_equal:order_date',
                    'company_currency_id' => 'required',
                    'total_amount_inclusive_vat' => 'required|numeric|regex:/\d+\.?\d*/',
                ];
                $rules = array_merge($rules,['total_item_discount' => 'nullable|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['coupon_discount_amount' => 'nullable|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['net_amount' => 'required|numeric|regex:/\d+\.?\d*/']);
                return $rules;
                break;
            case 'PUT':
                $rules = [
                    'customer_id' => 'required',
                    'order_date' => 'required|date',
                    'expected_delivery_date' => 'required|date|after_or_equal:order_date',
                    'company_currency_id' => 'required',
                    'total_amount_inclusive_vat' => 'required|numeric|regex:/\d+\.?\d*/',
                ];
                $rules = array_merge($rules,['total_item_discount' => 'nullable|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['coupon_discount_amount' => 'nullable|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['net_amount' => 'required|numeric|regex:/\d+\.?\d*/']);
                return $rules;
                break;
        }
    }
}
