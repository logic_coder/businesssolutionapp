<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductDiscountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'product_id' => 'required',
                    'company_id' => 'required',
                    'discount_amount' => 'required',
                    'discount_percent' => 'required',
                    'active_date' => 'required',
                    'expire_date' => 'required',
                ];
                break;

            case 'PUT':
                return [
                    'product_id' => 'required',
                    'company_id' => 'required',
                    'discount_amount' => 'required',
                    'discount_percent' => 'required',
                    'active_date' => 'required',
                    'expire_date' => 'required',
                ];
                break;
        }
    }
}
