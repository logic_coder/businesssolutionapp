<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Factory;

class SaleRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        return [            
            'customer_id.required_without' => "Customer Or Guest field is required",
            'net_amount.min' => "Net amount must be grater than zero",
            'net_amount.not_in' => "Net amount must be grater than zero",
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        switch ( $this->method() ){
            case 'POST':
                $rules = [];
                if($this->input('status') == 'sold'  || $this->input('status') == 'hold-on'){
                    $rules = [                    
                        'discount_amount' => 'nullable|numeric|regex:/\d+\.?\d*/',
                        'coupon_discount_amount' => 'nullable|numeric|regex:/\d+\.?\d*/',
                        'vat_amount' => 'nullable|numeric|regex:/\d+\.?\d*/',
                        'net_amount' => 'required|numeric|regex:/\d+\.?\d*/|min:0|not_in:0', 
                        'company_currency_id' => 'required',                          
                    ];
                    $rules = array_merge($rules,['customer_id' => 'required_without:guest']);
                }    
                return $rules;
                break;
            case 'PUT':
                $rules = [                    
                    'discount_amount' => 'nullable|numeric|regex:/\d+\.?\d*/',
                    'coupon_discount_amount' => 'nullable|numeric|regex:/\d+\.?\d*/',
                    'vat_amount' => 'nullable|numeric|regex:/\d+\.?\d*/',
                    'net_amount' => 'required|numeric|regex:/\d+\.?\d*/|min:0|not_in:0', 
                    'company_currency_id' => 'required',                          
                ];
                $rules = array_merge($rules,['customer_id' => 'required_without:guest']);
                return $rules;
                break;
        }
    }
}