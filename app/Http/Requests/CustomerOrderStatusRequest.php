<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Factory;

class CustomerOrderStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                $rules = [
                    'name' => ['required','min:3','max:100', Rule::unique('customer_order_statuses')->where(function ($query) {
                        $query->where('deleted_at', Null);
                        $query->where('company_id', Auth::user()->company_id);
                    })
                    ],
                ];
                return $rules;
                break;
            case 'PUT':
                $rules = [
                    'name' => ['required','min:3','max:100', Rule::unique('customer_order_statuses')->where(function ($query) {
                        $query->where('deleted_at', Null);
                        $query->where('company_id', Auth::user()->company_id);
                        $query->where('id', '!=', $this->route('customer_order_status'));
                    })
                    ],
                ];
                return $rules;
                break;
        }
    }
}
