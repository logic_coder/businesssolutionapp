<?php
namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => ['required','min:3', Rule::unique('products')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                        })
                    ],
                    'item_code' => ['required','min:6', Rule::unique('products')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                        })
                    ],
                ];
                break;

            case 'PUT':
                return [
                    'name' => ['required','min:3', Rule::unique('products')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                            $query->where('id', '!=', $this->route('product')->id);
                        })
                    ],
                    'item_code' => ['required','min:6', Rule::unique('products')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                            $query->where('id', '!=', $this->route('product')->id);
                        })
                    ],
                ];
                break;
        }
    }
}
