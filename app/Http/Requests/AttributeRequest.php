<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use App\Attribute;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Factory;

class AttributeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => ['required','min:3','max:15', Rule::unique('attributes')->where(function ($query) {
                        $query->where('deleted_at', Null);
                        $query->where('company_id', Auth::user()->company_id);
                    })
                    ],
                    'type' => 'required',
                ];
                break;
            case 'PUT':
                return [
                    'name' => ['required','min:3','max:100', Rule::unique('attributes')->where(function ($query) {
                        $query->where('deleted_at', Null);
                        $query->where('company_id', Auth::user()->company_id);
                        $query->where('id', '!=', $this->route('attribute')->id);
                    })
                    ],
                    'type' => 'required',
                ];
                break;
        }
    }
}
