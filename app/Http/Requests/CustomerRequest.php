<?php
namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Factory;
class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                $rules = [];
                $rules = array_merge($rules,['customer_no' => ['required', Rule::unique('customers')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                        })
                    ]
                ]);
                if($this->input('email')!=""){
                    $rules = array_merge($rules,['email' => ['email', Rule::unique('customers')->where(function ($query) {
                                $query->where('deleted_at', Null);
                                $query->where('company_id', Auth::user()->company_id);
                            })
                        ]
                    ]);
                }
                if($this->input('cell_phone')!=""){
                    $rules = array_merge($rules,['cell_phone' => ['regex:/(01)[0-9]{9}$/', Rule::unique('customers')->where(function ($query) {
                                $query->where('deleted_at', Null);
                                $query->where('company_id', Auth::user()->company_id);
                            })
                        ]
                    ]);
                }             
                $rules = array_merge($rules,['first_name' => 'required|string|min:2|max:100']);
                $rules = array_merge($rules,['last_name' => 'required|string|min:2|max:100']);
                $rules = array_merge($rules,['gender_id' => 'required']);
                $rules = array_merge($rules,['address' => 'required']);
                $rules = array_merge($rules,['city' => 'required']);
                $rules = array_merge($rules,['country_id' => 'required']);
                return $rules;
                break;

            case 'PUT':
                $rules = [];
                $rules = array_merge($rules,['customer_no' => ['required', Rule::unique('customers')->where(function ($query) {
                            $query->where('deleted_at', null);
                            $query->where('company_id', Auth::user()->company_id);
                            $query->where('id', '!=', $this->get('id'));
                        })
                    ]
                ]);
                if($this->input('email')!=""){
                    $rules = array_merge($rules,['email' => ['email', Rule::unique('customers')->where(function ($query) {
                                $query->where('deleted_at', Null);
                                $query->where('company_id', Auth::user()->company_id);
                                $query->where('id', '!=', $this->get('id'));
                            })
                        ]
                    ]);
                }
                if($this->input('cell_phone')!=""){
                    $rules = array_merge($rules,['cell_phone' => ['regex:/(01)[0-9]{9}$/', Rule::unique('customers')->where(function ($query) {
                                $query->where('deleted_at', Null);
                                $query->where('company_id', Auth::user()->company_id);
                                $query->where('id', '!=', $this->get('id'));
                            })
                        ]
                    ]);
                }             
                $rules = array_merge($rules,['first_name' => 'required|string|min:2|max:100']);
                $rules = array_merge($rules,['last_name' => 'required|string|min:2|max:100']);
                $rules = array_merge($rules,['gender_id' => 'required']);
                $rules = array_merge($rules,['address' => 'required']);
                $rules = array_merge($rules,['city' => 'required']);
                $rules = array_merge($rules,['country_id' => 'required']);
                return $rules;
                break;               
        }
    }
}
