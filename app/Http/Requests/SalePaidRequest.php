<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Factory;
use Request;

class SalePaidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        return [     
            'digital_payment_amount.required' => "Amount field required",
            'digital_payment_amount.numeric' => "Amount field must be number",
            'digital_payment_amount.regex' => "Amount format invalid",
            'digital_payment_amount.min' => "Amount must be grater than zero",
            'digital_payment_amount.not_in' => "Amount must be grater than zero",
            'cash_payment_amount.required' => "Amount field required",
            'cash_payment_amount.numeric' => "Amount field must be number",
            'cash_payment_amount.regex' => "Amount format invalid",
            'cash_payment_amount.min' => "Amount must be grater than zero",
            'cash_payment_amount.not_in' => "Amount must be grater than zero",
            'sale_paids.*.sale_paid_details.*.value.required' => "This field required",            
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        switch ( $this->method() ){
            case 'POST': 
                $rules = [];                
                if( ($this->input('status') == 'sold' || $this->input('status') == 'hold-on') && ( $this->input('digital_payment_amount') == '00' || empty($this->input('digital_payment_amount')) ) ){
                    $rules = array_merge($rules,['cash_payment_amount' => 'required|numeric|regex:/\d+\.?\d*/|min:0|not_in:0']);
                } 
                if( $this->input('paymentMethod') == "Master Card" || $this->input('paymentMethod') == "Visa Card" || $this->input('paymentMethod') == "Bkash"){     
                    $rules = array_merge($rules,['digital_payment_amount' => 'required|numeric|regex:/\d+\.?\d*/|min:0|not_in:0']);
                    $rules = array_merge($rules,['sale_paids.*.sale_paid_details.*.value' => 'required']);
                }
                return $rules;
                break;

            case 'PUT':  
            $rules = [];  
                if( empty($this->input('digital_payment_amount')) ){
                    $rules = array_merge($rules,['cash_payment_amount' => 'required|numeric|regex:/\d+\.?\d*/|min:0|not_in:0']);
                } 
                if( $this->input('paymentMethod') == "Master Card" || $this->input('paymentMethod') == "Visa Card" || $this->input('paymentMethod') == "Bkash"){     
                    $rules = array_merge($rules,['digital_payment_amount' => 'required|numeric|regex:/\d+\.?\d*/|min:0|not_in:0']);               
                    $rules = array_merge($rules,['sale_paids.*.sale_paid_details.*.value' => 'required']);
                }
                return $rules;
                break;
        }
    }
}
