<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;


class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => 'required|string|min:5|max:200|unique:companies,name',
                    'phone' => 'required|regex:/(01)[0-9]{9}$/',
                    'email' => 'required|email|unique:companies,email',
                    'business_category_id' => 'required|exists:business_categories,id',
                    'address' => 'required|min:15|max:200',
                    'city' => 'required|min:3|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',
                    'receipt_address' => 'required|min:10|max:200',
                    'receipt_text' => 'required|min:9|max:100',
                    'signature_line_text' => 'required|min:9',
                ];
                break;

            case 'PUT':
                return [
                    'name' => 'required|string|max:200|unique:companies,name,'.Auth::user()->company_id,
                    'phone' => 'required|regex:/(01)[0-9]{9}$/',
                    'email' => 'required|email|unique:companies,email,'.Auth::user()->company_id,
                    'business_category_id' => 'required|exists:business_categories,id',
                    'address' => 'required|max:200',
                    'city' => 'required|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',
                    'receipt_address' => 'required|max:200',
                    'receipt_text' => 'required|max:100',
                    'signature_line_text' => 'required|max:100',
                ];
                break;
        }

    }

}
