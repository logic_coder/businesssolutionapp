<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Factory;
use Request;

class CustomerOrderDetailPaidDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                $rules = [
                    'payment_by' => 'sometimes|required',
                    'name_on_card' => 'sometimes|required',
                    'card_no' => 'sometimes|required',
                    'account_type' => 'sometimes|required',
                    'received_from_no' => 'sometimes|required',
                ];
                return $rules;
                break;

            case 'PUT':
                $rules = [
                    'payment_by' => 'sometimes|required',
                    'name_on_card' => 'sometimes|required',
                    'card_no' => 'sometimes|required',
                    'account_type' => 'sometimes|required',
                    'received_from_no' => 'sometimes|required',
                ];
                return $rules;
                break;
        }
    }
}
