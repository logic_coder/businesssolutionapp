<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;

class CouponRestrictionRequest extends FormRequest
{
    public function messages()
    {
        return [
            'coupon_restrictions.*.number_of_products_required_in_carts.required' => "The number of products field is required",
            'coupon_restrictions.*.number_of_products_required_in_carts.numeric' => "The number of products field must be a number",
            'coupon_restrictions.*.number_of_products_required_in_carts.min' => "The number of products field must be at least 1",
            'coupon_restrictions.*.number_of_products_required_in_carts.regex' => "The number of products format is invalid",
            'coupon_restrictions.*.rule_concerning.required' => "Rule concerning products/categories can't be empty",
            'coupon_restrictions.*.products.required_without' => "Products Or categories can't be empty",
        ];
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                $rules = [];
                $rules = [
                    'coupon_restrictions.*.number_of_products_required_in_carts' => 'required|numeric|min:1|regex:/^[0-9]+()*$/',
                ];
                if($this->input('rule_concerning')==''){
                    $rules = array_merge($rules,['coupon_restrictions.*.rule_concerning' => 'required']);
                }
                $rules = array_merge($rules,['coupon_restrictions.*.products' => 'required_without:coupon_restrictions.*.categories']);

                return $rules;
                break;

            case 'PUT':
                $rules = [
                    'coupon_restrictions.*.number_of_products_required_in_carts' => 'required|numeric|min:1|regex:/^[0-9]+()*$/',
                ];
                if($this->input('rule_concerning')==''){
                    $rules = array_merge($rules,['coupon_restrictions.*.rule_concerning' => 'required']);
                }
                $rules = array_merge($rules,['coupon_restrictions.*.products' => 'required_without:coupon_restrictions.*.categories']);
                return $rules;
                break;
        }
    }
}
