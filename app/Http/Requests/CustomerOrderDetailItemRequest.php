<?php
namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Factory;

class CustomerOrderDetailItemRequest extends FormRequest
{
    public function messages()
    {
        return [
            'customer_order_detail_items.*.description.required' => "required",
            'customer_order_detail_items.*.description.min' => "must be 3 charaters",
            'customer_order_detail_items.*.categories.*.id.required' => "required",
            'customer_order_detail_items.*.unit_price.required' => "required",
            'customer_order_detail_items.*.unit_price.regex' => "must be numeric",
            'customer_order_detail_items.*.weight_tola.numeric' => "must be numeric",
            'customer_order_detail_items.*.weight_tola.regex' => "format is invalid",
            'customer_order_detail_items.*.weight_ana.numeric' => "must be numeric",
            'customer_order_detail_items.*.weight_ana.regex' => "format is invalid",
            'customer_order_detail_items.*.weight_rotti.numeric' => "must be numeric",
            'customer_order_detail_items.*.weight_rotti.regex' => "format is invalid",
            'customer_order_detail_items.*.weight_milli.numeric' => "must be numeric",
            'customer_order_detail_items.*.weight_milli.regex' => "format is invalid",
            'customer_order_detail_items.*.material_price.required' => "required",
            'customer_order_detail_items.*.material_price.numeric' => "must be numeric",
            'customer_order_detail_items.*.material_price.regex' => "format is invalid",
            'customer_order_detail_items.*.making_charge.required' => "required",
            'customer_order_detail_items.*.making_charge.numeric' => "must be numeric",
            'customer_order_detail_items.*.making_charge.regex' => "format is invalid",
            'customer_order_detail_items.*.price_exclusive_vat.required' => "required",
            'customer_order_detail_items.*.price_exclusive_vat.numeric' => "must be numeric",
            'customer_order_detail_items.*.price_exclusive_vat.regex' => "format is invalid",
            'customer_order_detail_items.*.price_inclusive_vat.required' => "required",
            'customer_order_detail_items.*.price_inclusive_vat.numeric' => "must be numeric",
            'customer_order_detail_items.*.price_inclusive_vat.regex' => "format is invalid",
            'customer_order_detail_items.*.item_discount_amount.numeric' => "must be numeric",
            'customer_order_detail_items.*.item_discount_amount.regex' => "format is invalid",
            'customer_order_detail_items.*.status.required' => "required",
        ];
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                $rules = [
                    'customer_order_detail_items.*.description' => 'required|min:3',
                    'customer_order_detail_items.*.categories.*.id' => 'required',
                    //'customer_order_detail_items.*.unit_price' => 'required|regex:/\d+\.?\d*/',
                ];
                $rules = array_merge($rules,['customer_order_detail_items.*.weight_tola' => 'nullable|numeric|regex:/^[0-9]+()*$/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.weight_ana' => 'nullable|numeric|regex:/^[0-9]+()*$/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.weight_rotti' => 'nullable|numeric|regex:/^[0-9]+()*$/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.weight_milli' => 'nullable|numeric|regex:/^[0-9]+()*$/']);
                //$rules = array_merge($rules,['customer_order_detail_items.*.material_price' => 'required|numeric|regex:/\d+\.?\d*/']);
                //$rules = array_merge($rules,['customer_order_detail_items.*.making_charge' => 'required|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.price_exclusive_vat' => 'required|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.price_inclusive_vat' => 'required|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.item_discount_amount' => 'nullable|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.status' => 'required']);
                return $rules;
                break;

            case 'PUT':
                $rules = [
                    'customer_order_detail_items.*.description' => 'required|min:3',
                    'customer_order_detail_items.*.categories.*.id' => 'required',
                    //'customer_order_detail_items.*.unit_price' => 'required|regex:/\d+\.?\d*/',
                ];
                $rules = array_merge($rules,['customer_order_detail_items.*.weight_tola' => 'nullable|numeric|regex:/^[0-9]+()*$/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.weight_ana' => 'nullable|numeric|regex:/^[0-9]+()*$/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.weight_rotti' => 'nullable|numeric|regex:/^[0-9]+()*$/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.weight_milli' => 'nullable|numeric|regex:/^[0-9]+()*$/']);
                //$rules = array_merge($rules,['customer_order_detail_items.*.material_price' => 'required|numeric|regex:/\d+\.?\d*/']);
                //$rules = array_merge($rules,['customer_order_detail_items.*.making_charge' => 'required|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.price_exclusive_vat' => 'required|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.price_inclusive_vat' => 'required|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.item_discount_amount' => 'nullable|numeric|regex:/\d+\.?\d*/']);
                $rules = array_merge($rules,['customer_order_detail_items.*.status' => 'required']);
                return $rules;
                break;
        }
    }
}
