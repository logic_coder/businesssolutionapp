<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ProductCombinationRequest extends FormRequest
{
    public function messages()
    {
        return [
            'product_id.required' => "The product field is required",
            'product_id.unique' => "Combination for this product already exists",
        ];
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                $rules = [
                    'product_id' => ['required', Rule::unique('product_combinations')->where(function ($query) {
                        $query->where('deleted_at', Null);
                    })
                    ],
                ];
                return $rules;
                break;
            case 'PUT':
                $rules = [
                    
                ];
                return $rules;
                break;
        }
    }
}
