<?php
namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class VatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = "regex:[0-9]{8}[.]?[0-9]{2}*";

        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => ['required','min:3','max:100', Rule::unique('vats')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                        })
                    ],
                    'percent' => 'required|regex:/\d+\.?\d*/',
                ];
                break;

            case 'PUT':
                return [
                    'name' => ['required','min:3','max:100', Rule::unique('vats')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                            $query->where('id', '!=', $this->route('vat'));
                        })
                    ],
                    'percent' => 'required|regex:/\d+\.?\d*/',
                ];
                break;
        }
    }
}
