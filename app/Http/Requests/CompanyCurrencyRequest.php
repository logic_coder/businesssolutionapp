<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Factory;

class CompanyCurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'currency_id.required' => 'The field is required',
            'currency_id.unique' => 'Currency has already been taken',
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'currency_id' => ['required', Rule::unique('company_currencies')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                        })
                    ],
                ];
                break;

            case 'PUT':
                return [
                    'currency_id' => ['required', Rule::unique('company_currencies')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                            $query->where('id', '!=', $this->route('company_currency'));
                        })
                    ],
                ];
                break;
        }
    }
}
