<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Factory;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the custom validation rules that apply to the request.
     *
     * @return array
     */
    public function validator(Factory $factory)
    {
        $validator = $factory->make($this->input(), $this->rules(), $this->messages(), $this->attributes());

        $validator->sometimes('discount_amount', 'required|numeric|min:1|regex:/^[0-9]+(\.[0-9]{1,2})*$/', function($input) {
            return $input->apply_discount_option === 'Amount';
        });
//        $validator->sometimes('discount_amount_currency_id', 'required', function($input) {
//            return $input->apply_discount_option === 'Amount';
//        });
//        $validator->sometimes('vat_include_on_discount_amount', 'required', function($input) {
//            return $input->apply_discount_option === 'Amount';
//        });
        $validator->sometimes('discount_percent', 'required|numeric|min:1|regex:/^[0-9]+(\.[0-9]{1,2})*$/', function($input) {
            return $input->apply_discount_option === 'Percent';
        });
        $validator->sometimes('discount_percent', 'required|numeric|min:1|regex:/^[0-9]+(\.[0-9]{1,2})*$/', function($input) {
            return $input->apply_discount_option === 'Percent';
        });
        return $validator;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                $rules = [
                    'name' => ['required','min:3','max:100', Rule::unique('coupons')->where(function ($query) {
                        $query->where('deleted_at', Null);
                        $query->where('company_id', Auth::user()->company_id);
                    })
                    ],
                    'code' => ['required','min:8','max:100', Rule::unique('coupons')->where(function ($query) {
                        $query->where('deleted_at', Null);
                        $query->where('company_id', Auth::user()->company_id);
                    })
                    ],
                    'company_currency_id' => 'required',
                    'valid_from' => 'required|date',
                    'valid_to' => 'required|date|after_or_equal:valid_from',
                    'minimum_purchase_amount' => 'required|numeric|min:1|regex:/^[0-9]+(\.[0-9]{1,2})*$/',
//                    'minimum_purchase_amount_currency_id' => 'required',
                    'vat_include_on_minimum_purchase_amount' => 'required',
                    'total_coupons' => 'required|numeric|min:1|regex:/^[0-9]+()*$/',
                    'total_coupons_for_each_user' => 'required|numeric|min:1|regex:/^[0-9]+()*$/',
                ];
                return $rules;
                break;

            case 'PUT':
                $rules = [
                    'name' => ['required','min:3','max:100', Rule::unique('coupons')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                            $query->where('id', '!=', $this->get('id'));
                        })
                    ],
                    'code' => ['required','min:8','max:100', Rule::unique('coupons')->where(function ($query) {
                        $query->where('deleted_at', Null);
                        $query->where('company_id', Auth::user()->company_id);
                        $query->where('id', '!=', $this->get('id'));
                    })
                    ],
                    'company_currency_id' => 'required',
                    'valid_from' => 'required|date',
                    'valid_to' => 'required|date|after_or_equal:valid_from',
                    'minimum_purchase_amount' => 'required|numeric|min:1|regex:/^[0-9]+(\.[0-9]{1,2})*$/',
                    'vat_include_on_minimum_purchase_amount' => 'required',
                    'total_coupons' => 'required|numeric|min:1|regex:/^[0-9]+()*$/',
                    'total_coupons_for_each_user' => 'required|numeric|min:1|regex:/^[0-9]+()*$/',
                ];

                if($this->input('number_of_products_required_in_carts')==1){ // If this field is un-hide
                    $rules = array_merge($rules,['number_of_products_required_in_carts' => 'required|numeric|min:1|regex:/^[0-9]+()*$/']);
                }
                return $rules;
                break;
        }
    }
}
