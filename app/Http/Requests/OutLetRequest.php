<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class OutLetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules( )
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => ['required','min:3','max:200', Rule::unique('out_lets')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                        })
                    ],
                    'outlet_type_id' => 'required|exists:outlet_types,id',
                    'space' => 'required|numeric',
                    'duration' => 'required|numeric',
                    'capacity' => 'required|numeric',
                    'display_products' => 'required|numeric',
                    'advance_amount' => 'required|regex:/\d+\.?\d*/',
                    'monthly_rent' => 'required|regex:/\d+\.?\d*/',
                    'trade_license_no' => 'required|min:5|max:100',
                    'start_date' => 'required|date_format:Y-m-d',
                    'location' => 'required',
                    'phone' => 'required|regex:/(01)[0-9]{9}$/',
                    'address' => 'required|max:200',
                    'city' => 'required|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',
                    'responsible_person' => 'required|max:200',
                    'trade_license_photo' => 'mimes:jpeg,jpg,png,pdf|max:10000'
                ];
                break;

            case 'PUT':
                return [
                        'name' => ['required','min:3','max:200', Rule::unique('out_lets')->where(function ($query) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                            $query->where('id', '!=', $this->route('out_let'));
                        })
                    ],
                    'outlet_type_id' => 'required|exists:outlet_types,id',
                    'space' => 'required|numeric',
                    'duration' => 'required|numeric',
                    'capacity' => 'required|numeric',
                    'display_products' => 'required|numeric',
                    'advance_amount' => 'required|regex:/\d+\.?\d*/',
                    'monthly_rent' => 'required|regex:/\d+\.?\d*/',
                    'trade_license_no' => 'required|min:5|max:100',
                    'start_date' => 'required|date_format:Y-m-d',
                    'location' => 'required',
                    'phone' => 'required|regex:/(01)[0-9]{9}$/',
                    'address' => 'required|max:200',
                    'city' => 'required|max:50',
                    'zip' => 'required|regex:/^[0-9]{4}$/',
                    'responsible_person' => 'required|max:200',
                    'trade_license_photo' => 'mimes:jpeg,jpg,png,pdf|max:10000'
                ];
                break;
        }
    }
}
