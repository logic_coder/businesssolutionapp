<?php
namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Factory;


class CompanyPrinterRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $printer = $this->route('company_printer');
        switch ( $this->method() ){
            case 'POST':
                $rules = [
                        'name' => ['required','min:2','max:100', Rule::unique('company_printers')->where(function ($query){
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                        })
                    ],                    
                    'model' => 'required|min:3|max:100',
                    'type' => 'required|min:3|max:50',
                    'status' => 'required'
                ];
                return $rules;
                break;

            case 'PUT':
                $rules = [
                    'name' => ['required','min:2','max:100', Rule::unique('company_printers')->where(function ($query) use($printer) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                            $query->where('id', '!=', $printer->id);
                        })
                    ],
                    'model' => 'required|min:3|max:100',
                    'type' => 'required|min:3|max:50',
                    'status' => 'required'
                ];
                return $rules;
                break;
        }
    }
}
