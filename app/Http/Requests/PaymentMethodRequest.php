<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PaymentMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        $payment_method = $this->route('payment_method');
        switch ( $this->method() ){
            case 'POST':
                return [
                    'name' => ['required','min:2','max:50', Rule::unique('payment_methods')->where(function ($query){
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                        })
                    ]
                ];
                break;
            case 'PUT':
                return [
                    'name' => ['required','min:2','max:50', Rule::unique('payment_methods')->where(function ($query) use($payment_method) {
                            $query->where('deleted_at', Null);
                            $query->where('company_id', Auth::user()->company_id);
                            $query->where('id', '!=', $payment_method);
                        })
                    ]
                ];
                break;
        }
    }
}
