<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;

class SalePaidDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        return [     
            'customer_order_details.name_on_card.required' => "Name on card field required",
            'customer_order_details.card_no.required' => "Card no field required",
            'customer_order_details.account_type.required' => "Account type field required",
            'customer_order_details.received_from_no.required' => "Received from no field required",
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        switch ( $this->method() ){
            case 'POST':
                $rules = [];     
                /*if($this->input('name_on_card')==null){
                    $rules = array_merge($rules,['customer_order_details.name_on_card' => 'required']);
                    $rules = array_merge($rules,['customer_order_details.card_no' => 'required']);
                }
                if($this->input('account_type')==null){
                    $rules = array_merge($rules,['customer_order_details.account_type' => 'required']);
                    $rules = array_merge($rules,['customer_order_details.received_from_no' => 'required']);
                }*/       
                return $rules;
                break;
            case 'PUT':
                $rules = [
                    
                ];               
                return $rules;
                break;
        }
    }
}
