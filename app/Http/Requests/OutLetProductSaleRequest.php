<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OutLetProductSaleRequest extends FormRequest
{
    public function messages()
    {
        return [
            'customer_order_detail_items.*.out_let_product_product_combination_id.required' => "Product specification is required",
            'customer_order_detail_items.*.quantity.required' => "Quantity required",
            'customer_order_detail_items.*.quantity.numeric' => "Quantity must be number",
            'customer_order_detail_items.*.quantity.min' => "Quantity at least 1",
            'customer_order_detail_items.*.quantity.regex' => "Quantity format invalid",
        ];
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':                
                $rules = [
                    'customer_order_detail_items.*.out_let_product_product_combination_id' => 'required',
                ];
                $rules = array_merge($rules,['customer_order_detail_items.*.quantity' => 'required|numeric|min:1|regex:/^[0-9]+()*$/']);
                return $rules;
                break;

            case 'PUT':
                $rules = [
                    'customer_order_detail_items.*.out_let_product_product_combination_id' => 'required',
                ];
                $rules = array_merge($rules,['customer_order_detail_items.*.quantity' => 'required|numeric|min:1|regex:/^[0-9]+()*$/']); 
                return $rules;
                break;
        }
    }
}
