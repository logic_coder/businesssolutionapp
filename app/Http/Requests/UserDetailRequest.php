<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                $rules = [];
                if( !empty($this->input('contact_email') ) ){
                    $rules = array_merge($rules,['contact_email' => 'email|unique:users,contact_email,'.$this->route('user')->id]);
                }
                $rules = array_merge($rules,['first_name' => 'required|min:3|max:20']);
                $rules = array_merge($rules,['last_name' => 'required|min:3|max:20']);
                $rules = array_merge($rules,['date_of_birth' => 'required']);
                $rules = array_merge($rules,['cell_phone' => 'required|regex:/(01)[0-9]{9}$/']);
                $rules = array_merge($rules,['work_phone' => 'regex:/(01)[0-9]{9}$/']);
                $rules = array_merge($rules,['address' => 'required|min:15|max:200']);
                $rules = array_merge($rules,['city' => 'required|min:3|max:50']);
                $rules = array_merge($rules,['zip' => 'required|regex:/^[0-9]{4}$/']);
                return $rules;
                break;

            case 'PUT':
                $rules = [];
                if( !empty($this->input('contact_email') ) ){
                    $rules = array_merge($rules,['contact_email' => 'email|unique:users,contact_email,'.$this->route('user')->id]);
                }
                if( !empty($this->input('work_phone') ) ){
                    $rules = array_merge($rules,['work_phone' => 'regex:/(01)[0-9]{9}$/']);
                }
                $rules = array_merge($rules,['first_name' => 'required|min:3|max:20']);
                $rules = array_merge($rules,['last_name' => 'required|min:3|max:20']);
                $rules = array_merge($rules,['date_of_birth' => 'required']);
                $rules = array_merge($rules,['cell_phone' => 'required|regex:/(01)[0-9]{9}$/']);
                $rules = array_merge($rules,['address' => 'required|min:15|max:200']);
                $rules = array_merge($rules,['city' => 'required|min:3|max:50']);
                $rules = array_merge($rules,['zip' => 'required|regex:/^[0-9]{4}$/']);
                return $rules;
                break;
        }
    }
}
