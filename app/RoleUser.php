<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleUser extends Model
{
    use SoftDeletes;
    protected $table = 'role_user';
    protected  $fillable = ['user_id', 'role_id'];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsToMany('App\User');
    }
}
