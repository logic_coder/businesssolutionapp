<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Company;

class WarehouseStoreInChallan extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse_store_in_challans';
    protected $fillable = ['company_id','warehouse_id','supplier_id','challan_no','status','request_date','created_by'];
    protected $dates = ['deleted_at'];

//    public function storein(){
//        return $this->hasMany('App\WarehouseStoreInChallanProduct');
//    }
    public function challanProducts(){
        return $this->hasMany('App\WarehouseStoreInChallanProduct');
    }

}
