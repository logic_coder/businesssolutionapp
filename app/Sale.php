<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use SoftDeletes;
    protected $table = 'sales';
    protected $dates = ['deleted_at'];

    protected $fillable = ['company_id','out_let_id','sales_by','customer_id',
    			'order_no','sale_date','invoice_prefix_id','invoice_no', 
    			'company_currency_id', 'sub_total', 'discount_amount','coupon_id',
				'coupon_discount_amount', 'total_amount_exclusive_vat',
				'vat_amount','net_amount','sales_note','status'
			];
    public function outLet()
    {
        return $this->belongsTo('App\OutLet');
    }    
	public function outLetProductProductCombinationSales()
    {
        return $this->hasMany('App\OutLetProductProductCombinationSale');
    }
    public function salePaids()
    {
        return $this->hasMany('App\SalePaid');
    }
}
