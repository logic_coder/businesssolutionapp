<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutLetUser extends Model
{
    use SoftDeletes;
    protected $table = 'out_let_user';
    protected $fillable = ['user_id', 'out_let_id'];
    protected $dates = ['deleted_at'];
}
