<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutLetProductSale extends Model
{
    use SoftDeletes;
    protected $table = 'out_let_product_sale';
    protected $fillable = ['sale_id','out_let_product_id','name','cost_price','unit_price','quantity'];
    protected $dates = ['deleted_at'];

    public function outLetProduct(){
        return $this->belongsTo('App\OutLetProduct');
    }

}
