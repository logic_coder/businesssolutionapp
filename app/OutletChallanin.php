<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutletChallanin extends Model
{
    use SoftDeletes;
    protected $table = 'outlet_challanins';
    protected $fillable = ['warehouse_storeout_challan_id','user_id','warehouse_id','outlet_id','challan_no','in_date','status'];
    protected $dates = ['deleted_at'];

}
