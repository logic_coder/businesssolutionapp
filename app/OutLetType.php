<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutLetType extends Model
{
    use SoftDeletes;
    protected $table = "outlet_types";
    protected $fillable = ['name'];
    protected $dates = ['deleted_at'];

    public function outlets()
    {
        return $this->hasMany('App\Outlet');
    }
}
