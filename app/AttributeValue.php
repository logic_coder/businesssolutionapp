<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttributeValue extends Model
{
    use SoftDeletes;
    protected $table = 'attribute_values';
    protected $fillable = ['attribute_id','value','color'];
    protected $dates = ['deleted_at'];

    public function attribute()
    {
        return $this->belongsTo('App\Attribute');
    }
}
