<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerOrderDetailPaidDetail extends Model
{
    use SoftDeletes;
    protected $table = 'customer_order_detail_paid_details';
    protected $fillable = ['customer_order_detail_paid_id','meta_data', 'value'];
    protected $dates = ['deleted_at'];
}
