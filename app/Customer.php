<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    protected $table = 'customers';

    protected $fillable = ['company_id','out_let_id', 'gender_id', 'country_id', 'customer_no', 'first_name', 'last_name', 'cell_phone',
        'work_phone', 'email', 'date_of_birth', 'address', 'address2', 'city', 'zip'];
    protected $dates = ['deleted_at'];
}
