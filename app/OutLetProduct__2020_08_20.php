<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutLetProduct extends Model
{
    use SoftDeletes;
    protected $table = 'out_let_product';
    protected  $fillable = ['id','out_let_id','product_id', 'product_combination_id','stock_quantity'];
    protected $dates = ['deleted_at'];

    public function product(){
        return $this->belongsTo('App\Product');
    }
}
