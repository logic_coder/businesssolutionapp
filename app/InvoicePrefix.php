<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoicePrefix extends Model
{
    use SoftDeletes;
    protected $table = 'invoice_prefixes';
    protected  $fillable = ['company_id', 'name','default'];
    protected $dates = ['deleted_at'];
}
