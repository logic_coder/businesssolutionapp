<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductDiscount extends Model
{
    use SoftDeletes;
    protected $table = 'product_discounts';
    protected $fillable = ['product_id', 'company_id', 'discount_amount', 'discount_percent','active_date','expire_date'];
    protected $dates = ['deleted_at'];

}
