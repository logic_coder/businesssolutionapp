<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutLetProduct extends Model
{
    use SoftDeletes;
    protected $table = 'out_let_products';
    protected  $fillable = ['id','out_let_id','product_id'];
    protected $dates = ['deleted_at'];

    public function product(){
        return $this->belongsTo('App\Product');
    }
    public function outLetProductCombinations() // Each outlet has outlet product and each product has many product combinations
    {
        return $this->belongsToMany('App\ProductCombination')->withPivot('id','stock_quantity');
    }
    public function out_let_product_combination()
    {
        return $this->hasMany('App\OutLetProductProductCombination', 'out_let_product_id');
    }
}

