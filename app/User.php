<?php

namespace App;
//use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Authenticatable implements JWTSubject
{
    //use HasApiTokens, Notifiable;
    use  Notifiable,SoftDeletes;
    protected $table = 'users';
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id','email', 'contact_email', 'password','is_pos_admin','active'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    public function company()
    {
        return $this->hasOne('App\Company');
    }
    /**
     * Get the user info record associated with the user.
     */
    public function userDetail()
    {
        return $this->hasOne('App\UserDetail');
    }

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function hasRole($roleData){

        if(is_string($roleData)){
            return $this->roles->contains('slug',$roleData);
        }

        foreach ($roleData as $role){
            if($this->hasRole($role->slug)){
                return true;
            }
        }
        return false;
    }
    public function role()
    {
        return $this->hasOne('App\Role');
    }
    public function out_lets(){
        return $this->belongsToMany('App\OutLet');
    }
}
