<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductRestriction extends Model
{
    use SoftDeletes;
    protected $table = 'product_restriction';
    protected $fillable = ['restriction_id', 'product_id'];

    protected $dates = ['deleted_at'];

//    public function restrictions(){
//        return $this->belongsToMany('App\Restrictions');
//    }
}
