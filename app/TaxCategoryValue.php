<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxCategoryValue extends Model
{
    use SoftDeletes;
    protected $table = "tax_category_values";
    protected $fillable = ['name','percent'];
    protected $dates = ['deleted_at'];

}
