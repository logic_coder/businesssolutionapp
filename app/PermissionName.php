<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PermissionName extends Model
{
    use SoftDeletes;
    protected $table = "permission_names";
    protected $fillable = "permission_name";
    protected $dates = ['deleted_at'];

}
