<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $table = 'products';
    protected $fillable = ['company_id','name','item_code','bar_code','reference_code', 'date_added'];
    protected $dates = ['deleted_at'];

    public function combinations()
    {
        return $this->hasMany('App\ProductCombination');
    }
    public function prices()
    {
        return $this->hasMany('App\ProductPrice')->orderBy('id', 'DESC');
    }
    public function images()
    {
        return $this->hasMany('App\ProductImage');
    }
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
    public function company()
    {
        return $this->belongsTo('App\Company');
    }
    public function restrictions()
    {
        return $this->belongsToMany('App\CouponRestriction');
    }
    public function outLets()
    {
        //return $this->belongsToMany('App\OutLet')->withPivot('id','product_combination_id','stock_quantity');
        return $this->belongsToMany('App\OutLet');
    }
//    public function challans()
//    {
//        return $this->belongsToMany('App\WarehouseStoreInChallan');
//    }
}
