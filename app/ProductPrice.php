<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPrice extends Model
{
    use SoftDeletes;
    protected $table = 'product_prices';
    protected $fillable = ['product_id','vat_id','price', 'cost', 'cost_price_date','active'];
    protected $dates = ['deleted_at'];

    public function product(){
        return $this->hasOne('App\Product');
    }
    public function vat(){
        return $this->belongsTo('App\Vat');
    }

}
