<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseStoreInChallanProduct extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse_store_in_challan_products';
    protected $fillable = ['warehouse_store_in_challan_id', 'product_id'];
    protected $dates = ['deleted_at'];

//    public function storeInCombinations(){
//        return $this->hasMany('App\WarehouseStoreInChallanProductCombination');
//    }
    public function product(){
        return $this->belongsTo('App\Product');
    }
    public function storeInProductCombinations()
    {
        return $this->belongsToMany('App\ProductCombination')->withPivot('id','requested_quantity','received_quantity','stock_quantity','missing_status');
    }
    public function product_combination()
    {
        return $this->hasMany('App\ProductCombinationWarehouseStoreInChallanProduct', 'warehouse_store_in_challan_product_id');
    }
}
