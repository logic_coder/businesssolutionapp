<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerOrderDetailItem extends Model
{
    use SoftDeletes;
    protected $table = 'customer_order_detail_items';
    protected $fillable = ['customer_order_detail_id','description','category_id',
        'product_id','quantity','unit_price', 'weight_tola','weight_ana','weight_rotti', 'weight_milli', 'weight_gram',
        'material_price', 'making_charge','price_exclusive_vat','vat_id','vat_amount', 'price_inclusive_vat', 'discount_amount',
        'status',
        ];
    protected $dates = ['deleted_at'];
}
