<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;

    protected $table = 'suppliers';
    protected $fillable = ['company_id','name','code','address','address2','city','zip','country_id','email','phone'];
    protected $dates = ['deleted_at'];
}
