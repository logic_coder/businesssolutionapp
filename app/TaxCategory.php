<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxCategory extends Model
{
    use SoftDeletes;
    protected $table = "tax_categories";
    protected $fillable = ['tax_category_name'];
    protected $dates = ['deleted_at'];

}
