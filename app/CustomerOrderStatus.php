<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;


class CustomerOrderStatus extends Model
{
    use SoftDeletes;
    // use Sluggable;

    protected $fillable = ['name','slug'];
    protected $dates = ['deleted_at'];
    
    
    // public function sluggable(): array
    // {
    //     return [
    //         'slug' => [
    //             'source' => 'name',
    //             'onUpdate' => true
    //         ]
    //     ];
    // }
}

