<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CountryPaymentMethod extends Model
{
    use SoftDeletes;
    protected $table = "country_payment_methods";
    protected $fillable = ['country_id','name'];
    protected $dates = ['deleted_at'];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
}
