<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalePaid extends Model
{
    use SoftDeletes;
    protected $table = 'sale_paids';
    protected $fillable = ['sale_id','payment_method_id','paid_amount', 'paid_date'];
    protected $dates = ['deleted_at'];

    public function salePaidDetails()
    {
        return $this->hasMany('App\SalePaidDetail');
    }
    public function paymentMethod()
    {
        return $this->belongsTo('App\PaymentMethod');
    }
}
