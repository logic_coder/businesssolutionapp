<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCombinationWarehouseStoreInChallanProduct extends Model
{
//    use SoftDeletes;
//    protected $table = 'product_combination_warehouse_store_in_challan';
//    protected $fillable = ['warehouse_store_in_challan_product_id', 'product_combination_id'];
//    protected $dates = ['deleted_at'];
    use SoftDeletes;
    protected $table = 'product_combination_warehouse_store_in_challan_product';
    protected $fillable = ['id','warehouse_store_in_challan_product_id', 'product_combination_id','requested_quantity','received_quantity','stock_quantity'];
    protected $dates = ['deleted_at'];

    public function combination(){
        return $this->belongsTo('App\ProductCombination','product_combination_id');
    }
}
