<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryProduct extends Model
{
    use SoftDeletes;
    protected $table = 'category_product';
    protected $fillable = ['category_id','product_id','active','possition'];
    protected $dates = ['deleted_at'];

   
}
