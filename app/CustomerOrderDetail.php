<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerOrderDetail extends Model
{
    use SoftDeletes;
    protected $table = 'customer_order_details';
    protected $fillable = ['company_id','out_let_id', 'sales_by', 'customer_id', 'order_no', 'order_date', 'expected_delivery_date',
        'delivered_date', 'invoice_prefix_id', 'invoice_no','company_currency_id','total_amount_exclusive_vat','total_vat',
        'total_amount_inclusive_vat', 'total_item_discount','coupon_id','coupon_discount_amount','net_amount',
        'status'];
    protected $dates = ['deleted_at'];

    public function customerOrderDetailItems()
    {
        return $this->hasMany('App\CustomerOrderDetailItem');
    }
    public function customerOrderDetailPaids()
    {
        return $this->hasMany('App\CustomerOrderDetailPaid');
    }
}

