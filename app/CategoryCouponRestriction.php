<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryCouponRestriction extends Model
{
    use SoftDeletes;
    protected $table = 'category_coupon_restriction';
    protected $fillable = ['coupon_restriction_id', 'category_id'];

    protected $dates = ['deleted_at'];
}
