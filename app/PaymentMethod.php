<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
    use SoftDeletes;
    protected $table = "payment_methods";
    protected $fillable = ['company_id','name'];
    protected $dates = ['deleted_at'];

    public function customerOrderDetailPaid()
    {
        return $this->hasMany('App\CustomerOrderDetailPaid');
    }
    public function salePaid()
    {
        return $this->hasMany('App\SalePaid');
    }
}
