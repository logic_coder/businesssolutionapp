<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CouponRestriction extends Model
{
    use SoftDeletes;
    protected $table = 'coupon_restrictions';
    protected $fillable = ['coupon_id', 'number_of_products_required_in_carts', 'rule_concerning_products', 'rule_concerning_categories'];
    
    protected $dates = ['deleted_at'];
    public function coupon(){
        return $this->belongsTo('App\Coupon');
    }
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
