<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutLetProductProductCombinationSale extends Model
{
    use SoftDeletes;
    protected $table = 'out_let_product_product_combination_sales';
    protected $fillable = ['sale_id','out_let_product_product_combination_id','product_id','name','cost_price','unit_price','quantity'];
    protected $dates = ['deleted_at'];

	public function sale()
    {
        //return $this->belongsToMany('App\OutLetProductProductCombination')->withPivot('id','name','cost_price','unit_price','quantity');
        return $this->belongsTo('App\Sale');
    }
    public function outLetProductProductCombination(){
        return $this->belongsTo('App\OutLetProductProductCombination');
    }
}
