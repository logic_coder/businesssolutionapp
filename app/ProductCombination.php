<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCombination extends Model
{
    use SoftDeletes;
    protected $table = 'product_combinations';
    protected $fillable = ['product_id'];
    protected $dates = ['deleted_at'];

    public function product(){
        return $this->belongsTo('App\Product');
    }
    public function combinationAttributeValues(){
        return $this->hasMany('App\ProductCombinationAttribute');
    }
    public function challanProducts()
    {
        return $this->belongsToMany('App\WarehouseStoreInChallanProduct')->withPivot('quantity');
    }
    public function outLetProducts()
    {
        return $this->belongsToMany('App\OutLetProduct');
    }
}