<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutLetProductProductCombination extends Model
{
    use SoftDeletes;
    protected $table = 'out_let_product_product_combination';
    protected $fillable = ['id','out_let_product_id','product_combination_id','stock_quantity'];
    protected $dates = ['deleted_at'];

    public function sales(){
        return $this->belongsTo('App\Sale');
    }
    public function productCombination(){
        return $this->belongsTo('App\ProductCombination');
    }
    public function outletProduct(){
        return $this->belongsTo('App\OutLetProduct',  'out_let_product_id');
    }
}