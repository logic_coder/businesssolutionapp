<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerReward extends Model
{
    use SoftDeletes;
    protected $table = 'customer_rewards';
    protected $fillable = [ 'daily_sale_id','reward_percent','reward_amount','total_reward_amount','expire_date' ];
    protected $dates = ['deleted_at'];

}
