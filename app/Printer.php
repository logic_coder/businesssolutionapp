<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Printer extends Model
{
    use SoftDeletes;
    protected $table = "printers";
    protected $fillable = ['company_id','name','model','type','status'];
    protected $dates = ['deleted_at'];
    
    public function company(){
        return $this->belongsTo('App\Company');
    }
    public function outlets(){
        return $this->belongsToMany('App\OutLet');
    }
}
