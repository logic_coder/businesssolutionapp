<?php

namespace App\Providers;
use App\Permission;
use App\User;
use Illuminate\Support\Facades\Schema;
//use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('permissions')) {
            $this->registerPolicies();
            foreach ($this->getPermissions() as $permission) {
                Gate::define($permission->slug, function ($user) use ($permission) {
                    return $user->hasRole($permission->roles);
                });
            }
            //Passport::routes();
            //Passport::enableImplicitGrant();
        }
    }
    protected function getPermissions(){
        $permissions =  Permission::with('roles')->get();
        if($permissions->count()>0){
            return $permissions;
        }else{
            return [];
        }
    }
}
