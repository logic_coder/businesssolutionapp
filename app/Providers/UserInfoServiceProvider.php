<?php

namespace App\Providers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\ServiceProvider;
use Cache;
use Illuminate\Contracts\Auth\Guard;


class UserInfoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Guard $auth) {
        /* view()->composer('*', function($view) use ($auth) {
             $currentUser = $auth->user();
             $view->with('currentUser', $currentUser);
        }); */
    }
}
