<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseStoreOutChallanProduct extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse_store_out_challan_products';
    protected $fillable = ['warehouse_store_out_challan_id', 'product_id'];
    protected $dates = ['deleted_at'];

    public function product(){
        return $this->belongsTo('App\Product');
    }
    public function storeOutProductCombinations()
    {
        return $this->belongsToMany('App\ProductCombination')->withPivot('id','product_combination_warehouse_store_in_challan_product_id','requested_quantity','received_quantity','stock_quantity','missing_status');
    }
    public function product_combination()
    {
        return $this->hasMany('App\ProductCombinationWarehouseStoreOutChallanProduct', 'warehouse_store_out_challan_product_id');
    }
}
