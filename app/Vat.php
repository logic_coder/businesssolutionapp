<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vat extends Model
{
    use SoftDeletes;
    protected $table = 'vats';
    protected  $fillable = ['company_id', 'percent','name'];
    protected $dates = ['deleted_at'];

    public function productPrice()
    {
        return $this->belongsToMany('App\ProductPrice');
    }
}
