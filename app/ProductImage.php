<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImage extends Model
{
    use SoftDeletes;
    protected $table = 'product_images';
    protected $fillable = ['product_id','name','is_cover_image'];
    protected $dates = ['deleted_at'];
    public function product(){
        return $this->belongsTo('App\Product');
    }
}
