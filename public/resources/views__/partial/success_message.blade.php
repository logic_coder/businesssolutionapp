<?php
    use Illuminate\Support\Facades\Redis;
?>
<div class="text-center">
    @include('flash::message')
</div>
@if ( Redis::get('success') )
    <div class="alert  alert-success text-center">
        {{ Redis::get('success') }}
    </div>
@endif
<?php
    Redis::set('success', null);
?>