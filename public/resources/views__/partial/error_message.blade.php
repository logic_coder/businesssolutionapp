@if (count($errors) > 0)
    <div class="alert alert-danger text-center">
        @foreach ($errors->all() as $error)
            {{ $error }} <br/>
        @endforeach
    </div>
@endif
@if ( Redis::get('error') )
    <div class="alert alert-danger text-center">
        {{ Redis::get('error') }}
    </div>
@endif
<?php
    Redis::set('error', null);
?>