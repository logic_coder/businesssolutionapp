<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
    <div class="sidebar-overlay-slide from-top" id="appMenu">
        <div class="row">
            <div class="col-xs-6 no-padding">
                <a href="#" class="p-l-40">
                    <img src="{{ asset('images/social_app.svg') }}" alt="socail" data-src="{{ asset('images/social_app.svg') }}" data-src-retina="{{ asset('images/social_app.svg') }}" width="78" height="22" >
                </a>
            </div>
            <div class="col-xs-6 no-padding">
                <a href="#" class="p-l-10">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 m-t-20 no-padding">
                <a href="#" class="p-l-40">
                    <img src="{{ asset('images/calendar_app.svg') }}" alt="socail" data-src="{{ asset('images/calendar_app.svg') }}" data-src-retina="{{ asset('images/calendar_app.svg') }}" >
                </a>
            </div>
            <div class="col-xs-6 m-t-20 no-padding">
                <a href="#" class="p-l-10">
                    <img src="{{ asset('images/add_more.svg') }}" alt="socail" data-src="{{ asset('images/add_more.svg') }}" data-src-retina="{{ asset('images/add_more.svg') }}" >
                </a>
            </div>
        </div>
    </div>
    <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <img src="{{ asset('images/logo_white.png') }}" alt="logo" class="brand" data-src="{{ asset('images/logo_white.png') }}" data-src-retina="{{ asset('images/logo_white.png') }}" width="78" height="22">
        <div class="sidebar-header-controls">
            {{--<button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu"><i class="fa fa-angle-down fs-16"></i> </button>--}}
            <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i></button>
        </div>
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
        @can('access-dashboard')
                <li class="m-t-30 open">
                    <a href="index.html" class="detailed">
                        <span class="title">Dashboard</span>
                        <span class="details">12 New Updates</span>
                    </a>
                    <span class="icon-thumbnail bg-success"><i class="pg-home"></i></span>
                </li>
            @endcan
            @can('create-view-customer')
                <li class="">
                    <a href="javascript:;">
                        <span class="title">Customers</span>
                        <span class="arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                    <ul class="sub-menu">
                        <li class="">
                            <a href="{{url('frontend/customer')}}">Customers</a>
                            <span class="icon-thumbnail">U</span>
                        </li>
                    </ul>
                </li>
            @endcan
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>