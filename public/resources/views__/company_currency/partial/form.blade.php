<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Company Currency Settings</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required form-group-default-selectFx">
                            {{ Form::label('Currency', 'Currency') }}
{{--                            {{Form::select('currency_id',$currency_lists,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control attribute-group-select','data-init-plugin' => 'cs-select']) }}--}}
                            <select class="cs-select cs-skin-slide cs-transparent form-control attribute-group-select" data-init-plugin ="cs-select" name="currency_id">
                                @foreach($currency_lists as $currency_list)
                                    <option @if(!empty($company_currencies)) @if($currency_list->id == $company_currencies->currency_id) selected  @endif @endif  value="{{$currency_list->id}}" >{{$currency_list->name}}( {{$currency_list->code}} )</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br/>
                    <div class="toggle-on-off toggle-padding-custom">
                        {{ Form::label('default', 'Default') }}:
                        <input type="checkbox" data-init-plugin="switchery" @php if($company_currencies) { if( $company_currencies->default=="Yes" ) echo 'checked'; } @endphp name="default"/>
                    </div>
                    <div class="form-group-attached pull-right padding-top">
                        <button class="btn btn-success btn-cons" type="submit">
                            <span>Save</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>