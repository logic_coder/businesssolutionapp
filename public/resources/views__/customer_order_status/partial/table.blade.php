<button class="btn delete-multi-items" action-url="customer_order_status/remove_customer_order_status" redirect-url="/customer_order_status"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Slug</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $customerOrderStatuses as $customerOrderStatus )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="item_ids[]" value={{$customerOrderStatus->id}} data-id="checkbox" id="checkbox{{$customerOrderStatus->id}}">
                    <label for="checkbox{{$customerOrderStatus->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                {{ $customerOrderStatus->name }}
            </td>
            <td class="v-align-middle">
                {{ $customerOrderStatus->slug }}
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn btn-success" href="{{url('customer_order_status/'.$customerOrderStatus->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$customerOrderStatus->id}}" type="button" class="btn btn-danger delete-single-item" action-url="customer_order_status/remove_customer_order_status" redirect-url="/customer_order_status"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
