<button class="btn delete-multi-items" action-url="user_title/remove_user_title" redirect-url="/user_title"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Title / Possition</th>
        <th style="width:20%">Last Updated</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach( $user_titles as $user_title )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="item_ids[]" value={{$user_title->id}} data-id="checkbox" id="checkbox{{$user_title->id}}">
                    <label for="checkbox{{$user_title->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <p>{{$user_title->title}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{ date('F d, Y H:i:s', strtotime($user_title->updated_at)) }}</p>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn btn-success" href="{{url('user_title/'.$user_title->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$user_title->id}}" type="button" class="btn btn-danger delete-single-item" action-url="user_title/remove_user_title" redirect-url="/user_title"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
