<button class="btn delete-multi-items" action-url="customer_order_detail/remove_customer_order_detail" redirect-url="/frontend/customer_order_detail"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Invoice No</th>
        <th style="width:20%">Outlet</th>
        <th style="width:20%">Customer</th>
        <th style="width:20%">Order No.</th>
        <th style="width:20%">Order By</th>
        <th style="width:20%">Order Date</th>
        <th style="width:20%">Expected Delivery</th>
        <th style="width:20%">Delivered</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @if( $customerOrderDetails )
        @foreach( $customerOrderDetails as $orderDetails )
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input class="item_ids" type="checkbox" name="item_ids[]" value={{$orderDetails->id}} data-id="checkbox" id="checkbox{{$orderDetails->id}}">
                        <label for="checkbox{{$orderDetails->id}}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    @if($orderDetails->invoice_prefix_id)
                        {{ App\InvoicePrefix::find($orderDetails->invoice_prefix_id)->name}}{{$orderDetails->invoice_no}}
                    @else
                        {{$orderDetails->invoice_no}}
                    @endif
                    <br/>
                    @can('access-sales-page')
                            <a href="{{url('frontend/customer_order_detail/'.$orderDetails->id.'/get_paid_details')}}">Check Paid Details </a>
                    @endcan
                </td>
                <td class="v-align-middle">
                    {{ App\OutLet::find($orderDetails->out_let_id)->name}}
                </td>
                <td class="v-align-middle">
                    {{ App\Customer::find($orderDetails->customer_id)->first_name}} {{ App\Customer::find($orderDetails->customer_id)->last_name}}
                    ( {{ App\Customer::find($orderDetails->customer_id)->customer_no}} )
                </td>
                <td class="v-align-middle">
                    {{ $orderDetails->order_no }}
                </td>
                <td class="v-align-middle">
                     @if(isset($orderDetails->sales_by) && App\UserDetail::find($orderDetails->sales_by) )
                        {{ App\UserDetail::find($orderDetails->sales_by)->first_name }} 
                    @else
                        Not Available
                    @endif
                    @if(isset($orderDetails->sales_by) && App\UserDetail::find($orderDetails->sales_by))
                        {{ App\UserDetail::find($orderDetails->sales_by)->last_name }} 
                    @endif  
                </td>
                <td class="v-align-middle">
                    {{ $orderDetails->order_date }}
                </td>
                <td class="v-align-middle">
                    {{ $orderDetails->expected_delivery_date }}
                </td>
                <td class="v-align-middle">
                    {{ $orderDetails->delivered_date }}
                </td>
                <td class="v-align-middle">
                    {{ $orderDetails->status }}
                </td>
                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        @can('edit-sales')
                            <a title="Edit" class="btn btn-success" href="{{url('frontend/customer_order_detail/'.$orderDetails->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        @endcan
                        @can('delete-sales')
                            <button title="Delete" item-ids="{{$orderDetails->id}}" type="button" class="btn btn-danger delete-single-item" action-url="customer_order_detail/remove_customer_order_detail" redirect-url="/frontend/customer_order_detail"><i class="fa fa-trash-o"></i></button>
                        @endcan
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>