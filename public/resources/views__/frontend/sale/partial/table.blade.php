'<button class="btn delete-multi-items" action-url="sale_detail/remove_sale" redirect-url="/frontend/sale"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Invoice No</th>
        <th style="width:20%">Outlet</th>
        <th style="width:20%">Customer</th>
        <th style="width:20%">Sales No.</th>
        <th style="width:20%">Sales By</th>
        <th style="width:20%">Sales Date</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead> 
    <tbody>
    @if( $saleDetails )
        @foreach( $saleDetails as $saleDetail )
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input class="item_ids" type="checkbox" name="item_ids[]" value={{$saleDetail->id}} data-id="checkbox" id="checkbox{{$saleDetail->id}}">
                        <label for="checkbox{{$saleDetail->id}}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    @if($saleDetail->invoice_prefix_id)
                        {{ App\InvoicePrefix::find($saleDetail->invoice_prefix_id)->name}}{{$saleDetail->invoice_no}}
                    @else
                        {{$saleDetail->invoice_no}}
                    @endif                    
                </td>
                <td class="v-align-middle">
                    {{ App\OutLet::find($saleDetail->out_let_id)->name }}
                </td>
                <td class="v-align-middle">
                    @if(isset($saleDetail->customer_id))
                        {{ App\Customer::find($saleDetail->customer_id)->first_name }} 
                        {{ App\Customer::find($saleDetail->customer_id)->last_name }}
                        ( {{ App\Customer::find($saleDetail->customer_id)->customer_no}} )
                    @else
                        Guest
                    @endif
                </td>
                <td class="v-align-middle">
                    {{ $saleDetail->order_no }}
                </td>
                <td class="v-align-middle">
                    @if(isset($saleDetail->sales_by) && App\UserDetail::find($saleDetail->sales_by) )
                        {{ App\UserDetail::find($saleDetail->sales_by)->first_name }} 
                    @else
                        Not Available
                    @endif
                    @if(isset($saleDetail->sales_by) && App\UserDetail::find($saleDetail->sales_by))
                        {{ App\UserDetail::find($saleDetail->sales_by)->last_name }} 
                    @endif            
                </td>
                <td class="v-align-middle">
                     {{ $saleDetail->sale_date }}
                </td>  
                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        @can('edit-sales')
                            <a title="Edit" class="btn btn-success" href="{{url('frontend/sale/'.$saleDetail->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        @endcan
                        @can('delete-sales')
                            <button title="Delete" item-ids="{{$saleDetail->id}}" type="button" class="btn btn-danger delete-single-item" action-url="sale_detail/remove_sale" redirect-url="/frontend/sale"><i class="fa fa-trash-o"></i></button>
                        @endcan
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>