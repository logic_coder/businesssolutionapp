<div class=null>
  <!-- Start Heading Panel -->
  <div class="row row-same-height">
    <div class="panel-default">
      <div class="panel-heading title-color">
        <h4 class="panel-title">MyPOS: SA Jewellers</h4>
      </div>                
    </div>
  </div>
  <!--End Heading Panel -->
  <!--Start sale-panel -->
  <div class="row row-same-height">
    <!--Start sale-panel-product-lists --> 
    <div class="panel panel-default sale-panel-product-lists">                
      <div class="panel-body sale-panel">
        <!-- Start Search Panel -->
        <div class="sale-panel-search-item">
          <div class="form-group-attached">
            <div class="row clearfix">                                   
              <div class="col-sm-6 col-div-100">
                <div class="form-group form-group-default">
                  <label for="category_id">Search item</label>
                  <input class ="form-control" placeholder="Item name">
                </div>
              </div>
              <div class="col-sm-6 col-div-33">
                <div class="form-group form-group-default">
                  <div class="btn-group btn-challan-product-div">
                    <a  title="product-search" class="btn btn-success btn-cons btn-challan-product-search">Search</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <br>
        </div>                
        <div class="col-md-7 col-div-full">
          <div class="sale-panel-product">
            <div class="form-group-attached">
              <div class="row clearfix">      
                <div class="col-sm-6 col-div-100">                             
                  <div class="btn-group btn-challan-product-div">
                    <a  title="product-search" class="btn btn-success btn-cons btn-challan-product-search">Product</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="sale-panel-category">
            <div class="form-group-attached">
              <div class="row clearfix">                                   
                <div class="col-sm-6 col-div-100">
                  <div class="form-group form-group-default">
                    <label for="category_id">Select Category</label>
                    <select  class="cs-select cs-skin-slide cs-transparent form-control">
                      <option value="">Select Category</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6 col-div-33">
                  <div class="form-group form-group-default">
                    <div class="btn-group btn-challan-product-div">
                      <a  title="product-search" class="btn btn-success btn-cons btn-challan-product-search">Search</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br>
          </div>
        </div>
        <!-- End Search Panel -->
        <!-- START CATEGORY -->
        <div class="gallery">                  
          <!-- START GALLERY ITEM -->
          <!-- 
                FOR DEMO PURPOSES, FIRST GALLERY ITEM (.first) IS HIDDEN 
                FOR SCREENS <920px. PLEASE REMOVE THE CLASS 'first' WHEN YOU IMPLEMENT 
            -->                  
          <div class="gallery-item" data-width="1" data-height="1">
            <!-- START PREVIEW -->
            <img src="{{ asset('uploads/gallery/1.jpg') }}" alt="" class="image-responsive-height">
            <!-- END PREVIEW -->
            <!-- START ITEM OVERLAY DESCRIPTION -->
            <div class="overlayer bottom-left full-width">
              <div class="overlayer-wrapper item-info ">
                <div class="p-l-20 p-r-20 p-t-20 p-b-5">
                  <div class="">
                    <p class="pull-left bold text-white fs-14 p-t-10">Happy Ninja</p>
                    <h5 class="pull-right semi-bold text-white font-montserrat bold">$25.00</h5>                        
                  </div>                      
                </div>
              </div>
            </div>
            <!-- END PRODUCT OVERLAY DESCRIPTION -->
          </div>
        
          <!-- END GALLERY ITEM -->

          <!-- START GALLERY ITEM -->
          <!-- 
                FOR DEMO PURPOSES, FIRST GALLERY ITEM (.first) IS HIDDEN 
                FOR SCREENS <920px. PLEASE REMOVE THE CLASS 'first' WHEN YOU IMPLEMENT 
            -->
          <div class="gallery-item" data-width="1" data-height="1">
            <!-- START PREVIEW -->
            <img src="{{ asset('uploads/gallery/2_1.jpg') }}" alt="" class="image-responsive-height">
            <!-- END PREVIEW -->
            <!-- START ITEM OVERLAY DESCRIPTION -->
            <div class="overlayer bottom-left full-width">
              <div class="overlayer-wrapper item-info ">
                <div class="p-l-20 p-r-20 p-t-20 p-b-5">
                  <div class="">
                    <p class="pull-left bold text-white fs-14 p-t-10">Happy Ninja</p>
                    <h5 class="pull-right semi-bold text-white font-montserrat bold">$25.00</h5>                        
                  </div>                      
                </div>
              </div>
            </div>
            <!-- END PRODUCT OVERLAY DESCRIPTION -->
          </div>
          <!-- END GALLERY ITEM -->

          <!-- START GALLERY ITEM -->
          <!-- 
                FOR DEMO PURPOSES, FIRST GALLERY ITEM (.first) IS HIDDEN 
                FOR SCREENS <920px. PLEASE REMOVE THE CLASS 'first' WHEN YOU IMPLEMENT 
            -->
          <div class="gallery-item" data-width="1" data-height="1">
            <!-- START PREVIEW -->
            <img src="{{ asset('uploads/gallery/2_2.jpg') }}" alt="" class="image-responsive-height">
            <!-- END PREVIEW -->
            <!-- START ITEM OVERLAY DESCRIPTION -->
            <div class="overlayer bottom-left full-width">
              <div class="overlayer-wrapper item-info ">
                <div class="p-l-20 p-r-20 p-t-20 p-b-5">
                  <div class="">
                    <p class="pull-left bold text-white fs-14 p-t-10">Happy Ninja</p>
                    <h5 class="pull-right semi-bold text-white font-montserrat bold">$25.00</h5>                        
                  </div>                      
                </div>
              </div>
            </div>
            <!-- END PRODUCT OVERLAY DESCRIPTION -->
          </div>
          <!-- END GALLERY ITEM -->

          <!-- START GALLERY ITEM -->
          <!-- 
                FOR DEMO PURPOSES, FIRST GALLERY ITEM (.first) IS HIDDEN 
                FOR SCREENS <920px. PLEASE REMOVE THE CLASS 'first' WHEN YOU IMPLEMENT 
            -->
          <div class="gallery-item" data-width="1" data-height="1">
            <!-- START PREVIEW -->
            <img src="{{ asset('uploads/gallery/3.jpg') }}" alt="" class="image-responsive-height">
            <!-- END PREVIEW -->
            <!-- START ITEM OVERLAY DESCRIPTION -->
            <div class="overlayer bottom-left full-width">
              <div class="overlayer-wrapper item-info ">
                <div class="p-l-20 p-r-20 p-t-20 p-b-5">
                  <div class="">
                    <p class="pull-left bold text-white fs-14 p-t-10">Happy Ninja</p>
                    <h5 class="pull-right semi-bold text-white font-montserrat bold">$25.00</h5>                        
                  </div>                      
                </div>
              </div>
            </div>
            <!-- END PRODUCT OVERLAY DESCRIPTION -->
          </div>
          <!-- END GALLERY ITEM -->              
        </div>
        <!-- END CATEGORY -->
      </div>
    </div>      
    <!--End sale-panel-product-lists -->
    <!--Start sale-panel-item-lists -->
    <div class="panel panel-default sale-panel-item-lists">               
      <div class="panel-body sale-panel">            
        <div class="sales-item-lists">          
          <div class="sales-item-descrption float-left">
            <label for="item_description">BALLOON LEG - Jeans relaxed fit</label>
          </div>                                
          <div class="sales-item-quantity general_field float-left">
            <input class ="form-control" type="text" placeholder=""> 
          </div>  
          <div class="sales-item-unit-price float-left">
            <div for="item_unit_price">x 4.00</div>                  
          </div>                
          <div class="sales-item-total-price float-right">
            <div for="item_total_price">4.00</div>                  
          </div>                                
        </div> 
        <div class="clearfix"></div>
        <hr>
        <div class="sales-item-lists">
          <div class="sales-item-descrption float-left">
            <label for="item_description">BALLOON LEG - Jeans relaxed fit</label>
          </div>                                
          <div class="sales-item-quantity general_field float-left">
            <input class ="form-control" type="text" placeholder=""> 
          </div>  
          <div class="sales-item-unit-price float-left">
            <div for="item_unit_price">x 4.00</div>                  
          </div>                
          <div class="sales-item-total-price float-right">
            <div for="item_total_price">4.00</div>                  
          </div>                                
        </div> 
        <div class="clearfix"></div>
        <hr>  
        <div class="sales-item-lists">
          <div class="sales-item-descrption float-left">
            <label for="item_description">BALLOON LEG - Jeans relaxed fit</label>
          </div>                                
          <div class="sales-item-quantity general_field float-left">
            <input class ="form-control" type="text" placeholder=""> 
          </div>  
          <div class="sales-item-unit-price float-left">
            <div for="item_unit_price">x 4.00</div>                  
          </div>                
          <div class="sales-item-total-price float-right">
            <div for="item_total_price">4.00</div>                  
          </div>                                
        </div> 
        <div class="clearfix"></div>
        <hr>           
      </div> 
      <div class="payment-calculation-divider"> </div>
      <!--Start payment-calculation -->    
      <div class="panel-heading sale-panel-item-lists heading-color">
        <div class="payment-calculation-title float-left"> Subtotal </div>
        <div class="payment-calculation-amount float-right"> 400 </div>          
      </div>    
      <!--End payment-calculation -->
      <!--Start payment-calculation -->
      <div class="panel-heading sale-panel-item-lists heading-color">
        <div class="payment-calculation-title float-left"> Discount </div>
        <div class="payment-calculation-discount float-right"> 
          <input class ="form-control payment-calculation-input-text" type="text" placeholder="Amount">
        </div>          
      </div>
      <!--End payment-calculation -->
      <!--Start payment-calculation -->    
      <div class="panel-heading sale-panel-item-lists heading-color">
        <div class="payment-calculation-title float-left"> Coupon Discount </div>
        <div class="payment-calculation-coupon-code float-left"> 
          <div class="payment-calculation-coupon-code-text-field float-left">        
            <input class ="form-control" type="text" placeholder="Coupon code">
          </div>
          <div class="payment-calculation-coupon-code-button float-left">
            <button class="btn btn-success">Apply</button>
          </div>
        </div> 
        <div class="payment-calculation-coupon-discount-amount float-left"> 
          <input class ="form-control payment-calculation-input-text" type="text" placeholder="Amount">
        </div>          
      </div>
      <!--End payment-calculation -->  
      <!--Start payment-calculation -->
      <div class="panel-heading sale-panel-item-lists heading-color">
        <div class="payment-calculation-title float-left"> Total exc. Vat </div>
        <div class="payment-calculation-amount float-right"> 400 </div>          
      </div>
      <!--End payment-calculation -->  
      <!--Start payment-calculation -->
      <div class="panel-heading sale-panel-item-lists heading-color">
        <div class="payment-calculation-title float-left"> VAT </div>
        <div class="payment-calculation-dropdown-lists float-left"> 
          <select class ="form-control payment-calculation-input-select" class="cs-select cs-skin-slide cs-transparent form-control">
            <option value="">Select Vat</option>
            <option>10%</option>
          </select>          
        </div> 
        <div class="payment-calculation-vat-amount float-left"> 
          <input class ="form-control payment-calculation-input-text" type="text" placeholder="Amount">
        </div>          
      </div>
      <!--End payment-calculation -->
      <!--Start payment-calculation -->
      <div class="panel-heading sale-panel-item-lists heading-color">
        <div class="payment-calculation-title float-left"> Net. Sum </div>
        <div class="payment-calculation-amount float-right"> 400 </div>          
      </div>
      <!--End payment-calculation -->     
      <!--Start payment-calculation -->
      <div class="panel-heading sale-panel-item-lists heading-color">
        <div class="payment-calculation-title float-left">         
            <i class="fa fa-user customer-icon"></i> Customer         
        </div>
        <div class="payment-calculation-dropdown-lists float-left"> 
          <select class ="form-control payment-calculation-input-select" class="cs-select cs-skin-slide cs-transparent form-control">
            <option value="">Select Customer</option>
            <option>10%</option>
          </select>          
        </div> 
        <div class="payment-calculation-vat-amount float-left"> 
          <div class="radio radio-success payment-calculation-input-text">                    
            <input type="radio" checked="checked" value="no" name="optionyes" id="no">
            <label for="no">Guest</label>
          </div>
        </div>          
      </div>
      <!--End payment-calculation --> 
      <!--Start payment-calculation -->
      <div class="panel-heading sale-panel-item-lists heading-color">
        <div class="payment-calculation-title float-left"> Cash Payment </div>
        <div class="payment-calculation-cash-payment float-right"> 
          <input class ="form-control payment-calculation-input-text" type="text" placeholder="Amount">          
        </div>          
      </div>
      <!--End payment-calculation -->
      <!--Start payment-calculation -->
      <div class="panel-heading sale-panel-item-lists heading-color">
        <div class="payment-calculation-title float-left"> Digital Payment </div>
        <div class="payment-calculation-digital-payment-dropdown-lists float-left"> 
          <select class ="form-control payment-calculation-digital-payment-input-select" class="cs-select cs-skin-slide cs-transparent form-control">
            <option value="">Select Payment Method</option>
            <option>10%</option>
          </select>          
        </div> 
        <div class="payment-calculation-digital-payment-meta-field float-left"> 
          <input class ="form-control payment-calculation-input-text" type="text" placeholder="Amount">
          <input class ="form-control payment-calculation-input-text" type="text" placeholder="Name On Card">
          <input class ="form-control payment-calculation-input-text" type="text" placeholder="Card No">
        </div>          
      </div>
      <!--End payment-calculation -->
      <!--Start payment-calculation -->    
      <div class="panel-heading sale-panel-item-lists heading-color">
        <div class="payment-calculation-title float-left"> Change Amount </div>
        <div class="payment-calculation-amount float-right"> 400 </div>          
      </div>    
      <!--End payment-calculation -->
      <!--Start payment-calculation -->    
      <div class="panel-heading sale-panel-item-lists heading-color">
        <div class="payment-calculation-action-button float-left"> 
          <button class="btn btn-danger btn-cons button-size">Cancel</button>
        </div>
        <div class="payment-calculation-action-button float-left"> 
          <button class="btn btn-success btn-cons button-size">Save</button>
        </div>
        <div class="payment-calculation-action-button float-left"> 
          <button class="btn btn-success btn-cons button-size">Notes</button>
        </div>          
      </div>    
      <!--End payment-calculation -->
      <!--Start payment-calculation -->    
      <div class="panel-heading sale-panel-item-lists heading-color wysiwyg5-wrapper b-a b-grey">
        <textarea class ="form-control payment-calculation-input-textarea wysiwyg demo-form-wysiwyg" placeholder="Customer Notes">    
        </textarea>       
      </div>    
      <!--End payment-calculation -->
    </div>
    <!--End sale-panel-item-lists -->
  </div>
  <!--End sale-panel-->
</div>


