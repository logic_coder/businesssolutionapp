<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Challan Info</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-full">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="row clearfix">
                            <div class="col-sm-6 col-div-33">
                                <div class="form-group form-group-default required">
                                    {{ Form::label('request_date', 'Request Date') }}
                                    {{ Form::text('request_date', \Carbon\Carbon::now()->format('Y-m-d'), ['id' => 'start-date', 'class' => 'form-control date', 'required']) }}
                                </div>
                            </div>
                            <div class="col-sm-6 col-div-33">
                                <div class="form-group form-group-default">
                                    {{ Form::label('to_ware_house', 'To ware house') }}
                                    {{Form::select('warehouse_id',$warehouse_lists,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control attribute-group-select','data-init-plugin' => 'cs-select']) }}
                                </div>
                            </div>
                            <div class="col-sm-6 col-div-33">
                                <div class="form-group form-group-default">
                                    {{ Form::label('supplier', 'Supplier') }}
                                    {{Form::select('supplier_id',$supplier_lists,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control attribute-group-select','data-init-plugin' => 'cs-select']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-attached">
                        <div class="row clearfix">
                            <div class="col-sm-6 col-div-33">
                                <div class="form-group form-group-default">
                                    {{ Form::label('search_product', 'Search Product') }}
                                    {{ Form::text('search_product', null, ['class' => 'form-control', 'placeholder' => 'product, item code, bar code', 'required',]) }}
                                </div>
                            </div>
                            <div class="col-sm-6 col-div-33">
                                <div class="form-group form-group-default required">
                                    {{ Form::label('category', 'Category') }}
                                    {{Form::select('category_id',$category_lists,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control attribute-group-select','data-init-plugin' => 'cs-select']) }}
                                </div>
                            </div>
                            <div class="col-sm-6 col-div-33">
                                <div class="form-group form-group-default">
                                    <button class="btn btn-success btn-cons btn-challan-product-search" type="submit">
                                        <span>Search</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>



