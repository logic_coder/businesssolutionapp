<button class="btn delete-multi-items" action-url="product_price/remove_product_price" redirect-url="{{'/product/'.$product->id.'/product_price'}}"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Sales Price</th>
        <th style="width:20%">Production Cost</th>
        <th style="width:20%">Vat Percent</th>
        <th style="width:20%">Cost/Price Date</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @if($product->prices)
        @foreach( $product->prices as $value)
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input type="checkbox" name="item_ids[]" value={{ $value->id }} data-id="checkbox" id="checkbox{{ $value->id }}">
                        <label for="checkbox{{ $value->id }}"></label>
                    </div>
                </td>
               <td class="v-align-middle">
                    {{ $value->price }}
                </td>
               <td class="v-align-middle">
                    {{ $value->cost }}
                </td>
               <td class="v-align-middle">
                    {{ App\Vat::find($value->vat_id)->percent}}
               </td>
               <td class="v-align-middle">
                  {{ date('F d, Y', strtotime($value->cost_price_date)) }}
               </td>
               <td class="v-align-middle">
                    {{ $value->active ? 'Active' : 'Inactive'}}
                </td>

                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <a title="Edit" class="btn btn-success" href="{{url('product/'.$product->id.'/product_price/'.$value->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        <button title="Delete" item-ids="{{$value->id}}" type="button" class="btn btn-danger delete-single-item" action-url="product_price/remove_product_price" redirect-url="{{'/product/'.$product->id.'/product_price'}}"><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>