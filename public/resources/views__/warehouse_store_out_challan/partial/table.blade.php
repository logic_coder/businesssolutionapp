<button class="btn delete-multi-items" action-url="warehouse_store_out_challan/remove_warehouse_store_out_challan" redirect-url="/warehouse_store_out_challan"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Challan No</th>
        <th style="width:20%">To Outlet</th>
        <th style="width:20%">From Warehouse</th>
        <th style="width:20%">Product</th>
        <th style="width:20%">Created By</th>
        <th style="width:20%">Request Date</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $store_out_challans as $store_out_challan )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input class="item_ids" type="checkbox" name="item_ids[]" value={{$store_out_challan->id}} data-id="checkbox" id="checkbox{{$store_out_challan->id}}">
                    <label for="checkbox{{$store_out_challan->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                {{ $store_out_challan->challan_no }}
            </td>
            <td class="v-align-middle">
                @if($store_out_challan->out_let_id)
                    {{ App\OutLet::find($store_out_challan->out_let_id)->name }}
                @endif
            </td>
            <td class="v-align-middle">
                @if($store_out_challan->warehouse_id)
                    {{ App\Warehouse::find($store_out_challan->warehouse_id)->name }}
                @endif
            </td>
            <td class="v-align-middle">
                <ul>
                    @if($store_out_challan->challanProducts)
                        @foreach( $store_out_challan->challanProducts as $challan_product )
                            <li>
                            @php
                                $product_details = App\Product::find($challan_product->product_id);
                                if($product_details){
                                   echo $product_details->name;
                                }
                            @endphp
                            </li>
                        @endforeach
                    @endif
                </ul>
            </td>
            <td class="v-align-middle">
                @php
                    $user = App\User::with('userDetail')->where('id', $store_out_challan->created_by )->get();
                    if($user[0]->userDetail){
                        echo $user[0]->userDetail->first_name ." ". $user[0]->userDetail->last_name;
                    }
                @endphp
            </td>
            <td class="v-align-middle">
                {{ $store_out_challan->request_date }}
            </td>
            <td class="v-align-middle">
                {{ $store_out_challan->status }}
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn btn-success" href="{{url('warehouse_store_out_challan/'.$store_out_challan->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$store_out_challan->id}}" type="button" class="btn btn-danger delete-single-item" action-url="warehouse_store_out_challan/remove_warehouse_store_out_challan" redirect-url="/warehouse_store_out_challan"><i class="fa fa-trash-o"></i></button>
                    <a title="Receive" class="btn btn-success" href="{{url('frontend/out_let_store_in_challan/'.$store_out_challan->id.'/receive')}}">Receive</a>
                    <!--
                    <a title="Receive" class="btn btn-success" href="{{url('warehouse_store_out_challan/'.$store_out_challan->id.'/receive')}}">Receive</a>
                    -->
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>