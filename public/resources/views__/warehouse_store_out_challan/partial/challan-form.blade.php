<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Challan Lists</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover" id="tableWithSearch">
                <thead>
                <tr>
                    <th style="width:20%">Name</th>
                    <th style="width:20%">Combination</th>
                    {{--<th style="width:20%">Quantity</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach( $products as $product )
                    @php
                        $com = 3;
                    @endphp
                    <tr>
                        <td class="v-align-middle">
                            {{ $product->name }}
                        </td><td class="v-align-middle">
                            @for ($i = 0; $i < $com; $i++)
                                <div class="product_combination">
                                    Green, Small, S
                                </div>
                                <div class="product_quantity">
                                    {{ Form::text('request_date', null, ['class' => 'form-product_quantity']) }}
                                </div>
                                <div class="clearfix"></div>
                            @endfor

                        </td>
                        {{--<td class="v-align-middle">--}}
                            {{--@for ($i = 0; $i < $com; $i++)--}}
                                {{--{{ Form::text('request_date', null, ['class' => 'form-control']) }}--}}
                            {{--@endfor--}}
                        {{--</td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>