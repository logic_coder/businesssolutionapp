<button class="btn delete-multi-items" action-url="warehouse/remove_warehouse" redirect-url="/warehouse"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Description</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $warehouses as $warehouse )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input class="item_ids" type="checkbox" name="item_ids[]" value={{$warehouse->id}} data-id="checkbox" id="checkbox{{$warehouse->id}}">
                    <label for="checkbox{{$warehouse->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                {{ $warehouse->name }}
            </td>
            <td class="v-align-middle">
                {{ $warehouse->description }}
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn btn-success" href="{{url('warehouse/'.$warehouse->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$warehouse->id}}" type="button" class="btn btn-danger delete-single-item" action-url="warehouse/remove_warehouse" redirect-url="/warehouse"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>