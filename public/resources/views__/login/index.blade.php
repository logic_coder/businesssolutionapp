@extends('layout.login')

@section('content')

    <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
            <img src="{{ asset('images/logo.png') }}" alt="logo" data-src="{{ asset('images_') }}" width="78" height="22">
            <p class="p-t-35">Sign into your pages account</p>
            <!-- START Login Form -->
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    </ul>
                </div>
             @endif
            {!! Form::open(['url'=>'login','id'=>"form-login", "name"=>"signin-form","class"=>"p-t-15","role"=>"form","method"=>"post"]) !!}
            <!-- START Form Control-->
                <div class="form-group form-group-default">
                    <label>Login</label>
                    <div class="controls">
                        {!! Form::email('email', null ,['class' => 'form-control validate instant-validate','placeholder'=>'Email','required'=>'required']) !!}
                    </div>
                </div>
                <!-- END Form Control-->
                <!-- START Form Control-->
                <div class="form-group form-group-default">
                    <label>Password</label>
                    <div class="controls">
                        {!! Form::password('password' ,['class' => 'form-control','placeholder'=>'Credentials','required'=>'required']) !!}
                    </div>
                </div>
                <!-- START Form Control-->
                <div class="row">
                    <div class="col-md-6 no-padding">
                        <div class="checkbox ">
                            <input type="checkbox" value="1" id="checkbox1">
                            <label for="checkbox1">Keep Me Signed in</label>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="#" class="text-info small">Help? Contact Support</a>
                    </div>
                </div>
                <!-- END Form Control-->
                <button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
            {!! Form::close() !!}
        <!--END Login Form-->
            <div class="pull-bottom sm-pull-bottom">
                <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
                    <div class="col-sm-3 col-md-2 no-padding">
                        <img alt="" class="m-t-5" data-src="{{ asset('images/pages_icon.png') }}" data-src-retina="{{ asset('images/pages_icon.png') }}" height="60" src="{{ asset('images/pages_icon.png') }}" width="60">
                    </div>
                    <div class="col-sm-9 no-padding m-t-10">
                        <p><small>
                                Create a pages account. If you have a facebook account, log into it for this process. Sign in with <a href="#" class="text-info">Facebook</a> or <a href="#" class="text-info">Google</a></small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection