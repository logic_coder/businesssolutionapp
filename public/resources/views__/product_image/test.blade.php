@extends('layouts.master_test')
@section('style')
    <link href="{{asset('theme/backend/assets/css/gallery.css')}}" rel="stylesheet" type="text/css" media="screen" /> 
    <link class="main-stylesheet" href="{{asset('theme/backend/assets/css/corporate.css')}}" rel="stylesheet" type="text/css" media="screen" />     
@endsection

<!-- START PAGE-CONTAINER -->
    <div class="page-container">      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
            <!-- START CATEGORY -->
            <div class="gallery">              
              <!-- START GALLERY ITEM -->
              <!-- 
                    FOR DEMO PURPOSES, FIRST GALLERY ITEM (.first) IS HIDDEN 
                    FOR SCREENS <920px. PLEASE REMOVE THE CLASS 'first' WHEN YOU IMPLEMENT 
                -->
              <div class="gallery-item first" data-width="1" data-height="1">
                <!-- START PREVIEW -->
                <img src="{{asset('uploads/out_lets/product_images/product_2020-07-22 09:19:26.jpeg')}}" alt="" class="image-responsive-height">
                <!-- END PREVIEW -->
                <!-- START ITEM OVERLAY DESCRIPTION -->
                <div class="overlayer bottom-left full-width">
                  <div class="overlayer-wrapper item-info ">
                    <div class="gradient-grey p-l-20 p-r-20 p-t-20 p-b-5">
                      <div class="">
                        <p class="pull-left bold text-white fs-14 p-t-10">Happy Ninja</p>
                        <h5 class="pull-right semi-bold text-white font-montserrat bold">$25.00</h5>
                        <div class="clearfix"></div>
                      </div>
                      <div class="m-t-10">
                        <div class="thumbnail-wrapper d32 circular m-t-5">
                          <img width="40" height="40" src="assets/img/profiles/avatar.jpg" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">
                        </div>
                        <div class="inline m-l-10">
                          <p class="no-margin text-white fs-12">Designed by Alex Nester</p>
                          <p class="rating">
                            <i class="fa fa-star rated"></i>
                            <i class="fa fa-star rated"></i>
                            <i class="fa fa-star rated"></i>
                            <i class="fa fa-star rated"></i>
                            <i class="fa fa-star"></i>
                          </p>
                        </div>
                        <div class="pull-right m-t-10">
                          <button class="btn btn-white btn-xs btn-mini bold fs-14" type="button">+</button>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END PRODUCT OVERLAY DESCRIPTION -->
              </div>
              <!-- END GALLERY ITEM -->
                          
              <!-- START GALLERY ITEM -->
              <!-- 
                    FOR DEMO PURPOSES, FIRST GALLERY ITEM (.first) IS HIDDEN 
                    FOR SCREENS <920px. PLEASE REMOVE THE CLASS 'first' WHEN YOU IMPLEMENT 
                -->
              <div class="gallery-item " data-width="1" data-height="1">
                <!-- START PREVIEW -->
                <img src="{{asset('uploads/out_lets/product_images/product_2020-07-22 09:19:27.png')}}" alt="" class="image-responsive-height">
                <!-- END PREVIEW -->
                <!-- START ITEM OVERLAY DESCRIPTION -->
                <div class="overlayer bottom-left full-width">
                  <div class="overlayer-wrapper item-info ">
                    <div class="gradient-grey p-l-20 p-r-20 p-t-20 p-b-5">
                      <div class="">
                        <p class="pull-left bold text-white fs-14 p-t-10">Happy Ninja</p>
                        <h5 class="pull-right semi-bold text-white font-montserrat bold">$25.00</h5>
                        <div class="clearfix"></div>
                      </div>
                      <div class="m-t-10">                        
                        <div class="pull-right m-t-10">
                          <button class="btn btn-white btn-xs btn-mini bold fs-14" type="button">x</button>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END PRODUCT OVERLAY DESCRIPTION -->
              </div>
              <!-- END GALLERY ITEM -->
            </div>
            <!-- END CATEGORY -->
          </div>
          <!-- START DIALOG -->
          <div id="itemDetails" class="dialog item-details">
            <div class="dialog__overlay"></div>
            <div class="dialog__content">
              <div class="container-fluid">
                <div class="row dialog__overview">
                  <div class="col-md-7 no-padding item-slideshow-wrapper full-height">
                    <div class="item-slideshow full-height owl-carousel">
                      <div class="slide" data-image="public/uploads/out_lets/product_images/product_2020-07-22 09:19:26.jpeg">                         
                      </div>
                      <div class="slide" data-image="public/uploads/out_lets/product_images/product_2020-07-22 11:26:18.jpg">                         
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 d-md-none d-lg-none d-xl-none bg-info-dark">
                    <div class="container-xs-height">
                      <div class="row row-xs-height">
                        <div class="col-8 col-xs-height col-middle no-padding">
                          <div class="thumbnail-wrapper d32 circular inline">
                            <img width="32" height="32" src="assets/img/profiles/2.jpg" data-src="assets/img/profiles/2.jpg" data-src-retina="assets/img/profiles/2x.jpg" alt="">
                          </div>
                          <div class="inline m-l-15">
                            <p class="text-white no-margin">Alex Nester</p>
                            <p class="hint-text text-white no-margin fs-12">Senior UI/UX designer</p>
                          </div>
                        </div>
                        <div class="col-4 col-xs-height col-middle text-right  no-padding">
                          <h2 class="bold text-white price font-montserrat">$20.00</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5 p-r-35 p-t-35 p-l-35 full-height item-description">
                    <h2 class="semi-bold no-margin font-montserrat">Happy Ninja</h2>
                    <p class="rating fs-12 m-t-5">
                      <i class="fa fa-star "></i>
                      <i class="fa fa-star "></i>
                      <i class="fa fa-star "></i>
                      <i class="fa fa-star-o"></i>
                      <i class="fa fa-star-o"></i>
                    </p>
                    <p class="fs-13">When it comes to digital design, the lines between functionality, aesthetics, and psychology are inseparably blurred. Without the constraints of the physical world, there’s no natural form to fall back on, and every bit of constraint and affordance must be introduced intentionally. Good design makes a product useful.
                    </p>
                    <div class="row m-b-20 m-t-20">
                      <div class="col-6"><span class="font-montserrat all-caps fs-11">Price ranges</span>
                      </div>
                      <div class="col-6 text-right">$20.00 - $40.00</div>
                    </div>
                    <div class="row m-t-20 m-b-10">
                      <div class="col-6"><span class="font-montserrat all-caps fs-11">Paint sizes</span>
                      </div>
                    </div>
                    <button class="btn btn-white">S</button>
                    <button class="btn btn-white">M</button>
                    <button class="btn btn-white">L</button>
                    <button class="btn btn-white">XL</button>
                    <br>
                    <button class="btn btn-primary buy-now">Buy Now</button>
                  </div>
                </div>
                <div class="row dialog__footer bg-info-dark d-sm-none d-none d-sm-flex d-lg-flex d-xl-flex">
                  <div class="col-md-7 full-height separator">
                    <div class="container-xs-height">
                      <div class="row row-xs-height">
                        <div class="col-7 col-xs-height col-middle no-padding">
                          <div class="thumbnail-wrapper d48 circular inline">
                            <img width="48" height="48" src="assets/img/profiles/2.jpg" data-src="assets/img/profiles/2.jpg" data-src-retina="assets/img/profiles/2x.jpg" alt="">
                          </div>
                          <div class="inline m-l-15">
                            <p class="text-white no-margin">Alex Nester</p>
                            <p class="hint-text text-white no-margin fs-12">Senior UI/UX designer</p>
                          </div>
                        </div>
                        <div class="col-5 col-xs-height col-middle text-right  no-padding">
                          <h2 class="bold text-white price font-montserrat">$20.00</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5 full-height">
                    <ul class="recommended list-inline pull-right m-t-10 m-b-0">
                      <li>
                        <a href="#"><img src="assets/img/gallery/thumb-1.jpg"></a>
                      </li>
                      <li>
                        <a href="#"><img src="assets/img/gallery/thumb-2.jpg"></a>
                      </li>
                      <li>
                        <a href="#"><img src="assets/img/gallery/thumb-3.jpg"></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <button class="close action top-right" data-dialog-close><i class="pg-close fs-14"></i>
              </button>
            </div>
          </div>
          <!-- END DIALOG -->   
        </div>
        <!-- END PAGE CONTENT -->        
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
<!-- END PAGE CONTAINER -->

@section('plugin-script') 
<script src="{{mix('/theme/backend/assets/js/plugins.js')}}" type="text/javascript"></script>    
<script src="{{mix('/theme/backend/assets/js/gallery-image-load.js')}}" type="text/javascript"></script>   
@endsection
@section('page-script')  
  <script src="{{mix('/theme/backend/assets/js/gallery.js')}}" type="text/javascript"></script> 
@endsection







