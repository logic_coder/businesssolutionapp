<button class="btn delete-multi-items" action-url="invoice_prefix/remove_invoice_prefix" redirect-url="/invoice_prefix"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Last Updated</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach( $invoice_prefixes as $invoice_prefix )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="item_ids[]" value={{$invoice_prefix->id}} data-id="checkbox" id="checkbox{{$invoice_prefix->id}}">
                    <label for="checkbox{{$invoice_prefix->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <p>{{$invoice_prefix->name}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{ date('F d, Y', strtotime($invoice_prefix->updated_at)) }}
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn btn-success" href="{{url('invoice_prefix/'.$invoice_prefix->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$invoice_prefix->id}}" type="button" class="btn btn-danger delete-single-item" action-url="invoice_prefix/remove_invoice_prefix" redirect-url="/invoice_prefix"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
