<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Edit access permission</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <h4 class="semi-bold"> Role : {{ $role->name }}</h4>
                    <div class="form-group-attached">
                        <div class="form-group form-group-defaul">
                            <ul class="permission-list-container">
                                @foreach( $module_lists as $module )
                                    <li class="module-name-li">
                                        {{ $module->name }}
                                        <ul class="permission-list-container">
                                            @foreach( $module->permissions as $permission )
                                                @php
                                                    $permission_role = \App\PermissionRole::where('role_id', $role->id)->where('permission_id', $permission->id)->first();
                                                @endphp
                                                <li class="permission-name-li">
                                                    <span class="permission-name-span">
                                                        {{ Form::checkbox('permission_ids[]',$permission->id, $permission_role ? True : False, ['class' => '', $role->slug == 'administrator' ? 'disabled' : '' ]) }}{{ $permission->name }}
                                                    </span>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if( $role->slug != 'administrator' )
        <button class="btn btn-success btn-cons pull-right" type="submit">
            <span>Save</span>
        </button>
    @endif
</div>
