<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Supplier Info</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('code', 'Code') }}
                            {{ Form::text('code',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('address', 'Address', ['class' => '']) }}
                            {{ Form::text('address',null, ['class' => 'form-control', 'placeholder' => 'Current address','required']) }}
                        </div>
                        <div class="form-group form-group-default">
                            {{ Form::label('address2', 'Address2', ['class' => '']) }}
                            {{ Form::text('address2',null, ['class' => 'form-control', 'placeholder' => 'Current address']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('city', 'City', ['class' => '']) }}
                            {{ Form::text('city',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('zip', 'Zip/Postal Code', ['class' => '']) }}
                            {{ Form::text('zip',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::text('email',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('phone', 'Phone') }}
                            {{ Form::text('phone',null, ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-success btn-cons pull-right" type="submit">
        <span>Save</span>
    </button>
</div>
