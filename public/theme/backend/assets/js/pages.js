//Pages Calendar - Version 1.1.0
(function($) {
    'use strict';

    $.fn.pagescalendar = function(method) {
        var methods = {

                init: function(options) {
                    this.pagescalendar.settings = $.extend({}, this.pagescalendar.defaults, options);
                    return this.each(function() {
                        var $element = $(this),
                            element = this;
                    });
                },

                today: function() {
                    helpers.today();
                },
                next: function() {
                    helpers.nextMonth();
                },
                prev: function() {
                    helpers.previousMonth();
                },
                setDate: function(date) {
                    helpers.setDate(date);
                },
                getDate: function(format) {
                    return helpers.getDate(format);
                },
                render: function() {
                    helpers.checkOptionsAndBuild();
                },
                destroy: function() {
                    //TODO
                },
                setLocale: function(lang) {
                    settings.locale = lang;
                    helpers.setLocale();
                },
                reloadEvents: function() {
                    helpers.loadEvents();
                },
                addEvent: function(event) {
                    helpers.addEvent(event);
                },
                removeEvent: function(index) {
                    helpers.deleteEvent(index)
                },
                updateEvent: function(index, eventObj) {
                    helpers.updateEvent(index, eventObj);
                },
                getEvents: function(option) {
                    return helpers.getEventArray(option);
                }
            }
            //Private Vars
        var settings = $.fn.pagescalendar.settings;
        var content;
        var daysOfMonth;
        var setActive;
        var timeFormat = settings.timeFormat;
        var globalEventList;
        var cellHeight;
        var startOfWeekDate;
        var calendar = $.fn.pagescalendar.defaults.calendar;
        var helpers = {
            /**
             * Main Init function
             */
            checkOptionsAndBuild: function() {
                if (settings.ui.year == null || settings.ui.month == null || settings.ui.date == null || settings.ui.week == null || settings.ui.grid == null) {
                    alert("You have not included the proper ui[] settings, please refer docs");
                }
                this.setLocale();
                calendar.monthLong = moment().format(settings.ui.month.format);

                if (settings.now != null) {
                    calendar.month = moment($.fn.pagescalendar.settings.now).month();
                    calendar.year = moment($.fn.pagescalendar.settings.now).year();
                    calendar.date = moment($.fn.pagescalendar.settings.now).format("D");
                    calendar.dayOfWeek = moment($.fn.pagescalendar.settings.now).day();
                    calendar.monthLong = moment($.fn.pagescalendar.settings.now).format('MMM');
                } else {
                    calendar.month = moment().month();
                    calendar.year = moment().year();
                    calendar.date = moment().format("D");
                    calendar.dayOfWeek = moment().day();
                    calendar.monthLong = moment().format('MMM');
                }

                this.buildYears();
                this.buildMonths();
                this.buildCurrentDateHeader();
                this.buildWeek();
                this.buildWeekViewCalendar();
                this.buildTimeSlots();
                this.loadDatesToWeekView();
                this.highlightActiveDay();
                $.fn.pagescalendar.settings.onViewRenderComplete.call(this);
                this.bindEventHanders();
                this.loadEvents();
                this.setEventBubbles(settings.events);
                this.autoFocusActiveElement();
                helpers.scrollToElement('#weeks-wrapper .active')
            },

            /**
             * Render Years
             */
            buildYears: function() {
                var container = "#years";
                $(container).html("");
                content = "";
                var diffYears = settings.ui.year.endYear - settings.ui.year.startYear
                diffYears = (diffYears > 90) ? 90 : diffYears;
                var yearInc = settings.ui.year.startYear;

                for (var i = 1; i <= diffYears; i++) {
                    yearInc = moment(yearInc, settings.ui.year.format).add(1, 'year').format(settings.ui.year.format);
                    var activeClass = (calendar.year == yearInc) ? 'active' : '';
                    content += '<div class="year">';
                    content += '<a href="#" class="year-selector ' + activeClass + '" data-year=' + yearInc + '>' + yearInc + '</a>';
                    // calendar.loadedYears.push(yearInc);
                    content += '</div>';
                }

                $(container).append(content);
                this.dragHandler('years');


            },

            /**
             * Render Months
             */
            buildMonths: function() {
                var container = "#months";
                $(container).html("");
                content = "";
                var monthInc = moment(moment().startOf('year'), 'MMMM').format(settings.ui.month.format);
                for (var i = 1; i <= 12; i++) {
                    var activeClass = (moment([calendar.year, calendar.month, calendar.date]).format(settings.ui.month.format) == monthInc) ? 'active' : '';
                    content += '<div class="month">';
                    content += '<a href="#" class="month-selector ' + activeClass + '" data-month="' + monthInc + '">' + monthInc + '</a>';
                    content += '</div>';
                    monthInc = moment(monthInc, settings.ui.month.format).add(1, 'month').format(settings.ui.month.format);
                }
                $(container).append(content);
                this.dragHandler('months');
            },
            /**
             * Higlight Month On Date Change
             */
            highlightMonth: function() {
                var container = "#months";
                $('.month a').removeClass('active');
                $('.month:nth-child(' + (parseInt(calendar.month) + 1) + ') > a').addClass('active');
            },
            /**
             * Higlight Year On Date Change
             */
            highlightYear: function() {
                var diff = calendar.year - settings.ui.year.startYear
                var container = "#years";
                $('.year a').removeClass('active');
                $('.year:nth-child(' + diff + ') > a').addClass('active')
            },
            /**
             * Display Date
             */
            buildCurrentDateHeader: function() {
                $("#currentDate").text(moment([calendar.year, calendar.month, calendar.date]).format(settings.ui.date.format));
            },

            /**
             * Touch Drag Handler
             * @param {string} element
             */
            dragHandler: function(element) {
                //Setup Drag Handler
                if ($('body').hasClass('mobile'))
                    return
                if ($('#' + element).length != 1)
                    return

                $('.drager').scrollbar();
                var myElement = document.getElementById(element);
                var hammertime = new Hammer(myElement);
                var element = $('#' + element).parent();
                var lP = element.scrollLeft();
                var inverseX;
                hammertime.on('panmove', function(ev) {
                    inverseX = -(ev.deltaX);
                    element.scrollLeft(inverseX + lP);
                    //lP=0;
                });
                hammertime.on('panend pancancel', function(ev) {
                    lP = element.scrollLeft();
                })
            },
            autoFocusActiveElement: function() {
                var timer;
                $(window).resize(function() {
                    clearTimeout(timer);
                    timer = setTimeout(function() {
                        helpers.scrollToElement('#weeks-wrapper .active');
                    }, 500);
                });
            },

            scrollToElement: function(el) {
                el = $(el);
                if (!el.length != 0)
                    return
                var par = $(el).parent();
                var t = helpers.isElementInViewport(el);
                //console.log(t)
                if (!t) {
                    var elOffset = el.offset().left;
                    var elHeight = par.children().width();
                    var windowHeight = $(window).width();
                    var offset;

                    if (elHeight < windowHeight) {
                        offset = elOffset - ((windowHeight / 2) - (elHeight / 2));
                    } else {
                        offset = elOffset;
                    }
                    $('#weeks-wrapper').parent().animate({
                        scrollLeft: offset
                    }, 10);
                }

            },

            isElementInViewport: function(el) {
                if (typeof jQuery === "function" && el instanceof jQuery) {
                    el = el[0];
                }
                var rect = el.getBoundingClientRect();
                return (
                    rect.top >= 0 &&
                    rect.left >= 0 &&
                    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
                    rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
                );
            },

            /**
             * Render Week
             */
            buildWeek: function() {
                var container = ".weeks-wrapper";
                $(container).html("");
                daysOfMonth = moment([calendar.year, calendar.month]).daysInMonth();
                content = "";

                for (var i = 1; i <= daysOfMonth; i++) {
                    var t = moment([calendar.year, calendar.month, i]).format('ddd');
                    var activeClass = (calendar.date == i) ? 'active current-date' : '';
                    (t ==  moment(settings.ui.week.startOfTheWeek,'d').format('ddd') || i == 1) ? content += '<div class="week ' + activeClass + '">': '';
                    content += '<div class="day-wrapper date-selector">';
                    content += '<div class="week-day">';
                    content += '<div class="day week-header">' + moment([calendar.year, calendar.month, i]).format('dd') + '</div>';
                    content += '</div>';
                    content += '<div class="week-date ' + activeClass + '">';
                    content += '<div class="day"><a href="#" data-date=' + moment([calendar.year, calendar.month, i]).format('D') + '>' + i + '</a></div>';
                    content += '</div>';
                    content += '</div>';
                    (t == moment(settings.ui.week.endOfTheWeek,'d').format('ddd')) ? content += '</div>': '';
                }
                content += '</div>';

                $(container).append(content);
                $('.weeks-wrapper .week .day-wrapper .week-date.active').closest(".week").addClass('active');
                this.dragHandler('weeks-wrapper');
            },

            /**
             * Highlight Selected Week
             * @param {jobject} elem
             */
            highlightWeek: function(elem) {
                $('.week').removeClass('active');
                $(elem).closest('.week').addClass('active');
            },
            /**
             * Show Event Bubble on Top
             * @param {array} eventArray
             */
            setEventBubbles: function(eventArray) {
                $('.has-event').removeClass('has-event');
                $('#weekViewTableHead .event-bubble').remove();
                for (var item in eventArray) {
                    var eventYear = moment(eventArray[item].start).format(settings.ui.year.format);
                    var eventMonth = moment(eventArray[item].start).format(settings.ui.month.format);
                    var eventDate = moment(eventArray[item].start).format('D');
                    $('.year > [data-year="' + eventYear + '"]').addClass('has-event');

                    if (calendar.year == moment(eventArray[item].start).format("YYYY")) {
                        $('.month > [data-month="' + eventMonth + '"]').addClass('has-event');

                        if (calendar.month + 1 == moment(eventArray[item].start).format("M")) {
                            $('.date-selector > .week-date > .day > [data-date="' + eventDate + '"]').addClass('has-event');
                            var toheaderDate = moment(eventArray[item].start).format('YYYY-MM-DD')
                            var headerDate = $("#weekViewTableHead").find('.thead > [data-day="' + toheaderDate + '"]');
                            if (eventArray[item].class != null) {
                                if (headerDate.length != 0) {
                                    if (headerDate.children('.' + eventArray[item].class).length == 0) {
                                        headerDate.append('<div class="event-bubble ' + eventArray[item].class + '"></div>');
                                    }
                                }
                            }
                        }
                    }
                }
            },

            /**
             * Render Week View
             */
            buildWeekViewCalendar: function() {
                var numberOfCells = 7;
                var headerContent = '<div class="thead" ><div class="tcell"></div><div class="tcell"></div><div class="tcell"></div><div class="tcell"></div><div class="tcell"></div><div class="tcell"></div><div class="tcell"></div></div>';
                if (window.innerWidth <= 1024) {
                    //Switch To single Cell on small screens
                    numberOfCells = 1;
                    headerContent = '<div class="thead" ><div class="tcell"></div></div>';
                }
                var container = '.calendar-container';
                $(container).html("");
                content = '';
                content += '<div class="week-view">';
                content += '<div class="allday-cell">';
                content += '</div>';
                content += '<div class="tble" id="weekViewTableHead">';
                //START TABLE HEADER
                content += headerContent;
                content += '</div>';
                content += '<div class="grid">';
                content += '<div class="time-slot-wrapper" id="time-slots">';
                content += '</div>';

                content += '<div class="tble" id="weekGrid">';
                //START TABLE CONTENT
                for (var i = 1; i <= 24; i++) {
                    content += '<div class="trow" >';
                    for (var j = 1; j <= numberOfCells; j++) {
                        content += '<div class="tcell">';
                        if (settings.slotDuration == '30') {
                            content += '<div class="cell-inner" data-time-slot="' + (i - 1) + ':00" ></div>';
                            content += '<div class="cell-inner" data-time-slot=' + (i - 1) + ':30" ></div>';
                        } else {
                            content += '<div class="cell-inner" data-time-slot="' + (i - 1) + ':00" ></div>';
                        }
                        content += '</div>';
                    }
                    content += '</div>';
                }
                //END TABLE CONTENT
                content += '</div>';
                content += '</div>';
                //END TABLE HEADER
                content += '</div>';
                $(container).append(content);
                cellHeight = $('.tcell').innerHeight();
                calendar.startOfWeekDate = moment([calendar.year, calendar.month, calendar.date]).startOf('week');
                calendar.endOfWeek = moment([calendar.year, calendar.month, calendar.date]).endOf('week').format('D');
            },

            /**
             * Render Time Slots
             */
            buildTimeSlots: function() {
                var container = '#time-slots';
                content = '';
                for (var i = 0; i < 24; i++) {
                    content += '<div class="time-slot"><span>' + moment().hour(i).format(settings.ui.grid.timeFormat) + '</span></div>';
                }
                $(container).append(content);
            },

            /**
             * Load Dates To DOM Seperately
             */
            loadDatesToWeekView: function() {
                if (window.innerWidth <= 1024) {
                    var d = moment([calendar.year, calendar.month, calendar.date]);
                    $("#weekViewTableHead").find(".thead .tcell:nth-child(1)")
                        .html('<div class="weekdate">' + moment(d).format('D') + '</div><div class="weekday">' + moment(d).format('dddd') + '</div>')
                        .attr('data-day', moment(d).format('YYYY-MM-DD'));
                    return
                }

                var startOfWeek, endOfWeek;
                startOfWeek = moment([calendar.year, calendar.month, calendar.date]).startOf('week');
                endOfWeek = moment([calendar.year, calendar.month, calendar.date]);

                $("#weekViewTableHead").find('.thead .tcell:nth-child(1)')
                    .html('<div class="weekdate">' + moment(startOfWeek).format('D') + '</div><div class="weekday">' + moment(startOfWeek).format('dddd') + '</div>')
                    .attr('data-day', moment(startOfWeek).format('YYYY-MM-DD'));

                for (var i = 2; i <= 7; i++) {

                    startOfWeek = moment(startOfWeek).add(1, 'days');
                    $("#weekViewTableHead").find(".thead .tcell:nth-child(" + i + ")")
                        .html('<div class="weekdate">' + moment(startOfWeek).format('D') + '</div><div class="weekday">' + moment(startOfWeek).format('dddd') + '</div>')
                        .attr('data-day', moment(startOfWeek).format('YYYY-MM-DD'));
                }
                calendar.startOfWeekDate = moment([calendar.year, calendar.month, calendar.date]).startOf('week');
            },

            /**
             * Higlight Active Week On Weekview/ view
             */
            highlightActiveDay: function() {
                $('#weekViewTableHead').find('.thead .tcell').removeClass('active');
                $('#weekGrid').find('.trow .tcell').removeClass('active');

                var d = moment([calendar.year, calendar.month, calendar.date]).day();
                $('#weekViewTableHead').find('.thead .tcell:nth-child(' + (d + 1) + ')').addClass('active');
                $('#weekGrid').find('.trow .tcell:nth-child(' + (d + 1) + ')').addClass('active');
            },

            /**
             * Util Calendar Functions
             */
            nextMonth: function() {
                currentYear = moment([calendar.year, calendar.month, calendar.date]).add(1, 'months').year();
                currentMonth = moment([calendar.year, calendar.month, calendar.date]).add(1, 'months').month();
                this.setHeaderDate();
                this.buildMiniCalender();
                this.loadDatesToWeekView();
                this.highlightActiveDay();
                this.loadEvents();
            },
            previousMonth: function() {
                currentYear = moment([calendar.year, calendar.month, calendar.date]).subtract(1, 'months').year();
                currentMonth = moment([calendar.year, calendar.month, calendar.date]).subtract(1, 'months').month();
                this.setHeaderDate();
                this.buildMiniCalender();
                this.loadDatesToWeekView();
                this.highlightActiveDay();
                this.loadEvents();
            },
            today: function() {
                calendar.year = moment().year();
                calendar.month = moment().month();
                calendar.date = moment().format("D");
                this.setHeaderDate();
                this.buildMiniCalender();
                this.highlightWeek();
                this.loadDatesToWeekView();
                this.highlightActiveDay();
            },

            /**
             * Add Event To Array
             * @param {object} event
             */
            addEvent: function(event) {
                settings.events.push(event);
                this.loadEvents();
            },

            /**
             * Delete Event From Array
             * @param {id} index
             */
            deleteEvent: function(index) {
                settings.events.splice(parseInt(index), 1);
                this.loadEvents();
            },
            /**
             * Delete Event From Array
             * @param {id} index
             * @param {object} eventObj
             */
            updateEvent: function(index, eventObj) {
                settings.events[index] = eventObj;
                this.loadEvents();
            },
            /**
             * Load Events From Array
             */
            loadEvents: function() {
                $('#weekGrid').find('.trow .tcell').children('.cell-inner').html("");

                //Small Screen Single Cell - TODO : Remove Repeating code
                if (window.innerWidth <= 1024) {
                    for (var i = 0; i < settings.events.length; i++) {
                        var eventStartDate = moment(settings.events[i].start).format("YYYY-MM-DD");
                        var currentD = moment([calendar.year, calendar.month, calendar.date]).format("YYYY-MM-DD");
                        if (currentD.localeCompare(eventStartDate) == 0) {
                            var cellNo = 0;
                            var eventStartHours = moment(settings.events[i].start).format('H');
                            var eventStartMins = moment(settings.events[i].start).format("m");
                            var eventEndHours, evenEndMins;
                            if (settings.events[i].end == null) {
                                eventEndHours = parseInt(eventStartHours) + 1;
                                evenEndMins = "0";
                            } else {
                                eventEndHours = moment(settings.events[i].end).format('H');
                                evenEndMins = moment(settings.events[i].end).format('m');

                            }
                            var eventDuration = moment(eventEndHours + ":" + evenEndMins, "h:mm").diff(moment(eventStartHours + ":" + eventStartMins, "h:mm"), 'hours', true);

                            this.drawEvent(eventStartHours, eventStartMins, eventEndHours, evenEndMins, cellNo, i, eventDuration);
                        }
                    }
                    this.setEventBubbles(settings.events);

                    return
                }

                for (var i = 0; i < settings.events.length; i++) {

                    if (settings.events[i].start == null) console.error('Pages Calendar : Undefined Event Start Time at Index ' + i);
                    var eventStartDate = moment(settings.events[i].start).format("YYYY-MM-DD");
                    var currentWeekStart = moment([calendar.year, calendar.month, calendar.date]).startOf('week').format("YYYY-MM-DD");

                    var eventWeekStart = moment(settings.events[i].start).startOf('week').format('YYYY-MM-DD');
                    if (currentWeekStart.localeCompare(eventWeekStart) == 0) {

                        var cellNo = moment(eventStartDate).day();
                        var eventStartHours = moment(settings.events[i].start).format('H');
                        var eventStartMins = moment(settings.events[i].start).format("m");
                        var eventEndHours, evenEndMins;
                        if (settings.events[i].end == null) {
                            eventEndHours = parseInt(eventStartHours) + 1;
                            evenEndMins = "0";
                        } else {
                            eventEndHours = moment(settings.events[i].end).format('H');
                            evenEndMins = moment(settings.events[i].end).format('m');

                        }
                        var eventDuration = moment(eventEndHours + ":" + evenEndMins, "h:mm").diff(moment(eventStartHours + ":" + eventStartMins, "h:mm"), 'hours', true);

                        this.drawEvent(eventStartHours, eventStartMins, eventEndHours, evenEndMins, cellNo, i, eventDuration);
                    }
                }
                this.setEventBubbles(settings.events);
            },

            /**
             * Render Event To View
             * @param {int} eventStartHours
             * @param {int} eventStartMins
             * @param {int} eventEndHours
             * @param {int} evenEndMins
             * @param {int} cellNo
             * @param {int} arrayIndex
             * @param {int} eventDuration
             */
            drawEvent: function(eventStartHours, eventStartMins, eventEndHours, evenEndMins, cellNo, arrayIndex, eventDuration) {
                cellHeight = $('.tcell').innerHeight();
                evenEndMins = parseInt(evenEndMins);
                var container = $('#weekGrid');
                var row = container.find('.trow:nth-child(' + (parseInt(eventStartHours) + 1) + ')');
                var cell;
                var top, height;
                height = cellHeight;
                if (settings.slotDuration == '30')
                    height = height * 2;
                //TODO ADD MORE OPTION FOR settings.slotDuration LIKE 15/30 mins options

                if (settings.slotDuration == '30') {

                    if (parseInt(eventStartMins) >= 30) {
                        eventStartMins = 30;

                        cell = row.find('.tcell:nth-child(' + (parseInt(cellNo) + 1) + ')').children('.cell-inner:nth-child(2)');
                    } else {
                        eventStartMins = 0
                        cell = row.find('.tcell:nth-child(' + (parseInt(cellNo) + 1) + ')').children('.cell-inner:nth-child(1)');
                    }

                    var timeGap = parseInt(eventEndHours) - parseInt(eventStartHours);
                    var minsGap;
                    //Fail Safe
                    if (timeGap == 0) {
                        timeGap = 1;
                    }
                    if (evenEndMins > 0 && evenEndMins <= 30) {
                        minsGap = cellHeight / 2;
                    } else {
                        minsGap = 0;
                    }

                    height = height * timeGap;
                } else {
                    cell = row.find('.tcell:nth-child(' + (parseInt(cellNo) + 1) + ')').children('.cell-inner');
                    //Time gap`
                    var timeGap = parseInt(eventEndHours) - parseInt(eventStartHours);
                    if (timeGap == 0) {
                        timeGap = 1;
                    }
                    height = height * timeGap;
                }

                height = "height:" + height + "px;";
                var id = 'ca_' + moment(settings.events[arrayIndex].start).unix() + arrayIndex;
                var eventContent = "<div class='event-container " + settings.events[arrayIndex].class + "' data-event-duration=" + eventDuration + " data-index=" + arrayIndex + " data-startTime=" + settings.events[arrayIndex].start + " data-endTime=" + settings.events[arrayIndex].end + " id=" + id + " data-id=" + id + " data-row=" + (parseInt(eventStartHours)) + " data-cell=" + cellNo + " style=" + height + ">"
                eventContent += "<div class='event-inner'>";
                eventContent += "<div class='event-title'>" + settings.events[arrayIndex].title + "</div>";
                eventContent += "<div class='time-wrap'><span class='event-start-time'>" + moment(settings.events[arrayIndex].start).format('h:mm') + "</span> - ";
                //console.log(settings.events[arrayIndex].end)

                eventContent += "<span class='event-end-time'>" + moment(settings.events[arrayIndex].end).format('h:mm') + "</span></div>";

                eventContent += "</div>"
                eventContent += "</div>"

                cell.append(eventContent);
                this.bindEventDraggers();
                settings.events[arrayIndex].dataID = id;
                $.fn.pagescalendar.settings.onEventRender.call(this);
            },

            /**
             * Apply Calenda Event Drag and Resize Options
             */
            bindEventDraggers: function() {
                var snapGridHeight = $('.cell-inner').outerHeight() / 2;
                var snapGridWidth = $('.tcell').outerWidth();
                var previousH, stepper;
                settings.slotDuration == '30' ? stepper = 30 : stepper = 60;
                $(".event-container")
                    .resizable({
                        handles: "s",
                        minHeight: 40,
                        grid: [snapGridWidth, 40],
                        start: function(event, ui) {
                            previousH = ui.size.height;
                        },
                        resize: function(event, ui) {
                            if (previousH > ui.size.height) {
                                var et = ui.element.attr('data-endtime');
                                var x = moment(et).subtract(stepper, 'minutes').format();
                                ui.element.attr('data-endtime', x);
                                ui.element.attr('data-event-duration', parseFloat(ui.element.attr('data-event-duration')) - (stepper / 60));
                                var elem = ui.element.children('.event-inner').children('.time-wrap').children('.event-end-time');
                                elem.html(moment(x).format('h:mm'));
                                previousH = ui.size.height;
                            } else {
                                var et = ui.element.attr('data-endtime');
                                var x = moment(et).add(stepper, 'minutes').format();
                                ui.element.attr('data-endtime', x);
                                ui.element.attr('data-event-duration', parseFloat(ui.element.attr('data-event-duration')) + (stepper / 60));
                                var elem = ui.element.children('.event-inner').children('.time-wrap').children('.event-end-time');
                                elem.html(moment(x).format('h:mm'));
                                previousH = ui.size.height;
                            }

                        },
                        stop: function(event, ui) {
                            var elem = ui.element;
                            var i = elem.attr('data-index');
                            settings.events[i].end = elem.attr('data-endtime');
                            var eventO = helpers.constructEventForUser(i);
                            helpers.setEventBubbles(settings.events);
                            $.fn.pagescalendar.settings.onEventResizeComplete(eventO);
                        }
                    });
                $('.cell-inner').sortable({
                    connectWith: '.cell-inner',
                    iframeFix: false,
                    items: '.event-container',
                    opacity: 0.8,
                    helper: 'original',
                    grid: [snapGridWidth, 40],
                    forceHelperSize: true,
                    placeholder: 'sortable-box-placeholder round-all',
                    forcePlaceholderSize: true,
                    tolerance: 'pointer',
                    greedy: true,
                    revert: 300,
                    over: function(event, ui) {
                        var elem = ui.placeholder.parent('.cell-inner');
                        var startTime = elem.attr("data-time-slot");

                        var date = $('#weekViewTableHead .thead .tcell:nth-child(' + (elem.parent().index() + 1) + ')').attr('data-day');
                        var duration = ui.helper.attr("data-event-duration");
                        var endTime = moment(startTime, ["h:mm"]).add(duration, 'hours').format('h:mm');
                        endTime = moment(date + endTime, 'YYYY/MM/D h:mm').format();

                        date = moment(date + startTime, 'YYYY/MM/D h:mm').format();

                        ui.helper.attr('data-starttime', date);
                        ui.helper.attr('data-endtime', endTime);

                        ui.helper.children('.event-inner').children('.time-wrap').children('.event-start-time').text(moment(ui.helper.attr('data-starttime')).format('h:mm'));
                        ui.helper.children('.event-inner').children('.time-wrap').children('.event-end-time').text(moment(ui.helper.attr('data-endtime')).format('h:mm'));

                    },
                    receive: function(event, ui) {
                        var elem = ui.item;
                        var i = elem.attr('data-index');
                        settings.events[i].start = elem.attr('data-starttime');
                        settings.events[i].end = elem.attr('data-endtime');
                        var eventO = helpers.constructEventForUser(i);
                        helpers.setEventBubbles(settings.events);
                        $.fn.pagescalendar.settings.onEventDragComplete(eventO);
                    }
                });
            },

            /**
             * Date Change Event Only for Horizontal Weeks
             */
            weekDateChange: function(day, elem) {
                calendar.date = day;
                this.buildCurrentDateHeader();
                this.highlightWeek(elem);
                this.loadDatesToWeekView();
                this.highlightActiveDay();
                this.loadEvents();
                this.setEventBubbles(settings.events);
            },

            /**
             * Time Slot Double Click
             * @param {object} obj
             * @return {object} timeSlot
             */
            timeSlotDblClick: function(obj) {
                var elem = $(obj);
                var h = "00";

                if (elem.index() == 1) {
                    h = "30";
                }

                var date = moment(calendar.startOfWeekDate).add(elem.parent().index(), 'days').format('YYYY/MM/D');
                var time = moment(elem.parent().parent().index() + ':' + h, ["H:mm"]).format(' H:mm');
                date = moment(date + time, 'YYYY/MM/D h:mm').format();
                var timeSlot = {
                    date: date,
                }

                $.fn.pagescalendar.settings.onTimeSlotDblClick(timeSlot);
            },

            bindEventHanders: function() {
                $(document).on("click", ".date-selector",function(e) {
                    $(".week-date").removeClass('active')
                    $(this).children('.week-date').addClass('active');
                    helpers.weekDateChange(parseInt($(this).children('.week-date').children('.day').children('a').attr('data-date')), $(this));
                });

                $(document).on("dblclick doubletap", ".cell-inner",function() {
                    helpers.timeSlotDblClick(this);
                });
                $(document).on('click','.event-container',function() {
                    var eventO = helpers.constructEventForUser($(this).attr('data-index'));
                    $.fn.pagescalendar.settings.onEventClick(eventO);
                });
                $('.year-selector').on('click', function() {
                    var year = $(this).attr('data-year');
                    calendar.year = moment(year, settings.ui.year.format).year();
                    helpers.highlightYear();
                    helpers.renderViewsOnDateChange();
                });
                $('.month-selector').on('click', function() {
                    var month = $(this).attr('data-month');
                    calendar.month = moment(month, settings.ui.month.format).month();
                    helpers.renderViewsOnDateChange();
                    helpers.highlightMonth();
                });

                //$(".event-container").resizable();
            },

            //MISC.
            constructEventForUser: function(i) {
                var eventO = {
                    index: i,
                    title: settings.events[i].title,
                    class: settings.events[i].class,
                    start: settings.events[i].start,
                    end: settings.events[i].end,
                    allDay: settings.events[i].allDay,
                    other: settings.events[i].other
                };
                return eventO
            },
            renderViewsOnDateChange: function() {
                helpers.buildWeek();
                helpers.loadDatesToWeekView();
                helpers.buildCurrentDateHeader();
                this.setEventBubbles(settings.events);
                helpers.loadEvents();
            },
            setLocale: function() {
                var currentLang = settings.locale;
                moment.locale(currentLang);
            },
            setDate: function(d) {
                calendar.month = moment(d).month();
                calendar.year = moment(d).year();
                calendar.date = moment(d).format("D");
                calendar.dayOfWeek = moment(d).day();
                calendar.monthLong = moment(d).format('MMM');
                this.checkOptionsAndBuild();
            },
            getDate: function(format) {
                if (format == null) {
                    format = 'MMMM Do YYYY'
                }
                return moment([calendar.year, calendar.month, calendar.date]).format(format);
            },
            getEventArray: function(option) {
                if (option == null || option == 'all')
                    return settings.events;
            },
        }

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method "' + method + '" does not exist in pagescalendar plugin!');
        }

    }

    $.fn.pagescalendar.defaults = {
        ui: {
            year: {
                visible: true,
                format: 'YYYY',
                startYear: '2000',
                endYear: moment().add(10, 'year').format('YYYY'),
                eventBubble: true
            },
            month: {
                visible: true,
                format: 'MMM',
                eventBubble: true
            },
            date: {
                format: 'MMMM YYYY, D dddd'
            },
            week: {
                day: {
                    format: 'D'
                },
                header: {
                    format: 'dd'
                },
                eventBubble: true,
                startOfTheWeek: '0',
                endOfTheWeek:'6'
            },
            grid: {
                dateFormat: 'D dddd',
                timeFormat: 'h A',
                eventBubble: true,
                slotDuration: '30'
            }
        },
        header: {
            visible: true,
            dateFormat: 'MMM YYYY'
        },
        miniCalendar: {
            visible: true,
            highlightWeek: true,
            showEventBubbles: true
        },
        eventObj: {
            editable: true
        },
        now: null,
        locale: 'en',
        timeFormat: 'h a',
        dateFormat: 'MMMM Do YYYY',
        slotDuration: '30', //In Mins : only supports 30 and 60
        events: [],
        //Event CallBacks
        onViewRenderComplete: function() {},
        onEventDblClick: function() {},
        onEventClick: function(event) {},
        onEventRender: function() {},
        onEventDragComplete: function(event) {},
        onEventResizeComplete: function(event) {},
        onTimeSlotDblClick: function(timeSlot) {}

        /*
        TO DO 
        eventDragStart (callback)
        eventResizeStart (callback)
        eventMouseover (callback)
        eventMouseout (callback)
        */

    }
    $.fn.pagescalendar.defaults.calendar = {
        year: moment().year(),
        date: moment().format("D"),
        month: moment().month(),
        monthLong: '',
        dayOfWeek: moment().day(),
        daysOfMonth: 0,
        startOfWeekDate: null,
        timeFormat: '',
        loadedYears: {}
    }
    $.fn.pagescalendar.settings = {}

})(jQuery);
(function($) {
    'use strict';

    // Wysiwyg editor custom options

    var editorTemplate = {
        "font-styles": function(locale) {
            return '<li class="dropdown dropup">' + '<a data-toggle="dropdown" class="btn btn-default dropdown-toggle ">    <span class="glyphicon glyphicon-font"></span>    <span class="current-font">Normal text</span>    <b class="caret"></b>  </a>' + '<ul class="dropdown-menu">    <li><a tabindex="-1" data-wysihtml5-command-value="p" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Normal text</a></li>     <li><a tabindex="-1" data-wysihtml5-command-value="h1" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 1</a></li>    <li><a tabindex="-1" data-wysihtml5-command-value="h2" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 2</a></li>    <li><a tabindex="-1" data-wysihtml5-command-value="h3" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 3</a></li>    <li><a tabindex="-1" data-wysihtml5-command-value="h4" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 4</a></li>    <li><a tabindex="-1" data-wysihtml5-command-value="h5" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 5</a></li>    <li><a tabindex="-1" data-wysihtml5-command-value="h6" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 6</a></li>  </ul>' + '</li>';
        },
        emphasis: function(locale) {
            return '<li>' + '<div class="btn-group">' + '<a tabindex="-1" title="CTRL+B" data-wysihtml5-command="bold" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-bold"></i></a>' + '<a tabindex="-1" title="CTRL+I" data-wysihtml5-command="italic" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-italic"></i></a>' + '<a tabindex="-1" title="CTRL+U" data-wysihtml5-command="underline" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-underline"></i></a>' + '</div>' + '</li>';
        },
        blockquote: function(locale) {
            return '<li>' + '<a tabindex="-1" data-wysihtml5-display-format-name="false" data-wysihtml5-command-value="blockquote" data-wysihtml5-command="formatBlock" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-quote"></i>' + '</a>' + '</li>'
        },
        lists: function(locale) {
            return '<li>' + '<div class="btn-group">' + '<a tabindex="-1" title="Unordered list" data-wysihtml5-command="insertUnorderedList" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-ul"></i></a>' + '<a tabindex="-1" title="Ordered list" data-wysihtml5-command="insertOrderedList" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-ol"></i></a>' + '<a tabindex="-1" title="Outdent" data-wysihtml5-command="Outdent" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-outdent"></i></a>' + '<a tabindex="-1" title="Indent" data-wysihtml5-command="Indent" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-indent"></i></a>' + '</div>' + '</li>'
        },
        image: function(locale) {
            return '<li>' + '<div class="bootstrap-wysihtml5-insert-image-modal modal fade">' + '<div class="modal-dialog ">' + '<div class="modal-content">' + '<div class="modal-header">' + '<a data-dismiss="modal" class="close">×</a>' + '<h3>Insert image</h3>' + '</div>' + '<div class="modal-body">' + '<input class="bootstrap-wysihtml5-insert-image-url form-control" value="http://">' + '</div>' + '<div class="modal-footer">' + '<a data-dismiss="modal" class="btn btn-default">Cancel</a>' + '<a data-dismiss="modal" class="btn btn-primary">Insert image</a>' + '</div>' + '</div>' + '</div>' + '</div>' + '<a tabindex="-1" title="Insert image" data-wysihtml5-command="insertImage" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-image"></i>' + '</a>' + '</li>'
        },
        link: function(locale) {
            return '<li>' + '<div class="bootstrap-wysihtml5-insert-link-modal modal fade">' + '<div class="modal-dialog ">' + '<div class="modal-content">' + '<div class="modal-header">' + '<a data-dismiss="modal" class="close">×</a>' + '<h3>Insert link</h3>' + '</div>' + '<div class="modal-body">' + '<input class="bootstrap-wysihtml5-insert-link-url form-control" value="http://">' + '<div class="checkbox check-success"> <input type="checkbox" class="bootstrap-wysihtml5-insert-link-target" checked="checked" value="1" id="link-checkbox"> <label for="link-checkbox">Open link in new window</label></div>' + '</div>' + '<div class="modal-footer">' + '<a data-dismiss="modal" class="btn btn-default">Cancel</a>' + '<a data-dismiss="modal" class="btn btn-primary" href="#">Insert link</a>' + '</div>' + '</div>' + '</div>' + '</div>' + '<a tabindex="-1" title="Insert link" data-wysihtml5-command="createLink" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-link"></i>' + '</a>' + '</li>'
        }
    }

    var editorOptions = {
        "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
        "emphasis": true, //Italics, bold, etc. Default true
        "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
        "html": false, //Button which allows you to edit the generated HTML. Default false
        "link": true, //Button to insert a link. Default true
        "image": true, //Button to insert an image. Default true,
        "color": false, //Button to change color of font  
        "blockquote": true, //Blockquote  
        stylesheets: ["pages/css/editor.css"],
        customTemplates: editorTemplate
    };


    $('#mark-email').click(function() {
        $('.item .checkbox').toggle();
    });

    // Load list of emails
    $('.email-list').length && $.ajax({
        dataType: "json",
        url: "http://revox.io/json/emails.json",
        success: function(data) {


            $.each(data.emails, function(i) {
                var obj = data.emails[i];
                var group = obj.group;
                var list = obj.list;

                var listViewGroupCont = $('<div/>', {
                    "class": "list-view-group-container"
                });
                listViewGroupCont.append('<div class="list-view-group-header"><span>' + group + '</span></div>');
                var ul = $('<ul/>', {
                    "class": "no-padding"
                });

                $.each(list, function(j) {
                    var $this = list[j];
                    var id = $this.id;
                    var dp = $this.dp;
                    var dpRetina = $this.dpRetina;
                    var to = $this.to.join();
                    var subject = $this.subject;
                    var body = $this.body.replace(/<(?:.|\n)*?>/gm, '');
                    var time = $this.time;
                    var colors = ['b-success', 'b-primary', 'b-warning', 'b-info', 'b-complete', 'b-danger'];
                    var color = colors[Math.floor(Math.random() * (6))];
                    var li = '<li class="item padding-15" data-email-id="' + id + '"> \
                                <div class="thumbnail-wrapper d32 circular bordered ' + color + '"> \
                                    <img_ width="40" height="40" alt="" data-src-retina="' + dpRetina + '" data-src="' + dp + '" src="' + dpRetina + '"> \
                                </div> \
                                <div class="checkbox  no-margin p-l-10"> \
                                    <input type="checkbox" value="1" id="emailcheckbox-' + i + "-" + j + '"> \
                                    <label for="emailcheckbox-' + i + "-" + j + '"></label> \
                                </div> \
                                <div class="inline m-l-15"> \
                                    <p class="recipients no-margin hint-text small">' + to + '</p> \
                                    <p class="subject no-margin">' + subject + '</p> \
                                    <p class="body no-margin"> \
                                     ' + body + ' \
                                    </p> \
                                </div> \
                                <div class="datetime">' + time + '</div> \
                                <div class="clearfix"></div> \
                            </li>';
                    ul.append(li);
                });

                listViewGroupCont.append(ul);
                $('#emailList').append(listViewGroupCont);
            });
            $("#emailList").ioslist();

        }
    });

    $('body').on('click', '.item .checkbox', function(e) {
        e.stopPropagation();
    });

    $('body').on('click', '.item', function(e) {
        e.stopPropagation();

        var id = $(this).attr('data-email-id');
        var email = null;
        var thumbnailWrapper = $(this).find('.thumbnail-wrapper');
        $.ajax({
            dataType: "json",
            url: "http://revox.io/json/emails.json",
            success: function(data) {
                $.each(data.emails, function(i) {
                    var obj = data.emails[i];
                    var list = obj.list;
                    $.each(list, function(j) {
                        if (list[j].id == id) {
                            email = list[j];

                            return;
                        }
                    });

                    if (email != null) return;
                });
                var emailOpened = $('.email-opened');

                emailOpened.find('.sender .name').text(email.from);
                emailOpened.find('.sender .datetime').text(email.datetime);
                emailOpened.find('.subject').text(email.subject);
                emailOpened.find('.email-content-body').html(email.body);

                var thumbnailClasses = thumbnailWrapper.attr('class').replace('d32', 'd48');
                emailOpened.find('.thumbnail-wrapper').html(thumbnailWrapper.html()).attr('class', thumbnailClasses);

                $('.no-email').hide();
                $('.actions-dropdown').toggle();
                $('.actions, .email-content-wrapper').show();
                if ($.Pages.isVisibleSm() || $.Pages.isVisibleXs()) {
                    $('.email-list').toggleClass('slideLeft');
                }

                !$('.email-reply').data('wysihtml5') && $('.email-reply').wysihtml5(editorOptions);

                $(".email-content-wrapper").scrollTop(0);

                // Initialize email action menu 
                $('.menuclipper').menuclipper({
                    bufferWidth: 20
                });
            }
        });

        $('.item').removeClass('active');
        $(this).addClass('active');

    });

    // Toggle email sidebar on mobile view
    $('.toggle-email-sidebar').click(function(e) {
        e.stopPropagation();
        $('.email-sidebar').toggle();
    });

    $('.email-list-toggle').click(function() {
        $('.email-list').toggleClass('slideLeft');
    });

    $('.email-sidebar').click(function(e) {
        e.stopPropagation();
    })

    $(window).resize(function() {

        if ($(window).width() <= 1024) {
            $('.email-sidebar').hide();

        } else {
            $('.email-list').length && $('.email-list').removeClass('slideLeft');
            $('.email-sidebar').show();
        }
    });


    // Email composer
    var emailComposerToolbarTemplate = {
        "font-styles": function(locale) {
            return '<li class="dropdown">' + '<a data-toggle="dropdown" class="btn btn-default dropdown-toggle ">' + '<span class="editor-icon editor-icon-headline"></span>' + '<span class="current-font">Normal</span>' + '<b class="caret"></b>' + '</a>' + '<ul class="dropdown-menu">' + '<li><a tabindex="-1" data-wysihtml5-command-value="p" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Normal</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h1" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">1</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h2" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">2</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h3" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">3</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h4" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">4</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h5" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">5</a></li>' + '<li><a tabindex="-1" data-wysihtml5-command-value="h6" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">6</a></li>' + '</ul>' + '</li>';
        },
        emphasis: function(locale) {
            return '<li>' + '<div class="btn-group">' + '<a tabindex="-1" title="CTRL+B" data-wysihtml5-command="bold" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-bold"></i></a>' + '<a tabindex="-1" title="CTRL+I" data-wysihtml5-command="italic" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-italic"></i></a>' + '<a tabindex="-1" title="CTRL+U" data-wysihtml5-command="underline" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-underline"></i></a>' + '</div>' + '</li>';
        },
        blockquote: function(locale) {
            return '<li>' + '<a tabindex="-1" data-wysihtml5-display-format-name="false" data-wysihtml5-command-value="blockquote" data-wysihtml5-command="formatBlock" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-quote"></i>' + '</a>' + '</li>'
        },
        lists: function(locale) {
            return '<li>' + '<div class="btn-group">' + '<a tabindex="-1" title="Unordered list" data-wysihtml5-command="insertUnorderedList" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-ul"></i></a>' + '<a tabindex="-1" title="Ordered list" data-wysihtml5-command="insertOrderedList" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-ol"></i></a>' + '<a tabindex="-1" title="Outdent" data-wysihtml5-command="Outdent" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-outdent"></i></a>' + '<a tabindex="-1" title="Indent" data-wysihtml5-command="Indent" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-indent"></i></a>' + '</div>' + '</li>'
        },
        image: function(locale) {
            return '<li>' + '<div class="bootstrap-wysihtml5-insert-image-modal modal fade">' + '<div class="modal-dialog ">' + '<div class="modal-content">' + '<div class="modal-header">' + '<a data-dismiss="modal" class="close">×</a>' + '<h3>Insert image</h3>' + '</div>' + '<div class="modal-body">' + '<input class="bootstrap-wysihtml5-insert-image-url form-control" value="http://">' + '</div>' + '<div class="modal-footer">' + '<a data-dismiss="modal" class="btn btn-default">Cancel</a>' + '<a data-dismiss="modal" class="btn btn-primary">Insert image</a>' + '</div>' + '</div>' + '</div>' + '</div>' + '<a tabindex="-1" title="Insert image" data-wysihtml5-command="insertImage" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-image"></i>' + '</a>' + '</li>'
        },
        link: function(locale) {
            return '<li>' + '<div class="bootstrap-wysihtml5-insert-link-modal modal fade">' + '<div class="modal-dialog ">' + '<div class="modal-content">' + '<div class="modal-header">' + '<a data-dismiss="modal" class="close">×</a>' + '<h3>Insert link</h3>' + '</div>' + '<div class="modal-body">' + '<input class="bootstrap-wysihtml5-insert-link-url form-control" value="http://">' + '<label class="checkbox"> <input type="checkbox" checked="" class="bootstrap-wysihtml5-insert-link-target">Open link in new window</label>' + '</div>' + '<div class="modal-footer">' + '<a data-dismiss="modal" class="btn btn-default">Cancel</a>' + '<a data-dismiss="modal" class="btn btn-primary" href="#">Insert link</a>' + '</div>' + '</div>' + '</div>' + '</div>' + '<a tabindex="-1" title="Insert link" data-wysihtml5-command="createLink" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-link"></i>' + '</a>' + '</li>'
        },
        html: function(locale) {
            return '<li>' + '<div class="btn-group">' + '<a tabindex="-1" title="Edit HTML" data-wysihtml5-action="change_view" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-html"></i>' + '</a>' + '</div>' + '</li>'
        }
    }

    setTimeout(function() {
        $('.email-body').length && $('.email-body').wysihtml5({
            html: true,
            stylesheets: ["pages/css/editor.css"],
            customTemplates: emailComposerToolbarTemplate
        });

        $('.email-composer .wysihtml5-toolbar').appendTo('.email-toolbar-wrapper');
    }, 500);


})(window.jQuery);
(function($) {

    'use strict';

    var Pages = function() {
        this.VERSION = "1.0.0";
        this.AUTHOR = "Revox";
        this.SUPPORT = "support@revox.io";

        this.pageScrollElement = 'html, body';
        this.$body = $('body');

        this.setUserOS();
        this.setUserAgent();
    }

    // Set environment vars
    Pages.prototype.setUserOS = function() {
        var OSName = "";
        if (navigator.appVersion.indexOf("Win") != -1) OSName = "windows";
        if (navigator.appVersion.indexOf("Mac") != -1) OSName = "mac";
        if (navigator.appVersion.indexOf("X11") != -1) OSName = "unix";
        if (navigator.appVersion.indexOf("Linux") != -1) OSName = "linux";

        this.$body.addClass(OSName);
    }

    Pages.prototype.setUserAgent = function() {
        if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
            this.$body.addClass('mobile');
        } else {
            this.$body.addClass('desktop');
            if (navigator.userAgent.match(/MSIE 9.0/)) {
                this.$body.addClass('ie9');
            }
        }
    }

    // Pages util functions
    Pages.prototype.isVisibleXs = function() {
        (!$('#pg-visible-xs').length) && this.$body.append('<div id="pg-visible-xs" class="visible-xs" />');
        return $('#pg-visible-xs').is(':visible');
    }

    Pages.prototype.isVisibleSm = function() {
        (!$('#pg-visible-sm').length) && this.$body.append('<div id="pg-visible-sm" class="visible-sm" />');
        return $('#pg-visible-sm').is(':visible');
    }

    Pages.prototype.isVisibleMd = function() {
        (!$('#pg-visible-md').length) && this.$body.append('<div id="pg-visible-md" class="visible-md" />');
        return $('#pg-visible-md').is(':visible');
    }

    Pages.prototype.isVisibleLg = function() {
        (!$('#pg-visible-lg').length) && this.$body.append('<div id="pg-visible-lg" class="visible-lg" />');
        return $('#pg-visible-lg').is(':visible');
    }

    Pages.prototype.getUserAgent = function() {
        return $('body').hasClass('mobile') ? "mobile" : "desktop";
    }

    Pages.prototype.setFullScreen = function(element) {
        // Supports most browsers and their versions.
        var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullscreen;

        if (requestMethod) { // Native full screen.
            requestMethod.call(element);
        } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
            var wscript = new ActiveXObject("WScript.Shell");
            if (wscript !== null) {
                wscript.SendKeys("{F11}");
            }
        }
    }

    Pages.prototype.getColor = function(color, opacity) {
        opacity = parseFloat(opacity) || 1;

        var elem = $('.pg-colors').length ? $('.pg-colors') : $('<div class="pg-colors"></div>').appendTo('body');

        var colorElem = elem.find('[data-color="' + color + '"]').length ? elem.find('[data-color="' + color + '"]') : $('<div class="bg-' + color + '" data-color="' + color + '"></div>').appendTo(elem);

        var color = colorElem.css('background-color');

        var rgb = color.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        var rgba = "rgba(" + rgb[1] + ", " + rgb[2] + ", " + rgb[3] + ', ' + opacity + ')';

        return rgba;
    }

    // Initialize Layout
    Pages.prototype.initDropDown = function() {
        // adjust width of each dropdown to match content width
        $('.dropdown-default').each(function() {
            var btn = $(this).find('.dropdown-menu').siblings('.dropdown-toggle');
            var offset = 0;

            var padding = btn.actual('innerWidth') - btn.actual('width');
            var menuWidth = $(this).find('.dropdown-menu').actual('outerWidth');

            if (btn.actual('outerWidth') < menuWidth) {
                btn.width(menuWidth - offset);
                $(this).find('.dropdown-menu').width(btn.actual('outerWidth'));
            } else {
                $(this).find('.dropdown-menu').width(btn.actual('outerWidth'));
            }
        });
    }

    Pages.prototype.initFormGroupDefault = function() {
        $('.form-group.form-group-default').click(function() {
            $(this).find(':input').focus();
        });
        $('body').on('focus', '.form-group.form-group-default :input', function() {
            $('.form-group.form-group-default').removeClass('focused');
            $(this).parents('.form-group').addClass('focused');
        });

        $('body').on('blur', '.form-group.form-group-default :input', function() {
            $(this).parents('.form-group').removeClass('focused');
            if ($(this).val()) {
                $(this).closest('.form-group').find('label').addClass('fade');
            } else {
                $(this).closest('.form-group').find('label').removeClass('fade');
            }
        });

        $('.form-group.form-group-default .checkbox, .form-group.form-group-default .radio').hover(function() {
            $(this).parents('.form-group').addClass('focused');
        }, function() {
            $(this).parents('.form-group').removeClass('focused');
        });
    }

    Pages.prototype.initSlidingTabs = function() {
        // TODO: move this to a separate file
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            //e = $(e.relatedTarget || e.target).parent().find('a[data-toggle=tab]');
            e = $(e.target).parent().find('a[data-toggle=tab]');

            var hrefPrev = e.attr('href');

            var hrefCurrent = e.attr('href');

            if (!$(hrefCurrent).is('.slide-left, .slide-right')) return;
            $(hrefCurrent).addClass('sliding');

            setTimeout(function() {
                $(hrefCurrent).removeClass('sliding');
            }, 100);
        });
    }

    Pages.prototype.initNotificationCenter = function() {
        $('.notification-list .dropdown-menu').on('click', function(event) {
            event.stopPropagation();
        });
        $('.toggle-more-details').on('click', function(event) {
            var p = $(this).closest('.heading');
            p.closest('.heading').children('.more-details').stop().slideToggle('fast', function() {
                p.toggleClass('open');
            });
        });
    }


    Pages.prototype.initProgressBars = function() {
        $(window).on('load', function() {
            $('.progress-bar').each(function() {
                $(this).css('width', $(this).attr("data-percentage"));
            });
            // Hack: FF doesn't play SVG animations set as background-image
            $('.progress-bar-indeterminate, .progress-circle-indeterminate, .mapplic-pin').hide().show(0);
        });
    }

    Pages.prototype.initView = function() {
            $('[data-navigate="view"]').on('click', function(e) {
                e.preventDefault();
                var el = $(this).attr('data-view-port');
                $(el).toggleClass($(this).attr('data-view-animation'));
                return false;
            })
        }
        // Initialize Plugins
    Pages.prototype.initTooltipPlugin = function() {
        $.fn.tooltip && $('[data-toggle="tooltip"]').tooltip();
    }

    Pages.prototype.initSelect2Plugin = function() {
        $.fn.select2 && $('[data-init-plugin="select2"]').each(function() {
            $(this).select2({
                minimumResultsForSearch: ($(this).attr('data-disable-search') == 'true' ? -1 : 1)
            }).on('select2-opening', function() {
                $.fn.scrollbar && $('.select2-results').scrollbar({
                    ignoreMobile: false
                })
            });
        });
    }

    Pages.prototype.initScrollBarPlugin = function() {
        $.fn.scrollbar && $('.scrollable').scrollbar({
            ignoreOverlay: false
        });
    }

    Pages.prototype.initListView = function() {
        $.fn.ioslist && $('[data-init-list-view="ioslist"]').ioslist();
        $.fn.scrollbar && $('.list-view-wrapper').scrollbar({
            ignoreOverlay: false
        });
    }

    Pages.prototype.initSwitcheryPlugin = function() {
        // Switchery - ios7 switch
        window.Switchery && $('[data-init-plugin="switchery"]').each(function() {
            new Switchery($(this).get(0), {
                color: $.Pages.getColor('success')
            });
        });
    }

    Pages.prototype.initSelectFxPlugin = function() {
        window.SelectFx && $('select[data-init-plugin="cs-select"]').each(function() {
            var el = $(this).get(0);
            $(el).wrap('<div class="cs-wrapper"></div>');
            new SelectFx(el);
        });
    }

    Pages.prototype.initUnveilPlugin = function() {
        // lazy load retina images
        $.fn.unveil && $("img_").unveil();
    }

    Pages.prototype.initValidatorPlugin = function() {
        $.validator && $.validator.setDefaults({
            ignore: "", // validate hidden fields, required for cs-select
            showErrors: function(errorMap, errorList) {
                var $this = this;
                $.each(this.successList, function(index, value) {
                    var parent = $(this).closest('.form-group-attached');
                    if (parent.length) return $(value).popover("hide");
                });
                return $.each(errorList, function(index, value) {

                    var parent = $(value.element).closest('.form-group-attached');
                    if (!parent.length) {
                        return $this.defaultShowErrors();
                    }
                    var _popover;
                    _popover = $(value.element).popover({
                        trigger: "manual",
                        placement: "top",
                        html: true,
                        container: parent.closest('form'),
                        content: value.message
                    });
                    _popover.data("bs.popover").options.content = value.message;
                    var parent = $(value.element).closest('.form-group');
                    parent.addClass('has-error');
                    $(value.element).popover("show");
                });
            },
            onfocusout: function(element) {
                var parent = $(element).closest('.form-group');
                if ($(element).valid()) {
                    parent.removeClass('has-error');
                    parent.next('.error').remove();
                }
            },
            onkeyup: function(element) {
                var parent = $(element).closest('.form-group');
                if ($(element).valid()) {
                    $(element).removeClass('error');
                    parent.removeClass('has-error');
                    parent.next('label.error').remove();
                    parent.find('label.error').remove();
                } else {
                    parent.addClass('has-error');
                }
            },
            errorPlacement: function(error, element) {
                var parent = $(element).closest('.form-group');
                if (parent.hasClass('form-group-default')) {
                    parent.addClass('has-error');
                    error.insertAfter(parent);
                } else {
                    error.insertAfter(element);
                }
            }
        });
    }

    // Call initializers
    Pages.prototype.init = function() {
        // init layout
        this.initDropDown();
        this.initFormGroupDefault();
        this.initSlidingTabs();
        this.initNotificationCenter();
        this.initProgressBars();
        // init plugins
        this.initTooltipPlugin();
        this.initSelect2Plugin();
        this.initScrollBarPlugin();
        this.initSwitcheryPlugin();
        this.initSelectFxPlugin();
        this.initUnveilPlugin();
        this.initValidatorPlugin();
        this.initView();
        this.initListView();
    }

    $.Pages = new Pages();
    $.Pages.Constructor = Pages;

})(window.jQuery);
/**
 * selectFx.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
;( function( window ) {
    
    'use strict';

    /**
     * based on from https://github.com/inuyaksa/jquery.nicescroll/blob/master/jquery.nicescroll.js
     */
    function hasParent( e, p ) {
        if (!e) return false;
        var el = e.target||e.srcElement||e||false;
        while (el && el != p) {
            el = el.parentNode||false;
        }
        return (el!==false);
    };
    
    /**
     * extend obj function
     */
    function extend( a, b ) {
        for( var key in b ) { 
            if( b.hasOwnProperty( key ) ) {
                a[key] = b[key];
            }
        }
        return a;
    }

    /**
     * SelectFx function
     */
    function SelectFx( el, options ) {  
        this.el = el;
        this.options = extend( {}, this.options );
        extend( this.options, options );
        this._init();
    }

    /**
    * Pure-JS alternative to jQuery closest()
    */
    function closest(elem, selector) {
       var matchesSelector = elem.matches || elem.webkitMatchesSelector || elem.mozMatchesSelector || elem.msMatchesSelector;
        while (elem) {
            if (matchesSelector.bind(elem)(selector)) {
                return elem;
            } else {
                elem = elem.parentElement;
            }
        }
        return false;
    }

    /**
    * jQuery offset() in pure JS
    */
    function offset(el) {
        return {
            left : el.getBoundingClientRect().left + window.pageXOffset - el.ownerDocument.documentElement.clientLeft,
            top : el.getBoundingClientRect().top + window.pageYOffset - el.ownerDocument.documentElement.clientTop
        }
        
    }

    /**
    * jQuery after() in pure JS
    */
    function insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }
    /**
     * SelectFx options
     */
    SelectFx.prototype.options = {
        // if true all the links will open in a new tab.
        // if we want to be redirected when we click an option, we need to define a data-link attr on the option of the native select element
        newTab : true,
        // when opening the select element, the default placeholder (if any) is shown
        stickyPlaceholder : true,
        // default container is body
        container : 'body',
        // callback when changing the value
        onChange : function( el ) { 
            var event = document.createEvent('HTMLEvents');
            event.initEvent('change', true, false);
            el.dispatchEvent(event);
        }
    }

    /**
     * init function
     * initialize and cache some vars
     */
    SelectFx.prototype._init = function() {
        // check if we are using a placeholder for the native select box
        // we assume the placeholder is disabled and selected by default
        var selectedOpt = this.el.querySelector( 'option[selected]' );
        this.hasDefaultPlaceholder = selectedOpt && selectedOpt.disabled;

        // get selected option (either the first option with attr selected or just the first option)
        this.selectedOpt = selectedOpt || this.el.querySelector( 'option' );

        // create structure
        this._createSelectEl();

        // all options
        this.selOpts = [].slice.call( this.selEl.querySelectorAll( 'li[data-option]' ) );
        
        // total options
        this.selOptsCount = this.selOpts.length;
        
        // current index
        this.current = this.selOpts.indexOf( this.selEl.querySelector( 'li.cs-selected' ) ) || -1;
        
        // placeholder elem
        this.selPlaceholder = this.selEl.querySelector( 'span.cs-placeholder' );

        // init events
        this._initEvents();
    }

    /**
     * creates the structure for the select element
     */
    SelectFx.prototype._createSelectEl = function() {
        var self = this, options = '', createOptionHTML = function(el) {
            var optclass = '', classes = '', link = '';

            if( el.selectedOpt && !this.foundSelected && !this.hasDefaultPlaceholder ) {
                classes += 'cs-selected ';
                this.foundSelected = true;
            }
            // extra classes
            if( el.getAttribute( 'data-class' ) ) {
                classes += el.getAttribute( 'data-class' );
            }
            // link options
            if( el.getAttribute( 'data-link' ) ) {
                link = 'data-link=' + el.getAttribute( 'data-link' );
            }

            if( classes !== '' ) {
                optclass = 'class="' + classes + '" ';
            }

            return '<li ' + optclass + link + ' data-option data-value="' + el.value + '"><span>' + el.textContent + '</span></li>';
        };

        [].slice.call( this.el.children ).forEach( function(el) {
            if( el.disabled ) { return; }

            var tag = el.tagName.toLowerCase();

            if( tag === 'option' ) {
                options += createOptionHTML(el);
            }
            else if( tag === 'optgroup' ) {
                options += '<li class="cs-optgroup"><span>' + el.label + '</span><ul>';
                [].slice.call( el.children ).forEach( function(opt) {
                    options += createOptionHTML(opt);
                } )
                options += '</ul></li>';
            }
        } );

        var opts_el = '<div class="cs-options"><ul>' + options + '</ul></div>';
        this.selEl = document.createElement( 'div' );
        this.selEl.className = this.el.className;
        this.selEl.tabIndex = this.el.tabIndex;
        this.selEl.innerHTML = '<span class="cs-placeholder">' + this.selectedOpt.textContent + '</span>' + opts_el;
        this.el.parentNode.appendChild( this.selEl );
        this.selEl.appendChild( this.el );

        // backdrop to support dynamic heights of the dropdown
        var backdrop = document.createElement('div');
        backdrop.className = 'cs-backdrop';
        this.selEl.appendChild(backdrop);
    }

    /**
     * initialize the events
     */
    SelectFx.prototype._initEvents = function() {
        var self = this;

        // open/close select
        this.selPlaceholder.addEventListener( 'click', function() {
            self._toggleSelect();
        } );

        // clicking the options
        this.selOpts.forEach( function(opt, idx) {
            opt.addEventListener( 'click', function() {
                self.current = idx;
                self._changeOption();
                // close select elem
                self._toggleSelect();
            } );
        } );

        // close the select element if the target it´s not the select element or one of its descendants..
        document.addEventListener( 'click', function(ev) {
            var target = ev.target;
            if( self._isOpen() && target !== self.selEl && !hasParent( target, self.selEl ) ) {
                self._toggleSelect();
            }
        } );

        // keyboard navigation events
        this.selEl.addEventListener( 'keydown', function( ev ) {
            var keyCode = ev.keyCode || ev.which;

            switch (keyCode) {
                // up key
                case 38:
                    ev.preventDefault();
                    self._navigateOpts('prev');
                    break;
                // down key
                case 40:
                    ev.preventDefault();
                    self._navigateOpts('next');
                    break;
                // space key
                case 32:
                    ev.preventDefault();
                    if( self._isOpen() && typeof self.preSelCurrent != 'undefined' && self.preSelCurrent !== -1 ) {
                        self._changeOption();
                    }
                    self._toggleSelect();
                    break;
                // enter key
                case 13:
                    ev.preventDefault();
                    if( self._isOpen() && typeof self.preSelCurrent != 'undefined' && self.preSelCurrent !== -1 ) {
                        self._changeOption();
                        self._toggleSelect();
                    }
                    break;
                // esc key
                case 27:
                    ev.preventDefault();
                    if( self._isOpen() ) {
                        self._toggleSelect();
                    }
                    break;
            }
        } );
    }

    /**
     * navigate with up/dpwn keys
     */
    SelectFx.prototype._navigateOpts = function(dir) {
        if( !this._isOpen() ) {
            this._toggleSelect();
        }

        var tmpcurrent = typeof this.preSelCurrent != 'undefined' && this.preSelCurrent !== -1 ? this.preSelCurrent : this.current;
        
        if( dir === 'prev' && tmpcurrent > 0 || dir === 'next' && tmpcurrent < this.selOptsCount - 1 ) {
            // save pre selected current - if we click on option, or press enter, or press space this is going to be the index of the current option
            this.preSelCurrent = dir === 'next' ? tmpcurrent + 1 : tmpcurrent - 1;
            // remove focus class if any..
            this._removeFocus();
            // add class focus - track which option we are navigating
            classie.add( this.selOpts[this.preSelCurrent], 'cs-focus' );
        }
    }

    /**
     * open/close select
     * when opened show the default placeholder if any
     */
    SelectFx.prototype._toggleSelect = function() {
        var backdrop = this.selEl.querySelector('.cs-backdrop');
        var container = document.querySelector(this.options.container);
        var mask = container.querySelector('.dropdown-mask');
        var csOptions = this.selEl.querySelector('.cs-options');
        var csPlaceholder = this.selEl.querySelector('.cs-placeholder');

        var csPlaceholderWidth = csPlaceholder.offsetWidth;
         var csPlaceholderHeight = csPlaceholder.offsetHeight;
        var csOptionsWidth = csOptions.scrollWidth;

        if (this._isOpen()) {
            if (this.current !== -1) {
                // update placeholder text
                this.selPlaceholder.textContent = this.selOpts[this.current].textContent;
            }

            var dummy = this.selEl.data;

            var parent = dummy.parentNode;
            //parent.appendChild(this.selEl);
            insertAfter(this.selEl,dummy);
            this.selEl.removeAttribute('style');

            parent.removeChild(dummy);

            // Hack for FF
            // http://stackoverflow.com/questions/12088819/css-transitions-on-new-elements
            var x = this.selEl.clientHeight;

            // reset backdrop
            backdrop.style.transform = backdrop.style.webkitTransform = backdrop.style.MozTransform = backdrop.style.msTransform = backdrop.style.OTransform = 'scale3d(1,1,1)';
            classie.remove(this.selEl, 'cs-active');

            mask.style.display = 'none';
            csOptions.style.overflowY = 'hidden';
            csOptions.style.width = 'auto';
            
            var parentFormGroup = closest(this.selEl,'.form-group');
            parentFormGroup && classie.removeClass(parentFormGroup, 'focused');

        } else {
            if (this.hasDefaultPlaceholder && this.options.stickyPlaceholder) {
                // everytime we open we wanna see the default placeholder text
                this.selPlaceholder.textContent = this.selectedOpt.textContent;
            }

            var dummy;
            if (this.selEl.parentNode.querySelector('.dropdown-placeholder')) {
                dummy = this.selEl.parentNode.querySelector('.dropdown-placeholder');
            } else {
                dummy = document.createElement('div');
                classie.add(dummy, 'dropdown-placeholder');
                //this.selEl.parentNode.appendChild(dummy);
                insertAfter(dummy, this.selEl);

            }


            dummy.style.height = csPlaceholderHeight + 'px';
            dummy.style.width = this.selEl.offsetWidth + 'px';

            this.selEl.data = dummy;



            this.selEl.style.position = 'absolute';
            var offsetselEl = offset(this.selEl);

            this.selEl.style.left = offsetselEl.left + 'px';
            this.selEl.style.top = offsetselEl.top + 'px';

            container.appendChild(this.selEl);

            // decide backdrop's scale factor depending on the content height
            var contentHeight = csOptions.offsetHeight;
            var originalHeight = csPlaceholder.offsetHeight;

            var contentWidth = csOptions.offsetWidth;
            var originalWidth = csPlaceholder.offsetWidth;

            var scaleV = contentHeight / originalHeight ;
            var scaleH = (contentWidth > originalWidth) ? contentWidth / originalWidth : 1.05;
            //backdrop.style.transform = backdrop.style.webkitTransform = backdrop.style.MozTransform = backdrop.style.msTransform = backdrop.style.OTransform = 'scale3d(' + scaleH + ', ' + scaleV + ', 1)';
            backdrop.style.transform = backdrop.style.webkitTransform = backdrop.style.MozTransform = backdrop.style.msTransform = backdrop.style.OTransform = 'scale3d(' + 1 + ', ' + scaleV + ', 1)';

            if (!mask) {
                mask = document.createElement('div');
                classie.add(mask, 'dropdown-mask');
                container.appendChild(mask);
            }

            mask.style.display = 'block';
            
            classie.add(this.selEl, 'cs-active');

            var resizedWidth = (csPlaceholderWidth < csOptionsWidth) ? csOptionsWidth : csPlaceholderWidth;

            this.selEl.style.width = resizedWidth + 'px';
            this.selEl.style.height = originalHeight + 'px';
            csOptions.style.width = '100%';

            setTimeout(function() {
                csOptions.style.overflowY = 'auto';
            }, 300);

        }
    }

    /**
     * change option - the new value is set
     */
    SelectFx.prototype._changeOption = function() {
        // if pre selected current (if we navigate with the keyboard)...
        if( typeof this.preSelCurrent != 'undefined' && this.preSelCurrent !== -1 ) {
            this.current = this.preSelCurrent;
            this.preSelCurrent = -1;
        }

        // current option
        var opt = this.selOpts[ this.current ];

        // update current selected value
        this.selPlaceholder.textContent = opt.textContent;
        
        // change native select element´s value
        this.el.value = opt.getAttribute( 'data-value' );

        // remove class cs-selected from old selected option and add it to current selected option
        var oldOpt = this.selEl.querySelector( 'li.cs-selected' );
        if( oldOpt ) {
            classie.remove( oldOpt, 'cs-selected' );
        }
        classie.add( opt, 'cs-selected' );

        // if there´s a link defined
        if( opt.getAttribute( 'data-link' ) ) {
            // open in new tab?
            if( this.options.newTab ) {
                window.open( opt.getAttribute( 'data-link' ), '_blank' );
            }
            else {
                window.location = opt.getAttribute( 'data-link' );
            }
        }

        // callback
        this.options.onChange( this.el );
    }

    /**
     * returns true if select element is opened
     */
    SelectFx.prototype._isOpen = function(opt) {
        return classie.has( this.selEl, 'cs-active' );
    }

    /**
     * removes the focus class from the option
     */
    SelectFx.prototype._removeFocus = function(opt) {
        var focusEl = this.selEl.querySelector( 'li.cs-focus' )
        if( focusEl ) {
            classie.remove( focusEl, 'cs-focus' );
        }
    }

    /**
     * add to global namespace
     */
    window.SelectFx = SelectFx;

} )( window );

/* ============================================================
 * Pages Chat
 * ============================================================ */

(function($) {
  'use strict';
  //To Open Chat When Clicked
  $('[data-chat-input]').on('keypress',function(e){
    if(e.which == 13) {
       var el = $(this).attr('data-chat-conversation');
       $(el).append('<div class="message clearfix">'+
        '<div class="chat-bubble from-me">'+$(this).val()+
        '</div></div>');
       $(this).val('');
    }
  });

})(window.jQuery);
/* ============================================================
 * Pages Circular Progress
 * ============================================================ */

(function($) {
    'use strict';
    // CIRCULAR PROGRESS CLASS DEFINITION
    // ======================

    var Progress = function(element, options) {
        this.$element = $(element);
        this.options = $.extend(true, {}, $.fn.circularProgress.defaults, options);


        // start adding to to DOM
        this.$container = $('<div class="progress-circle"></div>');

        this.$element.attr('data-color') && this.$container.addClass('progress-circle-' + this.$element.attr('data-color'));
        this.$element.attr('data-thick') && this.$container.addClass('progress-circle-thick');

        this.$pie = $('<div class="pie"></div>');

        this.$pie.$left = $('<div class="left-side half-circle"></div>');
        this.$pie.$right = $('<div class="right-side half-circle"></div>');

        this.$pie.append(this.$pie.$left).append(this.$pie.$right);

        this.$container.append(this.$pie).append('<div class="shadow"></div>');

        this.$element.after(this.$container);
        // end DOM adding

        this.val = this.$element.val();
        var deg = perc2deg(this.val);


        if (this.val <= 50) {
            this.$pie.$right.css('transform', 'rotate(' + deg + 'deg)');
        } else {
            this.$pie.css('clip', 'rect(auto, auto, auto, auto)');
            this.$pie.$right.css('transform', 'rotate(180deg)');
            this.$pie.$left.css('transform', 'rotate(' + deg + 'deg)');
        }

    }
    Progress.VERSION = "1.0.0";

    Progress.prototype.value = function(val) {
        if (typeof val == 'undefined') return;

        var deg = perc2deg(val);

        this.$pie.removeAttr('style');

        this.$pie.$right.removeAttr('style');
        this.$pie.$left.removeAttr('style');

        if (val <= 50) {
            this.$pie.$right.css('transform', 'rotate(' + deg + 'deg)');
        } else {
            this.$pie.css('clip', 'rect(auto, auto, auto, auto)');
            this.$pie.$right.css('transform', 'rotate(180deg)');
            this.$pie.$left.css('transform', 'rotate(' + deg + 'deg)');
        }

    }

    // CIRCULAR PROGRESS PLUGIN DEFINITION
    // =======================
    function Plugin(option) {
        return this.filter(':input').each(function() {
            var $this = $(this);
            var data = $this.data('pg.circularProgress');
            var options = typeof option == 'object' && option;

            if (!data) $this.data('pg.circularProgress', (data = new Progress(this, options)));
            if (typeof option == 'string') data[option]();
            else if (options.hasOwnProperty('value')) data.value(options.value);
        })
    }

    var old = $.fn.circularProgress

    $.fn.circularProgress = Plugin
    $.fn.circularProgress.Constructor = Progress


    $.fn.circularProgress.defaults = {
        value: 0
    }

    // CIRCULAR PROGRESS NO CONFLICT
    // ====================

    $.fn.circularProgress.noConflict = function() {
        $.fn.circularProgress = old;
        return this;
    }

    // CIRCULAR PROGRESS DATA API
    //===================

    $(window).on('load', function() {
        $('[data-pages-progress="circle"]').each(function() {
            var $progress = $(this)
            $progress.circularProgress($progress.data())
        })
    })

    function perc2deg(p) {
        return parseInt(p / 100 * 360);
    }

    // TODO: Add API to change size, stroke width, color

})(window.jQuery);

/* ============================================================
 * Pages Notifications
 * ============================================================ */

(function($) {

    'use strict';

    var Notification = function(container, options) {

        var self = this;

        // Element collection
        self.container = $(container); // 'body' recommended
        self.notification = $('<div class="pgn"></div>');
        self.options = $.extend(true, {}, $.fn.pgNotification.defaults, options);

        if (!self.container.find('.pgn-wrapper[data-position=' + this.options.position + ']').length) {
            self.wrapper = $('<div class="pgn-wrapper" data-position="' + this.options.position + '"></div>');
            self.container.append(self.wrapper);
        } else {
            self.wrapper = $('.pgn-wrapper[data-position=' + this.options.position + ']');
        }

        self.alert = $('<div class="alert"></div>');
        self.alert.addClass('alert-' + self.options.type);

        if (self.options.style == 'bar') {
            new BarNotification();
        } else if (self.options.style == 'flip') {
            new FlipNotification();
        } else if (self.options.style == 'circle') {
            new CircleNotification();
        } else if (self.options.style == 'simple') {
            new SimpleNotification();
        } else { // default = 'simple'
            new SimpleNotification();
        }

        // Notification styles
        function SimpleNotification() {

            self.notification.addClass('pgn-simple');

            self.alert.append(self.options.message);
            if (self.options.showClose) {
                var close = $('<button type="button" class="close" data-dismiss="alert"></button>')
                    .append('<span aria-hidden="true">&times;</span>')
                    .append('<span class="sr-only">Close</span>');

                self.alert.prepend(close);
            }

        }

        function BarNotification() {

            self.notification.addClass('pgn-bar');

            self.alert.append('<span>' + self.options.message + '</span>');
            self.alert.addClass('alert-' + self.options.type);


            if (self.options.showClose) {
                var close = $('<button type="button" class="close" data-dismiss="alert"></button>')
                    .append('<span aria-hidden="true">&times;</span>')
                    .append('<span class="sr-only">Close</span>');

                self.alert.prepend(close);
            }

        }

        function CircleNotification() {

            self.notification.addClass('pgn-circle');

            var table = '<div>';
            if (self.options.thumbnail) {
                table += '<div class="pgn-thumbnail"><div>' + self.options.thumbnail + '</div></div>';
            }

            table += '<div class="pgn-message"><div>';

            if (self.options.title) {
                table += '<p class="bold">' + self.options.title + '</p>';
            }
            table += '<p>' + self.options.message + '</p></div></div>';
            table += '</div>';

            if (self.options.showClose) {
                table += '<button type="button" class="close" data-dismiss="alert">';
                table += '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>';
                table += '</button>';
            }


            self.alert.append(table);
            self.alert.after('<div class="clearfix"></div>');

        }

        function FlipNotification() {

            self.notification.addClass('pgn-flip');
            self.alert.append("<span>" + self.options.message + "</span>");
            if (self.options.showClose) {
                var close = $('<button type="button" class="close" data-dismiss="alert"></button>')
                    .append('<span aria-hidden="true">&times;</span>')
                    .append('<span class="sr-only">Close</span>');

                self.alert.prepend(close);
            }

        }

        self.notification.append(self.alert);

        // bind to Bootstrap closed event for alerts 
        self.alert.on('closed.bs.alert', function() {
            self.notification.remove();
            self.options.onClosed();
            // refresh layout after removal
        });

        return this; // enable chaining
    };

    Notification.VERSION = "1.0.0";
    
    Notification.prototype.show = function() {

        // TODO: add fadeOut animation on show as option
        this.wrapper.prepend(this.notification);

        this.options.onShown();

        if (this.options.timeout != 0) {
            var _this = this;
            // settimeout removes scope. use .bind(this)
            setTimeout(function() {
                this.notification.fadeOut("slow", function() {
                    $(this).remove();
                    _this.options.onClosed();
                });
            }.bind(this), this.options.timeout);
        }

    };

    $.fn.pgNotification = function(options) {
        return new Notification(this, options);
    };

    $.fn.pgNotification.defaults = {
        style: 'simple',
        message: null,
        position: 'top-right',
        type: 'info',
        showClose: true,
        timeout: 4000,
        onShown: function() {},
        onClosed: function() {}
    }
})(window.jQuery);
/* ============================================================
 * Pages Portlet
 * ============================================================ */

(function($) {
    'use strict';
    // PORTLET CLASS DEFINITION
    // ======================

    var Portlet = function(element, options) {
        this.$element = $(element);
        this.options = $.extend(true, {}, $.fn.portlet.defaults, options);
        this.$loader = null;
        this.$body = this.$element.find('.panel-body');
    }
    Portlet.VERSION = "1.0.0";
    // Button actions
    Portlet.prototype.collapse = function() {
        var icon = this.$element.find('[data-toggle="collapse"] > i');
        var heading = this.$element.find('.panel-heading');

        this.$body.stop().slideToggle("fast");

        if (this.$element.hasClass('panel-collapsed')) {
            this.$element.removeClass('panel-collapsed');
            icon.removeClass().addClass('pg-arrow_maximize');
            $.isFunction(this.options.onExpand) && this.options.onExpand();
            return
        }
        this.$element.addClass('panel-collapsed');
        icon.removeClass().addClass('pg-arrow_minimize');
        $.isFunction(this.options.onCollapse) && this.options.onCollapse();
    }

    Portlet.prototype.close = function() {
        this.$element.remove();
        $.isFunction(this.options.onClose) && this.options.onClose();
    }

    Portlet.prototype.maximize = function() {
        var icon = this.$element.find('[data-toggle="maximize"] > i');

        if (this.$element.hasClass('panel-maximized')) {
            this.$element.removeClass('panel-maximized');
            icon.removeClass('pg-fullscreen_restore').addClass('pg-fullscreen');
            $.isFunction(this.options.onRestore) && this.options.onRestore();
        } else {
            this.$element.addClass('panel-maximized');
            icon.removeClass('pg-fullscreen').addClass('pg-fullscreen_restore');
            $.isFunction(this.options.onMaximize) && this.options.onMaximize();
        }
    }

    // Options
    Portlet.prototype.refresh = function(refresh) {
        var toggle = this.$element.find('[data-toggle="refresh"]');

        if (refresh) {
            if (this.$loader && this.$loader.is(':visible')) return;
            if (!$.isFunction(this.options.onRefresh)) return; // onRefresh() not set
            this.$loader = $('<div class="portlet-progress"></div>');
            this.$loader.css({
                'background-color': 'rgba('+this.options.overlayColor+','+this.options.overlayOpacity+')'
           
            });

            var elem = '';
            if (this.options.progress == 'circle') {
                elem += '<div class="progress-circle-indeterminate progress-circle-' + this.options.progressColor + '"></div>';
            } else if (this.options.progress == 'bar') {

                elem += '<div class="progress progress-small">';
                elem += '    <div class="progress-bar-indeterminate progress-bar-' + this.options.progressColor + '"></div>';
                elem += '</div>';
            } else if (this.options.progress == 'circle-lg') {
                toggle.addClass('refreshing');
                var iconOld = toggle.find('> i').first();
                var iconNew;
                if (!toggle.find('[class$="-animated"]').length) {
                    iconNew = $('<i/>');
                    iconNew.css({
                        'position': 'absolute',
                        'top': iconOld.position().top,
                        'left': iconOld.position().left
                    });
                    iconNew.addClass('portlet-icon-refresh-lg-' + this.options.progressColor + '-animated');
                    toggle.append(iconNew);
                } else {
                    iconNew = toggle.find('[class$="-animated"]');
                }

                iconOld.addClass('fade');
                iconNew.addClass('active');


            } else {
                elem += '<div class="progress progress-small">';
                elem += '    <div class="progress-bar-indeterminate progress-bar-' + this.options.progressColor + '"></div>';
                elem += '</div>';
            }

            this.$loader.append(elem);
            this.$element.append(this.$loader);

            // Start Fix for FF: pre-loading animated to SVGs
            var _loader = this.$loader;
            setTimeout(function(){
                this.$loader.remove();
                this.$element.append(_loader);
            }.bind(this), 300);
            // End fix
            this.$loader.fadeIn();

            $.isFunction(this.options.onRefresh) && this.options.onRefresh();

        } else {
            var _this = this;
            this.$loader.fadeOut(function() {
                $(this).remove();
                if (_this.options.progress == 'circle-lg') {
                    var iconNew = toggle.find('.active');
                    var iconOld = toggle.find('.fade');
                    iconNew.removeClass('active');
                    iconOld.removeClass('fade');
                    toggle.removeClass('refreshing');

                }
                _this.options.refresh = false;
            });
        }
    }

    Portlet.prototype.error = function(error) {
        if (error) {
            var _this = this;
            this.$element.pgNotification({
                style: 'bar',
                message: error,
                position: 'top',
                timeout: 0,
                type: 'danger',
                onShown: function() {
                    _this.$loader.find('> div').fadeOut()
                },
                onClosed: function() {
                    _this.refresh(false)
                }
            }).show();
        }
    }

    // PORTLET PLUGIN DEFINITION
    // =======================

    function Plugin(option) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('pg.portlet');
            var options = typeof option == 'object' && option;

            if (!data) $this.data('pg.portlet', (data = new Portlet(this, options)));
            if (typeof option == 'string') data[option]();
            else if (options.hasOwnProperty('refresh')) data.refresh(options.refresh);
            else if (options.hasOwnProperty('error')) data.error(options.error);
        })
    }

    var old = $.fn.portlet

    $.fn.portlet = Plugin
    $.fn.portlet.Constructor = Portlet


    $.fn.portlet.defaults = {
        progress: 'circle',
        progressColor: 'master',
        refresh: false,
        error: null,
        overlayColor: '255,255,255',
        overlayOpacity: 0.8
            // onRefresh: function() {},
            // onCollapse: function() {},
            // onExpand: function() {},
            // onMaximize: function() {},
            // onRestore: function() {},
            // onClose: function() {}
    }

    // PORTLET NO CONFLICT
    // ====================

    $.fn.portlet.noConflict = function() {
        $.fn.portlet = old;
        return this;
    }

    // PORTLET DATA API
    //===================

    $(document).on('click.pg.portlet.data-api', '[data-toggle="collapse"]', function(e) {
        var $this = $(this);
        var $target = $this.closest('.panel');
        if ($this.is('a')) e.preventDefault();
        $target.data('pg.portlet') && $target.portlet('collapse');
    })

    $(document).on('click.pg.portlet.data-api', '[data-toggle="close"]', function(e) {
        var $this = $(this);
        var $target = $this.closest('.panel');
        if ($this.is('a')) e.preventDefault();
        $target.data('pg.portlet') && $target.portlet('close');
    })

    $(document).on('click.pg.portlet.data-api', '[data-toggle="refresh"]', function(e) {
        var $this = $(this);
        var $target = $this.closest('.panel');
        if ($this.is('a')) e.preventDefault();
        $target.data('pg.portlet') && $target.portlet({
            refresh: true
        })
    })

    $(document).on('click.pg.portlet.data-api', '[data-toggle="maximize"]', function(e) {
        var $this = $(this);
        var $target = $this.closest('.panel');
        if ($this.is('a')) e.preventDefault();
        $target.data('pg.portlet') && $target.portlet('maximize');
    })

    $(window).on('load', function() {
        $('[data-pages="portlet"]').each(function() {
            var $portlet = $(this)
            $portlet.portlet($portlet.data())
        })
    })

})(window.jQuery);
/* ============================================================
 * Pages Quickview Plugin
 * ============================================================ */

(function($) {
    'use strict';
    // QUICKVIEW CLASS DEFINITION
    // ======================

    var Quickview = function(element, options) {
        this.$element = $(element);
        this.options = $.extend(true, {}, $.fn.quickview.defaults, options);
        this.bezierEasing = [.05, .74, .27, .99];
        var _this = this;

        $(this.options.notes).on('click', '.list > ul > li', function(e) {
            var note = $(this).find('.note-preview');
            var note = $(this).find('.note-preview');
            $(_this.options.noteEditor).html(note.html());
            $(_this.options.notes).toggleClass('push');
        });
        $(this.options.notes).on('click', '.list > ul > li .checkbox', function(e) {
            e.stopPropagation();
        });
        $(this.options.notes).on('click', _this.options.backButton, function(e) {
            $(_this.options.notes).find('.toolbar > li > a').removeClass('active');
            $(_this.options.notes).toggleClass('push');
        });
        $(this.options.deleteNoteButton).click(function(e) {
            e.preventDefault();
            $(this).toggleClass('selected');
            $(_this.options.notes).find('.list > ul > li .checkbox').fadeToggle("fast");
            $(_this.options.deleteNoteConfirmButton).fadeToggle("fast").removeClass('hide');
        });
        $(this.options.newNoteButton).click(function(e) {
            e.preventDefault();
            $(_this.options.noteEditor).html('');
        });

        $(this.options.deleteNoteConfirmButton).click(function() {
            var checked = $(_this.options.notes).find('input[type=checkbox]:checked');
            checked.each(function() {
                $(this).parents('li').remove();
            });
        });
        $(this.options.notes).on('click', '.toolbar > li > a', function(e) {
            //e.preventDefault();
            var command = $(this).attr('data-action');
            document.execCommand(command, false, null);
            $(this).toggleClass('active');
        });

    }
    Quickview.VERSION = "1.0.0";

    // QUICKVIEW PLUGIN DEFINITION
    // =======================
    function Plugin(option) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('pg.quickview');
            var options = typeof option == 'object' && option;

            if (!data) $this.data('pg.quickview', (data = new Quickview(this, options)));
            if (typeof option == 'string') data[option]();
        })
    }

    var old = $.fn.quickview

    $.fn.quickview = Plugin
    $.fn.quickview.Constructor = Quickview


    $.fn.quickview.defaults = {
        notes: '#note-views',
        alerts: '#alerts',
        chat: '#chat',
        notesList: '.list',
        noteEditor: '.quick-note-editor',
        deleteNoteButton: '.delete-note-link',
        deleteNoteConfirmButton: '.btn-remove-notes',
        newNoteButton: '.new-note-link',
        backButton: '.close-note-link'
    }

    // QUICKVIEW NO CONFLICT
    // ====================

    $.fn.quickview.noConflict = function() {
        $.fn.quickview = old;
        return this;
    }

    // QUICKVIEW DATA API
    //===================

    $(window).on('load', function() {

        $('[data-pages="quickview"]').each(function() {
            var $quickview = $(this)
            $quickview.quickview($quickview.data())
        })
    });


    $(document).on('click.pg.quickview.data-api touchstart', '[data-toggle="quickview"]', function(e) {
        var elem = $(this).attr('data-toggle-element');
        if (Modernizr.csstransitions) {
            $(elem).toggleClass('open');
        } else {
            var width = $(elem).width();
            if (!$(elem).hasClass('open-ie')) {
                $(elem).stop().animate({
                    right: -1 * width
                }, 400, $.bez([.05, .74, .27, .99]), function() {
                    $(elem).addClass('open-ie')
                });
            } else {
                $(elem).stop().animate({
                    right: 0
                }, 400, $.bez([.05, .74, .27, .99]), function() {
                    $(elem).removeClass('open-ie')
                });
            }
        }
        e.preventDefault();
    })

})(window.jQuery);
/* ============================================================
 * Pages Parallax Plugin
 * ============================================================ */

(function($) {
    'use strict';
    // PARALLAX CLASS DEFINITION
    // ======================

    var Parallax = function(element, options) {
        this.$element = $(element);
        this.options = $.extend(true, {}, $.fn.parallax.defaults, options);
        this.$coverPhoto = this.$element.find('.cover-photo');
        // TODO: rename .inner to .page-cover-content
        this.$content = this.$element.find('.inner');

        // if cover photo img_ is found make it a background-image
        if (this.$coverPhoto.find('> img_').length) {
            var img = this.$coverPhoto.find('> img_');
            this.$coverPhoto.css('background-image', 'url(' + img.attr('src') + ')');
            img.remove();
        }

    }
    Parallax.VERSION = "1.0.0";

    Parallax.prototype.animate = function() {

        var scrollPos;
        var pagecoverWidth = this.$element.height();
        //opactiy to text starts at 50% scroll length
        var opacityKeyFrame = pagecoverWidth * 50 / 100;
        var direction = 'translateX';

        scrollPos = $(window).scrollTop();
        direction = 'translateY';


        this.$coverPhoto.css({
            'transform': direction + '(' + scrollPos * this.options.speed.coverPhoto + 'px)'
        });

        this.$content.css({
            'transform': direction + '(' + scrollPos * this.options.speed.content + 'px)',
        });

        if (scrollPos > opacityKeyFrame) {
            this.$content.css({
                'opacity': 1 - scrollPos / 1200
            });
        } else {
            this.$content.css({
                'opacity': 1
            });
        }

    }

    // PARALLAX PLUGIN DEFINITION
    // =======================
    function Plugin(option) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('pg.parallax');
            var options = typeof option == 'object' && option;

            if (!data) $this.data('pg.parallax', (data = new Parallax(this, options)));
            if (typeof option == 'string') data[option]();
        })
    }

    var old = $.fn.parallax

    $.fn.parallax = Plugin
    $.fn.parallax.Constructor = Parallax


    $.fn.parallax.defaults = {
        speed: {
            coverPhoto: 0.3,
            content: 0.17
        }
    }

    // PARALLAX NO CONFLICT
    // ====================
    
    $.fn.parallax.noConflict = function() {
        $.fn.parallax = old;
        return this;
    }

    // PARALLAX DATA API
    //===================

    $(window).on('load', function() {

        $('[data-pages="parallax"]').each(function() {
            var $parallax = $(this)
            $parallax.parallax($parallax.data())
        })
    });

    $(window).on('scroll', function() {
        // Disable parallax for Touch devices
        if (Modernizr.touch) {
            return;
        }
        $('[data-pages="parallax"]').parallax('animate');
    });

})(window.jQuery);
 /* ============================================================
  * Pages Sidebar
  * ============================================================ */

 (function($) {
     'use strict';
     // SIDEBAR CLASS DEFINITION
     // ======================

     var Sidebar = function(element, options) {
         this.$element = $(element);
         this.options = $.extend(true, {}, $.fn.sidebar.defaults, options);

         this.bezierEasing = [.05, .74, .27, .99];
         this.cssAnimation = true;
         this.menuClosedCSS;
         this.menuOpenCSS;
         this.css3d = true;

         this.sideBarWidth = 280;
         this.sideBarWidthCondensed = 280 - 70;

         this.$sidebarMenu = this.$element.find('.sidebar-menu > ul');
         this.$pageContainer = $(this.options.pageContainer);
         this.$body = $('body');

         if (!this.$sidebarMenu.length) return;

         // apply perfectScrollbar plugin only for desktops
         ($.Pages.getUserAgent() == 'desktop') && this.$sidebarMenu.scrollbar({
             ignoreOverlay:false
         });


         if (!Modernizr.csstransitions)
             this.cssAnimation = false;
         if (!Modernizr.csstransforms3d)
             this.css3d = false;

         this.menuOpenCSS = (this.css3d == true ? 'translate3d(' + this.sideBarWidthCondensed + 'px, 0,0)' : 'translate(' + this.sideBarWidthCondensed + 'px, 0)');
         this.menuClosedCSS = (this.css3d == true ? 'translate3d(0, 0,0)' : 'translate(0, 0)');


         // Bind events
         // Toggle sub menus
         this.$sidebarMenu.find('li > a').on('click', function(e) {

             if ($(this).parent().children('.sub-menu') === false) {
                 return;
             }

             var parent = $(this).parent().parent();
             var tempElem = $(this).parent();

             parent.children('li.open').children('a').children('.arrow').removeClass('open');
             parent.children('li.open').children('a').children('.arrow').removeClass('active');
             parent.children('li.open').children('.sub-menu').slideUp(200, function() {

             });
             parent.children('li').removeClass('open');
             var sub = $(this).parent().children('.sub-menu');
             if (sub.is(":visible")) {
                 $('.arrow', $(this)).removeClass("open");

                 sub.slideUp(200, function() {
                     $(this).parent().removeClass("active");
                 });
             } else {
                 $('.arrow', $(this)).addClass("open");
                 $(this).parent().addClass("open");
                 sub.slideDown(200, function() {});
             }
             //e.preventDefault();
         });

         // Toggle sidebar
         $('.sidebar-slide-toggle').on('click touchend',function(e) {
            e.preventDefault();
             $(this).toggleClass('active');
             var el = $(this).attr('data-pages-toggle');
             if (el != null) {
                 $(el).toggleClass('show');
             }
         });

         var _this = this;

         function sidebarMouseEnter(e) {
             if ($.Pages.isVisibleSm() || $.Pages.isVisibleXs()) {
                 return false
             }
             if ($('.close-sidebar').data('clicked')) {
                 return;
             }
             if (_this.$body.hasClass('menu-pin'))
                 return;
             if (_this.cssAnimation) {
                 _this.$element.css({
                     'transform': _this.menuOpenCSS
                 });
                 _this.$body.addClass('sidebar-visible');
             } else {
                 _this.$element.stop().animate({
                     left: '0px'
                 }, 400, $.bez(_this.bezierEasing), function() {
                     _this.$body.addClass('sidebar-visible');
                 });
             }
         }

         function sidebarMouseLeave(e) {
             if ($.Pages.isVisibleSm() || $.Pages.isVisibleXs()) {
                 return false
             }
             if (typeof e != 'undefined') {
                 var target = $(e.target);
                 if (target.parent('.page-sidebar').length) {
                     return;
                 }
             }
             if (_this.$body.hasClass('menu-pin'))
                 return;

             if ($('.sidebar-overlay-slide').hasClass('show')) {
                 $('.sidebar-overlay-slide').removeClass('show')
                 $("[data-pages-toggle']").removeClass('active')

             }

             if (_this.cssAnimation) {
                 _this.$element.css({
                     'transform': _this.menuClosedCSS
                 });
                 _this.$body.removeClass('sidebar-visible');
             } else {

                 _this.$element.stop().animate({
                     left: '-' + _this.sideBarWidthCondensed + 'px'
                 }, 400, $.bez(_this.bezierEasing), function() {

                     _this.$body.removeClass('sidebar-visible')
                     setTimeout(function() {
                         $('.close-sidebar').data({
                             clicked: false
                         });
                     }, 100);
                 });
             }
         }


         this.$element.bind('mouseenter mouseleave', sidebarMouseEnter);
         this.$pageContainer.bind('mouseover', sidebarMouseLeave);

     }


     // Toggle sidebar for mobile view   
     Sidebar.prototype.toggleSidebar = function(toggle) {
         var timer;
         if (this.$body.hasClass('sidebar-open')) {
             this.$body.removeClass('sidebar-open');
             timer = setTimeout(function() {
                 this.$element.removeClass('visible');
             }.bind(this), 400);
         } else {
             clearTimeout(timer);
             this.$element.addClass('visible');
             setTimeout(function() {
                 this.$body.addClass('sidebar-open');
             }.bind(this), 10);

         }

     }

     Sidebar.prototype.togglePinSidebar = function(toggle) {
         if (toggle == 'hide') {
             this.$body.removeClass('menu-pin');
         } else if (toggle == 'show') {
             this.$body.addClass('menu-pin');
         } else {
             this.$body.toggleClass('menu-pin');
         }

     }


     // SIDEBAR PLUGIN DEFINITION
     // =======================
     function Plugin(option) {
         return this.each(function() {
             var $this = $(this);
             var data = $this.data('pg.sidebar');
             var options = typeof option == 'object' && option;

             if (!data) $this.data('pg.sidebar', (data = new Sidebar(this, options)));
             if (typeof option == 'string') data[option]();
         })
     }

     var old = $.fn.sidebar;

     $.fn.sidebar = Plugin;
     $.fn.sidebar.Constructor = Sidebar;


     $.fn.sidebar.defaults = {
         pageContainer: '.page-container'
     }

     // SIDEBAR PROGRESS NO CONFLICT
     // ====================

     $.fn.sidebar.noConflict = function() {
         $.fn.sidebar = old;
         return this;
     }

     // SIDEBAR PROGRESS DATA API
     //===================

     $(document).on('ready', function() {
         $('[data-pages="sidebar"]').each(function() {
             var $sidebar = $(this)
             $sidebar.sidebar($sidebar.data())
         })
     })

     $(document).on('click.pg.sidebar.data-api', '[data-toggle-pin="sidebar"]', function(e) {
         e.preventDefault();
         var $this = $(this);
         var $target = $('[data-pages="sidebar"]');
         $target.data('pg.sidebar').togglePinSidebar();
         return false;
     })
     $(document).on('click.pg.sidebar.data-api touchstart', '[data-toggle="sidebar"]', function(e) {
         e.preventDefault();
         var $this = $(this);
         var $target = $('[data-pages="sidebar"]');
         $target.data('pg.sidebar').toggleSidebar();
         return false
     })

 })(window.jQuery);
/* ============================================================
 * Pages Search overlay
 * ============================================================ */

(function($) {

    'use strict';

    // SEARCH CLASS DEFINITION
    // ======================

    var Search = function(element, options) {
        this.$element = $(element);
        this.options = $.extend(true, {}, $.fn.search.defaults, options);
        this.init();
    }
    Search.VERSION = "1.0.0";

    Search.prototype.init = function() {
        var _this = this;
        this.pressedKeys = [];
        this.ignoredKeys = [];

        //Cache elements
        this.$searchField = this.$element.find(this.options.searchField);
        this.$closeButton = this.$element.find(this.options.closeButton);
        this.$suggestions = this.$element.find(this.options.suggestions);
        this.$brand = this.$element.find(this.options.brand);

        this.$searchField.on('keyup', function(e) {
            _this.$suggestions.html($(this).val());
        });

        this.$searchField.on('keyup', function(e) {
            _this.options.onKeyEnter(_this.$searchField.val());
            if (e.keyCode == 13) { //Enter pressed
                e.preventDefault();
                _this.options.onSearchSubmit(_this.$searchField.val());
            }
            if ($('body').hasClass('overlay-disabled')) {
                return 0;
            }

        });

        this.$closeButton.on('click', function() {
            _this.toggleOverlay('hide');
        });

        this.$element.on('click', function(e) {
            if ($(e.target).data('pages') == 'search') {
                _this.toggleOverlay('hide');
            }
        });

        $(document).on('keypress.pg.search', function(e) {
            _this.keypress(e);
        });

        $(document).on('keyup', function(e) {
            // Dismiss overlay on ESC is pressed
            if (_this.$element.is(':visible') && e.keyCode == 27) {
                _this.toggleOverlay('hide');
            }
        });

    }


    Search.prototype.keypress = function(e) {

        e = e || event; // to deal with IE
        var nodeName = e.target.nodeName;
        if ($('body').hasClass('overlay-disabled') ||
            $(e.target).hasClass('js-input') ||
            nodeName == 'INPUT' ||
            nodeName == 'TEXTAREA') {
            return;
        }

        if (e.which !== 0 && e.charCode !== 0 && !e.ctrlKey && !e.metaKey && !e.altKey) {
            this.toggleOverlay('show', String.fromCharCode(e.keyCode | e.charCode));
        }
    }


    Search.prototype.toggleOverlay = function(action, key) {
        var _this = this;
        if (action == 'show') {
            this.$element.removeClass("hide");
            this.$element.fadeIn("fast");
            if(!this.$searchField.is(':focus')) { 
                this.$searchField.val(key);
                setTimeout(function(){
                    this.$searchField.focus();
                    var tmpStr = this.$searchField.val();
                        this.$searchField.val('');
                        this.$searchField.val(tmpStr);
                }.bind(this), 100);
            }

            this.$element.removeClass("closed");
            this.$brand.toggleClass('invisible');
            $(document).off('keypress.pg.search');
        } else {
            this.$element.fadeOut("fast").addClass("closed");
            this.$searchField.val('').blur();
            setTimeout(function() {
                if ((this.$element).is(':visible')) {
                    this.$brand.toggleClass('invisible');
                }
                $(document).on('keypress.pg.search', function(e) {
                    _this.keypress(e);
                });
            }.bind(this), 100);
        }
    };

    // SEARCH PLUGIN DEFINITION
    // =======================


    function Plugin(option) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('pg.search');
            var options = typeof option == 'object' && option;

            if (!data) {
                $this.data('pg.search', (data = new Search(this, options)));

            }
            if (typeof option == 'string') data[option]();
        })
    }

    var old = $.fn.search

    $.fn.search = Plugin
    $.fn.search.Constructor = Search

    $.fn.search.defaults = {
        searchField: '[data-search="searchField"]',
        closeButton: '[data-search="closeButton"]',
        suggestions: '[data-search="suggestions"]',
        brand: '[data-search="brand"]'
    }

    // SEARCH NO CONFLICT
    // ====================

    $.fn.search.noConflict = function() {
        $.fn.search = old;
        return this;
    }

    $(document).on('click.pg.search.data-api', '[data-toggle="search"]', function(e) {
        var $this = $(this);
        var $target = $('[data-pages="search"]');
        if ($this.is('a')) e.preventDefault();
        $target.data('pg.search').toggleOverlay('show');
    })


})(window.jQuery);
(function($) {
	'use strict';
	// Initialize layouts and plugins
    $.Pages.init();
})(window.jQuery);
/* ============================================================
 * Pages Social
 * ============================================================ */

(function($) {

    'use strict';

    // SOCIAL CLASS DEFINITION
    // ======================

    var Social = function(element, options) {
        this.$element = $(element);
        this.options = $.extend(true, {}, $.fn.social.defaults, options);
        this.$day =
            this.resizeTimeout =
            this.columns =
            this.colWidth = null;
        this.init();
    }
    Social.VERSION = "1.0.0";

    Social.prototype.init = function() {
        this.$cover = this.$element.find(this.options.cover);
        this.$day = this.$element.find(this.options.day);
        this.$item = this.$element.find(this.options.item);
        this.$status = this.$element.find(this.options.status);
        this.colWidth = this.options.colWidth;

        var _this = this;

        // TODO: transition disabled for mobile (animation starts after touch end)
    
        // Dependency: stepsForm 
        if (typeof stepsForm != 'undefined') {
            this.$status.length && new stepsForm(this.$status.get(0), {
                onSubmit: function(form) {
                    _this.$status.find('.status-form-inner').addClass('hide');
                    // form.submit()
                    // show success message
                    _this.$status.find('.final-message').html('<i class="fa fa-check-circle-o"></i> Status updated').addClass('show');
                }
            });


        }
        // Prevent 'vh' bug on iOS7
        if($.Pages.getUserAgent() == 'mobile'){
            //var wh = $(window).height();
            this.$cover.length && this.$cover.css('height', 400);
        }
       
        setTimeout(function() {
            this.$day.length && this.$day.isotope({
                "itemSelector": this.options.item,
                "masonry": {
                    "columnWidth": this.colWidth,
                    "gutter": 20,
                    "isFitWidth": true
                }
            });
        }.bind(this), 500);

    }

    // Set container width in order to align it horizontally. 

    Social.prototype.setContainerWidth = function() {
        var currentColumns = Math.floor(($('body').width() - 100) / this.colWidth);
        if (currentColumns !== this.columns) {
            // set new column count
            this.columns = currentColumns;
            // apply width to container manually, then trigger relayout
            this.$day.length && this.$day.width(this.columns * (this.colWidth + 20));
        }
    }


    // SOCIAL PLUGIN DEFINITION
    // =======================

    function Plugin(option) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('pg.social');
            var options = typeof option == 'object' && option;

            if (!data) $this.data('pg.social', (data = new Social(this, options)));
            if (typeof option == 'string') data[option]();
        })
    }

    var old = $.fn.social

    $.fn.social = Plugin
    $.fn.social.Constructor = Social

    $.fn.social.defaults = {
        cover: '[data-social="cover"]',
        day: '[data-social="day"]',
        status: '[data-social="status"]',
        item: '[data-social="item"]',
        colWidth: 300
    }

    // SOCIAL NO CONFLICT
    // ====================

    $.fn.social.noConflict = function() {
        $.fn.social = old;
        return this;
    }

    // SOCIAL DATA API
    //===================

    $(window).on('load', function() {
        $('[data-pages="social"]').each(function() {
            var $social = $(this);
            $social.social($social.data());

            setTimeout(function() {
                $social.find('[data-social="status"] li.current input').focus();
            }, 1000);

        })
    })

    $(window).on('resize', function() {
        $('[data-pages="social"]').each(function() {
            var $social = $(this);

            clearTimeout($social.data('pg.social').resizeTimeout);

            $social.data('pg.social').resizeTimeout = setTimeout(function() {
                // $social.data('pg.social').setContainerWidth();
                $social.data('pg.social').$day.isotope('layout');
            }, 300);

        });

    });


})(window.jQuery);