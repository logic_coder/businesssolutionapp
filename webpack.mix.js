// const { mix } = require('laravel-mix');
//
// /*
//  |--------------------------------------------------------------------------
//  | Mix Asset Management
//  |--------------------------------------------------------------------------
//  |
//  | Mix provides a clean, fluent API for defining some Webpack build steps
//  | for your Laravel application. By default, we are compiling the Sass
//  | file for the application as well as bundling up all the JS files.
//  |
//  */
//

// const mix = require('laravel-mix').mix;
const mix  = require('laravel-mix');

/*
 |-----------------------------------------------------------------------form_elements.js---
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

//mix.js('resources/assets/js/app.js', 'public/js').sourceMaps().version();
//mix.js('resources/assets/js/app.js', 'public/js').vue();
mix.js('resources/assets/js/app.js', 'public/js').vue()
.sourceMaps();


<!-- BEGIN jquery JS -->
mix.combine(
    [        
        'resources/assets/plugins/jquery/jquery-1.11.1.min.js',       
    ],  'public/theme/backend/assets/js/jquery-1.11.1.min.js'
).sourceMaps().version();
<!-- END jquery JS -->
<!-- BEGIN VENDOR JS -->
//'resources/assets/plugins/jquery/jquery-1.11.1.min.js', 
//'resources/assets/plugins/jquery/jquery-easy.js',

mix.combine(
    [
        'resources/assets/plugins/pace/pace.min.js',         
        'resources/assets/plugins/modernizr.custom.js',
        'resources/assets/plugins/jquery-ui/jquery-ui.min.js',
        'resources/assets/plugins/boostrapv3/js/bootstrap.min.js',
        'resources/assets/plugins/jquery/jquery-easy.js',
        
        'resources/assets/plugins/jquery-unveil/jquery.unveil.min.js',
        'resources/assets/plugins/jquery-bez/jquery.bez.min.js',
        'resources/assets/plugins/jquery-ios-list/jquery.ioslist.min.js',

        'resources/assets/plugins/jquery-actual/jquery.actual.min.js',
        'resources/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
        'resources/assets/plugins/bootstrap-select2/select2.min.js',
        'resources/assets/plugins/classie/classie.js',
        'resources/assets/plugins/switchery/js/switchery.min.js',
        'resources/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
        'resources/pages/js/pages.min.js',

        'resources/assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js',
        'resources/assets/plugins/jquery-validation/js/jquery.validate.min.js',
        'resources/assets/js/scripts.js',
    ],  'public/theme/backend/assets/js/plugins.js'
).sourceMaps().version();
<!-- END VENDOR JS -->

<!-- BEGIN Data-Tables JS -->
mix.combine(
    [
        'resources/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js',
        'resources/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js',
        'resources/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js',
        'resources/assets/plugins/datatables-responsive/js/datatables.responsive.js',
        'resources/assets/plugins/datatables-responsive/js/lodash.min.js',
        'resources/assets/js/datatables.js',
    ],  'public/theme/backend/assets/js/datatables.js'
).sourceMaps().version();

<!-- BEGIN Data-Tables JS -->

<!-- BEGIN CORE TEMPLATE JS -->
mix.combine(
     [
         'resources/pages/js/pages.calendar.js',
         'resources/pages/js/pages.email.js',
         'resources/pages/js/pages.js',
         'resources/pages/js/pages.social.js',
     ],  'public/theme/backend/assets/js/pages.js'
).version();
<!-- END CORE TEMPLATE JS -->

<!-- BEGIN PAGE LEVEL JS -->
mix.combine(
    [
        'resources/assets/js/dashboard.js',
        'resources/assets/js/form_layouts.js',
    ],  'public/theme/backend/assets/js/page-level.js'
).sourceMaps().version();
<!-- END PAGE LEVEL JS -->

<!-- BEGIN Gallery JS Image Load -->
mix.combine(
    [    
        'resources/assets/plugins/jquery-metrojs/MetroJs.min.js',
        'resources/assets/plugins/imagesloaded/imagesloaded.pkgd.min.js',
        'resources/assets/plugins/jquery-isotope/isotope.pkgd.min.js',
        'resources/assets/plugins/codrops-dialogFx/dialogFx.js',
        'resources/assets/plugins/owl-carousel/owl.carousel.min.js',
        'resources/assets/plugins/jquery-nouislider/jquery.nouislider.min.js',
        'resources/assets/plugins/jquery-nouislider/jquery.liblink.js',
    ],  'public/theme/backend/assets/js/gallery-image-load.js'
).sourceMaps().version();
<!-- END Gallery JS Image Load -->

<!-- BEGIN Gallery JS -->
mix.combine(
    [
        'resources/assets/js/gallery.js',
    ],  'public/theme/backend/assets/js/gallery.js'
).sourceMaps().version();
<!-- END Gallery JS -->

<!-- BEGIN Dyna-tree JS -->
mix.combine(
    [
        //'resources/assets/plugins/jquery-dynatree/jquery.dynatree.min.js',
        'resources/assets/js/libs/jquery.fancytree-all-deps.min.js',
    ],  'public/theme/backend/assets/js/fancytree.js'
).sourceMaps().version();
<!-- END Dyna-tree JS -->

<!-- BEGIN Thirt Party -->
mix.combine(
    [
        'resources/thirt-party/bootstrap-colorpicker/bootstrap-colorpicker.js',
    ],  'public/theme/backend/assets/js/thirt-party.js'
).sourceMaps().version();
<!-- END Thirt Party -->

<!-- BEGIN Custom JS For This Project -->
mix.combine(
    [
        'resources/customs/js/common.js',
    ],  'public/theme/backend/assets/js/customs.js'
).sourceMaps().version();
<!-- END Custom JS For This Project -->

<!-- BEGIN Multi-select JS -->
mix.combine(
    [
        'resources/customs/js/multiselect.js',
    ],  'public/theme/backend/assets/js/multiselect.js'
).sourceMaps().version();
<!-- END Multi-select JS -->

<!-- BEGIN Tree-category JS For This Project -->
mix.combine(
    [
        'resources/customs/js/tree_category_script.js',
    ],  'public/theme/backend/assets/js/tree_category_script.js'
).sourceMaps().version();
<!-- END Tree-category JS For This Project -->
<!-- BEGIN simple.gallery Js -->
mix.combine(
    [
       'resources/assets/plugins/fancybox-2.1.7/lib/simple-gallery.js',
    ], 'public/theme/backend/assets/js/simple-gallery.js'
).sourceMaps().version();
<!-- End simple.gallery Js -->
<!-- BEGIN Fancybox Light Box Js -->
mix.combine(
    [
        'resources/assets/plugins/fancybox-2.1.7/lib/jquery.mousewheel.pack.js',
        'resources/assets/plugins/fancybox-2.1.7/source/jquery.fancybox.pack.js',
        'resources/assets/plugins/fancybox-2.1.7/source/helpers/jquery.fancybox-buttons.js',
        'resources/assets/plugins/fancybox-2.1.7/source/helpers/jquery.fancybox-thumbs.js',
        'resources/assets/plugins/fancybox-2.1.7/source/helpers/jquery.fancybox-media.js',
    ],  'public/theme/backend/assets/js/fancybox-light-box.js'
).sourceMaps().version();
<!-- End Fancybox Light Box Js -->


<!-- BEGIN Fancybox Light Box CSS -->
mix.combine(
    [
        'resources/assets/plugins/fancybox-2.1.7/source/jquery.fancybox.css',
        'resources/assets/plugins/fancybox-2.1.7/source/helpers/jquery.fancybox-buttons.css',
        'resources/assets/plugins/fancybox-2.1.7/source/helpers/jquery.fancybox-thumbs.css',          
    ],  'public/theme/backend/assets/css/fancybox-light-box.css'
).sourceMaps().version();
<!-- BEGIN Fancybox Light Box CSS -->


<!-- BEGIN Plugins CSS -->
mix.combine(
    [
        'resources/assets/plugins/pace/pace-theme-flash.css', 
        'resources/assets/plugins/boostrapv3/css/bootstrap.min.css',       
        'resources/assets/plugins/font-awesome/css/font-awesome.css',
        'resources/assets/plugins/jquery-scrollbar/jquery.scrollbar.css',
        'resources/assets/plugins/bootstrap-select2/select2.css',
        'resources/assets/plugins/switchery/css/switchery.min.css',
        'resources/assets/plugins/nvd3/nv.d3.min.css',
        'resources/assets/plugins/mapplic/css/mapplic.css',
        'resources/assets/plugins/rickshaw/rickshaw.min.css',
        'resources/assets/plugins/bootstrap-datepicker/css/datepicker3.css',
        'resources/assets/plugins/jquery-metrojs/MetroJs.css',
        'resources/assets/plugins/bootstrap-tag/bootstrap-tagsinput.css',
        'resources/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css',
        'resources/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css',
        'resources/assets/plugins/datatables-responsive/css/datatables.responsive.css',        
    ],  'public/theme/backend/assets/css/plugins.css'
).sourceMaps().version();
<!-- END Plugins CSS -->

<!-- BEGIN Data-Tables CSS -->
mix.combine(
    [
        'resources/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css',
        'resources/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css',
        'resources/assets/plugins/datatables-responsive/css/datatables.responsive.css',
    ],  'public/theme/backend/assets/css/datatables.css'
).sourceMaps().version();
<!-- END Data-Tables CSS -->

<!-- BEGIN Gallery CSS -->
mix.combine(
    [
        'resources/assets/plugins/jquery-metrojs/MetroJs.css',
        'resources/assets/plugins/codrops-dialogFx/dialog.css',
        'resources/assets/plugins/codrops-dialogFx/dialog-sandra.css',
        'resources/assets/plugins/owl-carousel/assets/owl.carousel.css',
        'resources/assets/plugins/jquery-nouislider/jquery.nouislider.css',
    ],  'public/theme/backend/assets/css/gallery.css'
).sourceMaps().version();
<!-- END Gallery CSS -->

<!-- BEGIN Corporate CSS -->
mix.combine(
    [
        'resources/pages/css/themes/corporate.css',       
    ],  'public/theme/backend/assets/css/corporate.css'
).sourceMaps().version();
<!-- END Corporate CSS -->

mix.combine(
    [
        'resources/pages/css/pages-icons.css',
        'resources/pages/css/pages.css',
        'resources/assets/plugins/simple-line-icons/simple-line-icons.css',
    ],  'public/theme/backend/assets/css/pages.css'
).sourceMaps().version();

<!-- BEGIN Custom CSS For This Project -->
mix.combine(
    [
        'resources/customs/css/common.css',
        'resources/customs/css/overwrite.css',
        'resources/customs/css/mypos.css',
    ],  'public/theme/backend/assets/css/customs.css'
).sourceMaps().version();
<!-- END Custom CSS For This Project -->


<!-- BEGIN Dashboard CSS For This Project -->
mix.combine(
    [
        'resources/customs/css/dashboard.css'       
    ],  'public/theme/backend/assets/css/dashboard.css'
).sourceMaps().version();
<!-- END Dashboard CSS For This Project -->


<!-- BEGIN Custom CSS For This Project -->
mix.combine(
    [     
        'resources/customs/css/chart.css',
    ],  'public/theme/backend/assets/css/chart.css'
).sourceMaps().version();
<!-- END Custom CSS For This Project -->


<!-- BEGIN Dyna-tree CSS For This Project -->
mix.combine(
    [
        // 'resources/assets/plugins/jquery-dynatree/skin/ui.dynatree.css',
        'resources/assets/css/libs/ui.fancytree.min.css',
    ],  'public/theme/backend/assets/css/fancytree/fancytree.css'
).sourceMaps().version();
<!-- END Dyna-tree CSS For This Project -->

<!-- BEGIN LTE-IE CSS For This Project -->
mix.combine(
    [
        'resources/pages/css/ie9.css',
    ],  'public/theme/backend/assets/css/lte-ie9.css'
).sourceMaps().version();
<!-- END LTE-IE CSS For This Project -->

<!-- BEGIN LT-IE CSS For This Project -->
mix.combine(
    [
        'resources/assets/plugins/mapplic/css/mapplic-ie.css',
    ],  'public/theme/backend/assets/css/lt-ie9.css'
).sourceMaps().version();
<!-- END LT-IE CSS For This Project -->
// mix.browserSync();
