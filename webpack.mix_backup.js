7// const { mix } = require('laravel-mix');
//
// /*
//  |--------------------------------------------------------------------------
//  | Mix Asset Management
//  |--------------------------------------------------------------------------
//  |
//  | Mix provides a clean, fluent API for defining some Webpack build steps
//  | for your Laravel application. By default, we are compiling the Sass
//  | file for the application as well as bundling up all the JS files.
//  |
//  */
//


const mix = require('laravel-mix').mix;



/*
 |-----------------------------------------------------------------------form_elements.js---
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
// mix.sass(['resources/assets/sass/app.scss'], 'public/css');

// mix.js('resources/apps/js/app.js', 'public/theme/backend/assets/js/app.js').version();


<!-- BEGIN VENDOR JS -->
    mix.js(
        [
            'resources/assets/plugins/pace/pace.min.js',
            'resources/assets/plugins/jquery/jquery-1.11.1.min.js',
            'resources/assets/plugins/modernizr.custom.js',
            'resources/assets/plugins/jquery-ui/jquery-ui.min.js',
            'resources/assets/plugins/boostrapv3/js/bootstrap.min.js',
            'resources/assets/plugins/jquery/jquery-easy.js',
            'resources/assets/plugins/jquery-unveil/jquery.unveil.min.js',
            'resources/assets/plugins/jquery-bez/jquery.bez.min.js',
            'resources/assets/plugins/jquery-ios-list/jquery.ioslist.min.js',
            'resources/assets/plugins/jquery-actual/jquery.actual.min.js',
            'resources/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
            'resources/assets/plugins/bootstrap-select2/select2.min.js',
            'resources/assets/plugins/classie/classie.js',
            'resources/assets/plugins/switchery/js/switchery.min.js',
            'resources/assets/plugins/nvd3/lib/d3.v3.js',
            'resources/assets/plugins/nvd3/nv.d3.min.js',
            'resources/assets/plugins/nvd3/src/utils.js',
            'resources/assets/plugins/nvd3/src/tooltip.js',
            'resources/assets/plugins/nvd3/src/interactiveLayer.js',
            'resources/assets/plugins/nvd3/src/models/axis.js',
            'resources/assets/plugins/nvd3/src/models/line.js',
            'resources/assets/plugins/nvd3/src/models/lineWithFocusChart.js',
            'resources/assets/plugins/mapplic/js/hammer.js',
            'resources/assets/plugins/mapplic/js/jquery.mousewheel.js',
            'resources/assets/plugins/mapplic/js/mapplic.js',
            'resources/assets/plugins/rickshaw/rickshaw.min.js',
            'resources/assets/plugins/jquery-metrojs/MetroJs.min.js',
            'resources/assets/plugins/jquery-sparkline/jquery.sparkline.min.js',
            'resources/assets/plugins/skycons/skycons.js',
            'resources/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'resources/assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js',
            'resources/assets/plugins/jquery-validation/js/jquery.validate.min.js',
        ],  'public/theme/backend/assets/js/plugins.js'
    ).version();

<!-- END VENDOR JS -->

<!-- BEGIN CORE TEMPLATE JS -->
mix.js(
    [
        'resources/pages/js/pages.calendar.js',
        'resources/pages/js/pages.email.js',
        'resources/pages/js/pages.js',
        'resources/pages/js/pages.social.js',
    ],  'public/theme/backend/assets/js/pages.js'
).version();
<!-- END CORE TEMPLATE JS -->

<!-- BEGIN PAGE LEVEL JS -->
mix.js(
    [
        'resources/assets/js/dashboard.js',
        'resources/assets/js/scripts.js',
        'resources/assets/js/form_layouts.js',
    ],  'public/theme/backend/assets/js/page-level.js'
).version();
<!-- END PAGE LEVEL JS -->

<!-- BEGIN Thirt Party -->
mix.js(
    [
        'resources/thirt-party/bootstrap-colorpicker/bootstrap-colorpicker.js',
    ],  'public/theme/backend/assets/js/thirt-party.js'
).version();
<!-- END Thirt Party -->

<!-- BEGIN Custom JS For This Project -->
mix.js(
    [
        'resources/customs/js/common.js',
    ],  'public/theme/backend/assets/js/customs.js'
).version();

<!-- END Custom JS For This Project -->
mix.combine(
            [
                'resources/assets/plugins/pace/pace-theme-flash.css',
                'resources/assets/plugins/boostrapv3/css/bootstrap.min.css',
                'resources/assets/plugins/font-awesome/css/font-awesome.css',
                'resources/assets/plugins/jquery-scrollbar/jquery.scrollbar.css',
                'resources/assets/plugins/bootstrap-select2/select2.css',
                'resources/assets/plugins/switchery/css/switchery.min.css',
                'resources/assets/plugins/nvd3/nv.d3.min.css',
                'resources/assets/plugins/mapplic/css/mapplic.css',
                'resources/assets/plugins/rickshaw/rickshaw.min.css',
                'resources/assets/plugins/bootstrap-datepicker/css/datepicker3.css',
                'resources/assets/plugins/jquery-metrojs/MetroJs.css',
                'resources/assets/plugins/bootstrap-tag/bootstrap-tagsinput.css',
            ],  'public/theme/backend/assets/css/plugins.css'
        ).version();

mix.combine(
    [
        'resources/pages/css/pages-icons.css',
        'resources/pages/css/pages.css',
    ],  'public/theme/backend/assets/css/pages.css'
).version();

<!-- BEGIN Custom CSS For This Project -->
mix.combine(
    [
        'resources/customs/css/common.css',
    ],  'public/theme/backend/assets/css/common.css'
).version();
<!-- END Custom CSS For This Project -->

<!-- BEGIN Overwrite CSS For This Project -->
mix.combine(
    [
        'resources/customs/css/overwrite.css',
    ],  'public/theme/backend/assets/css/overwrite.css'
).version();
<!-- END Overwrite CSS For This Project -->

<!-- BEGIN LTE-IE CSS For This Project -->
mix.combine(
    [
        'resources/pages/css/ie9.css',
    ],  'public/theme/backend/assets/css/lte-ie9.css'
).version();
<!-- END LTE-IE CSS For This Project -->

<!-- BEGIN LT-IE CSS For This Project -->
mix.combine(
    [
        'resources/assets/plugins/mapplic/css/mapplic-ie.css',
    ],  'public/theme/backend/assets/css/lt-ie9.css'
).version();
<!-- END LT-IE CSS For This Project -->
// mix.browserSync();
