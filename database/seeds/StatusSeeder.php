<?php

use Illuminate\Database\Seeder;
use App\Status;


class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = array('Draft','Pending','Approved','Active');

        foreach($statuses as $key => $value ){
            $status_obj = new Status();
            $status_obj->name = $value;
            $status_obj->save();
        }
    }
}
