<?php

use Illuminate\Database\Seeder;

class WarehouseStoreOutTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['Store Supply','Corporate','Dealer','Fair/Temporary Sale','Non Brand Trading','Online Sales'];

        foreach($types as $type){
            $type_obj = new \App\WarehouseStoreOutType();

            $type_obj->name = $type;
            $type_obj->save();
        }
    }
}
