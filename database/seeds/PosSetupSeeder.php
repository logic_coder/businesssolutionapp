<?php

use App\Company;
use App\Vat;
use App\PaymentMethod;
use App\InvoicePrefix;
use App\User;
use App\UserTitle;
use App\UserDetail;
use App\Module;
use App\Permission;
use App\Role;
use App\UserRole;
use App\Category;
use App\Gender;
use App\CustomerOrderStatus;
use Illuminate\Database\Seeder;

class PosSetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Modules Info
         */
        $modules = [
            'Home' => ['access-dashboard' => 'Access Dashboard', 'access-my-profile' => 'Access My Profile'],

            'Sales( Cash Drawer )' => ['access-sales-page' => 'Access Sales Page(Can create and view sales)', 'edit-sales' => 'Edit Sales','delete-sales' => 'Delete Sales','access-discount-at-sales' => 'Access Discount at Sales',
                'remove-tax-in-sales' => 'Remove Tax in Sales','allow-refund' => 'Allow Refund'],

            'Customer' => ['create-view-customer' => 'Create / View Customer', 'edit-update-customer' => 'Edit / Update Customer',
                'delete-customer' => 'Delete Customer', 'activate-deactivate-customer' => 'Activate / Deactivate Customer',
                'create-view-customer-notes' => 'Create / View Customer Notes', 'edit-update-customer-notes' => 'Edit / Update Customer Notes',
                'delete-customer-notes' => 'Delete Customer Notes', 'edit-credit-balance-reward' => 'Edit credit/balance/reward',
                'access-purchase-history' => 'Access Purchase History'],

            'Sync' => ['sync-challan' => 'Sync Challan', 'sync-sales-report' => 'Sync Sales Report', 'sync-all' => 'Sync All( Challan and Sales Report )'],

            'Company Settings' => ['access-company-info' => 'Access Company info', 'access-company-currency-settings' => 'Access Company Currency Settings','access-global-vat-settings' => 'Access Global VAT Settings',
                'access-global-payment-method-settings' => 'Access Global Payment Method Settings', 'access-invoice-prefix-settings' => 'Access Invoice Prefix Settings','create-view-outlets' => 'Create / View Outlets',
                'edit-update-outlets' => 'Edit / Update Outlets', 'delete-outlets' => 'Delete Outlets', 'activate-deactivate-outlets' => 'Activate / Deactivate Outlets', 'access-coupon-settings' => 'Access Coupon Settings'
                ,'access-customer-order-statuses-settings' => 'Access Customer Order Statuses Settings'],

            'Users' => ['access-position-title' => 'Access Position / Title', 'add-edit-access-permission' => 'Add / Edit Access Permission( Create Permission Name Also)', 'create-view-users' => 'Create / View Users', 'edit-update-user' => 'Edit / Update User',
                'delete-user' => 'Delete User', 'activate-deactivate-user' => 'Activate / Deactivate User'],

            'Catalog' => ['access-category' => 'Access Category', 'create-view-products' => 'Create / View Products',
                'edit-update-products' => 'Edit / Update Products', 'delete-products' => 'Delete Products','access-product-prices' => 'Access Product Prices','access-product-attributes' =>'Access Product Attributes',
                'access-product-combination' => 'Access Product Combination'],

            'Inventory' => ['access-warehouse' => 'Access Warehouse','access-vendor-supplier' => 'Access Vendor / Supplier', 'access-warehouse-store-in-challan' => 'Access Warehouse Store-in Challan', 'access-warehouse-store-out-challan' => 'Access Warehouse Store-out Challan', 'access-outlet-store-in-challan' => 'Access Outlet Store-in Challan'],
             'Printer' => ['access-company-printer-settings' => 'Access Company Printer Settings', 'access-outlet-printer-settings' => 'Access Outlet Printer Settings'],

        ];

        /**
         * Company Info
         */
        /*$companyObj = new Company();
        $companyObj->country_id = '23'; // Bangladesh
        $companyObj->currency_id = '9'; // Bangladeshi Taka ( BDT )
        $companyObj->time_zone_id = '327'; // Asia/Dhaka( (UTC+06:00) Dhaka )
        $companyObj->business_category_id = '1';
        $companyObj->name = 'Shah Alam Jewellers';
        $companyObj->phone = '01729010683';
        $companyObj->email = 'admin@sajewellers.com';
        $companyObj->save();*/

        /**
         * Vat
         */
        /*$vatObj = new Vat();
        $vatObj->company_id = 1;
        $vatObj->name = 'General';
        $vatObj->percent = '12';
        $vatObj->save();*/

        /**
         * Payment Method
         */
        /*$paymentMethodObj = new PaymentMethod();
        $paymentMethodObj->company_id = 1;
        $paymentMethodObj->name = 'Cash';
        $paymentMethodObj->save();*/

        /**
         * Invoice Prefix
         */
        /*$invoicePrefixObj = new InvoicePrefix();
        $invoicePrefixObj->company_id = 1;
        $invoicePrefixObj->name = 'Gen';
        $invoicePrefixObj->save();*/

        /**
         * User
         */
        /*$userObj = new User();
        $userObj->company_id = '1';
        $userObj->login_email_or_mobile = 'admin@sajewellers.com';
        $userObj->email = 'admin@sajewellers.com';
        $userObj->password = '$2y$10$wtUlc2Rx5j3z8/e5moNOVu/gDjNxCtTvXchUD3d4fY9KF3E9PSlAC'; // pass = 123456
        $userObj->is_pos_admin = '1';
        $userObj->save();*/

        /**
         * User Title
         */
        /*$userTitleObj = new UserTitle();
        $userTitleObj->company_id = '1';
        $userTitleObj->title = 'Sales Executive';
        $userTitleObj->save();*/

        /**
         * Gender List
         */
        $gender_list = ['Male', 'Female','Other'];
        foreach( $gender_list as $key => $value ){
            $gender_listObj = new Gender();
            $gender_listObj->name = $value;
            $gender_listObj->save();
        }
        /**
         * UserDetail
         */
        /*$userDetailObj = new UserDetail();
        $userDetailObj->user_id = '1';
        $userDetailObj->country_id = '23';
        $userDetailObj->first_name = 'Abu';
        $userDetailObj->last_name = 'Taher';
        $userDetailObj->save();*/

        /**
         * Role
         */
        /*$role = new Role();
        $role->company_id = 1;
        $role->name = 'Company Administrator( Read Only )';
        $role->slug = 'administrator';
        $role->save();
        $role->users()->attach(1);*/

        /**
         * Modules Info Insert
         */
        $sequence = 1;
        $module_id = 1;
        foreach( $modules as $key => $module_access ){
            $modulesObj = new Module();
            $modulesObj->name = $key;
            $modulesObj->sequence = $sequence;
            $modulesObj->save();

            foreach( $module_access as $key => $value ){
                $permision = new Permission();
                $permision->module_id = $module_id;
                $permision->slug = $key;
                $permision->name = $value;
                $permision->save();
                //$role->permissions()->attach($permision);
            }
            $sequence += 1;
            $module_id += 1;
        }
        /**
         * Category
         */
        /*$categoryObj = new Category();
        $categoryObj->company_id = '1';
        $categoryObj->parent_id = '0';
        $categoryObj->name = 'Home';
        $categoryObj->is_shop_default = '1';
        $categoryObj->level_depth = '0';
        $categoryObj->nleft = '0';
        $categoryObj->nright = '0';
        $categoryObj->active = '1';
        $categoryObj->position = '1';
        $categoryObj->is_root_category = '1';
        $categoryObj->save();*/

        /**
         * CustomerOrderStatus
         */
        /*$customer_order_status_list = ['Not Assigned' => 'not-assigned','Karigor' => 'karigor', 'Chila Polish' => 'chila-polish', 'Show Room' => 'show-room','Halmark' => 'halmark', 'Delivered' => 'delivered' ];
        foreach( $customer_order_status_list as $name => $slug ){
            $customerOrderStatusObj = new CustomerOrderStatus();
            $customerOrderStatusObj->company_id = '1';
            $customerOrderStatusObj->name = $name;
            $customerOrderStatusObj->slug = $slug;
            $customerOrderStatusObj->save();
        }*/
    }
}
