<?php

use Illuminate\Database\Seeder;
use App\OutLetType;

class OutletTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $outLetType = array(
            'Own', 'Franchise','Pop-up Store'
        );

        foreach( $outLetType as $key => $value ){
            $outLetTypeObj = new OutletType();
            $outLetTypeObj->name = $value;
            $outLetTypeObj->save();
        }
    }
}
