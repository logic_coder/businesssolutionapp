<?php

use App\BusinessCategory;
use Illuminate\Database\Seeder;

class BusinessCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $businessCategory = array(
            'Jewellers','Shop', 'Food', 'Restaurant','Leather Craft','Clothing'
        );

        foreach( $businessCategory as $key => $value ){
            $businessCategoryObj = new BusinessCategory();
            $businessCategoryObj->name = $value;
            $businessCategoryObj->save();
        }
    }
}
