<?php

use Illuminate\Database\Seeder;
use App\ScheduleType;

class ScheduleTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $scheduleType = array(
            'Regular', 'Vacation'
        );

        foreach( $scheduleType as $key => $value ){
            $scheduleTypeObj = new ScheduleType();
            $scheduleTypeObj->name = $value;
            $scheduleTypeObj->description = 'Here you can describe the schedule purpose';
            $scheduleTypeObj->save();
        }
    }
}
