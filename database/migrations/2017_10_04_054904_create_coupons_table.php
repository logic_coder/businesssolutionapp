<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->string('name',100);
            $table->integer('company_currency_id')->unsigned();
            $table->text('description')->nullable();
            $table->text('code');
            $table->boolean('high_light')->default(true)->comment('Default:True, Can set False as well.');
            $table->boolean('status')->default(false)->comment('Default:False, Can set True as well.');
            $table->integer('customer_id')->nullable()->comment('Limit to single customer.');
            $table->date('valid_from');
            $table->date('valid_to');
            $table->decimal('minimum_purchase_amount',10,2);
//            $table->integer('minimum_purchase_amount_currency_id')->comment('Minimum purchase amount currency');
            $table->string('vat_include_on_minimum_purchase_amount')->default('No')->comment('Default:No, Can set Yes as well.');
            $table->integer('total_coupons')->comment('How many times this coupon can use in total.');
            $table->integer('total_coupons_for_each_user')->comment('How many times this coupon can use for each user.');
            $table->decimal('discount_percent',10,2)->nullable();
            $table->string('exclude_discounted_products', 3)->default('Yes')->comment('Default:Yes, Can set No as well, it is applicable when discount as percentage %.');
            $table->decimal('discount_amount',10,2)->nullable();
//            $table->integer('discount_amount_currency_id')->nullable()->comment('Discount amount currency');
            $table->string('vat_include_on_discount_amount',3)->default('No')->nullable()->comment('Default:No, Can set Yes as well.');
            $table->string('no_discount',3)->default('Yes')->comment('Default:Yes, Can set No as well.');
            $table->string('apply_discount_option',7)->default('None')->comment('Default:None, Can set Percent/Amount as well.');
            $table->boolean('product_selection_button')->default(false)->comment('Default:false, Can set true as well.');
            $table->boolean('product_selection_checked')->default(false)->comment('Default:false, Can set true as well.');

            $table->softDeletes();
            $table->timestamps();
            $table->foreign('company_id')->references('id')->on('companies')->delete('cascade');
            $table->foreign('company_currency_id')->references('id')->on('company_currencies')->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
