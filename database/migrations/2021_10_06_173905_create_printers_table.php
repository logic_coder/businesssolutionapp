<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrintersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('printers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->text('name')->nullable();
            $table->string('model',250)->nullable();
            $table->string('type',250)->default('pos')->comment('default=pos,regular');
            $table->string('status',12)->default('in-active')->comment('active,in-active');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
        Schema::create('out_let_printer',function(Blueprint $table) {
            $table->integer('printer_id')->unsigned();
            $table->integer('out_let_id')->unsigned();
            $table->softDeletes();

            $table->foreign('printer_id')->references('id')->on('printers')->delete('cascade');
            $table->foreign('out_let_id')->references('id')->on('out_lets')->delete('cascade');
            $table->primary(['printer_id','out_let_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('printers');
        Schema::dropIfExists('out_let_printer');
    }
}
