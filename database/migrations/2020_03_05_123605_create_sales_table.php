<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('out_let_id')->unsigned()->comment('This Sales Created In Which Outlet');
            $table->integer('sales_by')->unsigned()->comment('Sales Person');
            $table->integer('customer_id')->unsigned()->nullable()->comment('Null value for guest');
            $table->text('order_no')->nullable()->comment('Formate: company_id-out_let_id-user_id-year+month+day+sale_id; This field will be filled by update');
            $table->date('sale_date')->comment('Current Date');
            $table->integer('invoice_prefix_id')->nullable();
            $table->text('invoice_no')->nullable()->comment('Formate: company_id+out_let_id+user_id+year+month+day+sale_id; This field will be filled by update; Sometimes Cash Memo No, Also can be custom invoice number');
            $table->integer('company_currency_id')->unsigned();
            $table->decimal('sub_total', 10, 2);
            $table->decimal('discount_amount', 10, 2)->nullable();
            $table->integer('coupon_id')->nullable();
            $table->decimal('coupon_discount_amount', 10, 2)->nullable();
            $table->decimal('total_amount_exclusive_vat', 10, 2);
            $table->decimal('vat_amount', 10, 2)->nullable();
            $table->decimal('net_amount', 10, 2);    
            $table->text('sales_note')->nullable(); 
            $table->string('status', 100)->default('sold')->comment('status=sold, status=hold-on');     
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('out_let_id')->references('id')->on('out_lets');
            $table->foreign('sales_by')->references('id')->on('users');
            $table->foreign('company_currency_id')->references('id')->on('company_currencies')->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('sales');
    }
}
