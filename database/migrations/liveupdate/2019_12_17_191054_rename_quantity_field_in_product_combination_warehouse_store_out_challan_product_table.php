<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameQuantityFieldInProductCombinationWarehouseStoreOutChallanProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_combination_warehouse_store_out_challan_product', function (Blueprint $table) {
            $table->renameColumn('quantity', 'requested_quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_combination_warehouse_store_out_challan_product', function (Blueprint $table) {
            //
        });
    }
}
