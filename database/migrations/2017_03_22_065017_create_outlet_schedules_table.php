<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutletSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outlet_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedule_type_id')->unsigned();
            $table->integer('out_let_id')->unsigned();
            $table->string('day', 10);
            $table->string('opening_hour', 10);
            $table->string('closing_hour', 10);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('schedule_type_id')->references('id')->on('schedule_types')->onDelete('cascade');
            $table->foreign('out_let_id')->references('id')->on('out_lets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outlet_schedules');
    }
}
