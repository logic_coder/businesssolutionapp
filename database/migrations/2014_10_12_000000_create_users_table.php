<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            //$table->string('login_email_or_mobile')->unique();
            $table->string('email',255)->unique()->comment('This will be used for Login');
            $table->string('contact_email',255)->nullable()->unique()->comment('This will be used for Email contact');
            $table->string('password',100);
            $table->boolean('is_pos_admin')->default('0');
            $table->boolean('active')->default('0');
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
