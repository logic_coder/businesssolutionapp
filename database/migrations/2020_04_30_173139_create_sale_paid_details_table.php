<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalePaidDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_paid_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sale_paid_id')->unsigned();
            $table->string('meta_data',255)->comment('If Cash payment then meta_data = Payment By');
            $table->string('value',255)->comment('If Cash payment then Value = Person who Paid');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('sale_paid_id','fk_sale_paid_id')->references('id')->on('sale_paids')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('sale_paid_details');
    }
}
