<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerOrderDetailPaidDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_order_detail_paid_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_order_detail_paid_id')->unsigned();
            $table->string('meta_data',255)->comment('If Cash payment then meta_data = Payment By');
            $table->string('value',255)->comment('If Cash payment then Value = Person who Paid');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('customer_order_detail_paid_id','fk_customer_order_detail_paid_id')->references('id')->on('customer_order_detail_paids')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('customer_order_detail_paid_details');
    }
}
