<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutLetProductProductCombination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('out_let_product_product_combination', function (Blueprint $table) {
            $table->increments('id');          
            $table->integer('out_let_product_id')->unsigned();
            $table->integer('product_combination_id')->unsigned()->nullable();
            $table->integer('stock_quantity');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('out_let_product_id','fk_out_let_product_id')->references('id')->on('out_let_products')->onDelete('cascade');
            $table->foreign('product_combination_id','fk_out_let_product_product_combination_id')->references('id')->on('product_combinations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('out_let_product_product_combination');
    }
}
