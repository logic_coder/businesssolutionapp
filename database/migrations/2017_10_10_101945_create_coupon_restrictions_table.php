<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponRestrictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_restrictions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_id')->unsigned();
            $table->integer('number_of_products_required_in_carts')->comment('To enjoy this coupon minimum number of products should be in carts.');
            $table->boolean('rule_concerning_products')->default(false)->comment('Default:false, Can set true as well.');
            $table->boolean('rule_concerning_categories')->default(false)->comment('Default:false, Can set true as well.');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('coupon_id')->references('id')->on('coupons');
//            ->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_restrictions');
    }
}
