<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseStoreOutChallanProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_store_out_challan_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_store_out_challan_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('warehouse_store_out_challan_id','fk_warehouse_store_out_challan_id')->references('id')->on('warehouse_store_out_challans')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_store_out_challan_products');
    }
}
