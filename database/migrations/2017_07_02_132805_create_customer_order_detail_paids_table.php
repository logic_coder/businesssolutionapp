<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerOrderDetailPaidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_order_detail_paids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_order_detail_id')->unsigned();
            $table->integer('payment_method_id')->unsigned();
            $table->decimal('paid_amount',10,2);
            $table->date('paid_date');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('customer_order_detail_id')->references('id')->on('customer_order_details')->onDelete('cascade');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('customer_order_detail_paids');
    }
}
