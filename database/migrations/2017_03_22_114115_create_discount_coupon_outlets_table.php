<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountCouponOutletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_coupon_outlets', function (Blueprint $table) {
            $table->integer('discount_coupon_id')->unsigned();
            $table->integer('out_let_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('discount_coupon_id')->references('id')->on('discount_coupons')->delete('cascade');
            $table->foreign('out_let_id')->references('id')->on('out_lets')->delete('cascade');
            $table->primary(['discount_coupon_id','out_let_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_coupon_outlets');
    }
}
