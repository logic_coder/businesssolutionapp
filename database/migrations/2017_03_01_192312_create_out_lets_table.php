<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutLetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('out_lets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned()->default('23');
            $table->integer('company_id')->unsigned();
            $table->string('name', 200);
            $table->integer('outlet_type_id')->unsigned();
            $table->decimal('space', 10,2);
            $table->decimal('duration', 10,2);
            $table->decimal('capacity', 10,2);
            $table->decimal('display_products', 10,2);
            $table->decimal('advance_amount', 10,2)->nullable();
            $table->decimal('monthly_rent', 10,2);
            $table->string('trade_license_no', 100);
            $table->text('trade_license_photo')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->text('location');
            $table->string('phone',20);
            $table->text('address');
            $table->string('city',100);
            $table->string('zip',10);
            $table->string('responsible_person',150);
            $table->text('notes')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('outlet_type_id')->references('id')->on('outlet_types')->onDelete('cascade');
        });
        Schema::create('out_let_user',function(Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('out_let_id')->unsigned();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->delete('cascade');
            $table->foreign('out_let_id')->references('id')->on('out_lets')->delete('cascade');
            $table->primary(['user_id','out_let_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('out_lets');
        Schema::dropIfExists('out_let_user');
    }
}


