<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('time_zone_id')->unsigned()->nullable();
            $table->integer('business_category_id')->unsigned();
            $table->integer('currency_id')->unsigned()->nullable();
            $table->string('name', 200);
            $table->string('phone', 50)->nullable();
            $table->string('email', 100)->nullable();
            $table->text('address')->nullable();
            $table->string('city', 100)->nullable();
            $table->string('zip', 10)->nullable();
            $table->text('receipt_address')->nullable();
            $table->text('receipt_text')->nullable();
            $table->text('signature_line_text')->nullable();
            $table->binary('receipt_logo')->nullable();
            $table->boolean('active')->default('0');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('time_zone_id')->references('id')->on('time_zones');
            $table->foreign('business_category_id')->references('id')->on('business_categories');
            $table->foreign('currency_id')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('companies');
    }
}
