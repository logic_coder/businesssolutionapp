<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDiscountOutletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_discount_outlets', function(Blueprint $table) {
            $table->integer('product_discount_id')->unsigned();
            $table->integer('out_let_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('product_discount_id')->references('id')->on('product_discounts')->delete('cascade');
            $table->foreign('out_let_id')->references('id')->on('out_lets')->delete('cascade');
            $table->primary(['product_discount_id','out_let_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_discount_outlets');
    }
}
