<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutLetProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('out_let_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('out_let_id')->unsigned();
            $table->integer('product_id')->unsigned();            
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('out_let_id')->references('id')->on('out_lets')->delete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->delete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('out_let_products');
    }
}
