<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('out_let_id')->unsigned()->comment('Which outlet created this customer');
            $table->integer('gender_id')->unsigned()->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->text('customer_no')->comment('Customer_no =  Current Year+This user id');
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->string('cell_phone', 30)->nullable();
            $table->string('work_phone', 30)->nullable();
            $table->string('email', 100)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->text('address')->nullable();
            $table->text('address2')->nullable();
            $table->string('city', 100)->nullable();
            $table->string('zip', 12)->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('out_let_id')->references('id')->on('out_lets');
            $table->foreign('gender_id')->references('id')->on('genders');
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
