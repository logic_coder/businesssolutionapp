<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutLetProductSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('out_let_product_sale', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sale_id')->unsigned();
            $table->integer('out_let_product_id')->unsigned();
            $table->text('name');
            $table->decimal('cost_price',10,2); 
            $table->decimal('unit_price',10,2);
            $table->integer('quantity');           
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('out_let_product_id')->references('id')->on('out_let_products');
            $table->foreign('sale_id')->references('id')->on('sales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('out_let_product_sale');
    }
}
