<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponRestrictionProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_restriction_product', function (Blueprint $table) {
            $table->integer('coupon_restriction_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('coupon_restriction_id','fk_coupon_restriction_id')->references('id')->on('coupon_restrictions')->onDelete('cascade');
            $table->foreign('product_id','coupon_restriction_product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_restriction_product');
    }
}
