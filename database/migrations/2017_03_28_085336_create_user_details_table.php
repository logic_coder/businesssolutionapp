<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('country_id')->unsigned()->default('23');
            $table->integer('user_title_id')->unsigned()->nullable();
            $table->text('photo')->nullable();
            $table->string('first_name', 100)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->string('email')->unique()->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('user_code',100)->nullable();
            $table->string('cell_phone',50)->nullable();
            $table->string('work_phone',50)->nullable();
            $table->text('address')->nullable();
            $table->text('address2')->nullable();
            $table->string('city', 100)->nullable();
            $table->string('zip', 12)->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->delete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')->delete('cascade');
            $table->foreign('user_title_id')->references('id')->on('user_titles')->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
