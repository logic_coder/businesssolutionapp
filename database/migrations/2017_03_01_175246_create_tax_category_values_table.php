<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxCategoryValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_category_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tax_category_id')->unsigned();
            $table->string('name',150);
            $table->decimal('percent',10,2);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('tax_category_id')->references('id')->on('tax_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_category_values');
    }
}
