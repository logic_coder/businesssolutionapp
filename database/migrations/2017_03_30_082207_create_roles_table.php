<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles',function(Blueprint $table){
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->string('name',255);
            $table->string('slug',300);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('company_id')->references('id')->on('companies')->delete('cascade');
        });

        Schema::create('permissions',function(Blueprint $table){
            $table->increments('id');
            $table->integer('module_id')->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('module_id')->references('id')->on('modules')->delete('cascade');
        });

        Schema::create('permission_role',function(Blueprint $table) {
            $table->integer('role_id')->unsigned();
            $table->integer('permission_id')->unsigned();
            $table->softDeletes();
            $table->foreign('permission_id')->references('id')->on('permissions')->delete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->delete('cascade');
            $table->primary(['permission_id','role_id']);
        });

        Schema::create('role_user',function(Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->softDeletes();
            $table->foreign('role_id')->references('id')->on('roles')->delete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->delete('cascade');
            $table->primary(['role_id','user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('role_user');
        Schema::dropIfExists('permission_role');
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('roles');
    }
}
