<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCombinationAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_combination_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_combination_id')->unsigned();
            $table->integer('attribute_value_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('product_combination_id')->references('id')->on('product_combinations')->onDelete('cascade');
            $table->foreign('attribute_value_id')->references('id')->on('attribute_values')->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_combination_attributes');
    }
}
