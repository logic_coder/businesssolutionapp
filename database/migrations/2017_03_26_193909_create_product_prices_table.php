<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('vat_id')->unsigned();
            $table->decimal('price', 10,2)->comment('Selling Price');
            $table->decimal('cost', 10,2)->comment('Production Cost');
            $table->date('cost_price_date');
            $table->string('active', 8)->default('Inactive')->comment('1= Active; 0=Inactive');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products')->delete('cascade');
            $table->foreign('vat_id')->references('id')->on('vats')->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
    }
}
