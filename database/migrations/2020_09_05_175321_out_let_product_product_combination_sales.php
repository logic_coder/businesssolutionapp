<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OutLetProductProductCombinationSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('out_let_product_product_combination_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sale_id')->unsigned();
            $table->integer('out_let_product_product_combination_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->text('name');
            $table->decimal('cost_price',10,2); 
            $table->decimal('unit_price',10,2);
            $table->integer('quantity');           
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('out_let_product_product_combination_id','fk_out_let_product_product_combination')->references('id')->on('out_let_product_product_combination')->onDelete('cascade');
            $table->foreign('sale_id')->references('id')->on('sales');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('out_let_product_product_combination_sales');
    }
}
