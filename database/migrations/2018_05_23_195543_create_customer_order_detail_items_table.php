<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerOrderDetailItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_order_detail_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_order_detail_id')->unsigned();
            $table->text('description');
            $table->integer('product_id')->nullable();
            $table->integer('category_id')->unsigned();
            $table->bigInteger('quantity');
            $table->decimal('unit_price', 10, 2)->nullable()->comment('Per Bhori/Tola');
            $table->bigInteger('weight_tola')->nullable();
            $table->bigInteger('weight_ana')->nullable();
            $table->bigInteger('weight_rotti')->nullable();
            $table->bigInteger('weight_milli')->nullable();
            $table->decimal('weight_gram', 10, 2)->nullable();
            $table->decimal('material_price', 10, 2)->nullable();
            $table->decimal('making_charge', 10, 2)->nullable();
            $table->decimal('price_exclusive_vat', 10, 2);
            $table->integer('vat_id')->nullable();
            $table->decimal('vat_amount', 10, 2)->nullable();
            $table->decimal('price_inclusive_vat', 10, 2);
            $table->decimal('discount_amount', 10, 2)->nullable();
            $table->string('status', 20)->default('not-assigned');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('customer_order_detail_id')->references('id')->on('customer_order_details');
            $table->foreign('category_id')->references('id')->on('categories')->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_order_detail_items');
    }
}
