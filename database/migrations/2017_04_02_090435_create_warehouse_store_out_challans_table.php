<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseStoreOutChallansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_store_out_challans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('warehouse_id')->unsigned();
            $table->integer('out_let_id')->unsigned();
            $table->integer('warehouse_store_out_type_id')->unsigned()->nullable();
            $table->string('challan_no', 32)->unique()->nullable();
            $table->string('status', 16);
            $table->date('request_date');
            $table->integer('created_by')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('warehouse_id')->references('id')->on('warehouses')->onDelete('cascade');
            $table->foreign('out_let_id')->references('id')->on('out_lets')->onDelete('cascade');
            $table->foreign('warehouse_store_out_type_id')->references('id')->on('warehouse_store_out_types')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_store_out_challans');
    }
}
