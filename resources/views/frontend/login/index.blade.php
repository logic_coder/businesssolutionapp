@extends('layouts.app')
@section('content')
    <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
            <img src="{{ asset('images/logo.png') }}" alt="logo" data-src="{{ asset('images/logo.png') }}" width="78" height="22">
            <div class="tab-pane padding-20 active slide-left" id="app">
                <frontend-login></frontend-login>
            </div>
            <!--END Login Form-->
            <div class="pull-bottom sm-pull-bottom">
                <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
                    <div class="col-sm-3 col-md-2 no-padding">
                        <img alt="" class="m-t-5" data-src="{{ asset('images/pages_icon.png') }}" data-src-retina="{{ asset('images/pages_icon.png') }}" height="60" src="{{ asset('images/pages_icon.png') }}" width="60">
                    </div>
                    <div class="col-sm-9 no-padding m-t-10">
                        <p><small>
                            Create a pages account. If you have a facebook account, log into it for this process. Sign in with <a href="#" class="text-info">Facebook</a> or <a href="#" class="text-info">Google</a></small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-script')
     <!--<script src="{{mix('/js/app.js')}}" /> -->
    <script defer src="{{ mix('js/app.js') }}"></script> 
@endsection