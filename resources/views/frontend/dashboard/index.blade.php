@extends('layouts.app_master')
@section('content')
<div id="app">
    <frontend-dash-board out_let_id="{{ $_COOKIE['out_let_id'] }}"></frontend-dash-board>   
</div>
@endsection
@section('page-script')
 <!-- Javascript Files: Vendors -->
  <!-- <script src="https://unpkg.com/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script> -->
  <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
  <script src="https://cdn.jsdelivr.net/npm/jsvectormap"></script>
  <script src="https://cdn.jsdelivr.net/npm/jsvectormap/dist/maps/world-merc.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/fullcalendar@6.1.9/index.global.min.js"></script> 
  <script src="https://cdn.jsdelivr.net/npm/@fullcalendar/bootstrap5@6.1.9/index.global.min.js"></script>

  <!-- Javascript Files: Controllers -->
  <script src="{{ asset('resources/chart_assets/controller/console-bsb.js') }}"></script>
  <!-- <script src="{{ asset('resources/chart_assets/controller/chart-1.js') }}"></script>  -->  
  <!-- <script src="{{ asset('resources/chart_assets/controller/chart-3.js') }}"></script> -->
  <!-- <script src="{{ asset('resources/chart_assets/controller/chasrt-4.js') }}"></script> -->
  <script src="{{ asset('resources/chart_assets/controller/map-2.js') }}"></script>
  <script src="{{ asset('resources/chart_assets/controller/calendar-1.js') }}"></script>
  
  <script src="{{mix('/js/app.js')}}" />  

   <!-- <script src="{{mix('js/app.js')}}" defer/>   -->  
@endsection