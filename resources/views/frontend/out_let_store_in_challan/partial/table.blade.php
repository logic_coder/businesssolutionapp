<button class="btn delete-multi-items" action-url="warehouse_store_in_challan/remove_warehouse_store_in_challan" redirect-url="/warehouse_store_in_challan"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Challan No</th>
        <th style="width:20%">To Warehouse</th>
        <th style="width:20%">Supplier</th>
        <th style="width:20%">Product</th>
        <th style="width:20%">Created By</th>
        <th style="width:20%">Request Date</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $store_in_challans as $store_in_challan )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input class="item_ids" type="checkbox" name="item_ids[]" value={{$store_in_challan->id}} data-id="checkbox" id="checkbox{{$store_in_challan->id}}">
                    <label for="checkbox{{$store_in_challan->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                {{ $store_in_challan->challan_no }}
            </td>
            <td class="v-align-middle">
                {{ App\Warehouse::find($store_in_challan->warehouse_id)->name }}
            </td>
            <td class="v-align-middle">
                @if($store_in_challan->supplier_id)
                    {{ App\Supplier::find($store_in_challan->supplier_id)->name }}
                @endif
            </td>
            <td class="v-align-middle">
                <ul>
                    @foreach( $store_in_challan->challanProducts as $challan_product )
                        <li>{{ App\Product::find($challan_product->product_id)->name }} </li>
                    @endforeach
                </ul>
            </td>
            <td class="v-align-middle">
                @php
                    $user = App\User::with('userDetail')->where('id', $store_in_challan->created_by )->get();
                    if($user[0]->userDetail){
                        echo $user[0]->userDetail->first_name ." ". $user[0]->userDetail->last_name;
                    }
                @endphp
            </td>
            <td class="v-align-middle">
                {{ $store_in_challan->request_date }}
            </td>
            <td class="v-align-middle">
                {{ $store_in_challan->status }}
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn btn-success" href="{{url('warehouse_store_in_challan/'.$store_in_challan->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$store_in_challan->id}}" type="button" class="btn btn-danger delete-single-item" action-url="warehouse_store_in_challan/remove_warehouse_store_in_challan" redirect-url="/warehouse_store_in_challan"><i class="fa fa-trash-o"></i></button>
                    <a title="Receive" class="btn btn-success" href="{{url('warehouse_store_in_challan/'.$store_in_challan->id.'/receive')}}">Receive</a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>