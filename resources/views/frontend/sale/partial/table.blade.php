<button class="btn delete-multi-items-api" action-url="api/sale_detail/remove_sale" redirect-url="{{ $redirect_url }}"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:12%">Invoice No</th>
        <th style="width:12%">Outlet</th>
        <th style="width:12%">Customer</th>
        <th style="width:12%">Sales No.</th>
        <th style="width:12%">Sales By</th>
        <th style="width:12%">Sales Date</th>
        <th style="width:12%">Status</th>
        <th style="width:12%">Action</th>
    </tr>
    </thead> 
    <tbody>
    @if( $saleDetails )
        @foreach( $saleDetails as $saleDetail )
            <tr id="row_{{$saleDetail->id}}">
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input class="item_ids" type="checkbox" name="item_ids[]" value={{$saleDetail->id}} data-id="checkbox" id="checkbox{{$saleDetail->id}}">
                        <label for="checkbox{{$saleDetail->id}}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    @if($saleDetail->invoice_prefix_id)
                        @php
                            $invoice_prefix = App\InvoicePrefix::find($saleDetail->invoice_prefix_id);
                            $invoice_prefix_name_and_no = $invoice_prefix ? $invoice_prefix->name.$saleDetail->invoice_no : $saleDetail->invoice_no;
                        @endphp                        
                        {{ $invoice_prefix_name_and_no }}  
                    @else
                        {{ $saleDetail->invoice_no }}  
                    @endif                     
                </td>
                <td class="v-align-middle">
                    @if(isset($saleDetail->out_let_id))
                    {{ App\OutLet::find($saleDetail->out_let_id)->name }}
                     @else
                        N/A
                    @endif
                </td>
                <td class="v-align-middle">
                    @if(isset($saleDetail->customer_id))
                        @php
                            $customer_detail = "Guest";
                            $customer_data = App\Customer::find($saleDetail->customer_id);
                            if($customer_data){
                                $customer_detail = $customer_data->first_name." ".$customer_data->last_name." (".$customer_data->customer_no. " )"; 
                            }
                        @endphp  
                        {{ $customer_detail }}
                    @else
                        Guest
                    @endif
                </td>
                <td class="v-align-middle">
                    {{ $saleDetail->order_no }}
                </td>
                <td class="v-align-middle">
                    @if(isset($saleDetail->sales_by) && App\UserDetail::find($saleDetail->sales_by) )
                        {{ App\UserDetail::find($saleDetail->sales_by)->first_name }} 
                    @else
                        Not Available
                    @endif
                    @if(isset($saleDetail->sales_by) && App\UserDetail::find($saleDetail->sales_by))
                        {{ App\UserDetail::find($saleDetail->sales_by)->last_name }} 
                    @endif            
                </td>
                <td class="v-align-middle">
                     {{ $saleDetail->sale_date }}
                </td> 
                <td class="v-align-middle">
                     {{ $saleDetail->status }}
                </td>   
                <td class="v-align-middle">
                    <div class="btn-group btn-actions">                        
                        <a title="Edit" class="btn edit-button" href="{{url('frontend/sale/'.$saleDetail->id.'/edit')}}"><i class="fa fa-pencil"></i></a>                    
                        <button title="Delete" item-ids="{{$saleDetail->id}}" type="button" class="btn create-button delete-single-item-api" action-url="api/sale_detail/remove_sale" redirect-url="{{ $redirect_url }}" ><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>