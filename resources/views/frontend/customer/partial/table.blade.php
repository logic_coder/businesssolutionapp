<button class="btn delete-multi-items-api" action-url="api/customer/remove_customer" redirect-url="/frontend/customer"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Customer No.</th>
        <th style="width:20%">Mobile</th>
        <th style="width:20%">Work Phone</th>
        <th style="width:20%">Email</th>
        <th style="width:20%">Gender</th>
        <th style="width:20%">Address</th>
        <th style="width:20%">City</th>
        <th style="width:20%">Zip</th>
        <th style="width:20%">Address2</th>
        <th style="width:20%">Outlet</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @if( $customers )
        @foreach( $customers as $customer )
            <tr id="row_{{$customer->id}}">
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input class="item_ids" type="checkbox" name="item_ids[]" value={{$customer->id}} data-id="checkbox" id="checkbox{{$customer->id}}">
                        <label for="checkbox{{$customer->id}}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    {{ $customer ? $customer->first_name.' '. $customer->last_name : ''}}
                </td>
                <td class="v-align-middle">
                    {{ $customer->customer_no }}
                </td>
                <td class="v-align-middle">
                    {{ $customer->cell_phone }}
                </td>
                <td class="v-align-middle">
                    {{ $customer->work_phone }}
                </td>
                <td class="v-align-middle">
                    {{ $customer->email }}
                </td>
                <td class="v-align-middle">
                    @if($customer->gender_id)
                        @php
                            $gender = App\Gender::find($customer->gender_id);
                            $gender_name = $gender ? $gender->name : 'N/A';
                        @endphp
                        {{ $gender_name }}
                    @endif
                </td>
                <td class="v-align-middle">
                    {{ $customer->address }}
                </td>
                <td class="v-align-middle">
                    {{ $customer->city }}
                </td>
                <td class="v-align-middle">
                    {{ $customer->zip }}
                </td>
                <td class="v-align-middle">
                    {{ $customer->address2 }}
                </td>
                <td class="v-align-middle">
                    @if($customer->out_let_id)
                        @php
                            $outlet = App\OutLet::find($customer->out_let_id);
                            $outlet_name = $outlet ? $outlet->name : 'N/A';
                        @endphp
                        {{ $outlet_name }}
                     @endif
                </td>
                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <a title="Edit" class="btn edit-button" href="{{url('frontend/customer/'.$customer->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        <button title="Delete" item-ids="{{$customer->id}}" type="button" class="btn create-button delete-single-item-api" action-url="api/customer/remove_customer" redirect-url="/frontend/customer"><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>