<button class="btn delete-multi-items-api" action-url="api/customer_order_detail/remove_customer_order_detail" redirect-url="/frontend/customer_order_detail"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Invoice No</th>
        <th style="width:20%">Outlet</th>
        <th style="width:20%">Customer</th>
        <th style="width:20%">Order No.</th>
        <th style="width:20%">Order By</th>
        <th style="width:20%">Order Date</th>
        <th style="width:20%">Expected Delivery</th>
        <th style="width:20%">Delivered</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @if( $customerOrderDetails )
        @foreach( $customerOrderDetails as $orderDetails )
            <tr id="row_{{$orderDetails->id}}">
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input class="item_ids" type="checkbox" name="item_ids[]" value={{$orderDetails->id}} data-id="checkbox" id="checkbox{{$orderDetails->id}}">
                        <label for="checkbox{{$orderDetails->id}}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    @if($orderDetails->invoice_prefix_id)
                        @php
                            $invoice_prefix = App\InvoicePrefix::find($orderDetails->invoice_prefix_id);
                            $invoice_prefix_name_and_no = $invoice_prefix ? $invoice_prefix->name.$orderDetails->invoice_no : $orderDetails->invoice_no;
                        @endphp                        
                        {{ $invoice_prefix_name_and_no }} 
                    @else
                        {{$orderDetails->invoice_no}}
                    @endif
                    <br/>
                    @can('access-sales-page')
                            <a href="{{url('frontend/customer_order_detail/'.$orderDetails->id.'/get_paid_details')}}">Check Paid Details </a>
                    @endcan
                </td>
                <td class="v-align-middle">
                    {{ App\OutLet::find($orderDetails->out_let_id)->name}}
                </td>
                <td class="v-align-middle">  
                    @if(isset($orderDetails->customer_id))
                        @php
                            $customer_detail = "Guest";
                            $customer_data = App\Customer::find($orderDetails->customer_id);
                            if($customer_data){
                                $customer_detail = $customer_data->first_name." ".$customer_data->last_name." (".$customer_data->customer_no. " )"; 
                            }
                        @endphp  
                        {{ $customer_detail }}
                    @else
                        Guest
                    @endif
                </td>
                <td class="v-align-middle">
                    {{ $orderDetails->order_no }}
                </td>
                <td class="v-align-middle">
                     @if(isset($orderDetails->sales_by) && App\UserDetail::find($orderDetails->sales_by) )
                        {{ App\UserDetail::find($orderDetails->sales_by)->first_name }} 
                    @else
                        Not Available
                    @endif
                    @if(isset($orderDetails->sales_by) && App\UserDetail::find($orderDetails->sales_by))
                        {{ App\UserDetail::find($orderDetails->sales_by)->last_name }} 
                    @endif  
                </td>
                <td class="v-align-middle">
                    {{ $orderDetails->order_date }}
                </td>
                <td class="v-align-middle">
                    {{ $orderDetails->expected_delivery_date }}
                </td>
                <td class="v-align-middle">
                    {{ $orderDetails->delivered_date }}
                </td>
                <td class="v-align-middle">
                    {{ $orderDetails->status }}
                </td>
                <td class="v-align-middle">
                    <div class="btn-group btn-actions"> 
                        
                        <a title="Edit" class="btn edit-button" href="{{url('frontend/customer_order_detail/'.$orderDetails->id.'/edit')}}"><i class="fa fa-pencil"></i></a>                      
                        <button title="Delete" item-ids="{{$orderDetails->id}}" type="button" class="btn create-button delete-single-item-api" action-url="api/customer_order_detail/remove_customer_order_detail" redirect-url="/frontend/customer_order_detail"><i class="fa fa-trash-o"></i></button>                        
                    </div>
                    <div class="btn-actions set-permission">
                        <a title="Edit" class="btn product_add_btn btn-success" href="{{url('frontend/customer_order_detail/'.$orderDetails->id.'/get_paid_details')}}" style="margin-bottom: :5px;">Check Paid Details</a>
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

 