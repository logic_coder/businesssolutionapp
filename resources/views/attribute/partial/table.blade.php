<button class="btn delete-multi-items" action-url="attribute/remove_attribute" redirect-url="/attribute"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Values</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $attributes as $attribute )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input class="item_ids" type="checkbox" name="item_ids[]" value={{$attribute->id}} data-id="checkbox" id="checkbox{{$attribute->id}}">
                    <label for="checkbox{{$attribute->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <a href="{{url('attribute/'.$attribute->id.'/attribute_value')}}">{{ $attribute->name }} </a>
            </td>
            <td class="v-align-middle">
                {{ $attribute->attributeValues()->count() }}
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn edit-button" href="{{url('attribute/'.$attribute->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$attribute->id}}" type="button" class="btn create-button delete-single-item" action-url="attribute/remove_attribute" redirect-url="/attribute"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>