<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Outlet Info</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required form-group-default-selectFx">
                            {{ Form::label('outlettypes', 'Outlet Type') }}
                            {{Form::select('outlet_type_id',$outlet_types,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control','data-init-plugin' => 'cs-select']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('space', 'Space( Sqft )') }}
                            {{ Form::text('space',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('duration', 'Duration( Month )') }}
                            {{ Form::text('duration',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('start_date', 'Start Date') }}
                            {{ Form::date('start_date', isset($outlets) ? null: \Carbon\Carbon::now()->format('Y-m-d'), ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('capacity', 'Capacity') }}
                            {{ Form::text('capacity',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('display_products', 'Display Products') }}
                            {{ Form::text('display_products',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('advance_amount', 'Advance Amount( BDT )') }}
                            {{ Form::text('advance_amount',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('monthly_rent', 'Monthly Rent') }}
                            {{ Form::text('monthly_rent',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('trade_license_no', 'Trade License No') }}
                            {{ Form::text('trade_license_no',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default">
                            {{ Form::label('trade_license_photo', 'Trade License Photo') }}
                            {{ Form::file('trade_license_photo',null, ['class' => 'form-control']) }}
                        </div>

                    </div>
                    <br>
                </div>
            </div>
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('location', 'Location') }}
                            {{ Form::text('location',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('address', 'Address', ['class' => '']) }}
                            {{ Form::text('address',null, ['class' => 'form-control', 'placeholder' => 'Current address','required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('city', 'City', ['class' => '']) }}
                            {{ Form::text('city',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('zip', 'Zip/Postal Code', ['class' => '']) }}
                            {{ Form::text('zip',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('phone', 'Phone') }}
                            {{ Form::text('phone',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="wysiwyg5-wrapper b-a b-grey">
                            {{ Form::label('notes', 'Notes') }}
                            {{ Form::textarea('notes',null, ['class' => 'form-control wysiwyg demo-form-wysiwyg', 'placeholder' => 'Notes',]) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('responsible_person', 'Responsible Person', ['class' => '']) }}
                            {{ Form::text('responsible_person',null, ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <button class="btn create-button btn-cons pull-right" type="submit">
        <span>Save</span>
    </button>
</div>
