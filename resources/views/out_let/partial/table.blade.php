    <button class="btn delete-multi-items" action-url="out_let/remove_out_let" redirect-url="/out_let"><i class="pg-trash"></i></button>
    <table class="table table-hover" id="tableWithSearch">
        <thead>
            <tr>
                <th style="width:1%">
                    <div class="checkbox">
                        <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                        <label for="check_all"></label>
                    </div>
                </th>
                <th style="width:20%">Name</th>
                <th style="width:20%">Type</th>
                <th style="width:20%">Space</th>
                <th style="width:20%">Duration</th>
                <th style="width:20%">Expire Date</th>
                <th style="width:20%">Rent</th>
                <th style="width:20%">Trade License</th>
                <th style="width:20%">Address</th>
                <th style="width:20%">Phone</th>
                <th style="width:20%">Action</th>
            </tr>
        </thead>
        <tbody>

        @foreach( $outlets as $outlet )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="item_ids[]" value={{$outlet->id}} data-id="checkbox" id="checkbox{{$outlet->id}}">
                    <label for="checkbox{{$outlet->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->name}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{ App\OutLetType::find($outlet->outlet_type_id)->name}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->space}} Sqft</p>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->duration}} Month(s)</p>
            </td>
            <td class="v-align-middle">
                <p>{{ date('F d, Y', strtotime($outlet->end_date)) }}
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->monthly_rent}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->trade_license_no}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->address}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$outlet->phone}}</p>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn edit-button" href="{{url('out_let/'.$outlet->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$outlet->id}}" type="button" class="btn create-button delete-single-item" action-url="out_let/remove_out_let" redirect-url="/out_let"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
</table>
