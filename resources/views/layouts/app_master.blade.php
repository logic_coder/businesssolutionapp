<!DOCTYPE html>
<html>
<head>
    <!-- Scripts --> 
    <script src="{{ asset('js/app.js') }}" defer></script>   
    <!-- Styles 
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">  -->  
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Logic POS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
        
    @yield('head')
    <link href="{{mix('/theme/backend/assets/css/plugins.css')}}" type="text/css" rel="stylesheet"/>
    <link href="{{mix('/theme/backend/assets/css/pages.css')}}" type="text/css" rel="stylesheet"/>
    <script src="{{asset('resources/assets/plugins/jquery/jquery-1.11.1.min.js')}}" type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    @php
        $current_route = Route::currentRouteName();     
    @endphp
    <!-- Chart Google Fonts Files -->
  
  <!-- CSS Files -->
  
  <!-- <link href="{{mix('/theme/backend/assets/css/chart.css')}}" type="text/css" rel="stylesheet"/> 
  <link href="{{mix('/theme/backend/assets/css/customs.css')}}" type="text/css" rel="stylesheet"/> -->
  
    @if($current_route == "frontend_dashboard" || $current_route == "dashboard.index")
       <link rel="stylesheet" href="{{ asset('resources/chart_assets/css/console-bsb.css') }}"> 
       <link href="{{mix('/theme/backend/assets/css/dashboard.css')}}" type="text/css" rel="stylesheet"/> 
    @else
        <link href="{{mix('/theme/backend/assets/css/customs.css')}}" type="text/css" rel="stylesheet"/> 
    @endif

    <!--[if lte IE 9]>
    <link href="{{mix('/theme/backend/assets/css/lte-ie9.css')}}" type="text/css" rel="stylesheet"/>
    <![endif]-->
    <!--[if lt IE 9]>
    <link href="{{mix('/theme/backend/assets/css/lt-ie9.css')}}" type="text/css" rel="stylesheet"/>
    <![endif]-->

    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.0.0/dist/vue-multiselect.min.css">

    <!-- Scripts -->
    <script>
        window.Laravel = {
            csrfToken: '{{csrf_token()}}'
        }
    </script>


    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="public/css/windows.chrome.fix.css" />'
        }
    </script>
    <style>         
        footer {
            position: fixed; 
            left: 3%; 
            right: 0px;
            height: 50px; 
            top:20%;
            /** Extra personal styles **/            
            line-height: 35px;
        }
    </style>
    @yield('style')
</head>
<body class="fixed-header ">
<!-- BEGIN SIDEBPANEL-->
@php
    $is_pos_admin = "";
    $logged_user = auth()->user();
    if($logged_user){
        $is_pos_admin = $logged_user->is_pos_admin;
    }
@endphp
@if($is_pos_admin)
    @include('partial/sidebar')
@else
    @include('partial/app_sidebar')
@endif
<!-- END SIDEBAR -->
<!-- END SIDEBPANEL-->

<!-- START PAGE-CONTAINER -->
<div class="page-container">
    <!-- START HEADER -->
    <div class="header ">        
        <div class="pull-left text-center font-montserrat text-uppercase demo-fs-23 header-text-class" > 
            @php
            if(isset($_COOKIE['out_let_id'])){
                $out_let = \App\OutLet::find($_COOKIE['out_let_id']);
                if($out_let){
                    echo $out_let->name;
                }                 
            }
            elseif($company_details){
                echo $company_details->name;
            }
            @endphp            
        </div>  
        <div class="pull-right user-info" style="width:30%">
            <!-- START User Info -->            
            @if($is_pos_admin)
                @include('user/partial/user_info')
            @else
                @include('user/partial/app_user_info')
            @endif
            <!-- END User Info -->
        </div>
    </div>
    
    <!-- END HEADER -->
    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content">
            @yield('content')
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg footer">
            <div class="copyright sm-text-center">
                <p class="small no-margin pull-left sm-pull-reset">
                    <span class="hint-text">Copyright © 2018 </span>
                    <span class="font-montserrat">Logic POS</span>.
                    <span class="hint-text">All rights reserved. </span>
                    <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
                </p>
                <p class="small no-margin pull-right sm-pull-reset">
                    <a href="#">Hand-crafted</a> <span class="hint-text">&amp; Made with Love ®</span>
                </p>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- END COPYRIGHT -->
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN VENDOR JS -->

<script src="{{mix('/theme/backend/assets/js/plugins.js')}}" type="text/javascript"></script> 
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<!-- <script src="{{mix('/theme/backend/assets/js/pages.js')}}"></script> -->
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<!-- <script src="{{mix('/theme/backend/assets/js/page-level.js')}}" type="text/javascript"></script> -->
<!-- END PAGE LEVEL JS -->
<!-- BEGIN CUSTOMS JS -->
<script src="{{mix('/theme/backend/assets/js/customs.js')}}" type="text/javascript"></script>
<!-- END CUSTOMS JS -->
<!-- BEGIN THIRT-PART JS -->
<script src="{{mix('/theme/backend/assets/js/thirt-party.js')}}" type="text/javascript"> </script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<!-- SweetAlert JS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">       
    $(".swal-button").on('mouseenter', function() {
        $(this).css('background', 'green')
    })
    
    const token = localStorage.getItem("token");
    $("#token").val(token);
     
    $("#logout").click(function(){  
        Swal.fire({ 
            title: "Are you sure?",
            text: "Once logged out, you will need to login again!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: false,
            confirmButtonText: "Ok!",
            closeOnConfirm: true,
            closeOnCancel: false
          }).then((willLogout) => {
            if (willLogout) {      
                $.ajax({
                    url: '/api/logout', // Adjust the URL as necessary
                    type: 'POST',
                    success: function(response) {
                        if(response.status == "success"){
                          var route_url =  window.location.origin+"/frontend";
                          Swal.fire({ 
                            icon: "success",                   
                            title: "Great!",
                            text: response.message,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonText: "Ok!",
                            closeOnConfirm: true,
                            closeOnCancel: false

                          }).then(response=>{
                            if(response.isConfirmed){
                              window.location.href = route_url;
                            }
                          });
                        }
                    },
                    error: function(xhr) {
                        console.error(xhr.responseJSON.error);
                        // Handle error response
                    }
                });
            }
        });
    });
    $('#logout-button').on('click', function(e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault(); // Prevent the form from submitting immediately       
        Swal.fire({ 
            title: "Are you sure?",
            text: "Once logged out, you will need to login again!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: false,
            confirmButtonText: "Ok!",
            closeOnConfirm: true,
            closeOnCancel: false

          }).then((willLogout) => {
            if (willLogout) {
                // $('#logout-form').submit();
                $.ajax({
                    url: '/logout', // Adjust the URL as necessary
                    type: 'POST',                   
                    success: function(response) {
                        if(response.status == "success"){
                          var route_url =  window.location.origin;
                          Swal.fire({ 
                            icon: "success",                   
                            title: "Great!",
                            text: response.message,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonText: "Ok!",
                            closeOnConfirm: true,
                            closeOnCancel: false

                          }).then(response=>{
                            if(response.isConfirmed){
                              window.location.href = route_url;
                            }
                          });
                        }
                    },
                    error: function(xhr) {
                        console.error(xhr.responseJSON.error);
                        // Handle error response
                    }
                });
            }
        });
    });
</script>
<!-- END THIRT-PART JS -->

@yield('plugin-script')
<!-- BEGIN PAGE LEVEL JS -->
@yield('page-script')
<!-- END PAGE LEVEL JS -->

</body>
</html>
