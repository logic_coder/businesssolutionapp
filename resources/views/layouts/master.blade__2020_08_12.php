<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Logic POS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('head')

    <link href="{{mix('/theme/backend/assets/css/plugins.css')}}" type="text/css" rel="stylesheet"/>
    <link href="{{mix('/theme/backend/assets/css/pages.css')}}" type="text/css" rel="stylesheet"/>
    <link href="{{mix('/theme/backend/assets/css/customs.css')}}" type="text/css" rel="stylesheet"/>

    <!--[if lte IE 9]>
    <link href="{{mix('/theme/backend/assets/css/lte-ie9.css')}}" type="text/css" rel="stylesheet"/>
    <![endif]-->
    <!--[if lt IE 9]>
    <link href="{{mix('/theme/backend/assets/css/lt-ie9.css')}}" type="text/css" rel="stylesheet"/>
    <![endif]-->

    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.0.0/dist/vue-multiselect.min.css">

    <!-- Scripts -->
    <script>
        window.Laravel = {
            csrfToken: '{{csrf_token()}}'
        }
    </script>


    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="public/css/windows.chrome.fix.css" />'
        }

    </script>
    @yield('style')
</head>
<body class="fixed-header ">
<!-- BEGIN SIDEBPANEL-->
@include('partial/sidebar')
<!-- END SIDEBAR -->
<!-- END SIDEBPANEL-->

<!-- START PAGE-CONTAINER -->
<div class="page-container">
    <!-- START HEADER -->
    <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="sm-action-bar">
                <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
                    <span class="icon-set menu-hambuger"></span>
                </a>
            </div>
            <!-- END ACTION BAR -->
        </div>
        <!-- RIGHT SIDE -->
        <div class="pull-right full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="sm-action-bar">
                <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
                    <span class="icon-set menu-hambuger-plus"></span>
                </a>
            </div>
            <!-- END ACTION BAR -->
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table">
            <div class="header-inner">
                <div class="brand inline">
                    <img src="{{ asset('images/logo.png') }}" alt="logo" data-src="{{ asset('images/logo.png') }}" data-src-retina="{{ asset('images/logo.png') }}" width="78" height="22" >
                </div>
                <!-- START NOTIFICATION LIST -->
                @include('partial/notification')
                <!-- END NOTIFICATIONS LIST -->
                <a href="#" class="search-link" data-toggle="search"><i class="pg-search"></i>Type anywhere to <span class="bold">search</span></a> </div>
        </div>
        <div class=" pull-right">
            <div class="header-inner">
                <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview"></a>
            </div>
        </div>
        <div class=" pull-right">
            <!-- START User Info-->
            @include('user/partial/user_info')
            <!-- END User Info-->
        </div>
    </div>
    <!-- END HEADER -->
    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content">
            @yield('content')
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg footer">
            <div class="copyright sm-text-center">
                <p class="small no-margin pull-left sm-pull-reset">
                    <span class="hint-text">Copyright © 2018 </span>
                    <span class="font-montserrat">Logic POS</span>.
                    <span class="hint-text">All rights reserved. </span>
                    <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
                </p>
                <p class="small no-margin pull-right sm-pull-reset">
                    <a href="#">Hand-crafted</a> <span class="hint-text">&amp; Made with Love ®</span>
                </p>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- END COPYRIGHT -->
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN VENDOR JS -->
<script src="{{mix('/theme/backend/assets/js/plugins.js')}}" type="text/javascript"></script> 
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
{{--<script src="{{mix('/theme/backend/assets/js/pages.js')}}"></script>--}}
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{mix('/theme/backend/assets/js/page-level.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
<!-- BEGIN CUSTOMS JS -->
<script src="{{mix('/theme/backend/assets/js/customs.js')}}" type="text/javascript"></script>
<!-- END CUSTOMS JS -->
<!-- BEGIN THIRT-PART JS -->
<script src="{{mix('/theme/backend/assets/js/thirt-party.js')}}" type="text/javascript"></script>
<!-- END THIRT-PART JS -->

@yield('plugin-script')
<!-- BEGIN PAGE LEVEL JS -->
@yield('page-script')
<!-- END PAGE LEVEL JS -->
</body>
</html>
