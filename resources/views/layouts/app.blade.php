<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Pages - Admin Dashboard UI Kit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="app_url" content="{{url('')}}">
    <!-- <script defer src="{{ mix('js/app.js') }}"></script> -->
    @yield('head')
    <link href="{{mix('/theme/backend/assets/css/plugins.css')}}" type="text/css" rel="stylesheet"/>
    <link href="{{mix('/theme/backend/assets/css/pages.css')}}" type="text/css" rel="stylesheet"/>
    <link href="{{mix('/theme/backend/assets/css/customs.css')}}" type="text/css" rel="stylesheet"/>
    <script src="{{asset('resources/assets/plugins/jquery/jquery-1.11.1.min.js')}}" type="text/javascript"></script>

    <!--[if lte IE 9]>
    <link href="{{mix('/theme/backend/assets/css/lte-ie9.css')}}" type="text/css" rel="stylesheet"/>
    <![endif]-->
    <!--[if lt IE 9]>
    <link href="{{mix('/theme/backend/assets/css/lt-ie9.css')}}" type="text/css" rel="stylesheet"/>
    <![endif]-->

    <!-- Scripts -->
    <script>
        window.Laravel = {
            csrfToken: '{{csrf_token()}}'
        }
    </script>
    <!-- Scripts -->
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="public/css/windows.chrome.fix.css" />'
        }
    </script>
    @yield('style')
</head>
<body class="fixed-header   ">
<!-- START PAGE-CONTAINER -->
<div class="login-wrapper ">
    <!-- START Login Background Pic Wrapper-->
    <div class="bg-pic">
        <!-- START Background Pic-->
        <img src="{{ asset('/theme/backend/assets/images/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg') }}" data-src="{{ asset('/theme/backend/assets/images/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg') }}" data-src-retina="{{ asset('images/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg') }}" alt="" class="lazy">
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
            <h2 class="semi-bold text-white">
                Pages make it easy to enjoy what matters the most in the life</h2>
            <p class="small">
                images Displayed are solely for representation purposes only, All work copyright of respective owner, otherwise © 2013-2014 REVOX.
            </p>
        </div>
        <!-- END Background Caption-->
    </div>
    <!-- END Login Background Pic Wrapper-->
@yield('content')
<!-- START Login Right Container-->

    <!-- END Login Right Container-->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN VENDOR JS -->
<script src="{{mix('/theme/backend/assets/js/plugins.js')}}" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
{{--<script src="{{mix('/theme/backend/assets/js/pages.js')}}"></script>--}}
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{mix('/theme/backend/assets/js/page-level.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
<!-- BEGIN CUSTOMS JS -->
<script src="{{mix('/theme/backend/assets/js/customs.js')}}" type="text/javascript"></script>
<!-- END CUSTOMS JS -->
<!-- BEGIN THIRT-PART JS -->
<script src="{{mix('/theme/backend/assets/js/thirt-party.js')}}" type="text/javascript"></script>
<!-- END THIRT-PART JS -->

@yield('plugin-script')
<!-- BEGIN PAGE LEVEL JS -->
@yield('page-script')
<!-- END PAGE LEVEL JS -->

</body>
</html>
