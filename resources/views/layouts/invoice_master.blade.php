<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Logic POS</title>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('head')
        <!-- <link href="{{mix('/theme/backend/assets/css/plugins.css')}}" type="text/css" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ asset('css/invoice.css') }}">  -->
    <!--[if lte IE 9]>
    <link href="{{mix('/theme/backend/assets/css/lte-ie9.css')}}" type="text/css" rel="stylesheet"/>
    <![endif]-->
    <!--[if lt IE 9]>
    <link href="{{mix('/theme/backend/assets/css/lt-ie9.css')}}" type="text/css" rel="stylesheet"/>
    <![endif]-->

    <!-- Scripts -->
    <script>
        window.Laravel = {
            csrfToken: '{{csrf_token()}}'
        }
    </script>


    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="public/css/windows.chrome.fix.css" />'
        }

    </script>
    <style>
        /* header {
            position: fixed;            
            left: 0px;
            right: 0px;
            height: auto;
            background-color: #f26716;
            color: white;
            text-align: center;
            line-height: 40px;
        } */
        @page {
                margin: 0cm 0cm;
            }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin-top: 3cm;
            margin-left: 0.5cm;
            margin-right: 0.5cm;
            margin-bottom: 2.5cm;
        }
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 100;

            /** Extra personal styles **/
            background-color: #f26716;
            color: white;
            text-align: center;
            line-height: 40px;
        }
        footer {
            position: fixed; 
            left: 3%; 
            right: 0px;
            height: 50px; 
            top:auto;
            /** Extra personal styles **/            
            line-height: 35px;
        }
    </style>
    @yield('style')
</head>

<body class="fixed-header ">       
    <!-- START PAGE CONTENT -->
    <div class="content">
        @yield('content')
    </div>
    <!-- END PAGE CONTENT -->
   
@yield('plugin-script')
<!-- BEGIN PAGE LEVEL JS -->
@yield('page-script')
<!-- END PAGE LEVEL JS -->
</body>
</html>
