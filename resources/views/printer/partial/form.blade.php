
<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Printer</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('name', 'Printer Name') }}
                            {{ Form::text('name',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('model', 'Model') }}
                            {{ Form::text('model',null, ['class' => 'form-control', '']) }}
                        </div>
                        <div class="form-group form-group-default required form-group-default-selectFx">
                            {{ Form::label('Type', 'Type') }}
                            {{Form::select('type',$type_lists,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control attribute-group-select','data-init-plugin' => 'cs-select']) }}
                        </div>
                        <div class="form-group form-group-default required form-group-default-selectFx">
                            {{ Form::label('active', 'Active') }}
                            {{Form::select('status',$status_lists,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control attribute-group-select','data-init-plugin' => 'cs-select']) }}
                        </div>
                       
                    </div>
                  
                    <div class="form-group-attached pull-right padding-top">
                        <button class="btn create-button btn-cons" type="submit">
                            <span>Save</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('plugin-script')

@endsection