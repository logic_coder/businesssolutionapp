<div class="row row-same-height">
    <div class="panel panel-default two-column-div">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Coupon Info</h4>
        </div>
        <div class="panel-body ">
            <div class="col-md-7">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('description', 'Description') }}
                            {{ Form::text('description',null, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group form-group-default float-left">
                            {{ Form::label('code', 'Code') }}
                            {{ Form::text('code',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="float-left">
                            <button class="btn btn-custom" type="submit">
                                <span>Generate</span>
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <br/>
                <div class="toggle-on-off toggle-padding-custom">
                    {{ Form::label('high_light', 'High Light') }}:
                    <input type="checkbox" data-init-plugin="switchery" @php if(1) { if( 1 ) echo 'checked'; } @endphp name="high_light"/>
                </div><br/>
                <div class="toggle-on-off toggle-padding-custom">
                    {{ Form::label('status', 'Status') }}:
                    <input type="checkbox" data-init-plugin="switchery" name="status"/>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default two-column-div">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Action</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('address', 'Address', ['class' => '']) }}
                            {{ Form::text('address',null, ['class' => 'form-control', 'placeholder' => 'Current address','required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('city', 'City', ['class' => '']) }}
                            {{ Form::text('city',null,  ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('zip', 'Zip/Postal Code', ['class' => '']) }}
                            {{ Form::text('zip',null, ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Conditions</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('limit_to_single_customer', 'Limit to single customer') }}
                            <select name="customer_id" class="cs-select cs-skin-slide cs-transparent form-control">
                                @foreach( $customers as $customer )
                                    <option value="{{$customer->id}}">
                                        {{$customer->first_name}} {{$customer->last_name}}({{$customer->customer_no}})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group form-group-default float-left required">
                            {{ Form::label('valid_from', 'Valid From') }}
                            {{ Form::date('valid_from',\Carbon\Carbon::now()->format('Y-m-d'), ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default float-left required">
                            {{ Form::label('valid_to', 'Valid To') }}
                            {{ Form::date('valid_to',\Carbon\Carbon::now()->format('Y-m-d'), ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group form-group-default float-left required">
                            {{ Form::label('minimum_purchase_amount', 'Minimum Amount', ['class' => '']) }}
                            {{ Form::text('minimum_purchase_amount', null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default float-left required">
                            {{ Form::label('currency', 'Currency', ['class' => '']) }}
                            {{Form::select('minimum_purchase_amount_currency_id',$currencies,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control','data-init-plugin' => 'cs-select']) }}
                        </div>
                        <div class="form-group form-group-default float-left required">
                            {{ Form::label('select', 'Select', ['class' => '']) }}
                            {{Form::select('vat_include_on_minimum_purchase_amount',$vat_include_list,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control','data-init-plugin' => 'cs-select']) }}
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('total_number_of_times', 'Total Limit', ['class' => '']) }}
                            {{ Form::text('total_number_of_times', null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('total_number_of_times_for_each_user', 'Total Limit For Each User', ['class' => '']) }}
                            {{ Form::text('total_number_of_times_for_each_user', null, ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <button class="btn create-button btn-cons pull-right" type="submit">
        <span>Save</span>
    </button>
</div>