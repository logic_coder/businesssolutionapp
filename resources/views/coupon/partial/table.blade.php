<button class="btn delete-multi-items" action-url="coupon/remove_coupon" redirect-url="/coupon"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Description</th>
        <th style="width:20%">Code</th>
        <th style="width:20%">Min. Purchase Amount</th>
        <th style="width:20%">Discount Percent</th>
        <th style="width:20%">Discount Amount</th>
        <th style="width:20%">Valid From</th>
        <th style="width:20%">Valid To</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach( $coupons as $coupon )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="item_ids[]" value={{$coupon->id}} data-id="checkbox" id="checkbox{{$coupon->id}}">
                    <label for="checkbox{{$coupon->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <p>{{$coupon->name}}</p>
            </td>
            <td class="v-align-middle">
                {{ $coupon->description }}
            </td>
            <td class="v-align-middle">
                {{ $coupon->code }}
            </td>
            <td class="v-align-middle">
                @php
                    if( $coupon->minimum_purchase_amount ){
                        $currencyCode = "";
                        $companyCurrencies = App\CompanyCurrency::find($coupon->company_currency_id);
                        if( $companyCurrencies ){
                            $currencyCode = App\Currency::find( $companyCurrencies->currency_id )->code;
                            echo $coupon->minimum_purchase_amount.$currencyCode;
                        }
                    }
                @endphp
            </td>
            <td class="v-align-middle">
                @if($coupon->discount_percent)
                    {{ $coupon->discount_percent }} %
                @endif
            </td>
            <td class="v-align-middle">
                @php
                if( $coupon->discount_amount )
                    echo $coupon->discount_amount.$currencyCode;
                @endphp
            </td>
            </td>
            <td class="v-align-middle">
                {{ $coupon->valid_from }}
            </td>
            <td class="v-align-middle">
                {{ $coupon->valid_to }}
            </td>
            <td class="v-align-middle">
                {{ $coupon->status }}
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn edit-button" href="{{url('coupon/'.$coupon->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$coupon->id}}" type="button" class="btn create-button delete-single-item" action-url="coupon/remove_coupon" redirect-url="/coupon"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
