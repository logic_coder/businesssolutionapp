<button class="btn delete-multi-items" action-url="vat/remove_vat" redirect-url="/vat"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Percent</th>
        <th style="width:20%">Last Updated</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $vats as $vat )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="item_ids[]" value={{$vat->id}} data-id="checkbox" id="checkbox{{$vat->id}}">
                    <label for="checkbox{{$vat->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                {{ $vat->name }}
            </td>
            <td class="v-align-middle">
                {{ $vat->percent }}
            </td>
            <td class="v-align-middle">
                <p>{{ date('F d, Y H:i:s', strtotime($vat->updated_at)) }}</p>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn edit-button" href="{{url('vat/'.$vat->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$vat->id}}" type="button" class="btn create-button delete-single-item" action-url="vat/remove_vat" redirect-url="/vat"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
