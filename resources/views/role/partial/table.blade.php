<button class="btn delete-multi-items" action-url="role/remove_role" redirect-url="/role"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $roles as $role )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    @if( $role->slug !='administrator' )
                        <input class="item_ids" type="checkbox" name="item_ids[]" value={{$role->id}} data-id="checkbox" id="checkbox{{$role->id}}">
                        <label for="checkbox{{$role->id}}"></label>
                    @endif
                </div>
            </td>
            <td class="v-align-middle">
                <a style="text-decoration: none" href="{{  url('role/'.$role->id.'/permission_role') }}">{{ $role->name }} </a>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    @if( $role->slug !='administrator' )
                        <a title="Edit" class="btn edit-button" href="{{url('role/'.$role->id.'/edit')}}" ><i class="fa fa-pencil"></i></a>
                        <button title="Delete" item-ids="{{$role->id}}" type="button" class="btn create-button delete-single-item" action-url="role/remove_role" redirect-url="/role"><i class="fa fa-trash-o"></i></button>
                    @endif
                </div>
                <div class="btn-actions set-permission">
                    <a title="Edit" class="btn product_add_btn btn-success" href="{{url('role/'.$role->id.'/permission_role')}}">Set Permission</a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
