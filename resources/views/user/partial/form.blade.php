<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">User</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('email', 'Login ID: Mobile no or email') }}
                            {{ Form::text('email',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            <input type="password" class="form-control" name="password" placeholder="Minimum of 8 characters." required>
                        </div>
                        <!--
                            <div class="form-group form-group-default required">
                                {{ Form::label('email', 'Email') }}
                                {{ Form::text('email',null, ['class' => 'form-control', 'required']) }}
                            </div>
                            <div class="form-group form-group-default required">
                                {{ Form::label('re-email', 'Re-type Email') }}
                                {{ Form::text('re-email',null, ['class' => 'form-control', 'required']) }}
                            </div>
                        -->
                        <div class="form-group form-group-default required form-group-default-selectFx">
                            {{ Form::label('role', 'User Role') }}
                            {{Form::select('role_id',$role_lists,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control','data-init-plugin' => 'cs-select']) }}
                        </div>
                    </div>
                    <div class="checkbox check-success">
                        <input type="checkbox" name="is_pos_admin" value='1' data-id="is_pos_admin" id="is_pos_admin">
                        {{ Form::label('is_pos_admin', 'Pos admin user') }}
                    </div>
                    <div class="checkbox check-success">
                        <input type="checkbox" name="pos_user" value='1' data-id="pos_user" id="pos_user" class="pos_user">
                        {{ Form::label('pos_user', 'Pos user') }}
                    </div>
                    <div class="multiple-select">
                        <h5>
                            Assign To Outlets
                        </h5>
                        <br>
                        {{Form::select('out_let_ids[]',$out_let_lists,null,['multiple'=>'multiple','id' => "multi", 'class' => "full-width"]) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button class="btn create-button btn-cons pull-right" type="submit">
        <span>Save</span>
    </button>
</div>



