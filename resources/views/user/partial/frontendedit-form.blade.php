<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">User Profile Photo</h4>
        </div>
        @php
            $role_id = '';
            if( $user->roles ){
            foreach( $user->roles as $user_role ){
                $role_id = $user_role->id;
            }
        }
        @endphp
        <div class="panel-body">            
            <div class="col-md-7 col-div-width">
                <div class="panel-body no-scroll no-padding" id="app">
                    <frontend-user-photo company_id="{{ \App\User::find($_COOKIE['user_id'])->company_id }}" user_id="{{ $user->id }}" ></frontend-user-photo>
                </div> 
            </div>
        </div>
    </div>
</div>
<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">User Details</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('email', 'Login ID: Mobile no or email') }}
                            {{ Form::text('email',$user->email, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('contact email', 'Contact Email') }}
                            {{ Form::text('contact_email',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default">
                            {{ Form::label('role', 'User Role') }} 
                            @if(sizeof($user->roles))
                                {{ $user->roles[0]->name }}
                            @else 
                                {{ 'N/A' }}
                            @endif
                        </div>
                    </div>
                    <div class="checkbox check-success">
                        <input disabled type="checkbox" name="is_pos_admin" @if( $user->is_pos_admin )  checked @endif  value='1' data-id="is_pos_admin" id="is_pos_admin" >
                        {{ Form::label('is_pos_admin', 'Pos admin user') }}
                    </div>
                    <div class="checkbox check-success">
                        <input disabled type="checkbox" name="pos_user" @if( count($user_out_lets) > 0 )  checked @endif value='1' data-id="pos_user" id="pos_user" class="pos_user">
                        {{ Form::label('pos_user', 'Pos user') }}
                    </div>
                    <div id="test" class="multiple-select">
                        <h5>
                            Assign To Outlets
                        </h5>
                        <br>
                        {{Form::select('out_let_ids[]',$out_let_lists, $user_out_lets,array('multiple'=>'multiple','id' => "multi", 'class' => "full-width", 'disabled' => true) ) }}
                    </div>
                </div>
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('address', 'Address', ['class' => '']) }}
                            {{ Form::text('address',isset($user_detail) ? $user_detail->address : '', ['class' => 'form-control', 'placeholder' => 'Current address','required']) }}
                        </div>
                        <div class="form-group form-group-default">
                            {{ Form::label('address2', 'Second Address', ['class' => '']) }}
                            {{ Form::text('address2',isset($user_detail) ? $user_detail->address2 : '', ['class' => 'form-control', 'placeholder' => 'Optional','']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('city', 'City', ['class' => '']) }}
                            {{ Form::text('city',isset($user_detail) ? $user_detail->city : '',  ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('zip', 'Zip/Postal Code', ['class' => '']) }}
                            {{ Form::text('zip',isset($user_detail) ? $user_detail->zip : '', ['class' => 'form-control', 'required']) }}
                        </div>
                    </div>
                    <br>
                </div>
                
            </div>
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default form-group-default-selectFx">
                            {{ Form::label('title', 'User Title') }}
                            {{Form::select('title_id',$title_lists,null,['class' => 'cs-select cs-skin-slide cs-transparent form-control','data-init-plugin' => 'cs-select']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('firstname', 'First Name') }}
                            {{ Form::text('first_name',isset($user_detail) ? $user_detail->first_name : '', ['class' => 'form-control', 'placeholder' => 'First name','required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('lastname', 'Last Name') }}
                            {{ Form::text('last_name',isset($user_detail) ? $user_detail->last_name : '', ['class' => 'form-control', 'placeholder' => 'Last name','required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('date_of_birth', 'Date of birth') }}
                            {{ Form::date('date_of_birth', isset( $user_detail->date_of_birth ) ? date('Y-m-d', strtotime( $user_detail->date_of_birth )) : \Carbon\Carbon::now()->format('Y-m-d'), ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default">
                            {{ Form::label('user_code', 'User Code') }}
                            {{ Form::text('user_code',isset($user_detail) ? $user_detail->user_code : '', ['class' => 'form-control', '']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('cell_phone', 'Cell Phone') }}
                            {{ Form::text('cell_phone',isset($user_detail) ? $user_detail->cell_phone : '', ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default">
                            {{ Form::label('work_phone', 'Work Phone') }}
                            {{ Form::text('work_phone',isset($user_detail) ? $user_detail->work_phone : '', ['class' => 'form-control', '']) }}
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <button class="btn create-button btn-cons pull-right" type="submit">
        <span>Save</span>
    </button>
</div>
@section('page-script')
  <script src="{{mix('js/app.js')}}" />
@endsection
