<button class="btn delete-multi-items" action-url="user/remove_user" redirect-url="/user"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Login ID</th>
        <!-- <th style="width:20%">Email</th> -->
        <th style="width:20%">Address</th>
        <th style="width:20%">Mobile</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $users as $user )
        @php
            $user_details = '';
            $role_slug = '';
            if( $user->userDetail ){
                $user_details = $user->userDetail;
            }
            if( $user->roles ){
                foreach( $user->roles as $user_role ){
                    $role_slug = $user_role->slug;
                }
            }
        @endphp
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    @if( $role_slug !='administrator' )
                        <input class="item_ids" type="checkbox" name="item_ids[]" value={{$user->id}} data-id="checkbox" id="checkbox{{$user->id}}">
                        {{--<input  @if( $role_slug == 'administrator' ) disabled @endif class="item_ids" type="checkbox" name="item_ids[]" value={{$user->id}} data-id="checkbox" id="checkbox{{$user->id}}">--}}
                        <label for="checkbox{{$user->id}}"></label>
                    @endif
                </div>
            </td>
            <td class="v-align-middle">
                {{ $user_details ? $user_details->first_name.' '. $user_details->last_name : ''}}
            </td>
            <!-- <td class="v-align-middle">
                {{ $user->login_email_or_mobile }}
            </td> -->
            <td class="v-align-middle">
                {{ $user->email }}
            </td><td class="v-align-middle">
                {{ $user_details ? $user_details->address : '' }}
            </td><td class="v-align-middle">
                {{ $user_details ? $user_details->cell_phone : '' }}
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn edit-button" href="{{url('user/'.$user->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    @if( $role_slug !='administrator' )
                        <button title="Delete" item-ids="{{$user->id}}" type="button" class="btn create-button delete-single-item" action-url="user/remove_user" redirect-url="/user"><i class="fa fa-trash-o"></i></button>
                    @endif
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>