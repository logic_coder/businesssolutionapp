
<div class="visible-lg visible-md m-t-10">
    <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
        <h3 class="font-montserrat no-margin text-uppercase demo-fs-23">

            @php
                if( Redis::get('out_let_id') ){
                    $outLet = App\OutLet::find( Redis::get('out_let_id') );
                    if( $outLet ){
                        echo $outLet->name;
                    }
                }
            @endphp

        </h3>
        <span class="semi-bold"> {{ Auth::user()->userDetail ? Auth::user()->userDetail->first_name : '' }} {{ Auth::user()->userDetail ? Auth::user()->userDetail->last_name : '' }}</span>
    </div>
    <div class="dropdown pull-right">
        <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="{{asset('uploads/company/'.Auth::user()->company_id.'/user_profile_photo/'.Auth::user()->id.'/32x32/'.Auth::user()->userDetail->photo )}}" alt="logo" data-src="{{asset('uploads/company/'.Auth::user()->company_id.'/user_profile_photo/'.Auth::user()->id.'/32x32/'.Auth::user()->userDetail->photo )}}" data-src-retina="{{asset('uploads/company/'.Auth::user()->company_id.'/user_profile_photo/'.Auth::user()->id.'/32x32/'.Auth::user()->userDetail->photo )}}">
           </span>
        </button>
        <ul class="dropdown-menu profile-dropdown" role="menu">
            <li><a href="#"><i class="pg-settings_small"></i> Settings</a>
            </li>
            <li><a href="#"><i class="pg-outdent"></i> Feedback</a>
            </li>
            <li><a href="#"><i class="pg-signals"></i> Help</a>
            </li>
            <li class="bg-master-lighter">
                <span class="pull-left">
                    {{Form::open(['url'=>'logout','method'=>'post'])}}
                        <button type="submit">Logout</button>
                    {{Form::close()}}
                </span>
                <span class="pull-right"><i class="pg-power"></i></span>
            </li>
        </ul>
    </div>
</div>


<div class="dropdown pull-right" id="app">
        <user-info company_id="{{ Auth::user()->company_id }}" user_id="{{ Auth::user()->id }}" ></user-info>
    </div>