
<div class="visible-lg visible-md m-t-10">
    <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
        <h3 class="font-montserrat no-margin text-uppercase demo-fs-23">

            @php
                /*if( Redis::get('out_let_id') ){
                    $outLet = App\OutLet::find( Redis::get('out_let_id') );
                    if( $outLet ){
                        echo $outLet->name;
                    }
                }*/
            @endphp

        </h3>
        <span class="semi-bold"> {{ Auth::user()->userDetail ? Auth::user()->userDetail->first_name : '' }} {{ Auth::user()->userDetail ? Auth::user()->userDetail->last_name : '' }}</span>
    </div>
    <div class="dropdown pull-right">
        <!-- <user-info company_id="{{  Auth::user()->company_id }}" user_id="{{  Auth::user()->id }}" ></user-info>  -->
    </div>
</div>
@section('page-script')
  <!-- <script src="{{mix('js/app.js')}}" /> -->
@endsection