
<div class="visible-lg visible-md m-t-10">
    <div class="pull-left p-r-10 p-t-10 fs-16 font-heading" style="padding-left:40%">
        <h3 class="font-montserrat no-margin text-uppercase demo-fs-23">
            @php
                $user_detail = Auth::user()->userDetail;
                $company_id = Auth::user()->company_id;
            @endphp

        </h3>
        <span class="semi-bold"> {{ Auth::user()->userDetail ? Auth::user()->userDetail->first_name : '' }} {{ Auth::user()->userDetail ? Auth::user()->userDetail->last_name : '' }}</span>
    </div>
    <div class="dropdown pull-right"> <span class="semi-bold">     
        <ul class="navbar-nav ml-auto py-4 py-md-0">
            <li class="subnav nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                <button class="profile-dropdown-toggle subnav" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="thumbnail-wrapper d32 circular inline m-t-5">
                        @if($user_detail && $user_detail->photo)
                            <img src="{{ asset('uploads/company/'.$company_id.'/user_profile_photo/'.$user_detail->user_id.'/'.$user_detail->photo) }}" alt="logo" data-src="{{ asset('uploads/company/'.$company_id.'/user_profile_photo/'. $user_detail->user_id.'/'.$user_detail->photo) }}" data-src-retina="{{ asset('uploads/company/'.$company_id.'/user_profile_photo/'.$user_detail->user_id.'/'.$user_detail->photo) }}" width="32" height="32" >
                        @else
                            <img src="{{ asset('uploads/default_user_profile_images/default_images.png') }}" alt="logo" data-src="{{ asset('uploads/default_user_profile_images/default_images.png') }}" data-src-retina="{{ asset('uploads/default_user_profile_images/default_images.png') }}" width="32" height="32" >
                        @endif
                    </span>
                </button>
                <ul class="navbar-nav-child dropdown-menu profile-dropdown">   
                    <li><a href="{{url('user/'.Auth::user()->id.'/edit')}}"><i class="pg-settings_small"></i> Profile </a>  </li>
                    <li><a href="#"><i class="pg-signals"></i> Help</a>  </li> 
                    <!--  <li><a href="#"><i class="pg-outdent"></i> Feedback </a>
                    </li> -->
                    
                    <li class="bg-master-lighter">
                        <span class="pull-left">
                            <!-- {{ Form::open(['url' => 'logout', 'method' => 'post', 'id' => 'logout-form']) }} -->
                                <button type="submit" id="logout-button">Logout</button>
                            <!-- {{ Form::close() }} -->
                        </span>
                        <span class="pull-right"><i class="pg-power"></i></span>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>


