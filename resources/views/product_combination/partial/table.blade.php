<button class="btn delete-multi-items" action-url="product_combination/remove_product_combinations_by_product" redirect-url="/product_combination"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Product</th>
        <th style="width:20%">No. Of Combinations</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach( $combinations as $combination )
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input class="product_combination_ids" type="checkbox" name="item_ids[]" value={{$combination->product_id}} data-id="checkbox" id="checkbox{{$combination->product_id}}">
                        <label for="checkbox{{$combination->product_id}}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    {{ App\Product::find( $combination->product_id )->name}}
                </td>
                <td class="v-align-middle">
                    <a title="" class="btn btn-success product_add_btn" href="{{url('product_combination/'.$combination->product_id.'/edit')}}"> {{ $combination->total }}</a>
                </td>
                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <a title="Edit" class="btn edit-button" href="{{url('product_combination/'.$combination->product_id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        <button title="Delete" item-ids="{{$combination->product_id}}" type="button" class="btn create-button delete-single-item" action-url="product_combination/remove_product_combinations_by_product" redirect-url="/product_combination"><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>