
<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Product Combination</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-product-left-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('product', 'Product') }}
                            {{Form::select('product_id',$product_lists, null,['class' => 'cs-select cs-skin-slide cs-transparent form-control attribute-group-select','data-init-plugin' => 'cs-select']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('name', 'Product Combination Name') }}
                            {{ Form::text('name',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('product_combination', 'Manaage Product Combination') }}
                            {{ Form::text('product_combination',null, ['class' => 'form-control tagsinput custom-tag-input', 'required']) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-product-right-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        @foreach($attributes as $attribute)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">{{ $attribute->name }}</div>
                            </div>
                            <div class="panel-body">
                                @foreach($attribute->attributeValues as $attributeValues)
                                    <div class="row">
                                        <input type="checkbox" value="{{ $attributeValues->id }}" name="{{ $attributeValues->value }}" class="attributes-checkbox"/>
                                        {{ Form::label($attributeValues->value, $attributeValues->value) }}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <br>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-success btn-cons pull-right" type="submit">
        <span>Save</span>
    </button>
</div>
