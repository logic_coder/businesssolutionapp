
<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Printer Assign To Out Let</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <h4 class="semi-bold"> Printer Name : {{ $printer->name }}( {{ $printer->type }} )</h4>
                <div class="padding-30">
                    <div class="form-group-attached">
                       <div class="multiple-select__">
                            <h5>
                                Assign To Outlets
                            </h5>
                            <br>
                            {{Form::select('out_let_ids[]',$out_let_lists, $printer_out_lets,array('multiple' => true, 'class' => 'input-select-printer') ) }}
                        </div>                       
                    </div>
                  
                    <div class="form-group-attached pull-right padding-top">
                        <button class="btn btn-success btn-cons" type="submit">
                            <span>Save</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>