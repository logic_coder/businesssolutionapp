<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Global Payment Method Settings</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('name', 'Payment Method') }}
                            <select name="payment_method_name" class="cs-select cs-skin-slide cs-transparent form-control" id="country_payment_method">
                                <option value="">-- Select Payment Method -- </option>
                                @foreach( $country_payment_methods as $each_payment_method )
                                    <option value="{{$each_payment_method->id}}" @if($payment_method && $payment_method->name == $each_payment_method->name) selected @endif > {{$each_payment_method->name}}  </option>
                                @endforeach
                            </select>

                            {{ Form::hidden('name', '', array('id' => 'payment_method_name')) }}
                        
                        </div>
                    </div>
                    <div class="form-group-attached pull-right padding-top">
                        <button class="btn create-button btn-cons" type="submit">
                            <span>Save</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>