<button class="btn delete-multi-items" action-url="payment_method/remove_payment_method" redirect-url="/payment_method"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Last Updated</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach( $payment_methods as $payment_method )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="item_ids[]" value={{$payment_method->id}} data-id="checkbox" id="checkbox{{$payment_method->id}}">
                    <label for="checkbox{{$payment_method->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <p>{{ $payment_method->name }} </p>
            </td>
            <td class="v-align-middle">
                <p>{{ date('F d, Y H:i:s', strtotime($payment_method->updated_at)) }}</p>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn edit-button" href="{{url('payment_method/'.$payment_method->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$payment_method->id}}" type="button" class="btn create-button delete-single-item" action-url="payment_method/remove_payment_method" redirect-url="/payment_method"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>