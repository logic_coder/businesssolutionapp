@extends('layouts.master')
@section('style')
    <link href="{{asset('backend_old')}}" rel="stylesheet" type="text/css" media="screen" />
{{--    <link href="{{asset('theme/backend/assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" media="screen" />--}}
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <!-- START WIDGET -->
            <div class="row-xs-height">
                <div class="col-xs-height">
                    <div class="p-l-20 p-r-20">
                        <div class="row">
                            <div class="col-lg-3 visible-xlg">
                                <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-xs-height">
                <div class="col-xs-height relative bg-master-lightest">
                    <div class="widget-14-chart_y_axis"></div>
                    <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                </div>
            </div>
            <!-- END WIDGET -->
        </div>

        <div id="rootwizard" class="m-t-50">

            <!-- Success Messages -->
        @include('partial/success_message')
        <!-- END Success Messages -->
            <!-- Error Messages -->
        @include('partial/error_message')
        <!-- END Error Messages -->
            <!-- START CONTAINER FLUID -->
            <div id="rootwizard" class="m-t-50">
                <!-- Nav tabs -->
                @include('product.partial.product-nav')
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane padding-20 active slide-left" id="tab1">
                        {{ Form::open(['url' => 'product','method' => 'post', 'id' => 'form-project', 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone no-margin', 'id' => 'image-upload']) }}
                        {{ csrf_field() }}
                            @include('product.partial.form')
                        {{ Form::close() }}
                    </div>
                    <div class="tab-pane slide-left padding-20" id="tab2">
                        <div class="row row-same-height">
                            <div class="panel-body">
                                <!-- START PANEL -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            Product Photos
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;"></a>
                                            <a class="config" data-toggle="modal" href="#grid-config"></a>
                                            <a class="reload" href="javascript:;"></a>
                                            <a class="remove" href="javascript:;"></a>
                                        </div>
                                    </div>
                                    <div class="panel-body no-scroll no-padding">
                                        {!! Form::open([ 'url' => 'product/upload-photo', 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload' ]) !!}
                                            @include('product.partial.photo-upload-form')
                                        {{ Form::close() }}
                                    </div>
                                </div>
                                <!-- END PANEL -->
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane slide-left padding-20" id="tab3">
                    </div>
                </div>
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
    </div>
@endsection

@section('plugin-script')
    <script src="{{asset('theme/backend/assets/plugins/jquery-dynatree/jquery.dynatree.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('theme/backend/assets/js/tree_category_script.js')}}" type="text/javascript"></script>
{{--    <script src="{{asset('theme/backend/assets/plugins/dropzone/dropzone.min.js')}}" type="text/javascript"></script>--}}
@endsection



