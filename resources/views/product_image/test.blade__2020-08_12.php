@extends('layouts.master')
@section('style') 
<script src="{{asset('resources/assets/plugins/fancybox-2.1.7/lib/jquery.mousewheel.pack.js?v=3.1.3')}}" type="text/javascript"></script>
<script src="{{asset('resources/assets/plugins/fancybox-2.1.7/source/jquery.fancybox.pack.js?v=2.1.5')}}" type="text/javascript"></script>
<script src="{{asset('resources/assets/plugins/fancybox-2.1.7/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')}}" type="text/javascript"></script>
<script src="{{asset('resources/assets/plugins/fancybox-2.1.7/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7')}}" type="text/javascript"></script>
<script src="{{asset('resources/assets/plugins/fancybox-2.1.7/source/helpers/jquery.fancybox-media.js?v=1.0.6')}}" type="text/javascript"></script>


<link href="{{asset('resources/assets/plugins/fancybox-2.1.7/source/jquery.fancybox.css?v=2.1.5')}}" type="text/css" rel="stylesheet"/>
<link href="{{asset('resources/assets/plugins/fancybox-2.1.7/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')}}" type="text/css" rel="stylesheet"/>
<link href="{{asset('resources/assets/plugins/fancybox-2.1.7/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7')}}" type="text/css" rel="stylesheet"/>
       <script type="text/javascript">
    $(document).ready(function() {
      /*
       *  Simple image gallery. Uses default settings
       */

      $('.fancybox').fancybox();

      /*
       *  Different effects
       */

      // Change title type, overlay closing speed
      $(".fancybox-effects-a").fancybox({
        helpers: {
          title : {
            type : 'outside'
          },
          overlay : {
            speedOut : 0
          }
        }
      });

      // Disable opening and closing animations, change title type
      $(".fancybox-effects-b").fancybox({
        openEffect  : 'none',
        closeEffect : 'none',

        helpers : {
          title : {
            type : 'over'
          }
        }
      });

      // Set custom style, close if clicked, change title type and overlay color
      $(".fancybox-effects-c").fancybox({
        wrapCSS    : 'fancybox-custom',
        closeClick : true,

        openEffect : 'none',

        helpers : {
          title : {
            type : 'inside'
          },
          overlay : {
            css : {
              'background' : 'rgba(238,238,238,0.85)'
            }
          }
        }
      });

      // Remove padding, set opening and closing animations, close if clicked and disable overlay
      $(".fancybox-effects-d").fancybox({
        padding: 0,

        openEffect : 'elastic',
        openSpeed  : 150,

        closeEffect : 'elastic',
        closeSpeed  : 150,

        closeClick : true,

        helpers : {
          overlay : null
        }
      });

      /*
       *  Button helper. Disable animations, hide close button, change title type and content
       */

      $('.fancybox-buttons').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',

        prevEffect : 'none',
        nextEffect : 'none',

        closeBtn  : false,

        helpers : {
          title : {
            type : 'inside'
          },
          buttons : {}
        },

        afterLoad : function() {
          this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
        }
      });


      /*
       *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
       */

      $('.fancybox-thumbs').fancybox({
        prevEffect : 'none',
        nextEffect : 'none',

        closeBtn  : false,
        arrows    : false,
        nextClick : true,

        helpers : {
          thumbs : {
            width  : 50,
            height : 50
          }
        }
      });

      /*
       *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
      */
      $('.fancybox-media')
        .attr('rel', 'media-gallery')
        .fancybox({
          openEffect : 'none',
          closeEffect : 'none',
          prevEffect : 'none',
          nextEffect : 'none',

          arrows : false,
          helpers : {
            media : {},
            buttons : {}
          }
        });

      /*
       *  Open manually
       */

      $("#fancybox-manual-a").click(function() {
        $.fancybox.open('1_b.jpg');
      });

      $("#fancybox-manual-b").click(function() {
        $.fancybox.open({
          href : 'iframe.html',
          type : 'iframe',
          padding : 5
        });
      });

      $("#fancybox-manual-c").click(function() {
        $.fancybox.open([
          {
            href : '1_b.jpg',
            title : 'My title'
          }, {
            href : '2_b.jpg',
            title : '2nd title'
          }, {
            href : '3_b.jpg'
          }
        ], {
          helpers : {
            thumbs : {
              width: 75,
              height: 50
            }
          }
        });
      });


    });
  </script>
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <!-- START WIDGET -->
            <div class="row-xs-height">
                <div class="col-xs-height">
                    <div class="p-l-20 p-r-20">
                        <div class="row">
                            <div class="col-lg-3 visible-xlg">
                                <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-xs-height">
                <div class="col-xs-height relative bg-master-lightest">
                    <div class="widget-14-chart_y_axis"></div>
                    <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                </div>
            </div>
            <!-- END WIDGET -->
        </div>

        <div id="rootwizard" class="m-t-50">

        <!-- Success Messages -->
            @include('partial/success_message')
        <!-- END Success Messages -->
        <!-- Error Messages -->
            @include('partial/error_message')
        <!-- END Error Messages -->
            <!-- START CONTAINER FLUID -->
            <div id="rootwizard" class="m-t-50">
                <!-- Nav tabs -->
                    @include('product.partial.product-nav')
                <!-- Tab panes -->
                <div class="tab-content">
                    <div>
                        <div class="row row-same-height">
                            <div class="panel-body">
                                <!-- START PANEL -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            Product : {{ $product->name }}
                                        </div>                                  
                                    </div>
                                    <div class="panel-body no-scroll no-padding" id="app">
                                        <product-gallery product_id="{{ $product->id }}"></product-gallery>
                                    </div>
                                </div>
                                <!-- END PANEL -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
    </div>
@endsection
@section('page-script')
  <script src="{{mix('js/app.js')}}" />
@endsection
@section('plugin-script')

@endsection


