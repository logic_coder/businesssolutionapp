@extends('layouts.master')
@section('style') 
  
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <!-- START WIDGET -->
            <div class="row-xs-height">
                <div class="col-xs-height">
                    <div class="p-l-20 p-r-20">
                        <div class="row">
                            <div class="col-lg-3 visible-xlg">
                                <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-xs-height">
                <div class="col-xs-height relative bg-master-lightest">
                    <div class="widget-14-chart_y_axis"></div>
                    <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                </div>
            </div>
            <!-- END WIDGET -->
        </div>

        <div id="rootwizard" class="m-t-50">
        <!-- Success Messages -->
            @include('partial/success_message')
        <!-- END Success Messages -->
        <!-- Error Messages -->
            @include('partial/error_message')
        <!-- END Error Messages -->
            <!-- START CONTAINER FLUID -->
            <div id="rootwizard" class="m-t-50">
                <!-- Nav tabs -->
                    @include('product.partial.product-nav')
                <!-- Tab panes -->
                <div class="tab-content">
                    <div>
                        <div class="row row-same-height">
                            <div class="panel-body">
                                <!-- START PANEL -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            Product : {{ $product->name }}
                                        </div>                                  
                                    </div>
                                    <div class="panel-body no-scroll no-padding" id="app">
                                        <product-gallery company_id="{{ Auth::user()->company_id }}" product_id="{{ $product->id }}" product_name="{{ $product->name }}"></product-gallery>
                                    </div>
                                </div>
                                <!-- END PANEL -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
    </div>
@endsection
@section('page-script')
  <!-- <script src="{{mix('/js/app.js')}}" /> -->
  <script defer src="{{ mix('js/app.js') }}"></script>  
@endsection

