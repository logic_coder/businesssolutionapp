<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Warehouse</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="wysiwyg5-wrapper b-a b-grey">
                            {{ Form::label('description', 'Description') }}
                            {{ Form::textarea('description',null, ['class' => 'form-control wysiwyg demo-form-wysiwyg', 'placeholder' => '']) }}
                        </div>
                    </div>
                    <div class="form-group-attached pull-right padding-top">
                        <button class="btn create-button btn-cons" type="submit">
                            <span>Save</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>