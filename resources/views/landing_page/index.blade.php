@extends('layouts.app')
@section('content')
    <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
            <!--
                <img src="{{ asset('images/logo.png') }}" alt="logo" data-src="{{ asset('images/logo.png') }}" width="78" height="22">
            -->
            <div class="tab-pane padding-20 active slide-left" id="app">
                <sign-up></sign-up>
            </div>            
            <!--END Login Form-->
        </div>
    </div>
@endsection
@section('page-script')
     <!--<script src="{{mix('/js/app.js')}}" /> -->
    <script defer src="{{ mix('js/app.js') }}"></script> 
@endsection

