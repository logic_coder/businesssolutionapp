<button class="btn delete-multi-items" action-url="supplier/remove_supplier" redirect-url="/supplier"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Code</th>
        <th style="width:20%">Address</th>
        <th style="width:20%">City</th>
        <th style="width:20%">Zip</th>
        <th style="width:20%">Country</th>
        <th style="width:20%">Email</th>
        <th style="width:20%">Phone</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach( $suppliers as $supplier )
        <tr>
            <td class="v-align-middle">
                <div class="checkbox ">
                    <input type="checkbox" name="item_ids[]" value={{$supplier->id}} data-id="checkbox" id="checkbox{{$supplier->id}}">
                    <label for="checkbox{{$supplier->id}}"></label>
                </div>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->name}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->code}} </p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->address}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->city}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->zip}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{ App\Country::find($supplier->country_id)->name}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->email}}</p>
            </td>
            <td class="v-align-middle">
                <p>{{$supplier->phone}}</p>
            </td>
            <td class="v-align-middle">
                <div class="btn-group btn-actions">
                    <a title="Edit" class="btn edit-button" href="{{url('supplier/'.$supplier->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                    <button title="Delete" item-ids="{{$supplier->id}}" type="button" class="btn create-button delete-single-item" action-url="supplier/remove_supplier" redirect-url="/supplier"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>