<button class="btn delete-multi-items" action-url="company_currency/remove_company_currency" redirect-url="/company_currency"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Default</th>
        <th style="width:20%">Last Updated</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @if($company_currencies)
        @foreach( $company_currencies as $company_currency )
            <tr id="row_{{$company_currency->id}}">
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input type="checkbox" name="item_ids[]" value={{$company_currency->id}} data-id="checkbox" id="checkbox{{$company_currency->id}}">
                        <label for="checkbox{{$company_currency->id}}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    @php
                        $currency = App\Currency::find($company_currency->currency_id);
                        echo $currency->name."(".$currency->code.")";
                    @endphp
                </td>
                <td class="v-align-middle">
                    <input class="product_add_btn" type="checkbox" data-init-plugin="switchery" @php if($company_currency) { if( $company_currency->default=="Yes" ) echo 'checked'; } @endphp name="default"/>
                </td>
                <td class="v-align-middle">
                    <p>{{ date('F d, Y H:i:s', strtotime($company_currency->updated_at)) }}</p>
                </td>
                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <a title="Edit" class="btn edit-button" href="{{url('company_currency/'.$company_currency->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        <button title="Delete" item-ids="{{$company_currency->id}}" type="button" class="btn create-button delete-single-item" action-url="company_currency/remove_company_currency" redirect-url="/company_currency"><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>