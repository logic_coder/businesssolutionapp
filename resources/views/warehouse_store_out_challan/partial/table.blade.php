<button class="btn delete-multi-items" action-url="warehouse_store_out_challan/remove_warehouse_store_out_challan" redirect-url="/warehouse_store_out_challan"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Challan No</th>
        <th style="width:20%">To Outlet</th>
        <th style="width:20%">From Warehouse</th>
        <th style="width:20%">Product</th>
        <th style="width:20%">Created By</th>
        <th style="width:20%">Request Date</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $store_out_challans as $store_out_challan )
        <tr>
            <td class="v-align-middle">
                @if($store_out_challan->status !='Received')
                    <div class="checkbox ">
                        <input class="item_ids" type="checkbox" name="item_ids[]" value={{$store_out_challan->id}} data-id="checkbox" id="checkbox{{$store_out_challan->id}}">
                        <label for="checkbox{{$store_out_challan->id}}"></label>
                    </div>
                @endif
            </td>
            <td class="v-align-middle">
                {{ $store_out_challan->challan_no }}
            </td>
            <td class="v-align-middle">
                @if(App\OutLet::find($store_out_challan->out_let_id))
                    <span>{{ App\OutLet::find($store_out_challan->out_let_id)->name }}</span>
                @else
                    <span>N/A</span>
                @endif
            </td>
            <td class="v-align-middle">
                @if(App\Warehouse::find($store_out_challan->warehouse_id))
                    <span>{{ App\Warehouse::find($store_out_challan->warehouse_id)->name }}</span>
                @else
                    <span>N/A</span>
                @endif
            </td>
            <td class="v-align-middle">
                <ul>
                    @if($store_out_challan->challanProducts)
                        @foreach( $store_out_challan->challanProducts as $challan_product )
                            <li>
                            @php
                                $product_details = App\Product::find($challan_product->product_id);
                                if($product_details){
                                   echo $product_details->name;
                                }
                            @endphp
                            </li>
                        @endforeach
                    @endif
                </ul>
            </td>
            <td class="v-align-middle">
                @php
                    $user = App\User::with('userDetail')->where('id', $store_out_challan->created_by )->get();
                    if($user[0]->userDetail){
                        echo $user[0]->userDetail->first_name ." ". $user[0]->userDetail->last_name;
                    }
                @endphp
            </td>
            <td class="v-align-middle">
                {{ $store_out_challan->request_date }}
            </td>
            <td class="v-align-middle">
                {{ $store_out_challan->status }}
            </td>
            @if($store_out_challan->status !='Received')
                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <a title="Edit" class="btn edit-button" href="{{url('warehouse_store_out_challan/'.$store_out_challan->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        <button title="Delete" item-ids="{{$store_out_challan->id}}" type="button" class="btn create-button delete-single-item" action-url="warehouse_store_out_challan/remove_warehouse_store_out_challan" redirect-url="/warehouse_store_out_challan"><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            @else 
                <td class="v-align-middle">
                    <span> N/A </span>
                </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>