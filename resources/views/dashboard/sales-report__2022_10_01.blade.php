@extends('layouts.invoice_master')
@section('content')
@php
  $total_discount = 0;
  $total_advance = 0;
  $total_net_sum = 0;
  $total = 0;
@endphp
  <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
      <!-- START card -->
      <div class="card card-default m-t-20">
        <div class="card-body">
          <!-- Define header and footer blocks before your content -->
          <header>
              <p>{{ $report_for }} </p>
              <p>{{ $report_for_date }}</p>
              <p>{{ $report_for_month }}</p> 
          </header>                          
          <div class="invoice padding-50 sm-padding-10" style="padding-left:unset !important;">
            <div class="row">
              <div class="col-lg-3" style="padding-left:5px !important;">
                <img style="width: 200px;margin-bottom: 20px" alt="" class="invoice-logo" src="{{ asset('uploads/company/'.$company_details->id.'/receipt_logo/'.$company_details->receipt_logo)}}">            
              </div>
              <div class="col-lg-8">                
                <address class="m-t-10">
                  @if($out_let)
                    {{ $out_let[0]->name }}
                  <br>{{ $out_let[0]->location }}
                  <br>{{ $out_let[0]->address }}
                  <br>{{ $out_let[0]->city }} , {{ $out_let[0]->zip }}
                  <br>Contact: {{ $out_let[0]->phone }}
                  <br>
                  @endif
                </address>
              </div>                 
            </div>  
            <div class="clearfix"></div>            
            <div class="table-responsive table-invoice">
              <table class="table m-t-25">
                <thead>
                  <tr>
                    <th class="text-center">Date</th>
                    <th class="text-center">Invoice</th>
                    <th class="text-center">Customer</th>
                    <th class="text-center">Total</th>  
                    <th class="text-center">Discount</th>
                    <th class="text-center">Net.Sum</th>  
                    <th class="text-center">Advance</th> 
                    <th class="text-center">Remain</th>  
                    <th class="text-center">Currency</th>
                  </tr>
                </thead>
                <tbody>
                  @if( $sales )
                    @foreach( $sales as $sale )
                      <tr>
                        <td class="text-center">
                          {{ $sale->created_at }}
                        </td>
                        <td class="text-center">
                          <p class="text-black">
                            @if(isset($sale->invoice_prefix_id))
                            {{ 
                              App\InvoicePrefix::find($sale->invoice_prefix_id)->name    
                            }}-{{ $sale->invoice_no 
                             }}                                                    
                            @else
                            {{ $sale->invoice_no }}
                            @endif
                          </p>
                        </td>
                        <td class="text-center">
                          @if(isset($sale->customer_id))
                            {{ App\Customer::find($sale->customer_id)->first_name }} 
                            {{ App\Customer::find($sale->customer_id)->last_name }}
                            ( {{ App\Customer::find($sale->customer_id)->customer_no}} )
                          @else
                            Guest
                          @endif
                        </td>
                        <td class="text-center">
                          @php
                            $total = $sale->sub_total + $sale->vat_amount;
                            echo  number_format($total);
                          @endphp
                        </td>
                        <td class="text-center">
                          @php
                            $total_discount = number_format($total_discount + ($sale->discount_amount +  $sale->coupon_discount_amount));
                            echo  number_format($sale->discount_amount +  $sale->coupon_discount_amount);
                          @endphp
                        </td>
                        <td class="text-center">
                          @php 
                            echo number_format($sale->net_amount);                    
                          @endphp
                        </td>
                        <td class="text-center">
                          @if(isset($sale->id))                          
                           @php
                              $advance = App\SalePaid::where('sale_id', $sale->id)->sum('paid_amount');                              
                              if($advance <= $sale->net_amount){
                                echo number_format($advance); 
                              }else{
                                echo number_format($sale->net_amount); 
                                $advance = $sale->net_amount;
                              }
                              $total_advance = $total_advance + $advance;
                              
                            @endphp                                                    
                          @endif
                        </td>
                        <td class="text-center">
                          @php 
                            echo number_format(max(($sale->net_amount - App\SalePaid::where('sale_id', $sale->id)->sum('paid_amount')), 0)); 
                            $total_net_sum = $total_net_sum + $sale->net_amount;                   
                          @endphp
                        </td>                        
                        <td class="text-center">
                          @if(isset($sale->company_currency_id))
                            {{ 
                              App\Currency::find(App\CompanyCurrency::find($sale->company_currency_id)->currency_id)->code                              
                            }} 
                          @else
                            N/A
                          @endif
                        </td>
                      </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div> 
            <br>                          
            <div class="panel-heading title-color">
              <table class="table table-hover table-heading" id="tableWithSearch">
                  <tbody class="table-body">
                      <tr>
                          <td class="v-align-middle">
                            <h5 class="font-montserrat all-caps small hint-text semi-bold">Discount: BDT 
                            @php
                              echo number_format($total_discount);
                            @endphp</h5>
                          </td>                          
                          <td class="v-align-middle">
                            <h5 class="font-montserrat all-caps small hint-text semi-bold">Advance: BDT  
                            @php 
                                echo number_format($total_advance); 
                            @endphp
                            </h5>
                          </td>
                          <td class="v-align-middle">
                             <h5 class="font-montserrat all-caps small hint-text semi-bold">Remain Sum: BDT 
                             @php 
                                echo number_format(max(($total_net_sum - $total_advance), 0));
                             @endphp
                             </h5>
                          </td>
                          <td class="v-align-middle">
                            <h5 class="font-montserrat all-caps small hint-text bold">Net. Sum: BDT 
                              @php 
                                echo number_format($total_net_sum); 
                              @endphp
                            </h5>
                          </td>
                      </tr>
                  </tbody>
              </table>
            </div><br/>          
            <div class="stamp-signature"> 
              <div class="authority-signature">
                Printed By: {{ Auth::user()->userDetail ? Auth::user()->userDetail->first_name : '' }} {{ Auth::user()->userDetail ? Auth::user()->userDetail->last_name : '.......................' }} 
              </div>              
              <div class="customer-signature">
                Authorized By: .......................
              </div>
            </div>
            <div class="clear"></div>
            <br>
            <br>
            <div> 
                <span class="bold hint-text">Notes: </span>
                <span class="small hint-text">This report generated on Date: {{ $report_generated_on }}. Please issue company stamp and sign in above section.
              </span>
            </div>
            
            <div class="footer-border" style="width: 100% !important;"></div>
            <footer> 
              <div class="footer-logo">
                 <img style="width: 80px; height: 22px;" alt="" class="invoice-logo" src="{{ asset('uploads/company/'.$company_details->id.'/receipt_logo/50x50/'.$company_details->receipt_logo)}}">
              </div>
              @if($out_let)
                <div class="footer-info-text">
                  | <span class="m-l-70 text-black sm-pull-right"> {{ $out_let[0]->name }} </span>
                  | <span class="m-l-70 text-black sm-pull-right"> 
                      {{ $out_let[0]->phone }}</span> <br/><br/>
                      <span class="m-l-70 text-black sm-pull-right"> Software By: {{ env('APP_URL')}}</span><br/>
                </div>
              @endif               
            </footer>
          </div>
        </div>
      </div>
      <!-- END card -->
    </div>
  <!-- END CONTAINER FLUID -->
@endsection