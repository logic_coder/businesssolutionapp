@extends('layouts.invoice_master')
@section('content')
@php
  $total_discount = 0;
  $total_advance = 0;
  $total_net_sum = 0;
  $total = 0;
@endphp  
  <!-- START CONTAINER FLUID -->    
    <div class=" container-fluid   container-fixed-lg">
      <div class="row" style="padding-bottom: 5%;">
        <!-- Define header and footer blocks before your content -->
        <header>            
            <center> 
              <div><b> {{ $report_for }} </b></div>
              <div>{{ $report_for_date }}</div>
              <div>{{ $report_for_month }}</div> 
            </center>      
        </header> 
      </div>
      <div class="row">
        <!-- START card -->
        <div class="card card-default m-t-20">
          <div class="card-body"> 
            <div class="invoice padding-50 sm-padding-10" style="padding-left:unset !important;">           
              <div class="row justify-content-center" >
                <div class="col-lg-4" >
                    <div class="header-left ml-4 text-left" v-if="out_let_details && company_details" style="float:left">
                        <img style="width: 200px;margin-bottom: 20px" alt="" class="invoice-logo" src="{{ public_path().'/uploads/company/'.$company_details->id.'/receipt_logo/'.$company_details->receipt_logo}}">
                    </div>
                    <div class="moha_add_inv" style="margin-left:30%">
                         @if(count($out_let))
                          <p style="line-height:.5;">
                              <b>Outlet : {{ $out_let[0]->name }}</b>
                          </p>
                          <p style="line-height:.5;font-size:13px;">
                             {{ $out_let[0]->location }}
                          </p>
                          <p style="line-height:.5;font-size:13px;">
                             {{ $out_let[0]->address }}
                          </p>
                          <p style="line-height:.5;font-size:13px;">
                            {{ $out_let[0]->city }} , {{ $out_let[0]->zip }}
                          </p>
                          <p style="line-height:.5;font-size:13px;">
                              {{ $out_let[0]->phone }}
                          </p>    
                        @else
                          <p style="line-height:.5;">
                            <b>Company : {{ $company_details->name }}</b>                            
                          </p>
                           <p style="line-height:.5;font-size:13px;">
                             {{ $company_details->address }}
                          </p>                          
                          <p style="line-height:.5;font-size:13px;">
                            {{ $company_details->city }} , {{ $company_details->zip }}
                          </p>
                          <p style="line-height:.5;font-size:13px;">
                              {{ $company_details->phone }}
                          </p> 
                        @endif    

                    </div>
                </div>
               <!-- <div class="col-lg-6 " style="float:right">
                    <div class="moha_add_inv pull-right" >
                         @if(count($out_let))
                          <p style="line-height:.5;">
                              <b>{{ $out_let[0]->name }}</b>
                          </p>
                          <p style="line-height:.5;font-size:13px;">
                             {{ $out_let[0]->location }}
                          </p>
                          <p style="line-height:.5;font-size:13px;">
                             {{ $out_let[0]->address }}
                          </p>
                          <p style="line-height:.5;font-size:13px;">
                            {{ $out_let[0]->city }} , {{ $out_let[0]->zip }}
                          </p>
                          <p style="line-height:.5;font-size:13px;">
                              {{ $out_let[0]->phone }}
                          </p>    
                        @endif    
                    </div>
                </div> -->
              </div>
              <div class="clearfix" style="clear: both;"></div>  
              @if( count($sales )) 
                <div class="row">         
                  <div class="table-responsive table-invoice">
                    <table class="table m-t-25">
                      <thead>
                        <tr>
                          <th class="" style="padding: 5px;">Date</th>
                          <th class="text-center">Invoice</th>
                          <th class="text-center">Customer</th>
                          <th class="text-center">Total</th>  
                          <th class="text-center">Discount</th>
                          <th class="text-center">Net.Sum</th>  
                          <th class="text-center">Advance</th> 
                          <th class="text-center">Remain</th>  
                          <th class="text-center">Currency</th>
                        </tr>
                      </thead>
                      <tbody>                    
                        @foreach( $sales as $sale )
                          <tr>
                            <td class="text-center" style="padding: 3px;">
                              {{ $sale->created_at }}
                            </td>
                            <td class="text-center" style="padding: 3px;">
                              <p class="text-black">
                                @if(isset($sale->invoice_prefix_id))
                                {{ 
                                  App\InvoicePrefix::find($sale->invoice_prefix_id)->name    
                                }}-{{ $sale->invoice_no 
                                 }}                                                    
                                @else
                                {{ $sale->invoice_no }}
                                @endif
                              </p>
                            </td>
                            <td class="text-center" style="padding: 3px;">
                              @if(isset($sale->customer_id))
                                {{ App\Customer::find($sale->customer_id)->first_name }} 
                                {{ App\Customer::find($sale->customer_id)->last_name }}
                                ( {{ App\Customer::find($sale->customer_id)->customer_no}} )
                              @else
                                Guest
                              @endif
                            </td>
                            <td class="text-center" style="padding: 3px;">
                              @php
                                $total = $sale->sub_total + $sale->vat_amount;
                                echo  number_format($total,2);
                              @endphp
                            </td>
                            <td class="text-center" style="padding: 3px 0px 3px 21px;">
                              @php
                                $total_discount = number_format($total_discount + ($sale->discount_amount +  $sale->coupon_discount_amount), 2);
                                echo  number_format($sale->discount_amount +  $sale->coupon_discount_amount, 2);
                              @endphp
                            </td>
                            <td class="text-center" style="padding: 3px 0px 3px 6px;">
                              @php 
                                echo number_format($sale->net_amount, 2);                    
                              @endphp
                            </td>
                            <td class="text-center" style="padding: 3px 0px 3px 21px;">
                              @if(isset($sale->id))                          
                               @php
                                  $advance = App\SalePaid::where('sale_id', $sale->id)->sum('paid_amount');                              
                                  if($advance <= $sale->net_amount){
                                    echo number_format($advance,2); 
                                  }else{
                                    echo number_format($sale->net_amount,2); 
                                    $advance = $sale->net_amount;
                                  }
                                  $total_advance = $total_advance + $advance;
                                  
                                @endphp                                                    
                              @endif
                            </td>
                            <td class="text-center" style="padding: 3px 0px 3px 21px;">
                              @php 
                                echo number_format(max(($sale->net_amount - App\SalePaid::where('sale_id', $sale->id)->sum('paid_amount')), 0), 2); 
                                $total_net_sum = $total_net_sum + $sale->net_amount;                   
                              @endphp
                            </td>                        
                            <td class="text-center" style="padding: 3px 0px 3px 21px;">
                              @if(isset($sale->company_currency_id))
                                {{ 
                                  App\Currency::find(App\CompanyCurrency::find($sale->company_currency_id)->currency_id)->code                              
                                }} 
                              @else
                                N/A
                              @endif
                            </td>
                          </tr>
                        @endforeach                    
                      </tbody>
                    </table>
                  </div> 
                </div>   
                <div class="row">                       
                  <div class="panel-heading title-color">
                  <table class="table table-hover table-heading" id="tableWithSearch">
                      <tbody class="table-body">
                          <tr>
                              <td class="v-align-middle" style="padding: 3px; font-size: 18px;">
                                <h5 class="font-montserrat all-caps small hint-text semi-bold">Discount: BDT 
                                @php
                                  echo number_format($total_discount, 2);
                                @endphp</h5>
                              </td>                          
                              <td class="v-align-middle" style="padding: 3px; font-size: 18px;">
                                <h5 class="font-montserrat all-caps small hint-text semi-bold">Advance: BDT  
                                @php 
                                    echo number_format($total_advance, 2); 
                                @endphp
                                </h5>
                              </td>
                              <td class="v-align-middle" style="padding: 3px; font-size: 18px;">
                                 <h5 class="font-montserrat all-caps small hint-text semi-bold">Remain Sum: BDT 
                                 @php 
                                    echo number_format(max(($total_net_sum - $total_advance), 0), 2);
                                 @endphp
                                 </h5>
                              </td>
                              <td class="v-align-middle" style="padding: 3px; font-size: 18px;">
                                <h5 class="font-montserrat all-caps small hint-text bold">Net. Sum: BDT 
                                  @php 
                                    echo number_format($total_net_sum, 2); 
                                  @endphp
                                </h5>
                              </td>
                          </tr>
                      </tbody>
                  </table>
                  </div>
                </div> 
                @else
                  <div class="row" style="padding: 15px 0px 25px 0px;">
                    No sales available for this period
                  </div>
                @endif 
                <footer> 
                  <div class="row">      
                    <div class="stamp-signature"> 
                    @if(\App\User::find($user_id)->userDetail)
                      <div class="authority-signature">
                        Printed By: 
                        {{  \App\User::find($user_id)->userDetail->first_name ?  \App\User::find($user_id)->userDetail->first_name : '' }} {{ \App\User::find($user_id)->userDetail->last_name ? \App\User::find($user_id)->userDetail->last_name : '.......................' }} 
                      </div> 
                    @endif    
                  </div>            
                  <div class="row" style="padding-top: 15px;">          
                    <div class="customer-signature">
                      Authorized By: .......................
                    </div>
                    </div>
                  </div>
                  <div class="clear"></div>
                  <br>
                  <br>
                  <div class="row">    
                    <div> 
                      <span class="bold hint-text">Notes: </span>
                      <span class="small hint-text">This report generated on Date: {{ $report_generated_on }}. Please issue company stamp and sign in above section.
                      </span>
                    </div>
                  </div>
                  <div class="footer-logo">
                     <img style="width: 80px; height: 22px;" alt="" class="invoice-logo" src="{{ public_path().'/uploads/company/'.$company_details->id.'/receipt_logo/50x50/'.$company_details->receipt_logo }}">
                  </div>                  
                    <div class="footer-info-text">
                      @if(count($out_let))
                      | <span class="m-l-70 text-black sm-pull-right"> {{ $out_let[0]->name }} </span>
                      | <span class="m-l-70 text-black sm-pull-right"> {{ $out_let[0]->phone }} </span> 
                      @endif <br/><br/>
                      <span class="m-l-70 text-black sm-pull-right"> Software By: {{ env('APP_URL')}}</span><br/>
                    </div>                                
                </footer>
            </div>
          </div>
        </div>
      </div>
      <!-- END card -->
    </div>
  <!-- END CONTAINER FLUID -->
@endsection