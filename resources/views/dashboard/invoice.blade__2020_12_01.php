@extends('layouts.invoice_master')
@section('content')
  <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">
      <!-- START card -->
      <div class="card card-default m-t-20">
        <div class="card-body">
          <div class="invoice padding-50 sm-padding-10">
            <div>
              <div class="pull-left">
                <img width="235" height="70" alt="" class="invoice-logo" data-src-retina="{{ asset('uploads/company/1/product_images/1/file_1_2020-11-20 14:02:20.png')}}" data-src="{{ asset('uploads/company/1/product_images/1/file_1_2020-11-20 14:02:20.png')}}" src="{{ asset('uploads/company/1/product_images/1/file_1_2020-11-20 14:02:20.png')}}">
                <address class="m-t-10">
                  Apple Enterprise Sales
                  <br>(877) 412-7753.
                  <br>
                </address>
              </div>
              <div class="pull-right sm-m-t-20">
                <h2 class="font-montserrat all-caps hint-text">Invoice</h2>
              </div> 
            </div>  
            <div class="clearfix"></div>
            <div class="col-12">
              <div class="row">
                <div class="col-lg-9 col-sm-height sm-no-padding">
                  <p class="small no-margin">Invoice to</p>
                  <h5 class="semi-bold m-t-0">Darren Forthway</h5>
                  <address class="m-t-10">
                      <strong>Pages Incoperated.</strong>
                       <br/>page.inc
                       <br/>1600 Amphitheatre Pkwy, Mountain View,
                       <br/>CA 94043, United States
                  </address>
                </div>
                <div class="col-lg-3 sm-p-b-20 d-flex align-items-end justify-content-between invoice-no-date">
                  <div>
                    <div class="font-montserrat bold all-caps">Invoice No : <span class="invoice-no-date-text"> 0047 </span> </div>
                    <div class="font-montserrat bold all-caps">Invoice date : <span class="invoice-no-date-text">29/09/2014 </span></div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>

            <div class="table-responsive table-invoice">
              <table class="table m-t-50">
                <thead>
                  <tr>
                    <th class="">Task Description</th>
                    <th class="text-center">Rate</th>
                    <th class="text-center">Hours</th>
                    <th class="text-center">Qty</th>
                    <th class="">Total</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="">
                      <p class="text-black">Character Illustration</p>
                      <p class="small hint-text">
                        Character Design projects from the latest top online portfolios on Behance.
                      </p>
                    </td>
                    <td class="text-center">$65.00</td>
                    <td class="text-center">84</td>
                    <td class="text-center">5pcs</td>
                    <td class="text-center">$5,376.00</td>
                  </tr>
                  <tr>
                    <td class="">
                      <p class="text-black">Cross Heart Charity Branding</p>
                      <p class="small hint-text">
                        Attempt to attach higher credibility to a new product by associating it with a well established company name
                      </p>
                    </td>
                    <td class="text-center">$89.00</td>
                    <td class="text-center">100</td>
                    <td class="text-center">5pcs</td>
                    <td class="text-center">$8,900.00</td>
                  </tr>
                  <tr>
                    <td class="">
                      <p class="text-black">iOs App</p>
                      <p class="small hint-text">
                        A video game franchise Inspired primarily by a sketch of stylized wingless - Including Branding / Graphics / Motion Picture &amp; Videos
                      </p>
                    </td>
                    <td class="text-center">$100.00</td>
                    <td class="text-center">500</td>
                    <td class="text-center">5pcs</td>
                    <td class="text-center">$50,000.00</td>
                  </tr>
                  <tr>
                    <td class="">
                      <p class="text-black">iOs App</p>
                      <p class="small hint-text">
                        A video game franchise Inspired primarily by a sketch of stylized wingless - Including Branding / Graphics / Motion Picture &amp; Videos
                      </p>
                    </td>
                    <td class="text-center">$100.00</td>
                    <td class="text-center">500</td>
                    <td class="text-center">5pcs</td>
                    <td class="text-center">$50,000.00</td>
                  </tr>
                </tbody>
              </table>
            </div> 
            <br>                          
            <div class="panel-heading title-color">
              <table class="table table-hover table-heading" id="tableWithSearch">
                  <tbody class="table-body">
                      <tr>
                          <td class="v-align-middle">
                            <h5 class="font-montserrat all-caps small hint-text semi-bold">Discount: BDT 25888</h5>
                          </td>                          
                          <td class="v-align-middle">
                            <h5 class="font-montserrat all-caps small hint-text semi-bold">Advance: BDT 545445</h5>
                          </td>
                          <td class="v-align-middle">
                             <h5 class="font-montserrat all-caps small hint-text semi-bold">Remain Sum: BDT 545445</h5>
                          </td>
                          <td class="v-align-middle">
                            <h5 class="font-montserrat all-caps small hint-text bold">Net. Sum: BDT 545445</h5>
                          </td>
                      </tr>
                  </tbody>
              </table>
            </div>            
            <div class="stamp-signature"> 
              <div class="authority-signature">
                Authority Signature
              </div>
              <div class="customer-signature">
                Customer Signature
              </div>
            </div>
            <div class="clear"></div>
            <br>
            <br>
            <footer>
              Copyright &copy;          
              <div> 
                <span class="bold hint-text">Terms: </span>
                <span class="small hint-text">Services will be invoiced in accordance with the Service Description. You must pay all undisputed invoices in full within 30 days of the invoice</span>
              </div>
              <br>            
              <div class="footer-logo">
                 <img width="78" height="22" alt="" data-src-retina="{{ asset('uploads/company/1/product_images/1/file_1_2020-11-22 09:36:59.png')}}" data-src="{{ asset('uploads/company/1/product_images/1/file_1_2020-11-22 09:36:59.png')}}" src="{{ asset('uploads/company/1/product_images/1/file_1_2020-11-22 09:36:59.png')}}">
              </div>
              <div class="footer-info-text">
                <span class="m-l-70 text-black sm-pull-right">+34 346 4546 445 &nbsp;</span>
                <span class="m-l-40 text-black sm-pull-right">&nbsp; support@revox.io</span>
              </div>
            </footer>
          </div>
        </div>
      </div>
      <!-- END card -->
    </div>
  <!-- END CONTAINER FLUID -->
@endsection