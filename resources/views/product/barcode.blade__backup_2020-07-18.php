@extends('layouts.master')
@section('style')
    <link href="{{asset('backend_old')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('theme/backend/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{asset('theme/backend/assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet"
          type="text/css" media="screen"/>
@stop
@section('content')
    <div class="container-fluid container-fixed-lg">
        <!--Start Search Product -->
        <div class="row row-same-height">
            <div class="panel panel-default">
                <div class="panel-heading title-color">
                    <h4 class="panel-title">Search Challan</h4>
                </div>
                <div class="panel-body">
                    <div class="col-md-7 col-div-full">
                        <div class="padding-30">
                            <div class="form-group-attached">
                                <div class="row clearfix">
                                    <div class="col-sm-6 col-div-33">
                                        <div class="form-group form-group-default">
                                            <label for="search_challan">Search Challan</label>
                                            <input name="challan_no" class ="form-control" placeholder="challan no" type="text" >
                                        </div>                                       
                                    </div>
                                    <div class="col-sm-6 col-div-33">
                                        <div class="form-group form-group-default required">
                                            <label for="request_date">Request Date</label>
                                            <input name="request_date" id="request_date" class ="form-control" placeholder="Select Date" type="text" >      
                                        </div>
                                    </div>                                    
                                    <div class="col-sm-6 col-div-33">
                                        <div class="form-group form-group-default">
                                            <div class="btn-group btn-challan-product-div">
                                                <a title="product-search" class="btn btn-success btn-cons btn-challan-product-search">Search</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Search Product-->        
        <!-- Success Messages -->
        @include('partial/success_message')
        <!-- END Success Messages -->
            <!-- Error Messages Section-->
        @include('partial/error_message')
        <!-- END Error Messages -->
            <!-- START CONTAINER FLUID -->
            <div class="container-fluid container-fixed-lg bg-white">
                <!-- START PANEL -->
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <div class="panel-title panel-title-margin">Product Bar Code</div><br/>
                    </div>
                    <div class="panel-body">
                        @php
                        use \Milon\Barcode\DNS1D;
                        $barcodeObj = new DNS1D();
                        foreach( $products as $product ){ @endphp
                            <div class="col-md-4 bar-code-content">
                                <p class="bar-code-name">{{$product->name}}</p>
                                <p class="bar-code-name-price">Price:
                                    @php
                                        foreach( $product->prices as $price ){
                                            echo $price->price;
                                        }
                                    @endphp
                                </p>
                                <div class="bar-code-label">{!! $barcodeObj->getBarcodeHTML($product->bar_code, "C128",1.4,22) !!}</div>
                                <p class="bar-code-item-code">{{$product->item_code}}</p>
                            </div>   
                        @php                                           
                        }
                        @endphp 
                    </div>
                </div>
                <!-- END PANEL -->
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
    </div>
@endsection

@section('plugin-script')
    <script src="{{asset('backend_old') }}"
            type="text/javascript"></script>
    <script src="{{asset('js_') }}"
            type="text/javascript"></script>
    <script src="{{asset('theme/backend/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="{{asset('theme/backend/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{asset('theme/backend/assets/plugins/datatables-responsive/js/lodash.min.js') }}"></script>
@endsection

@section('page-script')
    <script src="{{asset('theme/backend/assets/js/datatables.js')}}" type="text/javascript"></script>
@endsection

