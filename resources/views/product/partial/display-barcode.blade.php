<!--Start Search Product -->
    <div class="row row-same-height">
        <div class="panel panel-default">
            <div class="panel-heading title-color">
                <h4 class="panel-title">Generate Product Barcode For The Date Added</h4>
            </div>  
            {{ Form::open(['url' => ['product/generateBarcode'],'method' => 'post', 'id' => 'form-project']) }}      
            <div class="panel-body">
                <div class="col-md-7 col-div-full">
                    <div class="padding-30">
                        <div class="form-group-attached">
                            <div class="row clearfix">          
                                <div class="col-sm-6 col-div-33">
                                    <div class="form-group form-group-default">
                                        {{ Form::label('from_date', 'From Date') }}
                                        {{ Form::date('from_date',  \Carbon\Carbon::now()->format('Y-m-d'), ['class' => 'form-control', 'required']) }}
                                    </div> 
                                </div>
                                <div class="col-sm-6 col-div-33">
                                    <div class="form-group form-group-default">
                                        {{ Form::label('to_date', 'To Date') }}
                                        {{ Form::date('to_date',  \Carbon\Carbon::now()->format('Y-m-d'), ['class' => 'form-control', 'required']) }}
                                    </div>
                                </div>
                                <div class="col-sm-6 col-div-33">
                                <div class="form-group form-group-default">
                                    <div class="btn-group btn-challan-product-div">
                                        <button title="product-search" class="btn btn-success btn-cons btn-challan-product-search">Generate</button>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
        <div class="panel panel-default">
            <div class="panel-heading title-color">
                <h4 class="panel-title">Generate Product Barcode For The Product/Item Code</h4>
            </div>
            {{ Form::open(['url' => ['product/generateBarcode'],'method' => 'post', 'id' => 'form-project']) }}  
            <div class="panel-body">
                <div class="col-md-7 col-div-full">
                    <div class="padding-30">
                        <div class="form-group-attached">
                            <div class="row clearfix">
                                <div class="col-sm-6 col-div-33">
                                    <div class="form-group form-group-default">
                                        {{ Form::label('product', 'Product') }}
                                        {{ Form::text('name', request()->name, ['class' => 'form-control', '']) }}
                                    </div>
                                </div>
                                <div class="col-sm-6 col-div-33">
                                    <div class="form-group form-group-default">
                                        {{ Form::label('item_code', 'Item Code') }}
                                        {{ Form::text('item_code', request()->item_code, ['class' => 'form-control', '']) }}
                                    </div>                                  
                                </div>                                    
                                <div class="col-sm-6 col-div-33">
                                <div class="form-group form-group-default">
                                    <div class="btn-group btn-challan-product-div">
                                        <button title="product-search" class="btn btn-success btn-cons btn-challan-product-search">Generate</button>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
<!--End Search Product-->   
<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Proudct Barcode</h4>
        </div>
        <div class="panel-body">
            @php
            use \Milon\Barcode\DNS1D;
            $barcodeObj = new DNS1D();
            if(sizeof($products) > 0 ){
                foreach( $products as $product ){ @endphp
                    <div class="col-md-4 bar-code-content">
                        <span class="bar-code-name">{{$product->name}}</span>
                        <span class="bar-code-name-price">Price:
                            @php
                                foreach( $product->prices as $price ){
                                    echo $price->price;
                                }
                            @endphp
                        </span>
                        <div class="bar-code-label">{!! $barcodeObj->getBarcodeHTML($product->bar_code, "C128",1.4,22) !!}</div>
                        <p class="bar-code-item-code">{{$product->item_code}}</p>
                    </div>   
                @php                                           
                }
            }
            @endphp 
        </div>
    </div>    
</div>