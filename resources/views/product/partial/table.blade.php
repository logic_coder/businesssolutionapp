<button class="btn delete-multi-items" action-url="product/remove_product" redirect-url="/product"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Image</th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Item Code</th>
        <th style="width:20%">Bar Code</th>
        <th style="width:20%">Category</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach( $products as $product )
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input type="checkbox" name="item_ids[]" value={{ $product->id }} data-id="checkbox" id="checkbox{{ $product->id }}">
                        <label for="checkbox{{ $product->id }}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    {{ $product->name }}
                </td>
                <td class="v-align-middle">
                    {{ $product->name }}
                </td><td class="v-align-middle">
                    {{ $product->item_code }}
                </td><td class="v-align-middle">
                    {{ $product->bar_code }}
                </td>
                <td class="v-align-middle">
                    <ul>
                        @if( $product->categories )
                            @foreach( $product->categories as $category )
                                <li> {{ $category->name  }} </li>
                            @endforeach
                        @endif
                    </ul>
                </td>
                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <a title="Edit" class="btn edit-button" href="{{url('product/'.$product->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        <button title="Delete" item-ids="{{$product->id}}" type="button" class="btn create-button delete-single-item" action-url="product/remove_product" redirect-url="/product"><i class="fa fa-trash-o"></i></button>
                        <a title="product_price" class="btn product_add_btn btn-success" href="{{url('product/'.$product->id.'/product_price')}}">Product Price</a>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
