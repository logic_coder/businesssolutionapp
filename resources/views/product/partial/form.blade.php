
<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Product Basic</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-product-left-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('item_code', 'Item Code') }}
                            {{ Form::text('item_code',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-group form-group-default">
                            {{ Form::label('reference_code', 'Reference Code') }}
                            {{ Form::text('reference_code',null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-product-right-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">Category</div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div id="check-tree" product-id="{{ $product ? $product->id : '' }}" class="tree-category col-md-6" tree-type-attr="check-tree"></div>
                                </div>
                                {{ Form::hidden('product_categories', '', ['id' => 'product_categories']) }}
                                {{--By default parent id: 1, as system default category is 'Home'--}}
                                {{ Form::hidden('parent', $category_parent, ['id' => 'parent']) }}
                                {{ Form::hidden('this_category', $category ? $category->id: '', ['id' => 'this_category']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button class="btn create-button btn-cons pull-right" type="submit">
        <span>Save</span>
    </button>
</div>
