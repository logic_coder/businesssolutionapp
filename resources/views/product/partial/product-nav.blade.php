<ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm">    
    <li @if( $tab_active == 'product' ) class="active" @endif >
        @if( $product )
            <a href="{{url('product/'.$product->id.'/edit')}}">
        @else <a href="#">
        @endif
            <i class="fa tab-icon"></i> <span>Your Product</span>
        </a>
    </li>
    @if( $product )
        <li @if( $tab_active == 'media' ) class="active" @endif>
            <a href="{{url('product/'.$product->id.'/product_image')}}"><i class="fa tab-icon"></i> <span>Media</span></a>
        </li>
        @can('access-product-prices')
            <li @if( $tab_active == 'price' ) class="active" @endif>
                <a href="{{url('product/'.$product->id.'/product_price')}}"><i class="fa tab-icon"></i> <span>Product Price</span></a>
            </li>
        @endcan
    @endif
</ul>