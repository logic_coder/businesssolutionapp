<button class="btn delete-multi-items" action-url="attribute_value/remove_attribute_value" redirect-url="{{'/attribute/'.$attribute->id.'/attribute_value'}}"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Value</th>
        @if( $attribute->type =='color-or-texture')
            <th style="width:20%">Color</th>
        @endif
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @if($attribute->attributeValues)
        @foreach( $attribute->attributeValues as $value)
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input type="checkbox" name="item_ids[]" value={{ $value->id }} data-id="checkbox" id="checkbox{{ $value->id }}">
                        <label for="checkbox{{ $value->id }}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    {{ $value->value }}
                </td>
                @if( $attribute->type =='color-or-texture')
                    <td class="v-align-middle">
                        <span class="attributes-color-container" style="background-color:{{ $value->color }} "></span>
                    </td>
                @endif
                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <a title="Edit" class="btn edit-button" href="{{url('attribute/'.$attribute->id.'/attribute_value/'.$value->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        <button title="Delete" item-ids="{{$value->id}}" type="button" class="btn create-button delete-single-item" action-url="attribute_value/remove_attribute_value" redirect-url="{{'/attribute/'.$attribute->id.'/attribute_value'}}"><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>