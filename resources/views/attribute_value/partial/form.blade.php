@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/css/bootstrap-colorpicker.css" rel="stylesheet">
@endsection
<div class="row row-same-height">
    <div class="panel panel-default">
        <div class="panel-heading title-color">
            <h4 class="panel-title">Attribute Value</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-7 col-div-width">
                <div class="padding-30">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required form-group-default-selectFx">
                            {{ Form::label('attribute_group', 'Attribute Group') }}
                            {{Form::select('attribute_id',$attribute_lists,$attribute->id,['class' => 'cs-select cs-skin-slide cs-transparent form-control attribute-group-select','data-init-plugin' => 'cs-select']) }}
                        </div>
                        <div class="form-group form-group-default required">
                            {{ Form::label('value', 'Value') }}
                            {{ Form::text('value',null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div @if( $attribute->type !='color-or-texture') style="display: none" @endif id="show-color">
                            <div id="cp2" class="form-group-default input-group colorpicker-component">
                                {{ Form::text('color', $selected_color,['id'=>'color-none','class' => 'form-control']) }}
                                <span class="input-group-addon"><i></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-attached pull-right padding-top">
                        {{ form::hidden('attribute_value_id', $attribute_value ? $attribute_value->id : '', ['id' => 'attribute_value_id']) }}
                        <button class="btn create-button btn-cons" type="submit">
                            <span>Save</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('plugin-script')

@endsection
