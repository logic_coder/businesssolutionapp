<button class="btn delete-multi-items" action-url="company_printer/remove_company_printer" redirect-url="{{'/company_printer'}}"><i class="pg-trash"></i></button>
<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th style="width:1%">
            <div class="checkbox">
                <input type="checkbox" name="check_all" data-id="checkbox" id="check_all">
                <label for="check_all"></label>
            </div>
        </th>
        <th style="width:20%">Name</th>
        <th style="width:20%">Model</th>
        <th style="width:20%">Type</th>
        <th style="width:20%">Status</th>
        <th style="width:20%">Action</th>
    </tr>
    </thead>
    <tbody>
    @if($company_printers)
        @foreach( $company_printers as $value)
            <tr>
                <td class="v-align-middle">
                    <div class="checkbox ">
                        <input type="checkbox" name="item_ids[]" value={{ $value->id }} data-id="checkbox" id="checkbox{{ $value->id }}">
                        <label for="checkbox{{ $value->id }}"></label>
                    </div>
                </td>
                <td class="v-align-middle">
                    {{ $value->name }}
                </td>
                <td class="v-align-middle">
                    {{ $value->model }}
                </td>
                <td class="v-align-middle">
                    {{ ($value->type=='pos') ? 'Pos' : 'Regular'}}
                </td>
              
                <td class="v-align-middle">
                    {{ ($value->status=='active') ? 'Active' : 'Inactive'}}
                </td>

                <td class="v-align-middle">
                    <div class="btn-group btn-actions">
                        <a title="Edit" class="btn btn-success" href="{{url('company_printer/'.$value->id.'/edit')}}"><i class="fa fa-pencil"></i></a>
                        <button title="Delete" item-ids="{{$value->id}}" type="button" class="btn btn-danger delete-single-item" action-url="company_printer/remove_company_printer" redirect-url="{{'/company_printer'}}"><i class="fa fa-trash-o"></i></button>
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>