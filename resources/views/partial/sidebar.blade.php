<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
    <div class="sidebar-overlay-slide from-top" id="appMenu">
        <div class="row">
            <div class="col-xs-6 no-padding">
                <a href="#" class="p-l-40">
                    <img src="{{ asset('images/social_app.svg') }}" alt="socail" data-src="{{ asset('images/social_app.svg') }}" data-src-retina="{{ asset('images/social_app.svg') }}" width="78" height="22" >
                </a>
            </div>
            <div class="col-xs-6 no-padding">
                <a href="#" class="p-l-10">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 m-t-20 no-padding">
                <a href="#" class="p-l-40">
                    <img src="{{ asset('images/calendar_app.svg') }}" alt="socail" data-src="{{ asset('images/calendar_app.svg') }}" data-src-retina="{{ asset('images/calendar_app.svg') }}" >
                </a>
            </div>
            <div class="col-xs-6 m-t-20 no-padding">
                <a href="#" class="p-l-10">
                    <img src="{{ asset('images/add_more.svg') }}" alt="socail" data-src="{{ asset('images/add_more.svg') }}" data-src-retina="{{ asset('images/add_more.svg') }}" >
                </a>
            </div>
        </div>
    </div>
    <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <img src="{{ asset('images/logo_white.png') }}" alt="logo" class="brand" data-src="{{ asset('images/logo_white.png') }}" data-src-retina="{{ asset('images/logo_white.png') }}" width="78" height="22">
        <div class="sidebar-header-controls">
            {{--<button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu"><i class="fa fa-angle-down fs-16"></i> </button>--}}
            <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i></button>
        </div>
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
        @can('access-dashboard')
                <li class="m-t-30 open">
                    <a href="{{url('/dashboard')}}" class="detailed">
                        <span class="title">Dashboard</span>
                        <span class="details">12 New Updates</span>
                    </a>
                    <span class="icon-thumbnail bg-success"><i class="pg-home"></i></span>
                </li>
            @endcan
            <li class="">
                <a href="javascript:;"><span class="title">Settings</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-cog"></i></span>
                <ul class="sub-menu">
                    @can('access-company-info')
                        <li class="">
                            <a href="{{ route('company.edit', Auth::user()->company_id ) }}">Company Info</a>
                            <span class="icon-thumbnail">Ci</span>
                        </li>
                    @endcan
                    @can('access-company-currency-settings')
                        <li class="">
                            <a href="{{url('company_currency')}}">Currency Settings</a>
                            <span class="icon-thumbnail">Cu</span>
                        </li>
                    @endcan
                    @can('access-global-vat-settings')
                        <li class="">
                            <a href="{{url('vat')}}">Vat Settings</a>
                            <span class="icon-thumbnail">Vs</span>
                        </li>
                    @endcan
                    @can('access-global-payment-method-settings')
                        <li class="">
                            <a href="{{url('payment_method')}}">Payment Methods Settings</a>
                            <span class="icon-thumbnail">Pms</span>
                        </li>
                    @endcan
                    @can('access-invoice-prefix-settings')
                        <li class="">
                            <a href="{{url('invoice_prefix')}}">Invoice Prefix Settings</a>
                            <span class="icon-thumbnail">Inp</span>
                        </li>
                    @endcan
                    @can('create-view-outlets')
                        <li class="">
                            <a href="{{url('out_let')}}">Outlets</a>
                            <span class="icon-thumbnail">Ol</span>
                        </li>
                    @endcan
                    @can('access-coupon-settings')
                        <li class="">
                            <a href="{{url('coupon')}}">Coupon Settings</a>
                            <span class="icon-thumbnail">Cs</span>
                        </li>
                    @endcan
                    @can('access-customer-order-statuses-settings')
                        <li class="">
                            <a href="{{url('customer_order_status')}}">Customer Order Status Settings</a>
                            <span class="icon-thumbnail">Cos</span>
                        </li>
                    @endcan
                    @can('access-company-printer-settings')
                        <!-- <li class="">
                            <a href="{{url('company_printer')}}">Company Printer Settings</a>
                            <span class="icon-thumbnail">Cps</span>
                        </li> -->
                    @endcan
                    @can('access-outlet-printer-settings')
                        <li class="">
                            <a href="{{url('printer')}}">OutLet Printers</a>
                            <span class="icon-thumbnail">Ops</span>
                        </li>
                    @endcan 
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <span class="title">Users</span>
                    <span class="arrow"></span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                <ul class="sub-menu">
                    @can('create-view-users')
                        <li class="">
                            <a href="{{url('user')}}">Users</a>
                            <span class="icon-thumbnail">U</span>
                        </li>
                    @endcan
                    @can('add-edit-access-permission')
                        <li class="">
                            <a href="{{url('role')}}">User Roles</a>
                            <span class="icon-thumbnail">Ol</span>
                        </li>
                    @endcan
                    @can('access-position-title')
                        <li class="">
                            <a href="{{url('user_title')}}">Position/Title</a>
                            <span class="icon-thumbnail">Pt</span>
                        </li>
                    @endcan
                </ul>
            </li>
            @can('create-view-customer')
                <!-- <li class="">
                    <a href="javascript:;">
                        <span class="title">Customers</span>
                        <span class="arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                    <ul class="sub-menu">
                        <li class="">
                            <a href="{{url('frontend/customer')}}">Customers</a>
                            <span class="icon-thumbnail">U</span>
                        </li>
                    </ul>
                </li> -->
            @endcan
            @can('access-sales-page')
                <!-- <li class="">
                    <a href="javascript:;">
                        <span class="title">Sales( Cash Drawer )</span>
                        <span class="arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                    <ul class="sub-menu">
                        <li class="">
                            <a href="{{url('frontend/sale')}}">Sales</a>
                            <span class="icon-thumbnail">S</span>
                        </li>
                        <li class="">
                            <a href="{{url('frontend/canceled_order')}}">Canceled Order</a>
                            <span class="icon-thumbnail">S</span>
                        </li>   
                        <li class="">
                            <a href="{{url('frontend/hold_order')}}">Hold Order</a>
                            <span class="icon-thumbnail">S</span>
                        </li>                    
                        <li class="">
                            <a href="{{url('frontend/customer_order_detail')}}">Pre Orders</a>
                            <span class="icon-thumbnail">CO</span>
                        </li>
                    </ul>
                </li> -->
            @endcan
            <li class="">
                <a href="javascript:;"><span class="title">Catalog</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="fa fa-book"></i></span>
                <ul class="sub-menu">
                    @can('create-view-products')
                        <li class="">
                            <a href="{{url('product')}}">Product</a>
                            <span class="icon-thumbnail">P</span>
                        </li>
                        <li class="">
                            <a href="{{url('product/barcode')}}">Generate Barcode</a>
                            <span class="icon-thumbnail">P</span>
                        </li>
                    @endcan
                    @can('access-category')
                        <li class="">
                            <a href="{{url('category')}}">Product Category</a>
                            <span class="icon-thumbnail">Pc</span>
                        </li>
                    @endcan
                    @can('access-product-attributes')
                        <li class="">
                            <a href="{{url('attribute')}}">Product Attribute</a>
                            <span class="icon-thumbnail">Pa</span>
                        </li>
                    @endcan
                    @can('access-product-combination')
                        <li class="">
                            <a href="{{url('product_combination')}}">Product Combination</a>
                            <span class="icon-thumbnail">Pc</span>
                        </li>
                    @endcan
                </ul>
            </li>
            <li class="">
                <a href="javascript:;"><span class="title">Inventory</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail "><i class="fa fa-th"></i></span>
                <ul class="sub-menu">
                    @can('access-vendor-supplier')
                        <li class="">
                            <a href="{{url('supplier')}}">Supplier</a>
                            <span class="icon-thumbnail">S</span>
                        </li>
                    @endcan
                    @can('access-warehouse')
                        <li class="">
                            <a href="{{url('warehouse')}}">Warehouse</a>
                            <span class="icon-thumbnail">S</span>
                        </li>
                    @endcan
                    @can('access-warehouse-store-in-challan')
                        <li class="">
                            <!-- <a href="{{url('warehouse_store_in_challan')}}">Warehouse Store-in Challan</a> -->
                            <a href="{{url('warehouse_store_in_challan')}}">Purchase Requisition</a>
                            <span class="icon-thumbnail">PUR</span>
                        </li>
                    @endcan
                    @can('access-warehouse-store-out-challan')
                        <li class="">
                            <!-- <a href="{{url('warehouse_store_out_challan')}}">Warehouse Store-out Challan</a> -->
                            <a href="{{url('warehouse_store_out_challan')}}">Product Transfer To Outlet</a>
                            <span class="icon-thumbnail">PTO</span>
                        </li>
                    @endcan
                    @can('access-outlet-store-in-challan')
                        <!-- <li class="">
                            <a href="{{url('frontend/out_let_store_in_challan')}}">Outlet Store-in Challan</a>
                            <span class="icon-thumbnail">Osi</span>
                        </li> -->
                    @endcan 
                </ul>
            </li>
            <li class="">
                <a href="javascript:;"><span class="title">Unit Conversion</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail "><i class="fa fa-th"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="{{url('tolagram')}}">Tola---Gram</a>
                        <span class="icon-thumbnail">TG</span>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>