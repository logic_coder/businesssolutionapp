
@if (count($errors) > 0)
    <div class="alert alert-danger text-center alert-dismissible show">
    <button type="button" class="close" data-dismiss="alert" style="right: -7px !important;">×</button> 
        @foreach ($errors->all() as $error)
            {{ $error }} <br/>
        @endforeach
    </div>
@endif
