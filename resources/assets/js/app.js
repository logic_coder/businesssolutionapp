/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue').default;
import VueResource from 'vue-resource';
 
// tell Vue to use the vue-resource plugin
Vue.use(VueResource);
import Vue from 'vue';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('warehouse-store-in-challan-create', require('./components/warehouse_store_in_challan_create.vue').default);
Vue.component('warehouse-store-in-challan-edit', require('./components/warehouse_store_in_challan_edit.vue').default);
Vue.component('warehouse-store-in-challan-receive', require('./components/warehouse_store_in_challan_receive.vue').default);
Vue.component('warehouse-store-out-challan-create', require('./components/warehouse_store_out_challan_create.vue').default);
Vue.component('warehouse-store-out-challan-edit', require('./components/warehouse_store_out_challan_edit.vue').default);
Vue.component('warehouse-store-out-challan-receive', require('./components/warehouse_store_out_challan_receive.vue').default);
Vue.component('out-let-store-in-challan-receive', require('./components/out_let_store_in_challan_receive.vue').default);
Vue.component('frontend-login', require('./components/frontend_login.vue').default);
Vue.component('customer-create', require('./components/customer_create.vue').default);
Vue.component('customer-edit', require('./components/customer_edit.vue').default);
Vue.component('customer-order-detail-create', require('./components/customer_order_detail_create.vue').default);
Vue.component('customer-order-detail-edit', require('./components/customer_order_detail_edit.vue').default);
Vue.component('customer-order-detail-paid-detail', require('./components/customer_order_detail_paid_detail.vue').default);
Vue.component('coupon-create', require('./components/coupon_create.vue').default);
Vue.component('coupon-edit', require('./components/coupon_edit.vue').default);
Vue.component('tolagram-convert', require('./components/tolagram_convert.vue').default);
Vue.component('product-combination-create', require('./components/product_combination_create.vue').default);
Vue.component('product-combination-edit', require('./components/product_combination_edit.vue').default);
Vue.component('sale-create', require('./components/sale_create.vue').default);
Vue.component('sale-edit', require('./components/sale_edit.vue').default);
Vue.component('product-gallery', require('./components/product_gallery.vue').default);
Vue.component('sign-up', require('./components/sign_up.vue').default);
Vue.component('dash-board', require('./components/dash_board.vue').default);
Vue.component('frontend-dash-board', require('./components/frontend_dash_board.vue').default);
Vue.component('user-info', require('./components/user_info.vue').default);
Vue.component('user-photo', require('./components/user_photo.vue').default);
Vue.component('frontend-user-photo', require('./components/frontend_user_photo.vue').default);

const app = new Vue({
    el: '#app'
});


