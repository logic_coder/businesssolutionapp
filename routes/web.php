<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::name('test')->get('test', function(){
   echo phpinfo();
});

Route::bind('attribute',function($id){
    return \App\Attribute::findorfail($id);
});
Route::bind('attribute_value',function($id){
    return \App\AttributeValue::findorfail($id);
});

Route::bind('product',function($id){
    return \App\Product::findorfail($id);
});
Route::bind('product_image',function($id){
    return \App\ProductImage::findorfail($id);
});

Route::bind('role',function($id){
    return \App\Role::findorfail($id);
});

/*Route::bind('user',function($id){
    return \App\User::findorfail($id);
});*/
Route::bind('warehouse',function($id){
    return \App\Warehouse::findorfail($id);
});
Route::bind('warehouse_store_in_challan',function($id){
    return \App\WarehouseStoreInChallan::findorfail($id);
});
Route::bind('warehouse_store_out_challan',function($id){
    return \App\WarehouseStoreOutChallan::findorfail($id);
});
Route::bind('customer',function($id){
    return \App\Customer::findorfail($id);
});
Route::bind('company_printer',function($id){
    return \App\CompanyPrinter::findorfail($id);
});
Route::bind('printer',function($id){
    return \App\Printer::findorfail($id);
});


Route::bind('coupon',function($id){
    return \App\Coupon::findorfail($id);
});

Route::name('login')->get('login', function(){
    return redirect('/');
});
Route::name('logout')->post('/logout', 'HomeController@logout');

Route::name('home')->get('/', 'HomeController@index');
Route::name('frontend')->get('frontend', 'FrontendController@index');
Route::name('frontend')->get('frontend/login', 'FrontendController@login');
Route::name('frontend')->post('frontend/logout', 'FrontendController@logout');
Route::name('frontend')->post('frontend/do_login', 'FrontendController@doLogin');
Route::post('company_outlets/enter_in_out_let', 'OutLetUserApiController@companyEnterInOutlet');
Route::name('frontend_dashboard')->get('frontend/dashboard', 'FrontendDashboardController@index');
Route::name('frontend_dashboard')->get('frontend/dashboard/dailyreport', 'FrontendDashboardController@dailyReport');
Route::name('frontend_dashboard')->get('frontend/dashboard/weeklyreport', 'FrontendDashboardController@weeklyReport');
Route::name('frontend_dashboard')->get('frontend/dashboard/monthlyreport', 'FrontendDashboardController@monthlyReport');
Route::name('frontend_dashboard')->get('frontend/dashboard/reportbetween', 'FrontendDashboardController@reportBetween');
// Route::name('frontend_enter_in_outlet')->post('user_outlets/enter_in_out_let', 'OutLetUserController@enterInOutlet');
Route::name('frontend_customer')->get('frontend/customer', 'CustomerController@index');
Route::name('frontend_customer')->get('frontend/customer/create', 'CustomerController@create');
Route::name('frontend_customer')->get('frontend/customer/{customer}/edit', 'CustomerController@edit');
Route::name('frontend_customer')->delete('frontend/customer/{customer}', 'CustomerController@destroy');
Route::name('frontend_customer')->post('frontend/customer/remove_customer', 'CustomerApiController@removeCustomer');
Route::name('tolagram')->get('tolagram', 'TolagramConversionController@tolaGram');
Route::post('tola_gram_conversion/tola_to_gram', 'TolagramConversionController@tolaToGram');
Route::post('tola_gram_conversion/gram_to_tola', 'TolagramConversionController@gramToTola');

Route::name('frontend_user')->get('frontend/user/{id}/edit', 'UserController@frontendEdit');

// Route::name('frontend_user_update')->put('frontend/user/update/{id}', 'UserController@frontendUserUpdate');

Route::name('frontend_user_update')->put('frontend/user/{id}/update', 'UserController@frontendUserUpdate');

Route::middleware('auth')->group(function () {
    Route::name('dashboard')->get('dashboard', 'DashboardController@index');
});
Route::middleware('auth')->group(function () {
    Route::name('attribute')->delete('attribute/remove_attributes', 'AttributeController@removeAttributes');
});
Route::middleware('auth')->group(function () {
    Route::name('attribute_value')->delete('attribute/{attribute}/attribute_value/remove_attribute_values', 'AttributeValueController@removeAttributeValues');
    Route::name('attribute_value')->get('attribute/{attribute}/attribute_value/get_attribute_details', 'AttributeValueController@getAttributeDetails');
    Route::name('attribute_value')->get('attribute/{attribute}/attribute_value/{attribute_value}/get_attribute_value_details', 'AttributeValueController@getAttributeValueDetails');
});
Route::middleware('auth')->group(function () {
    Route::name('product')->post('product/{{product}}/product_image/upload-photo', 'ProductController@uploadPhoto');
    Route::name('product')->delete('product/remove_products', 'ProductController@removeProducts');
    Route::name('product')->get('product/barcode', 'ProductController@barcode');
    Route::name('product')->post('product/generateBarcode', 'ProductController@generateBarcode');
});
Route::middleware('auth')->group(function () {
    Route::name('product_price')->delete('product/{product}/product_price/remove_product_prices', 'ProductPriceController@removeProductPrices');
});
Route::middleware('auth')->group(function () {
    Route::name('product_price')->delete('role/remove_roles', 'RoleController@removeRoles');
});
Route::middleware('auth')->group(function () {
    Route::name('permission_role')->put('role/{role}/update_permission_role', 'PermissionRoleController@updatePermissionRole');
});
Route::middleware('auth')->group(function () {
    Route::name('product_combination')->delete('product_combination/remove_product_combinations', 'ProductCombinationController@removeProductCombinations');
});
Route::middleware('api')->group(function () {
    Route::name('sale')->delete('frontend/remove_sale', 'SaleController@removeSale');
    Route::name('sale')->get('frontend/sale/{sale}/get_paid_details', 'SaleController@getPaidDetails');
    Route::name('sale')->get('frontend/hold_order', 'SaleController@getHoldOrder');
    Route::name('sale')->get('frontend/canceled_order', 'SaleController@getCanceledOrder');
    Route::resource('frontend/customer_order_detail', 'CustomerOrderDetailController');
    Route::name('customer_order_detail')->delete('frontend/remove_customer_order_detail', 'CustomerOrderDetailController@removeCustomerOrderDetail');
    Route::name('customer_order_detail')->get('frontend/customer_order_detail/{customer_order_detail}/get_paid_details', 'CustomerOrderDetailController@getPaidDetails');
});
Route::middleware('auth')->group(function () {
    Route::name('warehouse_store_in_challan')->get('warehouse_store_in_challan/{warehouse_store_in_challan}/receive', 'WarehouseStoreInChallanController@receiveChallan');
    Route::post('company_currency/remove_company_currency', 'CurrencyApiController@removeCompanyCurrency');
    Route::post('vat/remove_vat', 'VatApiController@removeVat');
    Route::post('payment_method/remove_payment_method', 'PaymentMethodApiController@removePaymentMethod');
    Route::post('invoice_prefix/remove_invoice_prefix', 'InvoicePrefixApiController@removeInvoicePrefix');
    Route::post('out_let/remove_out_let', 'OutLetApiController@removeOutLet');
    Route::post('coupon/remove_coupon', 'CouponApiController@removeCoupon');
    Route::post('customer_order_status/remove_customer_order_status', 'CustomerOrderStatusApiController@removeCustomerOrderStatus');
    Route::post('company_printer/remove_company_printer', 'CompanyPrinterApiController@removePrinter');
    Route::post('printer/remove_printer', 'PrinterApiController@removePrinter');
    Route::post('user/remove_user', 'UserApiController@removeUser');
    Route::post('role/remove_role', 'RoleApiController@removeRole');
    Route::post('user_title/remove_user_title', 'UserTitleApiController@removeUserTitle');
    Route::post('product/remove_product', 'ProductApiController@removeProduct');
    Route::get('category/get_tree_data', 'CategoryApiController@getTreeData');
    Route::get('product/remove_image', 'ProductApiController@removeImage');

    Route::post('product_image/save_product_image_gallery', 'ProductImageApiController@saveProductImageGallery');
    Route::post('product_image/save_product_cover_image', 'ProductImageApiController@saveProductCoverImage');
    Route::post('product_image/get_product_image_gallery', 'ProductImageApiController@getProductImageGallery');
    Route::post('product_image/get_product_cover_image', 'ProductImageApiController@getProductCoverImage');
    Route::post('product_price/remove_product_price', 'ProductPriceApiController@removeProductPrice');
    Route::post('category/remove_category', 'CategoryApiController@removeCategory');
    Route::post('attribute/remove_attribute', 'AttributeApiController@removeAttribute');

    Route::post('company_product/get_product_name', 'ProductApiController@getProductName');
    Route::get('attributes/get_attributes', 'AttributeApiController@getAttributes');
    Route::post('product_combination/save_product_combination', 'ProductCombinationApiController@saveProductCombination');
    Route::put('product_combination/update_product_combination', 'ProductCombinationApiController@updateProductCombination');
    Route::post('product_combination/get_product_combination', 'ProductCombinationApiController@getProductCombinationData');
    Route::post('product_combination/delete_product_combination', 'ProductCombinationApiController@deleteProductCombinationData');
    Route::get('product/remove_image', 'ProductApiController@removeImage');
    Route::post('product_combination/remove_product_combinations_by_product', 'ProductCombinationApiController@removeProductCombinationsByProduct');
    Route::post('supplier/remove_supplier', 'SupplierApiController@removeSupplier');
    Route::post('warehouse/remove_warehouse', 'WarehouseApiController@removeWarehouse');
    Route::get('category/get_categories', 'CategoryApiController@getCategories');
    Route::get('supplier/get_suppliers', 'SupplierApiController@getSuppliers');
    Route::get('warehouse/get_warehouses', 'WarehouseApiController@getWarehouses');
    Route::post('warehouse/get_warehouse_data', 'WarehouseApiController@getWarehouseData');
    Route::post('out_let/get_out_let_data', 'OutLetApiController@getOutLetData');
    Route::post('product/search_products', 'ProductApiController@searchProducts');
    Route::post('warehouse_store_in_challan/save_warehouse_store_in_challan', 'WarehouseStoreInChallanApiController@saveWarehouseStoreInChallan');
    Route::post('warehouse_store_in_challan/get_challan_data', 'WarehouseStoreInChallanApiController@getChallanData');
    Route::post('warehouse_store_in_challan/update_warehouse_store_in_challan', 'WarehouseStoreInChallanApiController@updateWarehouseStoreInChallan');
    Route::post('warehouse_store_in_challan/remove_warehouse_store_in_challan', 'WarehouseStoreInChallanApiController@removeWarehouseStoreInChallan');
    Route::post('warehouse_store_in_challan/get_challan_data', 'WarehouseStoreInChallanApiController@getChallanData');
    Route::post('warehouse_store_in_challan/update_warehouse_store_in_challan', 'WarehouseStoreInChallanApiController@updateWarehouseStoreInChallan');
    Route::post('supplier/get_supplier_data', 'SupplierApiController@getSupplierData');

    Route::get('category/get_categories', 'CategoryApiController@getCategories');
    Route::get('supplier/get_suppliers', 'SupplierApiController@getSuppliers');
    Route::get('warehouse/get_warehouses', 'WarehouseApiController@getWarehouses');
    Route::post('warehouse/get_warehouse_data', 'WarehouseApiController@getWarehouseData');
    Route::post('out_let/get_out_let_data', 'OutLetApiController@getOutLetData');
    Route::post('warehouse_store_out_challan/save_warehouse_store_out_challan', 'WarehouseStoreOutChallanApiController@saveWarehouseStoreOutChallan');
    Route::post('warehouse_store_in_challan/search_warehouse_products', 'WarehouseStoreInChallanApiController@searchWarehouseProducts');
    Route::post('warehouse_store_in_challan/get_warehouse_store_in_challan_product_combination', 'WarehouseStoreInChallanApiController@getWarehouseStoreinChallanProductCombination');
    Route::get('out_let/get_company_out_let', 'OutLetApiController@getCompanyOutLet');
    Route::post('warehouse_store_out_challan/get_challan_data', 'WarehouseStoreOutChallanApiController@getChallanData');
    Route::post('warehouse_store_out_challan/update_warehouse_store_out_challan', 'WarehouseStoreOutChallanApiController@updateWarehouseStoreOutChallan');
    Route::post('warehouse_store_out_challan/remove_warehouse_store_out_challan', 'WarehouseStoreOutChallanApiController@removeWarehouseStoreOutChallan');    
 });
Route::middleware('auth')->group(function () {
    Route::name('warehouse_store_out_challan')->get('warehouse_store_out_challan/{warehouse_store_out_challan}/receive', 'WarehouseStoreOutChallanController@receiveChallan');
});
//Route::middleware('auth')->group(function () {
    Route::name('out_let_store_in_challan')->get('frontend/out_let_store_in_challan/{challan_id}/receive', 'OutLetStoreInChallanController@receiveChallan');
//});
Route::name('business_category')->get('get_business_category', 'BusinessCategoryController@getCategories');

Route::name('business_category')->get('get_business_category', 'BusinessCategoryController@getCategories');


Route::get('country_lists/get_country_lists', 'CountryApiController@getCountryLists');


Route::name('signup')->post('do_company_signup', 'SignupController@doCompanySignup');
Route::post('do_company_signin', 'SignupController@doCompanySignIn'); 

/*Route::middleware('auth')->group(function () {
    Route::name('outlet_printer')->post('printer/{printer}/outlet_printer', 'OutLetPrinterController@assignOutletPrinter');
});*/

Route::get('company_product/get_company_product', 'ProductApiController@getCompanyProduct');
Route::get('company_category/get_company_category', 'CategoryApiController@getCategories');
Route::get('customer_lists/get_customer_lists', 'CustomerApiController@getCustomerLists');
Route::get('company_currency_lists/get_company_currency_lists', 'CurrencyApiController@getCompanyCurrencyLists');
Route::post('company_currency_details/get_company_currency_details', 'CurrencyApiController@getCompanyCurrencyDetails');
Route::get('coupon_settings/generate_coupon_code', 'CouponApiController@generateCouponCode');
Route::post('coupon_settings/save_coupon_settings', 'CouponApiController@saveCouponSettings');
Route::post('coupon_list/get_coupon_data', 'CouponApiController@getCouponData');
Route::put('coupon_settings/update_coupon_settings', 'CouponApiController@updateCouponSettings');
Route::post('user/save_profile_photo', 'UserApiController@saveProfilePhoto');
Route::get('company_currency_lists/get_company_currency_lists', 'CurrencyApiController@getCompanyCurrencyLists');

Route::post('company_outlets/get_company_outlets', 'OutLetUserApiController@getCompanyOutlets');
Route::post('sale_detail/get_sales_report', 'SaleApiController@getSalesReport');
Route::post('sale_detail/get_yearly_sales_overview', 'SaleApiController@getYearlySalesOverview');
Route::post('sale_detail/get_category_wise_sales_overview', 'SaleApiController@getCategoryWiseSalesOverview');
Route::post('sale_detail/revenue_stats_data', 'SaleApiController@getRevenueStatsData');

Route::resource('business_category', 'BusinessCategoryController');
Route::resource('user', 'UserController');
Route::resource('dashboard', 'DashboardController');
Route::resource('company', 'CompanyController');
Route::resource('company_currency', 'CompanyCurrencyController');
Route::resource('company_printer', 'CompanyPrinterController');
Route::resource('printer', 'PrinterController');
Route::resource('vat', 'VatController');
Route::resource('payment_method', 'PaymentMethodController');
Route::resource('invoice_prefix', 'InvoicePrefixController');
Route::resource('out_let', 'OutLetController');
Route::resource('coupon', 'CouponController');
Route::resource('customer_order_status', 'CustomerOrderStatusController');
Route::resource('user_title', 'UserTitleController');
Route::resource('category', 'CategoryController');
Route::resource('supplier', 'SupplierController');
Route::resource('attribute', 'AttributeController');
Route::resource('product_combination', 'ProductCombinationController');
Route::resource('attribute.attribute_value', 'AttributeValueController');
Route::resource('product', 'ProductController');
Route::resource('product.product_image', 'ProductImageController');
Route::resource('product.product_price', 'ProductPriceController');
Route::resource('product_combination', 'ProductCombinationController');
Route::resource('role', 'RoleController');
Route::resource('role.permission_role', 'PermissionRoleController');
Route::resource('warehouse_store_in_challan', 'WarehouseStoreInChallanController');
Route::resource('warehouse_store_out_challan', 'WarehouseStoreOutChallanController');
Route::resource('warehouse', 'WarehouseController');
Route::resource('customer', 'CustomerController');
Route::resource('frontend/out_let_store_in_challan', 'OutLetStoreInChallanController');
Route::resource('frontend/sale', 'SaleController');
Route::resource('printer.outlet_printer', 'OutLetPrinterController');

