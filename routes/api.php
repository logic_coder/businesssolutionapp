<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
 
Route::post('login_me', 'FrontendController@loginMe');
Route::post('register', 'FrontendController@register');

Route::post('logout', 'FrontendController@logout')->middleware('auth:api');


Route::get('user_outlets/get_user_outlets', 'OutLetUserApiController@userOutlets')->middleware('auth:api');
/* 
 Route::get('/user', function (Request $request){
    return $request->user();
})->middleware('auth:api');

Route::get('/user/{user}', function (App\user $user) {
    return $user->email;
}); */
Route::get('business_category/get_business_category', 'BusinessCategoryController@getCategories')->middleware('auth:api');

 Route::get('basic_resources/get_basic_resources', 'BasicResourcesApiController@index')->middleware('auth:api');

Route::get('category/get_tree_data', 'CategoryApiController@getTreeData')->middleware('auth:api');
Route::get('category/get_categories', 'CategoryApiController@getCategories')->middleware('auth:api');
Route::get('supplier/get_suppliers', 'SupplierApiController@getSuppliers');
Route::get('warehouse/get_warehouses', 'WarehouseApiController@getWarehouses');
Route::post('warehouse/get_warehouse_data', 'WarehouseApiController@getWarehouseData');
Route::post('out_let/get_out_let_data', 'OutLetApiController@getOutLetData');


Route::post('product_combination/save_product_combination', 'ProductCombinationApiController@saveProductCombination')->middleware('auth:api');
Route::put('product_combination/update_product_combination', 'ProductCombinationApiController@updateProductCombination')->middleware('auth:api');
Route::post('product_combination/get_product_combination', 'ProductCombinationApiController@getProductCombinationData')->middleware('auth:api');
Route::post('product_combination/delete_product_combination', 'ProductCombinationApiController@deleteProductCombinationData')->middleware('auth:api');
Route::get('product/remove_image', 'ProductApiController@removeImage');

Route::post('product_image/save_product_image_gallery
', 'ProductImageApiController@saveProductImageGallery');
Route::post('product_image/save_product_cover_image
', 'ProductImageApiController@saveProductCoverImage');
Route::post('product_image/get_product_image_gallery
', 'ProductImageApiController@getProductImageGallery');
Route::post('product_image/get_product_cover_image
', 'ProductImageApiController@getProductCoverImage');

Route::get('module/get_modules', 'ModuleApiController@getModules');
Route::post('product/search_products', 'ProductApiController@searchProducts');
Route::post('product/out_let_products', 'ProductApiController@outLetProducts');
Route::post('product/search_out_let_products', 'ProductApiController@searchOutLetProducts')->middleware('auth:api');
Route::post('product/out_let_product_sales_data', 'ProductApiController@getOutLetProductSalesData');
Route::post('warehouse_store_in_challan/save_warehouse_store_in_challan', 'WarehouseStoreInChallanApiController@saveWarehouseStoreInChallan')->middleware('auth:api');
Route::post('warehouse_store_in_challan/update_warehouse_store_in_challan', 'WarehouseStoreInChallanApiController@updateWarehouseStoreInChallan')->middleware('auth:api');
Route::post('warehouse_store_in_challan/remove_warehouse_store_in_challan', 'WarehouseStoreInChallanApiController@removeWarehouseStoreInChallan')->middleware('auth:api');
Route::post('warehouse_store_in_challan/search_warehouse_products', 'WarehouseStoreInChallanApiController@searchWarehouseProducts')->middleware('auth:api');
Route::post('warehouse_store_in_challan/get_challan_data', 'WarehouseStoreInChallanApiController@getChallanData')->middleware('auth:api');
Route::post('warehouse_store_in_challan/get_warehouse_store_in_challan_product_combination', 'WarehouseStoreInChallanApiController@getWarehouseStoreinChallanProductCombination')->middleware('auth:api');
Route::post('warehouse_store_in_challan/get_product_combination_warehouse_store_in_challan_product', 'WarehouseStoreInChallanApiController@getProductCombinationChallanProduct')->middleware('auth:api');
Route::post('warehouse_store_out_challan/save_warehouse_store_out_challan', 'WarehouseStoreOutChallanApiController@saveWarehouseStoreOutChallan')->middleware('auth:api');
Route::post('warehouse_store_out_challan/update_warehouse_store_out_challan', 'WarehouseStoreOutChallanApiController@updateWarehouseStoreOutChallan')->middleware('auth:api');
Route::post('warehouse_store_out_challan/search_warehouse_products', 'WarehouseStoreOutChallanApiController@searchWarehouseProducts')->middleware('auth:api');
Route::post('warehouse_store_out_challan/get_challan_data', 'WarehouseStoreOutChallanApiController@getChallanData')->middleware('auth:api');
Route::post('out_let_store_in_challan/receive_out_let_store_in_challan', 'OutLetStoreInChallanApiController@receiveOutLetStoreInChallan')->middleware('auth:api');
//// Need Follow up /////
Route::post('warehouse_store_out_challan/remove_warehouse_store_out_challan', 'WarehouseStoreOutChallanApiController@removeWarehouseStoreOutChallan')->middleware('auth:api');
//Route::post('warehouse_store_out_challan/get_warehouse_store_in_challan_product_combination', 'WarehouseStoreOutChallanApiController@getWarehouseStoreinChallanProductCombination')->middleware('auth:api');
//Route::post('warehouse_store_out_challan/get_product_combination_warehouse_store_in_challan_product', 'WarehouseStoreOutChallanApiController@getProductCombinationChallanProduct')->middleware('auth:api');
//// Need Follow up /////
Route::post('coupon_list/get_coupon_data', 'CouponApiController@getCouponData')->middleware('auth:api');
Route::post('coupon_settings/save_coupon_settings', 'CouponApiController@saveCouponSettings')->middleware('auth:api');
Route::put('coupon_settings/update_coupon_settings', 'CouponApiController@updateCouponSettings')->middleware('auth:api');

Route::put('customer/update_customer_details', 'CustomerApiController@update_customer_details')->middleware('auth:api');

Route::get('coupon_settings/generate_coupon_code', 'CouponApiController@generateCouponCode')->middleware('auth:api');
Route::post('coupon_code/apply_coupon_code', 'CouponApiController@applyCouponCode')->middleware('auth:api');
Route::post('coupon/remove_coupon', 'CouponApiController@removeCoupon')->middleware('auth:api');
Route::post('customer_order_status/remove_customer_order_status', 'CustomerOrderStatusApiController@removeCustomerOrderStatus')->middleware('auth:api');
Route::get('company_customer_order_status/get_company_customer_order_status', 'CustomerOrderStatusApiController@getCompanyCustomerOrderStatus')->middleware('auth:api');
Route::post('tola_gram_conversion/tola_to_gram', 'TolagramConversionApiController@tolaToGram')->middleware('auth:api');
Route::post('tola_gram_conversion/gram_to_tola', 'TolagramConversionApiController@gramToTola')->middleware('auth:api');
Route::resource('customer', 'CustomerApiController');
Route::post('customer/save_customer', 'CustomerApiController@saveCustomer')->middleware('auth:api');
Route::put('customer/update_customer', 'CustomerApiController@updateCustomer')->middleware('auth:api');
Route::post('customer/get_customer_data', 'CustomerApiController@getCustomerData')->middleware('auth:api');
Route::post('customer/remove_customer', 'CustomerApiController@removeCustomer')->middleware('auth:api');
Route::post('customer_order_detail/save_customer_order_detail', 'CustomerOrderDetailApiController@saveCustomerOrderDetail')->middleware('auth:api');
Route::post('customer_order_detail/get_customer_order_detail', 'CustomerOrderDetailApiController@getCustomerOrderDetail')->middleware('auth:api');
Route::post('customer_order_detail_paids/get_customer_order_detail_paids_data', 'CustomerOrderDetailPaidApiController@getCustomerOrderDetailPaidsData')->middleware('auth:api');
Route::post('customer_order_detail_paids/get_customer_order_detail_paid_history', 'CustomerOrderDetailPaidApiController@getCustomerOrderDetailPaidHistory')->middleware('auth:api');
Route::put('customer_order_detail/update_customer_order_detail', 'CustomerOrderDetailApiController@updateCustomerOrderDetail')->middleware('auth:api');
Route::post('customer_order_detail/remove_customer_order_detail', 'CustomerOrderDetailApiController@removeCustomerOrderDetail')->middleware('auth:api');
Route::put('customer_order_detail_paids/remove_customer_order_detail_paid', 'CustomerOrderDetailPaidApiController@removeCustomerOrderDetailPaid')->middleware('auth:api');

Route::post('sale_detail/save_sale_detail', 'SaleApiController@saveSaleDetail')->middleware('auth:api');
Route::put('sale_detail/update_sale_detail', 'SaleApiController@updateSaleDetail')->middleware('auth:api');
Route::post('sale_detail/remove_sale', 'SaleApiController@removeSale')->middleware('auth:api');
Route::post('sale_detail/get_sale_detail', 'SaleApiController@getSaleDetail')->middleware('auth:api');
Route::post('sale_detail/generate_sale_paid_details', 'SaleApiController@generateSalePaidDetails')->middleware('auth:api');
Route::post('sale_detail/get_sales_report', 'SaleApiController@getSalesReport')->middleware('auth:api');
Route::post('sale_detail/get_yearly_sales_overview', 'SaleApiController@getYearlySalesOverview')->middleware('auth:api');
Route::post('sale_detail/get_category_wise_sales_overview', 'SaleApiController@getCategoryWiseSalesOverview')->middleware('auth:api');
Route::post('sale_detail/revenue_stats_data', 'SaleApiController@getRevenueStatsData')->middleware('auth:api');

Route::post('sale_paids/get_sale_paid_history', 'SalePaidApiController@getSalePaidHistory')->middleware('auth:api');
Route::resource('login', 'LoginApiController');
Route::resource('dashboard', 'DashboardApiController');
Route::post('login_authentication/check_login_authentication', 'LoginApiController@checkLoginAuthentication');
Route::get('gender_lists/get_gender_lists', 'GenderApiController@getGenderLists')->middleware('auth:api');
Route::get('customer_lists/get_customer_lists', 'CustomerApiController@getCustomerLists')->middleware('auth:api');
Route::get('currency_lists/get_currency_lists', 'CurrencyApiController@getCurrencyLists')->middleware('auth:api');

Route::post('company/company_details', 'CompanyApiController@getCompanyDetails')->middleware('auth:api');

Route::get('company_currency_lists/get_company_currency_lists', 'CurrencyApiController@getCompanyCurrencyLists')->middleware('auth:api');
Route::post('company_currency_details/get_company_currency_details', 'CurrencyApiController@getCompanyCurrencyDetails')->middleware('auth:api');
Route::post('company_currency/remove_company_currency', 'CurrencyApiController@removeCompanyCurrency');
Route::get('invoice_prefix_lists/get_invoice_prefix_lists', 'InvoicePrefixApiController@getInvoicePrefixLists')->middleware('auth:api');
Route::post('invoice_prefix/remove_invoice_prefix', 'InvoicePrefixApiController@removeInvoicePrefix')->middleware('auth:api');
Route::post('invoice_prefix/get_invoice_prefix_data', 'InvoicePrefixApiController@getInvoicePrefixData')->middleware('auth:api');
Route::get('vat_lists/get_vat_lists', 'VatApiController@getVatLists')->middleware('auth:api');
Route::get('company_product/get_company_product', 'ProductApiController@getCompanyProduct')->middleware('auth:api');
Route::post('company_product/get_product_name', 'ProductApiController@getProductName')->middleware('auth:api');
Route::get('company_category/get_company_category', 'CategoryApiController@getCategories')->middleware('auth:api');
Route::post('vat_percent/get_vat_percent', 'VatApiController@getVatPercent')->middleware('auth:api');
Route::post('vat/remove_vat', 'VatApiController@removeVat')->middleware('auth:api');
Route::get('country_lists/get_country_lists', 'CountryApiController@getCountryLists')->middleware('auth:api');
Route::post('payment_method/remove_payment_method', 'PaymentMethodApiController@removePaymentMethod')->middleware('auth:api');
Route::get('payment_method/get_company_payment_method', 'PaymentMethodApiController@getCompanyPaymentMethod')->middleware('auth:api');
Route::get('attributes/get_attributes', 'AttributeApiController@getAttributes')->middleware('auth:api');
Route::post('user/remove_user', 'UserApiController@removeUser')->middleware('auth:api');
Route::post('user/save_profile_photo', 'UserApiController@saveProfilePhoto');
Route::post('user/get_profile_photo', 'UserApiController@getProfilePhoto');

Route::post('user_outlets/enter_in_out_let', 'OutLetUserApiController@enterInOutlet')->middleware('auth:api');
Route::post('out_let/remove_out_let', 'OutLetApiController@removeOutLet')->middleware('auth:api');
Route::post('company_printer/remove_company_printer', 'CompanyPrinterApiController@removePrinter')->middleware('auth:api');
Route::post('printer/remove_printer', 'PrinterApiController@removePrinter')->middleware('auth:api');
Route::post('printer/outlet_printers', 'OutLetPrinterApiController@index')->middleware('auth:api');
Route::get('out_let/get_company_out_let', 'OutLetApiController@getCompanyOutLet')->middleware('auth:api');
Route::post('role/remove_role', 'RoleApiController@removeRole')->middleware('auth:api');
Route::post('user_title/remove_user_title', 'UserTitleApiController@removeUserTitle')->middleware('auth:api');
Route::post('product/remove_product', 'ProductApiController@removeProduct')->middleware('auth:api');
Route::post('product_price/remove_product_price', 'ProductPriceApiController@removeProductPrice')->middleware('auth:api');
Route::post('category/remove_category', 'CategoryApiController@removeCategory')->middleware('auth:api');
Route::post('attribute/remove_attribute', 'AttributeApiController@removeAttribute')->middleware('auth:api');
Route::post('attribute_value/remove_attribute_value', 'AttributeApiController@removeAttributeValue')->middleware('auth:api');
Route::post('product_combination/remove_product_combinations_by_product', 'ProductCombinationApiController@removeProductCombinationsByProduct')->middleware('auth:api');
Route::post('supplier/remove_supplier', 'SupplierApiController@removeSupplier')->middleware('auth:api');
Route::post('supplier/get_supplier_data', 'SupplierApiController@getSupplierData')->middleware('auth:api');
Route::post('warehouse/remove_warehouse', 'WarehouseApiController@removeWarehouse')->middleware('auth:api');

Route::post('customer/remove_customer', 'CustomerApiController@removeCustomer')->middleware('auth:api');
 